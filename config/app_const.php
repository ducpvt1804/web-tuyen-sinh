<?php
Class AppConst {
    const SITE_CONST = "0";
    const FLG_LANGUAGE_ENGLISH = 1;
    const FLG_LANGUAGE_VIETNAM = 0;
    //0: Dai Nam | 1: Tuyen sinh
    const SITE_TYPE = "0";
    const FLG_CATE_TYPE_NEWS = 2;
    const FLG_NEWS_TYPE_NORMAL = 0;
    const FLG_STATUS_SHOW = 0;
    const FLG_STATUS_HIDDEN = 1;

    const FLG_TYPE_LIBRARIES_IMG = 1;
    const FLG_TYPE_LIBRARIES_VIDEO = 2;

    const SLUG_EVENT_VI = "su-kien";
    const SLUG_EVENT_EN = "events";

    const SLUG_CONTACT_VI = "lien-he";
    const SLUG_CONTACT_EN = "contact";

    const SLUG_COMPLETE_VI = "hoan-thanh";
    const SLUG_COMPLETE_EN = "complete";

    const SLUG_LIBRARIES_VI = "thu-vien-hinh-anh-va-video";
    const SLUG_LIBRARIES_EN = "libraries";
}
