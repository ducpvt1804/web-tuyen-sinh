<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\ORM\TableRegistry;

if ($_SERVER['REQUEST_URI'] == '/'){
    Router::connect('/', ['controller' => 'Home', 'action' => 'index']);
}

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/:lang', function (RouteBuilder $routes) {
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));
    $routes->applyMiddleware('csrf');

    $routes->scope('/', ['prefix' => 'FrontEnd'], function ($routes) {

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Home
        $routes->connect('/', ['controller' => 'Home', 'action' => 'index'], ['lang' => 'vi|en']);
        $routes->connect('/ket-qua', ['controller' => 'Home', 'action' => 'search'], ['lang' => 'vi']);
        $routes->connect('/result', ['controller' => 'Home', 'action' => 'search'], ['lang' => 'en']);

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Router Faculty
        $ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $data = $ListDepartments
            ->find('all')
            ->where([
                'flg_delete' => 0,
                'flg_status' => 0
            ])
            ->toArray();
        $faculty = ['vi' => '', 'en' => ''];
        foreach ($data as $item) {
            if ($item['flg_language'] == 0) {
                $faculty['vi'] .= $item['slug'] . ' ';
            } else {
                $faculty['en'] .= $item['slug'] . ' ';
            }
        }
        $faculty['vi'] = str_replace(' ', '|', trim($faculty['vi']));
        $faculty['en'] = str_replace(' ', '|', trim($faculty['en']));
        // vi
        $routes->connect('/:slug_faculty', ['controller' => 'Faculty', 'action' => 'index'], ['lang' => 'vi', 'slug_faculty' => $faculty['vi']]);
        $routes->connect('/:slug_faculty/:slug_faculty_list', ['controller' => 'Faculty', 'action' => 'list'], ['lang' => 'vi', 'slug_faculty' => $faculty['vi']]);
        $routes->connect('/:slug_faculty/:slug_faculty_list/:slug_faculty_detail', ['controller' => 'Faculty', 'action' => 'detail'], ['lang' => 'vi', 'slug_faculty' => $faculty['vi']]);
        // en
        $routes->connect('/:slug_faculty', ['controller' => 'Faculty', 'action' => 'index'], ['lang' => 'en', 'slug_faculty' => $faculty['en']]);
        $routes->connect('/:slug_faculty/:slug_faculty_list', ['controller' => 'Faculty', 'action' => 'list'], ['lang' => 'en', 'slug_faculty' => $faculty['en']]);
        $routes->connect('/:slug_faculty/:slug_faculty_list/:slug_faculty_detail', ['controller' => 'Faculty', 'action' => 'detail'], ['lang' => 'en', 'slug_faculty' => $faculty['en']]);
        $routes->connect('/more_list', ['controller' => 'Faculty', 'action' => 'moreList']);

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Contact
        $routes->connect('/:slug', ['controller' => 'Contacts', 'action' => 'index'], ['slug' => 'lien-he', 'lang' => 'vi']);
        $routes->connect('/:slug', ['controller' => 'Contacts', 'action' => 'index'], ['slug' => 'contact', 'lang' => 'en']);
        $routes->connect('/:slug/:complete', ['controller' => 'Contacts', 'action' => 'complete'], ['slug' => 'lien-he|contact']);

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Libraries
        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'index'], ['slug' => 'thu-vien-hinh-anh-va-video', 'lang' => 'vi']);
        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'index'], ['slug' => 'libraries', 'lang' => 'en']);

        $routes->connect('/:slug/:detail', ['controller' => 'Library', 'action' => 'detail'], ['slug' => 'thu-vien-hinh-anh|image-gallery'])->setPass(['detail']);
        $routes->connect('/:slug/:detail', ['controller' => 'Library', 'action' => 'detail'], ['slug' => 'thu-vien-video|video-library'])->setPass(['detail']);

        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'image'], ['slug' => 'thu-vien-hinh-anh', 'lang' => 'vi']);
        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'image'], ['slug' => 'image-gallery', 'lang' => 'en']);
        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'video'], ['slug' => 'thu-vien-video', 'lang' => 'vi']);
        $routes->connect('/:slug', ['controller' => 'Library', 'action' => 'video'], ['slug' => 'video-library', 'lang' => 'en']);

        $routes->connect('/load_more_img', ['controller' => 'Library', 'action' => 'loadMoreImage']);
        $routes->connect('/load_more_video', ['controller' => 'Library', 'action' => 'loadMoreVideo']);

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Router News
        $Categories = TableRegistry::getTableLocator()->get('Categories');
        $cateNew = $Categories
            ->find('all')
            ->where([
                'flg_delete' => 0,
                'flg_site' => 0,
                'flg_show' => 0,
            ])
            ->toArray();
        $cate = ['vi' => 'tin-tuc ', 'en' => 'news '];
        foreach ($cateNew as $item) {
            if ($item['flg_language'] == 0) {
                $cate['vi'] .= $item['slug'] . ' ';
            } else {
                $cate['en'] .= $item['slug'] . ' ';
            }
        }
        $cate['vi'] = str_replace(' ', '|', trim($cate['vi']));
        $cate['en'] = str_replace(' ', '|', trim($cate['en']));
        // vi
        $routes->connect('/:slug', ['controller' => 'News', 'action' => 'index'], ['lang' => 'vi', 'slug' => $cate['vi']]);
        $routes->connect('/:slug/:slug2', ['controller' => 'News', 'action' => 'detail'], ['lang' => 'vi', 'slug' => $cate['vi']]);
        // en
        $routes->connect('/:slug', ['controller' => 'News', 'action' => 'index'], ['lang' => 'en', 'slug' => $cate['en']]);
        $routes->connect('/:slug/:slug2', ['controller' => 'News', 'action' => 'detail'], ['lang' => 'en', 'slug' => $cate['en']]);
        $routes->connect('/more_list_news', ['controller' => 'News', 'action' => 'moreList']);

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Tags
        $routes->connect('/:slug', ['controller' => 'Tags', 'action' => 'index'], ['slug' => 'tags', 'lang' => 'vi'])->setPass(['slug']);;
        $routes->connect('/:slug', ['controller' => 'Tags', 'action' => 'index'], ['slug' => 'tags', 'lang' => 'en'])->setPass(['slug']);;

        /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Events
        $routes->connect('/:slug', ['controller' => 'Events', 'action' => 'index'], ['slug' => 'su-kien', 'lang' => 'vi']);
        $routes->connect('/:slug', ['controller' => 'Events', 'action' => 'index'], ['slug' => 'events', 'lang' => 'en']);
        $routes->connect('/:slug/:detail', ['controller' => 'Events', 'action' => 'detail'], ['slug' => 'events|su-kien'])->setPass(['detail']);
        $routes->connect('/:slug/:s/:faculty', ['controller' => 'Events', 'action' => 'findbyfaculty'], ['slug' => 'events|su-kien', 's' => 's'])->setPass(['faculty']);
    });
});

Router::scope('/admin', ['prefix' => 'Admin'], function (RouteBuilder $routes) {

    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Quản lý categories
    $routes->connect('/categories/lists', ['controller' => 'Categories', 'action' => 'lists']);
    $routes->connect('/categories/add', ['controller' => 'Categories', 'action' => 'add']);
    $routes->connect('/categories/edit/:id', ['controller' => 'Categories', 'action' => 'edit']);
    $routes->connect('/categories/delete/:id', ['controller' => 'Categories', 'action' => 'delete']);
    $routes->connect('/categories/move-up/:id/:num', ['controller' => 'Categories', 'action' => 'move']);
    $routes->connect('/categories/move-down/:id/:num', ['controller' => 'Categories', 'action' => 'move']);
    $routes->connect('/categories/change-status/:id', ['controller' => 'Categories', 'action' => 'changeStatus']);

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Quản lý tin tức
    $routes->connect('/news/lists', ['controller' => 'News', 'action' => 'lists']);
    $routes->connect('/news/add', ['controller' => 'News', 'action' => 'add']);
    $routes->connect('/news/edit/:id', ['controller' => 'News', 'action' => 'edit']);
    $routes->connect('/news/delete/:id', ['controller' => 'News', 'action' => 'delete']);
    $routes->connect('/news/change-status/:id', ['controller' => 'News', 'action' => 'changeStatus']);

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Quản lý thư viện ảnh/video
    $routes->connect('/libraries/lists', ['controller' => 'Libraries', 'action' => 'lists']);
    $routes->connect('/libraries/add', ['controller' => 'Libraries', 'action' => 'add']);
    $routes->connect('/libraries/edit/:id', ['controller' => 'Libraries', 'action' => 'edit']);
    $routes->connect('/libraries/delete/:id', ['controller' => 'Libraries', 'action' => 'delete']);
    $routes->connect('/libraries/detail/:id', ['controller' => 'Libraries', 'action' => 'detail']);
    $routes->connect('/libraries/delete-media/:id', ['controller' => 'Libraries', 'action' => 'deleteMedia']);
    $routes->connect('/libraries/change-status/:id', ['controller' => 'Libraries', 'action' => 'changeStatus']);

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //route for event module
    $routes->scope('/event', function ($routes) {
        $routes->connect('/index', ['controller' => 'Events', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Events', 'action' => 'add']);
        $routes->connect('/view/:id', ['controller' => 'Events', 'action' => 'view'])->setPass(['id']);
        $routes->connect('/edit/:id', ['controller' => 'Events', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Events', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //route for event user
    $routes->scope('/user', function ($routes) {
        $routes->connect('/index', ['controller' => 'Users', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Users', 'action' => 'add']);
        $routes->connect('/view/:id', ['controller' => 'Users', 'action' => 'view'])->setPass(['id']);
        $routes->connect('/edit/:id', ['controller' => 'Users', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Users', 'action' => 'delete'])->setPass(['id']);
        $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);
        $routes->connect('/remindpw', ['controller' => 'Users', 'action' => 'remindpw']);
        $routes->connect('/updatepw/:flg', ['controller' => 'Users', 'action' => 'updatepw'])->setPass(['flg']);
        $routes->connect('/edit_profile', ['controller' => 'Users', 'action' => 'edit_profile']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //route for banner module
    $routes->scope('/banner', function ($routes) {
        $routes->connect('/index', ['controller' => 'Banner', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Banner', 'action' => 'add']);
        $routes->connect('/add_detail', ['controller' => 'Banner', 'action' => 'add_detail']);
        $routes->connect('/edit/:id', ['controller' => 'Banner', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/edit_detail', ['controller' => 'Banner', 'action' => 'edit_detail']);
        $routes->connect('/save_detail', ['controller' => 'Banner', 'action' => 'save_detail']);
        $routes->connect('/delete/:id', ['controller' => 'Banner', 'action' => 'delete'])->setPass(['id']);
        $routes->connect('/delete_detail/:id', ['controller' => 'Banner', 'action' => 'delete_detail'])->setPass(['id']);
        $routes->connect('/saveImgToRam', ['controller' => 'Banner', 'action' => 'saveImgToRam']);
        $routes->connect('/removeImgToRam', ['controller' => 'Banner', 'action' => 'removeImgToRam']);
        $routes->connect('/status/:id', ['controller' => 'Banner', 'action' => 'status'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for config module
    $routes->scope('/config', function ($routes) {
        $routes->connect('/', ['controller' => 'Config', 'action' => 'index']);
        $routes->connect('/view/:id', ['controller' => 'Config', 'action' => 'view'])->setPass(['id']);
        $routes->connect('/edit/:id', ['controller' => 'Config', 'action' => 'edit'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for departments module
    $routes->scope('/lstfaculty', function ($routes) {
        $routes->connect('/', ['controller' => 'ListDepartments', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'ListDepartments', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'ListDepartments', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/swstatus/:id', ['controller' => 'ListDepartments', 'action' => 'swstatus'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'ListDepartments', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for departments module
    $routes->scope('/faculty', function ($routes) {
        $routes->connect('/', ['controller' => 'Departments', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Departments', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'Departments', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Departments', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for cate departments module
    $routes->scope('/faculty_cate', function ($routes) {
        $routes->connect('/', ['controller' => 'CateDepartments', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'CateDepartments', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'CateDepartments', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/swstatus/:id', ['controller' => 'CateDepartments', 'action' => 'swstatus'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'CateDepartments', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for news departments module
    $routes->scope('/faculty_news', function ($routes) {
        $routes->connect('/lists', ['controller' => 'NewsDepartments', 'action' => 'lists']);
        $routes->connect('/add', ['controller' => 'NewsDepartments', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'NewsDepartments', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'NewsDepartments', 'action' => 'delete'])->setPass(['id']);
        $routes->connect('/change-status/:id', ['controller' => 'NewsDepartments', 'action' => 'changeStatus'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for Quote module
    $routes->scope('/quotes', function ($routes) {
        $routes->connect('/', ['controller' => 'Quotes', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Quotes', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'Quotes', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/status/:id', ['controller' => 'Quotes', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Quotes', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for Partners module
    $routes->scope('/partners', function ($routes) {
        $routes->connect('/', ['controller' => 'Partners', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Partners', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'Partners', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/status/:id', ['controller' => 'Partners', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Partners', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for Student Feels module
    $routes->scope('/student_feels', function ($routes) {
        $routes->connect('/', ['controller' => 'StudentFeels', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'StudentFeels', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'StudentFeels', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/status/:id', ['controller' => 'StudentFeels', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'StudentFeels', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for Partners module
    $routes->scope('/contact', function ($routes) {
        $routes->connect('/', ['controller' => 'Contacts', 'action' => 'index']);
        $routes->connect('/view/:id', ['controller' => 'Contacts', 'action' => 'view'])->setPass(['id']);
        $routes->connect('/reply/:id', ['controller' => 'Contacts', 'action' => 'reply'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Contacts', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for departments module
    $routes->scope('/lstdepartment', function ($routes) {
        $routes->connect('/', ['controller' => 'ListDepartmentspb', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'ListDepartmentspb', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'ListDepartmentspb', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/swstatus/:id', ['controller' => 'ListDepartmentspb', 'action' => 'swstatus'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'ListDepartmentspb', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for departments module
    $routes->scope('/department', function ($routes) {
        $routes->connect('/', ['controller' => 'Departmentspb', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'Departmentspb', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'Departmentspb', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'Departmentspb', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for cate departments module
    $routes->scope('/department_cate', function ($routes) {
        $routes->connect('/', ['controller' => 'CateDepartmentspb', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'CateDepartmentspb', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'CateDepartmentspb', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/swstatus/:id', ['controller' => 'CateDepartmentspb', 'action' => 'swstatus'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'CateDepartmentspb', 'action' => 'delete'])->setPass(['id']);
    });

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // route for news departments module
    $routes->scope('/department_news', function ($routes) {
        $routes->connect('/lists', ['controller' => 'NewsDepartmentspb', 'action' => 'lists']);
        $routes->connect('/add', ['controller' => 'NewsDepartmentspb', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'NewsDepartmentspb', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'NewsDepartmentspb', 'action' => 'delete'])->setPass(['id']);
        $routes->connect('/change-status/:id', ['controller' => 'NewsDepartmentspb', 'action' => 'changeStatus'])->setPass(['id']);
    });

    // for site TuyenSinh.
    // route for Why choose us module
    $routes->scope('/choose_us', function ($routes) {
        $routes->connect('/', ['controller' => 'ChooseUs', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'ChooseUs', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'ChooseUs', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/status/:id', ['controller' => 'ChooseUs', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'ChooseUs', 'action' => 'delete'])->setPass(['id']);
    });
    // route for enrollment registration module
    $routes->scope('/enroll_registration', function ($routes) {
        $routes->connect('/', ['controller' => 'EnrollmentRegistrations', 'action' => 'index']);
        $routes->connect('/status/:id', ['controller' => 'EnrollmentRegistrations', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/view/:id', ['controller' => 'EnrollmentRegistrations', 'action' => 'view'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'EnrollmentRegistrations', 'action' => 'delete'])->setPass(['id']);
    });
    // route for enrollment options module
    $routes->scope('/enroll_options', function ($routes) {
        $routes->connect('/', ['controller' => 'EnrollmentOptions', 'action' => 'index']);
        $routes->connect('/add', ['controller' => 'EnrollmentOptions', 'action' => 'add']);
        $routes->connect('/edit/:id', ['controller' => 'EnrollmentOptions', 'action' => 'edit'])->setPass(['id']);
        $routes->connect('/status/:id', ['controller' => 'EnrollmentOptions', 'action' => 'status'])->setPass(['id']);
        $routes->connect('/delete/:id', ['controller' => 'EnrollmentOptions', 'action' => 'delete'])->setPass(['id']);
    });
});
