<?php
return [
    'CATE_DAPARTMENT' => [
        'EN' => [
            'INTRO'=>'intro',
            'NEW'=>'news',
            'ACTION'=>'action',
            'STUDENT'=>'student',
            'CONTACT'=>'contact',
        ],
        'VI' => [
            'INTRO'=>'gioi-thieu',
            'NEW'=>'tin-tuc',
            'ACTION'=>'hoat-dong',
            'STUDENT'=>'lien-he',
            'CONTACT'=>'sinh-vien',
        ]
    ],
    'HOME' => [
        'VI' => [
            'HEADER_TOP'=>'top-menu-header',
            'INTRO'=>'gioi-thieu',
            'DEPARTMENT'=>'phong-khoa',
            'TRAINING'=>'dao-tao',
            'KHCN_HTĐT'=>'KHCN_HTĐT',
            'STUDENT'=>'sinh-vien',
            'THREE_PUBLIC'=>'THREE_PUBLIC',
            'CONTACT'=>'lien-he',
            'EXTEND'=>'EXTEND',
        ],
        'EN' => [
            'HEADER_TOP'=>'top-menu-header',
            'INTRO'=>'introduction',
            'DEPARTMENT'=>'department',
            'TRAINING'=>'training',
            'KHCN_HTĐT'=>'KHCN_HTĐT',
            'STUDENT'=>'student',
            'THREE_PUBLIC'=>'THREE_PUBLIC',
            'CONTACT'=>'contact',
            'EXTEND'=>'EXTEND',
        ]
    ]
]

?>
