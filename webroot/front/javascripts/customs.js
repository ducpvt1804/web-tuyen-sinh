(function ($) {
  'use strict';

  $(window).ready(function () {

    $('.dotdotdot').dotdotdot();

   var heightBanner = $('.cover-height').height();
   var heightBannerMB = $('.cover-height-mb').height();
   if ($(window).width() >= 992){
    $('.inner-panel').css({
      'height': heightBanner
    });
   }
   else{
    $('.inner-panel').css({
      'height': heightBannerMB
    });
   }

    // new WOW().init();

    // $('#back-to-top').on('click', function (e) {
    //   e.preventDefault();
    //   $('html,body').animate({
    //     scrollTop: 0
    //   }, 700);
    // });

    $('.slider-student').owlCarousel({
      loop: false,
      margin: 60,
      dots: false,
      nav: true,
      autoplay: true,
      responsive:{
        0:{
          items: 2,
          slideBy:2,
          margin: 12
        },
        575:{
          items: 2,
          nav: false,
          dots: true,
          slideBy:2,
          margin: 12
        },
        768:{
          items: 3,
          slideBy:2,
          margin: 30
        },
        992:{
          items: 4,
          margin: 60
        }
      }
    });

    $('.slider-partner').owlCarousel({
      loop:false,
      margin:0,
      items:1,
      dots: false,
      nav:true,
      autoplay: false,
      responsiveClass:true
    });

    $('.banner-slider').owlCarousel({
      loop:true,
      margin:0,
      items:1,
      dots: true,
      nav:false,
      autoplay: true
    });

    // slider for events - homepage
    $('.events').owlCarousel({
      loop:false,
      margin:0,
      items:1,
      dots: false,
      nav:true,
      autoplay: false,
      responsiveClass:true,
      responsive:{
        0:{
          dots: true,
          nav: false
        },
        768:{
          dots: false,
          nav: true
        }
      }
    });

    // slider for testmonial - homepage
    $('.testmonials').owlCarousel({
      loop:false,
      margin:0,
      items:1,
      dots: true,
      nav: false,
      autoplay: false,
      responsiveClass:true
    });



    //popupvideo
    $('.popup-youtube').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    });

    $('.image-popup').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      }
    });

    $('.btn-toggle-menu').click(function(){
      $('.sidebar-menu-bg, .sidebar-menu').addClass('show');
    });

    $('.btn-close-menu, .sidebar-menu-bg').click(function(){
      $('.sidebar-menu-bg, .sidebar-menu').removeClass('show');
    });

    $('.form-register input').focus(function(){
      $(this).parent().find('label').addClass('hide');
    });

    $('.form-register input').focusout(function(){
      if($(this).val() == ""){
        $(this).parent().find('label').removeClass('hide');
      }
    });

    $('.datepicker').datepicker({
      onSelect: function(dateText, inst) { alert(dateText); }
    });

    $('.day').on('click',function(){
      $('.form-control.datepicker').parent().find('label').addClass('hide');
    });


    $('#switch-tab').on('change', function (e) {
      // With $(this).val(), you can **(and have to!)** specify the target in your <option> values.
      $('.nav-tabs-custom li a').eq($(this).val()).tab('show');
      // If you do not care about the sorting, you can work with $(this).index().
      // $('#the-tab li a').eq($(this).index()).tab('show');
    });

    $('.form-register select').focusout(function(){
      if($(this).val() == ""){
        $(this).parent().find('label').removeClass('hide');
      }
    });

    $('.form-register select').change(function(){
      $(this).parent().find('label').addClass('hide');
    });

    $(window).scroll(function() {
      // if ($(window).scrollTop()>300){
      //   // $('#back-to-top').addClass('show');
      //   $('body').addClass('fixed-header');
      // }
      // else{
      //   // $('#back-to-top').removeClass('show');
      //   $('body').removeClass('fixed-header');
      // }
    });
  });

  // **********************************************************************//
  // ! Window resize
  // **********************************************************************//
  $(window).on('resize', function () {
    var heightBanner = $('.cover-height').height();
     var heightBannerMB = $('.cover-height-mb').height();
     if ($(window).width() >= 992){
      $('.inner-panel').css({
        'height': heightBanner
      });
     }
     else{
      $('.inner-panel').css({
        'height': heightBannerMB
      });
     }
  });

  // Js moment js vietnam
    var interval = setInterval(function() {
        var momentNow = moment();
        momentNow.locale('vi');
        $('#date-part').html(momentNow.format('dddd') +', ' + 'ngày' + ' ' + momentNow.format('D/M/Y') + ' '
        );
        $('#time-part').html(momentNow.format('hh:mm:ss A') + ' ,');
    }, 100);

  // js facebook
    
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v5.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
})(jQuery);


