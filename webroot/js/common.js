/*-----------------------------------------------------------------*/
$(":input").click(function(){
    let _this = $(this);
    let id = _this.attr('name');
    if(id){
        id = id.replace(/\[|\]/g,'');
        let errId = `err-${id}`;
        $('#'+errId).hide();
        $("#"+id).removeClass('err-ctrl');
        _this.removeClass('err-ctrl');
    }
})

$(".mess-err").click(function(){
    $(this).hide();
})

/*-----------------------------------------------------------------*/
//load preview img
$(document).on('change', '.input_file', function() {
    readURL(this, $(this).attr('name'));
});

function readURL(input, img) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
            $('.img_preview_' + img.replace(/\[|\]/g,'')).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}