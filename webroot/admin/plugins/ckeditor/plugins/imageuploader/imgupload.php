<?php

// Copyright (c) 2015, Fujana Solutions - Moritz Maleck. All rights reserved.
// For licensing, see LICENSE.md

session_start();

if(!isset($_SESSION['user'])) {
    exit;
}

// checking lang value
if(isset($_COOKIE['sy_lang'])) {
    $load_lang_code = $_COOKIE['sy_lang'];
} else {
    $load_lang_code = "en";
}

// including lang files
switch ($load_lang_code) {
    case "en":
        require(__DIR__ . '/lang/en.php');
        break;
    case "pl":
        require(__DIR__ . '/lang/pl.php');
        break;
}

// Including the plugin config file, don't delete the following row!
require(__DIR__ . '/pluginconfig.php');

/*--------------------------------------------------------------------*/
function imageResize($imageResourceId, $width, $height)
{
    $targetWidth = $width;
    $targetHeight = $height;
    if ($width > 1500) {
        $targetWidth = (int)$width/2;
    }
    if ($width > 1500) {
        $targetHeight = (int)$height/2;
    }
    $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}
/*--------------------------------------------------------------------*/
function generateName($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString . date("YmdHis");
}

$info = pathinfo($_FILES["upload"]["name"]);
$ext = $info['extension'];
$target_dir = $useruploadpath;
$ckpath = "ckeditor/plugins/imageuploader/$useruploadpath";
$randomLetters = $rand = strtotime("now");
$imgnumber = count(scandir($target_dir));
$filename = "$imgnumber$randomLetters.$ext";
$target_file = $target_dir . $filename;
$target_file_new = $target_dir . generateName() . "_thump." . $ext;
$ckfile = $ckpath . generateName() . "_thump." . $ext;
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
$check = getimagesize($_FILES["upload"]["tmp_name"]);
if($check !== false) {
    $uploadOk = 1;
} else {
    echo "<script>alert('".$uploadimgerrors1."');</script>";
    $uploadOk = 0;
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "<script>alert('".$uploadimgerrors2."');</script>";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["upload"]["size"] > 1024000) {
    echo "<script>alert('".$uploadimgerrors3."');</script>";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "ico" ) {
    echo "<script>alert('".$uploadimgerrors4."');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<script>alert('".$uploadimgerrors5."');</script>";
// if everything is ok, try to upload file
} else {
    // var_dump($imageFileType); die;
    switch ($check[2]) {
        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($_FILES["upload"]["tmp_name"]);
            $targetLayer = imageResize($imageResourceId, $check[0], $check[1]);
            imagepng($targetLayer, $target_file_new);
            // var_dump($targetLayer); die;
            break;


        case IMAGETYPE_GIF:
            $imageResourceId = imagecreatefromgif($_FILES["upload"]["tmp_name"]);
            $targetLayer = imageResize($imageResourceId, $check[0], $check[1]);
            imagegif($targetLayer, $target_file_new);
            // var_dump($targetLayer); die;
            break;


        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($_FILES["upload"]["tmp_name"]);
            $targetLayer = imageResize($imageResourceId, $check[0], $check[1]);
            imagejpeg($targetLayer, $target_file_new);
            // var_dump($targetLayer); die;
            break;
    }
    if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
        unlink($target_file);
        if(isset($_GET['CKEditorFuncNum'])){
            $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$ckfile', '');</script>";
        }
    } else {
        echo "<script>alert('".$uploadimgerrors6." ".$target_file_new." ".$uploadimgerrors7."');</script>";
    }
}
//Back to previous site
if(!isset($_GET['CKEditorFuncNum'])){
    echo '<script>history.back();</script>';
}