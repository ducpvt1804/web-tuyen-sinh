<?php

namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;

class HomeController extends FrontEndController {
	public function initialize() {
		parent::initialize();
		// Load component
		$this->loadComponent('Config');
		$this->loadComponent('Banner');
		$this->loadComponent('BannerDetail');
		$this->loadComponent('Categories');
		$this->loadComponent('NewsDepartment');
		$this->loadComponent('NewsDepartmentpb');
		$this->loadComponent('News');
		$this->loadComponent('Events');
		$this->loadComponent('Quote');
        $this->loadComponent('WhyChooseUs');
	}

	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
	}

	public function index() {
		// Get banner images
		$bannerPC = $this->Banner->first(['flg_language' => $this->flgLanguage, 'flg_status' => 1, 'mode' => 0])->toArray();
		$bannerPCDetail = $this->BannerDetail->all(['banner_id' => $bannerPC['id']]);
		$bannerSP = $this->Banner->first(['flg_language' => $this->flgLanguage, 'flg_status' => 1, 'mode' => 1])->toArray();
		$bannerSPDetail = $this->BannerDetail->all(['banner_id' => $bannerSP['id']]);
		$this->set('bannerPCDetail', $bannerPCDetail);
		$this->set('bannerSPDetail', $bannerSPDetail);

		// Get danh sách thông báo mới nhất
		$cateTB = $this->Categories->first([
			'flg_language' => $this->flgLanguage,
			'flg_show' => AppConst::FLG_STATUS_SHOW,
			'type' => 2,
			'flg_site' => AppConst::SITE_CONST,
			'slug' => 'thong-bao',
		]);

		$listTB = array();
		if (!empty($cateTB)) {
			$listTB = $this->News->getListNewsByCategories([
				'News.flg_language' => $this->flgLanguage,
				'News.flg_status' => AppConst::FLG_STATUS_SHOW,
				'NewsRelationship.cate_id' => $cateTB['id'],
			], null, ['News.date_edit' => 'DESC'], 3);
		}
		$this->set('listTB', $listTB);

		// Get list bản tin đại nam
		$listBTDN = array();
		$listBTDN = $this->NewsDepartment->getListNews([
			'News.flg_language' => $this->flgLanguage,
			'CateDepartments.flg_language' => $this->flgLanguage,
			'ListDepartments.flg_language' => $this->flgLanguage,
			'News.flg_status' => AppConst::FLG_STATUS_SHOW,
		], ['News.date_edit' => 'DESC'], 8);
		$this->set('listBTDN', $listBTDN);

		$this->set('site', AppConst::SITE_CONST);

		//
        $chooseus = $this->WhyChooseUs->getAllChooseUs([
            'flg_language' => $this->flgLanguage,
        ]);

        $this->set('listChoose', $chooseus);
	}

	public function search()
	{
		if($this->getRequest()->is('post')){

		}
	}

}
