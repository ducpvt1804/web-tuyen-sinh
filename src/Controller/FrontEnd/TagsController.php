<?php

namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;

class TagsController extends FrontEndController
{
    public function initialize()
    {
        parent::initialize();
        // Load component
        $this->loadComponent('Seo');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index($slug)
    {
        $this->viewBuilder()->setLayout('');

    }
}
