<?php


namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;
use Cake\Routing\Router;

class CommonController extends FrontEndController
{
	public $paginate = [
        'limit' => 1,
        'order' => [
            'Events.start_date' => 'desc'
        ]
    ];
	public function initialize()
	{
		parent::initialize();
		// Load component
		$this->loadComponent('Categories');
		$this->loadComponent('ListDepartment');
		$this->loadComponent('NewsDepartment');
		$this->loadComponent('StudentFeel');
		$this->loadComponent('Partner');
		$this->loadComponent('CateDepartment');
		$this->loadComponent('Events');
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
	}

	public function index()
	{
		$this->autoRender = false;
		$this->set('site', AppConst::SITE_CONST);
		//Get slug from url
		$slug = $this->getRequest()->getParam('slug');
		$lang = ($this->getRequest()->getParam('lang') == 'vi') ? 0 : (($this->getRequest()->getParam('lang') == 'en') ? 1 : '');
		$slug = str_replace('.html', '', $slug);
		if (strpos($slug, "khoa") !== false) {
			$this->faculty($slug, $lang);
		}
		if ($slug == 'su-kien' || $slug == 'events') {
			$data = $this->Events->getAllView(['Events.flg_language' => $this->flgLanguage]);
			$this->set('data', $data);
			$this->set('site', AppConst::SITE_CONST);
			$this->render('/FrontEnd/Events/index');
		}

		//Check slug to render view
		if ($slug == 'gioi-thieu') {
			$this->render('/GioiThieu/index');
		}
	}

	public function faculty($slug, $lang)
	{
		$r = Router::url(['controller' => 'Common', 'action' => 'add', 'lang' => 'vi', 'slug' => $slug.'htm']);
		// return $this->redirect(['controller' => 'Common', 'action' => 'add', 'lang' => 'vi', 'slug' => $slug.'htm']);
		die($r);
		$this->viewBuilder()->setLayout('');
		$data = $this->ListDepartment->getData($slug, $lang);
		$dataIntro = $this->NewsDepartment->all(['CateDepartments.slug' => 'gioi-thieu', 'News.flg_status' => 0], 4);
		$dataNews = $this->NewsDepartment->all(['CateDepartments.slug IN' => ['tin-tuc', 'hoat-dong'], 'News.flg_status' => 0], 4);
		$partners = $this->Partner->all(['flg_status' => 0]);
		$conditionsMenu = [
			'CateDepartments.department_id' => $data['id'],
			'CateDepartments.flg_showheader' => 0,
			'CateDepartments.flg_status' => 0,
			'CateDepartments.flg_language' => $lang
		];

		$slug = [
			'gioi_thieu' => $lang === 0 ? 'gioi-thieu' : ($lang === 1 ? 'gioi-thieu' : ''),
			'tin_tuc' => $lang === 0 ? 'tin-tuc' : ($lang === 1 ? 'tin-tuc' : ''),
			'hoat_dong' => $lang === 0 ? 'hoat-dong' : ($lang === 1 ? 'hoat-dong' : ''),
			'lien_he' => $lang === 0 ? 'lien-he' : ($lang === 1 ? 'lien-he' : ''),
			'sinh_vien' => $lang === 0 ? 'sinh-vien' : ($lang === 1 ? 'sinh-vien' : ''),
		];

		$menus = [
			'gioi_thieu' => $this->CateDepartment->all(array_merge($conditionsMenu, ['CateDepartments.slug' => $slug['gioi_thieu']]))->first(),
			'tin_tuc' => $this->CateDepartment->all(array_merge($conditionsMenu, ['CateDepartments.slug' => $slug['tin_tuc']]))->first(),
			'hoat_dong' => $this->CateDepartment->all(array_merge($conditionsMenu, ['CateDepartments.slug' => $slug['hoat_dong']]))->first(),
			'lien_he' => $this->CateDepartment->all(array_merge($conditionsMenu, ['CateDepartments.slug' => $slug['lien_he']]))->first(),
			'sinh_vien' => $this->CateDepartment->all(array_merge($conditionsMenu, ['CateDepartments.slug' => $slug['sinh_vien']]))->first(),
		];

		$dataStudentFeel = $this->StudentFeel->all([
			'department_id' => $data['id'],
			'flg_language' => $lang,
			'flg_status' => 0
		])->toArray();
		$this->set(compact('data', 'dataStudentFeel', 'dataIntro', 'dataNews', 'partners', 'menus', 'slug'));
		$this->render('/FrontEnd/Faculty/index');
	}

	public function add()
	{
		
		die('add');
	}
}
