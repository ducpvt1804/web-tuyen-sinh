<?php


namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use Cake\Routing\Router;
use Cake\Core\Configure;

class NewsController extends FrontEndController
{
	private $slugLang;
	private $cate;
	private $intLang;
	private $slugNew;
	private $slugTitle;
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('News');
		$this->loadComponent('Seo');
		$this->loadComponent('Categories');
		$this->slugLang = $this->getRequest()->getParam('lang');
		$this->slugNew = $this->getRequest()->getParam('slug');
		$this->slugTitle = $this->Categories->first(['slug' => $this->slugNew])['title'];
		$this->intLang = $this->slugLang == 'vi' ? 0 : 1;
		$this->cate = Configure::read('CATE_DAPARTMENT');
		$this->set('slugLang', $this->slugLang);
		$this->set('slugNew', $this->slugNew);
	}

	public function index()
	{
		$title = '';
		$conditions = [
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		];

		if (!in_array($this->slugNew, ['tin-tuc', 'news'])) {
			$conditions = array_merge($conditions, ['Categories.slug' => $this->slugNew]);
		}

		if ($this->slugNew == 'tin-tuc') {
			$title = 'Bảng tin Đại Nam';
		} else
		if ($this->slugNew == 'tin-tuc') {
			$title = 'News of Dai Nam';
		} else {
			$title = $this->slugTitle;
		}

		$dataNews = $this->News->getListNews($conditions, 8);
		$this->set(compact('dataNews', 'title'));
		$this->render('index');
	}

	public function detail()
	{
		$slugDetail = $this->getRequest()->getParam('slug2');
		$dataDetail = $this->News->getListNews([
			'Categories.slug' => $this->slugNew,
			'News.slug' => $slugDetail,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		])->first();
		$conditions = [
			'Categories.slug' => $this->slugNew,
			'News.slug <>' => $slugDetail,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		];
		$title = $dataDetail['title'];
		$listNews = $this->News->getListNews($conditions, 3);
		$seo = $this->Seo->first(['id' => $dataDetail['seo_id']]);
		$this->set(compact('dataDetail', 'seo', 'listNews', 'title'));
		$this->render('detail');
	}

	public function moreList()
	{
		$this->viewBuilder()->setLayout('');
		if ($this->getRequest()->is('ajax')) {
			$req = $this->getRequest()->getData();
			$conditions = [
				'News.flg_language' => $this->intLang,
				'News.flg_status' => 0
			];
			if (!in_array($req['slug'], ['tin-tuc', 'news'])) {
				$conditions = array_merge($conditions, ['Categories.slug' => $req['slug']]);
			}
			$dataNews = $this->News->getListNews($conditions, 4, $req['page']);
			$this->set(compact('dataNews'));
		}
		$this->render('more_list');
	}
}
