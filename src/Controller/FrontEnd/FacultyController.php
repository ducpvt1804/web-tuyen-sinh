<?php


namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use Cake\Routing\Router;
use Cake\Core\Configure;

class FacultyController extends FrontEndController
{
	private $slugLang;
	private $slugFaculty;
	private $intro;
	private $action;
	private $new;
	private $cate;
	private $intLang;
	private $data;
	private $menus;
	public function initialize()
	{
		parent::initialize();
		$this->viewBuilder()->setLayout('faculty');
		$this->loadComponent('Categories');
		$this->loadComponent('ListDepartment');
		$this->loadComponent('NewsDepartment');
		$this->loadComponent('StudentFeel');
		$this->loadComponent('Partner');
		$this->loadComponent('CateDepartment');
		$this->loadComponent('Seo');
		$this->slugLang = $this->getRequest()->getParam('lang');
		$this->intLang = $this->slugLang == 'vi' ? 0 : 1;
		$this->slugFaculty = $this->getRequest()->getParam('slug_faculty');
		$this->cate = Configure::read('CATE_DAPARTMENT');
		$this->intro = $this->intLang === 0 ? $this->cate['VI']['INTRO'] : $this->cate['EN']['INTRO'];
		$this->action = $this->intLang === 0 ? $this->cate['VI']['ACTION'] : $this->cate['EN']['ACTION'];
		$this->new = $this->intLang === 0 ? $this->cate['VI']['NEW'] : $this->cate['EN']['NEW'];
		$this->data = $this->ListDepartment->getData($this->slugFaculty, $this->intLang);
		$this->menus = $this->CateDepartment->getMenus($this->data['id'], $this->intLang);
		$this->set('data', $this->data);
		$this->set('menus', $this->menus);
		$this->set('slugLang', $this->slugLang);
		$this->set('slugFaculty', $this->slugFaculty);
	}

	public function index()
	{
		$title = $this->data['title'];
		$seo = $this->Seo->first(['id' => $this->data['Departments']['seo_id']]);
		$dataIntro = $this->NewsDepartment->all([
			'CateDepartments.slug' => $this->intro,
			'CateDepartments.department_id' => $this->data['id'],
			'CateDepartments.flg_language' => $this->intLang,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		], 4);
		$dataNews = $this->NewsDepartment->all([
			'CateDepartments.slug IN' => [$this->new, $this->action],
			'CateDepartments.department_id' => $this->data['id'],
			'CateDepartments.flg_language' => $this->intLang,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		], 4);
		$partners = $this->Partner->all(['flg_status' => 0]);
		$dataStudentFeel = $this->StudentFeel->all([
			'department_id' => $this->data['id'],
			'flg_language' => $this->intLang,
			'flg_status' => 0
		])->toArray();
		$this->set(compact('dataStudentFeel', 'dataIntro', 'dataNews', 'partners', 'seo', 'title'));
		$this->render('index');
	}

	public function list()
	{
		$slugFacultyList = $this->getRequest()->getParam('slug_faculty_list');
		$dataNews = $this->NewsDepartment->all([
			'CateDepartments.slug' => $slugFacultyList,
			'CateDepartments.department_id' => $this->data['id'],
			'CateDepartments.flg_language' => $this->intLang,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		], 8);
		$title = $this->CateDepartment->first(['slug' => $slugFacultyList])['title'];
		$this->set(compact('dataNews', 'slugFacultyList', 'title'));
		$this->render('list');
	}

	public function detail()
	{
		$slugFacultyDetail = $this->getRequest()->getParam('slug_faculty_detail');
		$slugFacultyList = $this->getRequest()->getParam('slug_faculty_list');
		$listNews = $this->NewsDepartment->getListNews([
			'CateDepartments.slug' => $slugFacultyList,
			'News.slug <>' => $slugFacultyDetail,
			'News.flg_language' => $this->intLang,
			'News.flg_status' => 0
		], null, 3);
		$dataDetail = $this->NewsDepartment->all([
			'CateDepartments.slug' => $slugFacultyList,
			'News.slug' => $slugFacultyDetail,
			'News.flg_status' => 0,
			'CateDepartments.department_id' => $this->data['id']
		])->first();
		$title = $dataDetail['title'];
		$seo = $this->Seo->first(['id' => $dataDetail['seo_id']]);
		$this->set(compact('listNews', 'dataDetail', 'seo', 'title'));
		$this->render('detail');
	}

	public function moreList()
	{
		$this->viewBuilder()->setLayout('');
		if ($this->getRequest()->is('ajax')) {
			$req = $this->getRequest()->getData();
			$cate = $this->ListDepartment->first(['id' => $req['depart_ment']]);
			$data = $this->NewsDepartment->all([
				'CateDepartments.slug' => $req['slugFacultyList'],
				'CateDepartments.department_id' => $req['depart_ment'],
				'CateDepartments.flg_language' => $this->intLang,
				'News.flg_language' => $this->intLang,
				'News.flg_status' => 0
			], 4, $req['page']);
			$this->set('faculty', $req['faculty']);
			$this->set('slugFacultyList', $req['slugFacultyList']);
			$this->set(compact('data', 'cate'));
		}
		$this->render('more_list');
	}
}
