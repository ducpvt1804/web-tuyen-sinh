<?php

namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;

class LibraryController extends FrontEndController
{
    protected $limit;
    public $paginate = [
        'limit' => 21
    ];

    public function initialize()
    {
        parent::initialize();
        // Load component
        $this->loadComponent('Libraries');
        $this->limit = 8;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $images = $this->Libraries->getListLibraryImages(['Libraries.type' => AppConst::FLG_TYPE_LIBRARIES_IMG, 'LibrariesDetail.flg_language' => $this->flgLanguage]);
        $videos = $this->Libraries->getListLibraryVideos(['Libraries.type' => AppConst::FLG_TYPE_LIBRARIES_VIDEO, 'LibrariesDetail.flg_language' => $this->flgLanguage]);
        $this->set('images', $images);
        $this->set('videos', $videos);
    }

    public function image()
    {
        $images = $this->Libraries->getListLibraryImages(['Libraries.type' => AppConst::FLG_TYPE_LIBRARIES_IMG, 'LibrariesDetail.flg_language' => $this->flgLanguage], $this->limit);
        $this->set('images', $images);
        $this->render('list_images');
    }

    public function video()
    {
        $videos = $this->Libraries->getListLibraryVideos(['Libraries.type' => AppConst::FLG_TYPE_LIBRARIES_VIDEO, 'LibrariesDetail.flg_language' => $this->flgLanguage], $this->limit);
        $this->set('videos', $videos);
        $this->render('list_videos');
    }

    public function detail($slug)
    {
        $conditions = ['Libraries.flg_delete' => 0, 'LibrariesDetail.flg_language' => $this->flgLanguage, 'LibrariesDetail.slug' => $slug];
        $data = $this->Libraries->getDetailLibrary($conditions);
        $detail_library = $this->Libraries->getDetailLibraryMedia($data['id']);
        $tags = explode(", ", $data['Seo']['tags']);
        $seo = array(
            'meta_title' => $data['Seo']['meta_title'],
            'meta_description' => $data['Seo']['meta_description'],
            'meta_keywords' => $data['Seo']['meta_keywords'],
        );
        $detail_library = $this->paginate($detail_library);
        $this->set('data', $data);
        $this->set('detail_library', $detail_library);
        $this->set('tags', $tags);
        $this->set('seo', $seo);
    }

    public function loadMoreImage()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            $lang = $req['lang'] == 'vi' ? 0 : 1;
            $type = $req['media_type'];
            $images = $this->Libraries->getListLibraryImages(
                [
                    'Libraries.type' => $type,
                    'LibrariesDetail.flg_language' => $lang
                ],
                $this->limit,
                $req['page']
            );
            $this->set('images', $images);
        }
        $this->render('more_list');
    }

    public function loadMoreVideo()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            $lang = $req['lang'] == 'vi' ? 0 : 1;
            $type = $req['media_type'];
            $videos = $this->Libraries->getListLibraryVideos(
                [
                    'Libraries.type' => $type,
                    'LibrariesDetail.flg_language' => $lang
                ],
                $this->limit,
                $req['page']
            );
            $this->set('videos', $videos);
        }
        $this->render('more_list');
    }
}
