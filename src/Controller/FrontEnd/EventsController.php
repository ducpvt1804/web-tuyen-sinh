<?php

namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;

class EventsController extends FrontEndController
{

    public $paginate = [
        'limit' => 20,
        'order' => [
            'Events.start_date' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        // Load component
        $this->loadComponent('Events');
        $this->loadComponent('ListDepartment');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $data = $this->Events->getAllView(['Events.flg_language' => $this->flgLanguage]);
        $data = $this->paginate($data);
        $this->set('data', $data);
    }

    public function detail($slug)
    {
        $data = $this->Events->etDetail(['Events.flg_language' => $this->flgLanguage, 'Events.slug' => $slug]);
        $tags = explode(", ", $data['Seo']['tags']);
        $seo = array(
            'meta_title' => $data['Seo']['meta_title'],
            'meta_description' => $data['Seo']['meta_description'],
            'meta_keywords' => $data['Seo']['meta_keywords'],
        );
        $this->set('data', $data);
        $this->set('tags', $tags);
        $this->set('seo', $seo);
    }

    public function findbyfaculty($faculty)
    {
        $fac = $this->ListDepartment->first(['slug' => $faculty]);
        $data = $this->Events->getAllView(['Events.flg_language' => $this->flgLanguage, 'Events.flg_department' => $fac->id]);
        $this->set('data', $data);
        $this->render('index');
    }
}
