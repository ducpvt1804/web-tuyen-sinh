<?php

namespace App\Controller\FrontEnd;

use App\Controller\FrontEndController;
use AppConst;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

class ContactsController extends FrontEndController
{

    public $paginate = [
        'limit' => 1,
        'order' => [
            'Events.start_date' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        // Load component
        $this->loadComponent('Events');
        $this->loadComponent('ListDepartment');
        $this->loadComponent('Contact');
        $this->connection = ConnectionManager::get('default');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $lang = $this->getRequest()->getParam('lang');
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            if($this->Contact->add($data)) {
                return $this->redirect(['controller' => 'Contacts', 'action' => 'complete', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_CONTACT_VI : AppConst::SLUG_CONTACT_EN, 'complete' => $lang == 'vi' ? AppConst::SLUG_COMPLETE_VI : AppConst::SLUG_COMPLETE_EN]);
            } else {
                $this->Flash->error('Không thành công');
            }
        }
    }

    public function complete(){

    }
}
