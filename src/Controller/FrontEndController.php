<?php

/**
 * Created by PhpStorm.
 * User: TienPV
 * Date: 2/22/2019
 * Time: 9:02 AM
 */

namespace App\Controller;

use App\Controller\AppController;
use AppConst;
use Cake\Event\Event;
use Cake\I18n\I18n;

class FrontEndController extends AppController
{

	/**
	 * Initialization hook method.
	 *
	 * Use this method to add common initialization code like loading components.
	 *
	 * e.g. `$this->loadComponent('Security');`
	 *
	 * @return void
	 */
	public function initialize()
	{
		parent::initialize();
		// Load component
		$this->loadComponent('Paginator');
		$this->loadComponent('Categories');
		$this->loadComponent('Config');
		$this->loadComponent('Department');
		$this->loadComponent('Departmentpb');
		$this->loadComponent('Seo');
		$this->loadComponent('ListDepartment');
		$this->loadComponent('ListDepartmentpb');
		$this->loadComponent('EnrollmentOption');
		$lang = $this->getRequest()->getParam('lang');
		$this->flgLanguage = AppConst::FLG_LANGUAGE_VIETNAM;
		// Changing the Locale at Runtime
		if ($lang == 'vi') {
			$this->flgLanguage = AppConst::FLG_LANGUAGE_VIETNAM;
			$nameLang = 'Tiếng Việt';
			I18n::setLocale('vi_VI');
		} else if ($lang == 'en') {
			$this->flgLanguage = AppConst::FLG_LANGUAGE_ENGLISH;
			$nameLang = 'English';
			I18n::setLocale('en_US');
		}

		$current_controller = $this->getRequest()->getParam('controller');

		//Get menu header
		// $menu = array();
		// $conditions = ['type' => '0', 'flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW];
		// $this->generateMenu(null, $menu, '', $conditions);
		$menu =	$this->Categories->getMenus($this->flgLanguage);

		//Get menu box
		$conditions = ['type' => '1', 'flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$menuFooter = $this->getMenu($this->flgLanguage == 'vi' ? 49 : '', $conditions);
		$this->set('menuFooter', $menuFooter);

		//Get list department
		$department = $this->Department->all([
			'Departments.flg_language' => $this->flgLanguage,
			'ListDepartments.flg_language' => $this->flgLanguage,
		])->all()->toArray();
		$departmentPB = $this->Departmentpb->all([
			'Departments.flg_language' => $this->flgLanguage,
			'ListDepartments.flg_language' => $this->flgLanguage
		])->all()->toArray();
		$listDepartment = array_merge($department, $departmentPB);
		$this->set('listDepartment', $listDepartment);
		$this->set('faculty', $department);
		$this->set('departmentPB', $departmentPB);

		//Get config site
		$config = $this->Config->first(['flg_language' => $this->flgLanguage]);
		$seo = $this->Config->first(['id' => $config['seo_id']]);
		$this->title = $config['page_title'];
		$this->set('config', $config);
		$this->set('seo', $seo);

		$this->set('nameLang', $nameLang);
		$this->set('current_controller', $current_controller);
		$this->set('lang', $lang);
		$this->set('menu', $menu);

		// Get list Top menu header
		$conditions = ['flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$menu_top_header = $this->getMenu($this->flgLanguage == 'vi' ? 71 : '', $conditions);
		$this->set('menu_top_header', $menu_top_header);

		// Get list DTCQ
		$conditions = ['flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$listDTCQ = $this->getMenu($this->flgLanguage == 'vi' ? 37 : '', $conditions);
		$this->set('listDTCQ', $listDTCQ);

		// Get list DTLT
		$conditions = ['flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$listDTLT = $this->getMenu($this->flgLanguage == 'vi' ? 42 : '', $conditions);
		$this->set('listDTLT', $listDTLT);

		// Get list menu thu vien
		$conditions = ['flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$thuvien = $this->getMenu($this->flgLanguage == 'vi' ? 33 : '', $conditions);
		$this->set('thuvien', $thuvien);

		// Get list menu_download
		$conditions = ['flg_language' => $this->flgLanguage, 'flg_show' => AppConst::FLG_STATUS_SHOW, 'flg_site' => AppConst::SITE_TYPE];
		$menu_download = $this->getMenu($this->flgLanguage == 'vi' ? 45 : '', $conditions);
		$this->set('menu_download', $menu_download);

		// Set the layout.
		$this->viewBuilder()->setLayout('frontend');
	}
	public function beforeFilter(Event $event)
	{
		//Set title of page
		$this->set('title', $this->title);
	}

	protected function generateMenu($parentId = null, &$listCategory, $categoryName, $conditions = null, $id = null, $level = 0)
	{
		if (!empty($conditions)) {
			if (!isset($conditions['parent_id']) || empty($conditions['parent_id'])) {
				$conditions = array_merge($conditions, ['parent_id is Null']);
			}
			$condition = $conditions;
		} else {
			if (!empty($parentId)) {
				$condition = array('parent_id' => $parentId, 'flg_delete' => 0);
			} else {
				$categoryName = '';
				$condition = array('parent_id is Null', 'flg_delete' => 0);
			}
			if (!empty($id)) {
				$condition = array_merge($condition, ['id <> ' . $id]);
			}
		}
		$condition = array_merge($condition, ['flg_site' => AppConst::SITE_CONST]);
		$order = ['order_num' => 'ASC'];
		$listChild = $this->Categories->where($condition, null, $order);

		if (empty($listChild)) {
			return;
		}
		if (!empty($parentId)) {
			$categoryName .= "";
		}
		if (isset($conditions[0])) {
			array_splice($conditions, 0);
		}
		$level++;

		foreach ($listChild as $key => $child) {
			$listCategory[] = array(
				'id' => $child['id'],
				'title' => $categoryName . $child['title'],
				'type' => $child['type'],
				'slug' => $child['slug'],
				'flg_language' => $child['flg_language'],
				'flg_show' => $child['flg_show'],
				'level' => $level,
			);
			if (!empty($conditions)) {
				$conditions['parent_id'] = $child['id'];
			}
			$this->generateMenu($child['id'], $listCategory, $categoryName, $conditions, $id, $level);
		}
	}

	protected function getMenu($parentId = null, $conditions = [])
	{
		$condition = [];
		if (!empty($parentId)) {
			$condition = ['parent_id' => $parentId, 'flg_delete' => 0];
		}
		$conditions = array_merge($conditions, $condition);
		$order = ['order_num' => 'ASC'];
		$listChild = $this->Categories->where($conditions, null, $order);
		return $listChild;
	}
}
