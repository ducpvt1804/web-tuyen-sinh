<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class AuthController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Auth', [

			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login',
			],
			'loginRedirect' => [
				'controller' => 'Home',
				'action' => 'index',
			],
			'logoutRedirect' => [
				'controller' => 'Users',
				'action' => 'login',
			],
			'authError' => 'Bạn không có quyền.',
		]);
	}
	public function beforeFilter(Event $event)
	{ }
}
