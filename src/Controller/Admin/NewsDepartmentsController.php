<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use \Cake\Datasource\Exception\RecordNotFoundException;
use App\Auth\CustomPasswordHasher;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\NewsDepartmentValidator;
use App\Model\Validation\SeoValidator;
use AppConst;

/**
 * Class NewsDepartments
 *
 * @package App\Controller\Admin
 */
class NewsDepartmentsController extends AdminController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('NewsDepartment');
        $this->loadComponent('CateDepartment');
        $this->loadComponent('Department');
        $this->loadComponent('Seo');
        $this->loadComponent('Categories');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
        $this->role_id = $this->userLogined['role_id'];
    }

    /*
     * Search news
     */
    public function lists()
    {
        $conditions = array();
        $param = array();
        if ($this->Session->check('News.search')) {
            $this->Session->delete('News.search');
        }
        $conditions = array();
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData('data');
            $this->getRequest()->getSession()->write('News.search', $data);
        } else {
            $data = $this->getRequest()->getSession()->read('News.search');
            if (!isset($data) || empty($data)) {
                $data['title'] = '';
                $data['type'] = '';
                $data['parent_id'] = '';
            }
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $conditions = array_merge($conditions, ["News.title LIKE '%" . $data['title'] . "%'"]);
        }
        if (isset($data['parent_id']) && !empty($data['parent_id'])) {
            $conditions = array_merge($conditions, ['CateDepartments.id' => $data['parent_id']]);
        }
        if (isset($data['flg_language']) && !empty($data['flg_language'])) {
            $conditions = array_merge($conditions, ['News.flg_language IN ' => $data['flg_language']]);
        }
        if (isset($data['flg_status']) && !empty($data['flg_status'])) {
            $conditions = array_merge($conditions, ['News.flg_status IN ' => $data['flg_status']]);
        }
        if ($this->role_id == 2) {
            array_push($conditions, ['CateDepartments.department_id IN' => $this->userLogined['module_id']]);
        }
        $news = $this->NewsDepartment->all($conditions);
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $listCategory = $this->CateDepartment->getList([]);
        $this->set('lstDepartments', $lstDepartments);
        $this->set('news', $news);
        $this->set('param', $data);
        $this->set('listCategory', $listCategory);
    }

    /*
     * Add new category
     */
    public function add()
    {
        $listCategory = array();
        $categoryName = '';
        if ($this->getRequest()->isPost()) {
            //--- Preparing data ---
            $data = $this->getRequest()->getData('data');
            $data['flg_delete'] = 0;
            $data['flg_site'] = AppConst::SITE_CONST;
            $data['Seo']['flg_delete'] = 0;
            $data['cate_depart_id'] = $data['parent_id'];

            //======= Validate data ====
            $newValid = new NewsDepartmentValidator();
            $seoValid = new SeoValidator();
            $errorsNew = $newValid->addVaild($data);
            $errorsSeo = $seoValid->errors($data['Seo']);
            //======= Save data ===
            if (empty($errorsNew) && empty($errorsSeo)) {
                //--- Transaction Begin ---
                $this->connection->begin();
                $seo = $this->Seo->add($data['Seo']);
                if ($seo !== false) {
                    $data['seo_id'] = $seo->id;
                    $category = $this->NewsDepartment->add($data);
                    if ($category !== false) {
                        $this->connection->commit();
                        $this->Flash->success(__('Thêm mới tin tức thành công!'));
                        return $this->redirect(['action' => 'lists']);
                    }
                }
                $this->connection->rollback();
                //--- Transaction End ---
            } else {
                $this->set('errDepartments', $errorsNew);
                $this->set('errSeo', $errorsSeo);
            }
            $this->Flash->error(__('Thêm mới tin tức không thành công!'));
            $this->set('data', $data);
        }
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $listCategory = $this->CateDepartment->getList([]);
        $this->set('listCategory', $listCategory);
        $this->set('lstDepartments', $lstDepartments);
    }

    /*
     * Edit category
     */
    public function edit()
    {
        $id = $this->getRequest()->getParam('id');
        if (isset($id) && !empty($id)) {
            $data = $dataOld = $this->NewsDepartment->getDataById($id);
            $data['schedule'] = $dataOld['schedule']->format('Y-m-d\TH:i');
            $data['date_edit'] = $dataOld['date_edit']->format('Y-m-d\TH:i');
            if (empty($dataOld)) {
                $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
                return $this->redirect(['action' => 'lists']);
            }
            if ($this->getRequest()->isPost()) {

                //--- Preparing data ---
                $data = array_merge($data, $this->getRequest()->getData('data'));
                $data['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
                $data['Seo']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
                $data['cate_depart_id'] = $data['parent_id'];
                //======= Validate data ====

                $ewsDepartmentValid = new NewsDepartmentValidator();
                $seoValid = new SeoValidator();
                $errorsNewsDepartment = $ewsDepartmentValid->editVaild($data);
                $errorsSeo = $seoValid->errors($data['Seo']);

                //======= Save data ===
                if (empty($errorsNewsDepartment) && empty($errorsSeo)) {
                    //--- Transaction Begin ---
                    $this->connection->begin();
                    $seo = $this->Seo->edit($data['Seo'], $data['seo_id']);
                    $category = $this->NewsDepartment->edit($data, $id);
                    if ($seo !== false && $category !== false) {
                        $this->connection->commit();
                        $this->Flash->success(__('Chỉnh sửa danh mục thành công!'));
                        return $this->redirect(['action' => 'lists']);
                    }
                    $this->connection->rollback();
                    //--- Transaction End ---
                } else {
                    $this->set('errDepartments', $errorsNewsDepartment);
                    $this->set('errSeo', $errorsSeo);
                }
                $this->Flash->error(__('Chỉnh sửa danh mục không thành công!'));
            }
            $data['img_highlight'] = $dataOld['img_highlight'];
            $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
            $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
            $listCategory = $this->CateDepartment->getList([]);
            $this->set('data', $data);
            $this->set('listCategory', $listCategory);
            $this->set('lstDepartments', $lstDepartments);
            $this->set('id', $id);
        } else {
            $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
            return $this->redirect(['action' => 'lists']);
        }
    }

    /*
     * Delete category
     *
     */
    public function delete()
    {
        $id = $this->getRequest()->getParam('id');
        if (isset($id) && !empty($id)) {
            $data = $this->NewsDepartment->first(['id' => $id]);
            if (empty($data)) {
                $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
                return $this->redirect(['action' => 'lists']);
            }
            //--- Transaction Begin ---
            $this->connection->begin();
            $seo = $this->Seo->delete($data['seo_id']);
            $category = $this->NewsDepartment->delete($id);
            if ($seo !== false && $category !== false) {
                $this->connection->commit();
                $this->Flash->success(__('Xoá danh mục thành công!'));
                return $this->redirect(['action' => 'lists']);
            }
            $this->connection->rollback();
            //--- Transaction End ---
            $this->Flash->error(__('Xoá danh mục không thành công!'));
            return $this->redirect(['action' => 'lists']);
        } else {
            $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
            return $this->redirect(['action' => 'lists']);
        }
    }

    /*
     * Change status category
     *
     */
    public function changeStatus()
    {
        $id = $this->getRequest()->getParam('id');
        if (isset($id) && !empty($id)) {
            $data = $this->NewsDepartment->first(['id' => $id]);
            if (empty($data)) {
                $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
                return $this->redirect(['action' => 'lists']);
            }

            //--- Transaction Begin ---
            $this->connection->begin();
            $category = $this->NewsDepartment->changeStatus($id, $data['flg_status']);
            if ($category !== false) {
                $this->connection->commit();
                $this->Flash->success(__('Thay đổi trạng thái danh mục thành công!'));
                return $this->redirect(['action' => 'lists']);
            }
            $this->connection->rollback();
            //--- Transaction End ---
            $this->Flash->error(__('Thay đổi trạng thái danh mục không thành công!'));
            return $this->redirect(['action' => 'lists']);
        } else {
            $this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
            return $this->redirect(['action' => 'lists']);
        }
    }

    /*
     * Move category
     *
     */
    public function move()
    {
        $id = $this->getRequest()->getParam('id');
        $num = $this->getRequest()->getParam('num');
        if (isset($id) && !empty($id)) {
            $news = $this->News->getDataById($id);
            $moveCategory = $this->News->getDataById($num);
            if (empty($news) || empty($moveCategory)) {
                $this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
                return $this->redirect('/admin/news/lists');
            }
            //--- Transaction Begin ---
            $this->connection->begin();
            $data['order_num'] = $moveCategory['order_num'];
            $category = $this->News->edit($data, $id);
            $data['order_num'] = $news['order_num'];
            $moveCategory = $this->News->edit($data, $num);
            if ($category !== false && $moveCategory !== false) {
                $this->connection->commit();
                $this->Flash->success(__('Di chuyển danh mục thành công!'));
                return $this->redirect('/admin/news/lists');
            }
            $this->connection->rollback();
            //--- Transaction End ---
            $this->Flash->error(__('Di chuyển danh mục không thành công!'));
            return $this->redirect('/admin/news/lists');
        } else {
            $this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
            return $this->redirect('/admin/logout');
        }
    }
}
