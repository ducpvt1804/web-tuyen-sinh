<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\CateDepartmentsValidator;
use App\Model\Validation\SeoValidator;

class CateDepartmentsController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('CateDepartment');
        $this->loadComponent('Department');
        $this->loadComponent('Function');
        $this->loadComponent('Seo');
        $this->loadModel('CateDepartments');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
        $this->role_id = $this->userLogined['role_id'];
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('CateDepartSearch')) {
            $this->Session->delete('CateDepartSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('CateDepartSearch', $data);
            $inputSearch = $this->Session->read('CateDepartSearch');
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $lang = 0;
                } else {
                    $lang = 1;
                }
                $conditions['CateDepartments.flg_language'] = $lang;
            }
            if (!empty($data["department_id"])) {
                $conditions['CateDepartments.department_id'] = $data["department_id"];
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['CateDepartments.title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        if ($this->role_id == 2) {
            array_push($conditions, ['CateDepartments.department_id IN' => $this->userLogined['module_id']]);
        }
        $cateDepartments = $this->CateDepartment->all($conditions);
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $cateDepartments = $this->paginate($cateDepartments);
        $this->set(compact('cateDepartments', 'inputSearch', 'lstDepartments'));
    }

    public function add()
    {
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $cateDepartmentsValidator = new CateDepartmentsValidator();
            $seoValidator = new SeoValidator($data);
            $errorsDepartments = $cateDepartmentsValidator->errors($data);
            $errorsSeo = $seoValidator->errors($data);
            if (empty($errorsDepartments) && empty($errorsSeo)) {
                try {
                    $this->connection->begin();
                    $resultSeo = $this->Seo->add($data);
                    $data["user_id"] = 1;
                    $data["seo_id"] = $resultSeo->id;
                    $this->CateDepartment->add($data);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errDepartments', $errorsDepartments);
                $this->set('errSeo', $errorsSeo);
            }
        }
        $this->set(compact('lstDepartments'));
        $this->render('add');
    }

    public function edit($id)
    {
        $cateDepartmests = $this->CateDepartment->first(['id' => $id]);
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $seo = $this->Seo->first(['id' => $cateDepartmests['seo_id']]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $cateDepartmentsValidator = new CateDepartmentsValidator();
            $seoValidator = new SeoValidator($data);
            $errorsDepartments = $cateDepartmentsValidator->errors($data);
            $errorsSeo = $seoValidator->errors($data);
            if (empty($errorsDepartments) && empty($errorsSeo)) {
                try {
                    $this->connection->begin();
                    $resultSeo = $this->Seo->edit($data, $seo['id']);
                    $this->CateDepartment->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errDepartments', $errorsDepartments);
                $this->set('errSeo', $errorsSeo);
            }
        }
        $this->set(compact('cateDepartmests', 'lstDepartments', 'seo'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->CateDepartment->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function swstatus($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->CateDepartment->swstatus($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
