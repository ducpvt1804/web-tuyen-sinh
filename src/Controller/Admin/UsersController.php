<?php

namespace App\Controller;

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;
use Cake\I18n\Time;
use App\Model\Validation\UsersValidator;

class UsersController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Users');
        $this->loadComponent('RoleDetail');
        $this->loadComponent('Function');
        $this->loadComponent('Mail');
        $this->connection = ConnectionManager::get('default');
        $this->Auth->allow(['remindpw', 'updatepw']);
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    /*--------------------------------------------------------------------*/
    public function index()
    {

        $conditions = [];
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            if ($req["sex"] != "") {
                $conditions['sex'] = $req["sex"];
            }
            if ($req["flg_status"] != "") {
                $conditions['flg_status'] = $req["flg_status"];
            }
            if (!empty(trim($req["keyword"]))) {
                $conditions = array_merge($conditions, [
                    'OR' => [
                        'fullname like' => '%' . trim($req["keyword"]) . '%',
                        'user_name like' => '%' . trim($req["keyword"]) . '%'
                    ]
                ]);
            }
        }
        $conditions = array_merge($conditions, ['id <>' => $this->userLogined['id']]);
        $users = $this->Users->all($conditions);
        $this->set(compact('users'));
    }

    /*--------------------------------------------------------------------*/
    public function view($id = null)
    {
        $user = $this->Users->get($id);

        $this->set('user', $user);
    }

    /*--------------------------------------------------------------------*/
    public function add()
    {
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $UsersValidator = new UsersValidator();
            $errorsUsers = $UsersValidator->addVaild($req);

            if (!empty($errorsUsers)) {
                $this->set('err', $errorsUsers);
                return;
            }
            try {
                $this->connection->begin();
                $resultUser = $this->Users->add($req);
                if ($req['role_id'] == 2) {
                    $req['role_detail'] = $req['role'];
                } elseif ($req['role_id'] == 1) {
                    $req['role_detail'] = $req['modules'];
                }
                $req["user_id"] = $resultUser->id;
                $this->RoleDetail->add($req);
                if ($resultUser) {
                    $this->Flash->success(__('Thêm tài khoản thành công.'));
                    $this->connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->connection->rollback();
                    $this->Flash->error(__('Thêm tài khoản không thành công. Vui lòng thử lại sau.'));
                }
            } catch (Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error(__('Thêm tài khoản không thành công. Vui lòng thử lại sau.'));
            }
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($id = null)
    {
        $user = $this->Users->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $UsersValidator = new UsersValidator();
            $errorsUsers = $UsersValidator->editVaild($req);

            if (!empty($errorsUsers)) {
                $this->set('err', $errorsUsers);
            } else {
                try {
                    $this->connection->begin();
                    $resultUser = $this->Users->edit($req, $id);
                    if ($req['role_id'] == 2 && isset($req['role'])) {
                        $req['role_detail'] = $req['role'];
                    } elseif ($req['role_id'] == 1 && isset($req['modules'])) {
                        $req['role_detail'] = $req['modules'];
                    }
                    $req["user_id"] = $resultUser->id;
                    $this->RoleDetail->add($req);
                    if ($resultUser) {
                        $this->Flash->success(__('Sửa tài khoản thành công.'));
                        $this->connection->commit();
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->connection->rollback();
                        $this->Flash->error(__('Sửa tài khoản không thành công. Vui lòng thử lại sau.'));
                    }
                } catch (Exception $exc) {
                    $this->connection->rollback();
                    $this->Flash->error(__('Sửa tài khoản không thành công. Vui lòng thử lại sau.'));
                }
            }
        }
        $this->set(compact('user'));
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        if ($id != null && $this->Users->delete($id)) {
            $this->Flash->success(__('Xóa tài khoản thành công.'));
            $this->connection->commit();
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Xóa tài khoản không thành công. Vui lòng thử lại sau.'));
        }
    }

    /*--------------------------------------------------------------------*/
    public function login($id = null)
    {
        $this->viewBuilder()->setLayout('admin_base');
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $passsword = md5($req['password']);
            $user = $this->Users->first(['user_name' => $req['user_name'], 'password' => $passsword]);
            if (!empty($user) && $user["flg_status"] == 1) {
                return $this->Flash->error(__('Tài khoản của bạn đã bị tạm ngưng xử dụng.'));
            }
            if (!empty($user)) {
                $user = $user->toArray() + $this->RoleDetail->getRole($user['id']) + $this->RoleDetail->getModules($user['id']);
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Home', 'action' => 'index']);
            } else {
                $this->Flash->error(__('Tài khoản hoặc mật khẩu không chính xác.'));
            }
        }
    }

    /*--------------------------------------------------------------------*/
    public function remindpw($id = null)
    {
        $this->viewBuilder()->setLayout('admin_base');
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $user = $this->Users->first(['Or' => ['user_name' => $req['user_name'], 'email' => $req['user_name']]]);
            if (!empty($user) && $user["flg_status"] !== "001") {
                return $this->Flash->error(__('Tài khoản của bạn đã bị tạm ngưng xử dụng.'));
            }
            if (!empty($user)) {
                try {
                    $this->connection->begin();
                    $data['remindpw_key'] = md5($this->Function->generateName()) . date("YmdHisu");
                    $now = date("Y-m-d H:i:s");
                    if (!empty($user['remindpw_key']) && strtotime($now) <= $user['time_out']) {
                        return $this->Flash->success(__('Email đổi mật khẩu mới đã được gửi về mail của bạn. Vui lòng kiểm tra thư.'));
                    }
                    $data['time_out'] = strtotime("$now +30 minutes");
                    $resultUser = $this->Users->edit($data, $user['id']);
                    $user['_url'] = Router::url(['controller' => 'Users', 'action' => 'updatepw', $resultUser['remindpw_key']], true);
                    if ($resultUser) {
                        $toEmail = $user['email'];
                        $subject = "Quên mật khẩu.";
                        $viewVar = ['user' => $user];
                        $template = 'remindpw';
                        $this->Mail->sendMail($toEmail, $subject, $viewVar, $template);
                        $this->Flash->success(__('Một email đã được gửi về mail của ban.'));
                        $this->connection->commit();
                    } else {
                        $this->connection->rollback();
                        $this->Flash->error(__('Một lỗi đã xảy ra. Vui lòng thử lại sau.'));
                    }
                } catch (Exception $exc) {
                    $this->connection->rollback();
                    $this->Flash->error(__('Một lỗi đã xảy ra. Vui lòng thử lại sau.'));
                }
            } else {
                $this->Flash->error(__('Không tìm thấy tài khoản.'));
            }
        }
        $this->render('remindpw');
    }

    /*--------------------------------------------------------------------*/
    public function updatepw($flg = null)
    {
        $this->viewBuilder()->setLayout('admin_base');
        $user = $this->Users->first(['remindpw_key' => $flg]);
        $now = date("Y-m-d H:i:s");
        if (!empty($user) && (strtotime($now) > $user['time_out'] || empty($user['remindpw_key']))) {
            return $this->Flash->success(__('Không tồn tại token hoặc đã hết hạn.'));
        } elseif (!empty($user)) {
            if ($this->getRequest()->isPost()) {
                $req = $this->getRequest()->getData();
                if ($req['password'] != $req['password_confirm']) {
                    return $this->Flash->error(__('Hai mật khẩu phải giống nhau.'));
                }
                $data['remindpw_key'] = '';
                $data['time_out'] = 0;
                $data['password'] = $req['password'];
                $resultUser = $this->Users->edit($data, $user['id']);
                if ($resultUser) {
                    $this->Flash->success(__('Thay đổi thông tin mật khẩu thành công.'));
                    $this->connection->commit();
                    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
                } else {
                    $this->connection->rollback();
                    $this->Flash->error(__('Một lỗi đã xảy ra. Vui lòng thử lại sau.'));
                }
            }
        } else {
            $this->Flash->error(__('Không tồn tại token hoặc đã hết hạn.'));
        }
        $this->render('updatepw');
    }

    /*--------------------------------------------------------------------*/
    public function editProfile()
    {
        $user = $this->userLogined;
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $validation = $this->Users->validation($req);
            if (empty($validation)) {
                try {
                    $this->connection->begin();
                    $resultUser = $this->Users->edit($req, $user['id']);
                    if ($resultUser) {
                        $this->Flash->success(__('Sửa tài khoản thành công.'));
                        $userNew = $this->Users->first(['id' => $resultUser['id']]);
                        $this->Auth->setUser($userNew->toArray() + $this->RoleDetail->getRole($resultUser['id']) + $this->RoleDetail->getModules($resultUser['id']));
                        $userLogined = $this->getRequest()->getSession()->read('Auth.User');
                        $this->set(compact('userLogined'));
                        $this->connection->commit();
                    } else {
                        $this->connection->rollback();
                        $this->Flash->error(__('Sửa tài khoản không thành công. Vui lòng thử lại sau.'));
                    }
                } catch (Exception $exc) {
                    $this->connection->rollback();
                    $this->Flash->error(__('Sửa tài khoản không thành công. Vui lòng thử lại sau.'));
                }
            } else {
                foreach ($validation as $k1 => $item) {
                    foreach ($item as $k => $message) {
                        $this->Flash->error(__($k1 . $message));
                    }
                }
            }
        }
        $this->set(compact('user'));
        $this->render('edit_profile');
    }
    /*--------------------------------------------------------------------*/
    public function logout($id = null)
    {
        $this->autoRender = false;
        return $this->redirect($this->Auth->logout());
    }
}
