<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Date;
use Cake\I18n\Time;

class ContactsController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('Contact');
        $this->loadModel('Contacts');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('ContactsSearch')) {
            $this->Session->delete('ContactsSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('ContactsSearch', $data);
            $inputSearch = $this->Session->read('ContactsSearch');
            if ($data["flg_read"] != "") {
                if ($data["flg_read"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_read'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['content like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $contact = $this->Contact->all($conditions);
        $contact = $this->paginate($contact);
        $this->set(compact('contact', 'inputSearch'));
    }

    public function add()
    {
        $input = "";
        if ($this->Session->check('InputContact')) {
            $this->Session->delete('InputContact');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('InputContact', $data);
            $input = $this->Session->read('InputContact');
            try {
                $this->connection->begin();
                $data["user_id"] = $this->userLogined['id'];
                $this->Contact->add($data);
                $this->connection->commit();
                $this->Session->delete('InputContact');
                $this->Flash->success('Dữ liệu được lưu thành công');
                return $this->redirect(['action' => 'index']);
            } catch (\Exception $e) {
                $this->connection->rollback();
                $this->Flash->error('Dữ liệu lưu không thành công');
            }
        }
        $this->set(compact('input'));
        $this->render('add');
    }

    public function edit($id)
    {
        $contact = $this->Contact->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            try {
                $this->connection->begin();
                $data["user_id"] = $this->userLogined['id'];
                $this->Contact->edit($data, $id);
                $this->connection->commit();
                $this->Flash->success('Dữ liệu được cập nhật thành công');
                return $this->redirect(['action' => 'index']);
            } catch (\Exception $e) {
                $this->connection->rollback();
                $this->Flash->error('Dữ liệu cập nhật không thành công');
            }
        }
        $this->set(compact('Contact'));
    }

    public function view($id)
    {
        $contact = $this->Contact->first(['id' => $id]);
        try {
            $this->connection->begin();
            $this->Contact->read($id);
            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollback();
        }
        $this->set(compact('contact'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Contact->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Contact->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
