<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\ListDepartmentsValidator;

class ListDepartmentspbController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('ListDepartmentpb');
        $this->loadComponent('Function');
        $this->loadModel('ListDepartments');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('DepartSearch')) {
            $this->Session->delete('DepartSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('DepartSearch', $data);
            $inputSearch = $this->Session->read('DepartSearch');
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $lang = 0;
                } else {
                    $lang = 1;
                }
                $conditions['flg_language'] = $lang;
            }
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $departments = $this->ListDepartmentpb->all($conditions);
        $departments = $this->paginate($departments);
        $this->set(compact('departments', 'inputSearch'));
    }

    public function add()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $listDepartmentsValidator = new ListDepartmentsValidator($data);
            $errorsListDepartments = $listDepartmentsValidator->errors($data);

            if (!empty($errorsListDepartments)) {
                $this->set('errListDepartments', $errorsListDepartments);
                return;
            }
            try {
                $this->connection->begin();
                $data["user_id"] = $this->userLogined['id'];
                $this->ListDepartmentpb->add($data);
                $this->connection->commit();
                $this->Flash->success('Dữ liệu được lưu thành công');
                return $this->redirect(['action' => 'index']);
            } catch (\Exception $e) {
                $this->connection->rollback();
                $this->Flash->error('Dữ liệu lưu không thành công');
            }
        }
    }

    public function edit($id)
    {
        $lstDepartment = $this->ListDepartmentpb->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $listDepartmentsValidator = new ListDepartmentsValidator($data);
            $errorsListDepartments = $listDepartmentsValidator->errors($data);

            if (!empty($errorsListDepartments)) {
                $this->set('errListDepartments', $errorsListDepartments);
            } else {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->ListDepartmentpb->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            }
        }
        $this->set(compact('lstDepartment'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                if ($this->ListDepartmentpb->delete($id)) {
                    $this->connection->commit();
                    $this->Flash->success('Bản ghi được xóa thành công');
                } else {
                    $this->Flash->error('Không thể xóa bản ghi.');
                }
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function swstatus($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->ListDepartmentpb->swstatus($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function search()
    { }
}
