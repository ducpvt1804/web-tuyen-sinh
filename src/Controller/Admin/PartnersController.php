<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\PartnersValidator;

class PartnersController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('Partner');
        $this->loadModel('Partners');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('PartnersSearch')) {
            $this->Session->delete('PartnersSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('PartnersSearch', $data);
            $inputSearch = $this->Session->read('PartnersSearch');
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $partner = $this->Partner->all($conditions);
        $partner = $this->paginate($partner);
        $this->set(compact('partner', 'inputSearch'));
    }

    public function add()
    {
        $input = "";
        if ($this->Session->check('InputPartner')) {
            $this->Session->delete('InputPartner');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $partnersValidator = new PartnersValidator();
            $errorsPartners = $partnersValidator->addVaild($data);
            if (empty($errorsPartners)) {
                $this->Session->write('InputPartner', $data);
                $input = $this->Session->read('InputPartner');
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->Partner->add($data);
                    $this->connection->commit();
                    $this->Session->delete('InputPartner');
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errQuotes', $errorsPartners);
            }
        }
        $this->set(compact('input'));
        $this->render('add');
    }

    public function edit($id)
    {
        $partner = $this->Partner->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $partnersValidator = new PartnersValidator();
            $errorsPartners = $partnersValidator->editVaild($data);
            if (empty($errorsPartners)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->Partner->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errQuotes', $errorsPartners);
            }
        }
        $this->set(compact('partner'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Partner->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Partner->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
