<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use \Cake\Datasource\Exception\RecordNotFoundException;
use App\Auth\CustomPasswordHasher;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\CategoriesValidator;
use App\Model\Validation\SeoValidator;
use AppConst;

/**
 * Class CategoriesController
 *
 * @package App\Controller\Admin
 */
class CategoriesController extends AdminController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Categories');
		$this->loadComponent('Seo');
		$this->connection = ConnectionManager::get('default');
	}

	/*
	 * Search categories
	 */
	public function lists()
	{
		$conditions = array();
		$param = array();

		$conditions = array('flg_delete' => 0);
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getData('data');
			$this->getRequest()->getSession()->write('Categories.search', $data);
		} else {
			$data = $this->getRequest()->getSession()->read('Categories.search');
			if (!isset($data) || empty($data)) {
				$data['title'] = '';
				$data['type'] = '';
				$data['parent_id'] = '';
			}
		}
		if (isset($data['title']) && !empty($data['title'])) {
			$conditions = array_merge($conditions, ["title LIKE '%" . $data['title'] . "%'"]);
		}
		if (isset($data['type']) && $data['type'] !== '') {
			$conditions = array_merge($conditions, ['type' => $data['type']]);
		}
		if (isset($data['parent_id']) && !empty($data['parent_id'])) {
			$conditions = array_merge($conditions, ['parent_id' => $data['parent_id']]);
		}
		if (isset($data['flg_language']) && !empty($data['flg_language'])) {
			$conditions = array_merge($conditions, ['flg_language IN ' => $data['flg_language']]);
		}
		if (isset($data['flg_show']) && !empty($data['flg_show'])) {
			$conditions = array_merge($conditions, ['flg_show IN ' => $data['flg_show']]);
		}
		if (!empty($conditions)) {
			$categories = array();
			$categoryName = '';
			$this->generateList(null, $categories, $categoryName, $conditions);
		} else {
			$categories = $this->Categories->all();
		}
		$listCategory = array();
		$categoryName = '';
		$this->generateList(null, $listCategory, $categoryName);
		// $countArray = $this->Categories->find()
		// 			->where($conditions)
		// 			->count();
		$this->set('categories', $categories);
		$this->set('param', $data);
		// $this->set('countArray', $countArray);
		$this->set('listCategory', $listCategory);
	}

	/*
	 * Add new category
	 */
	public function add()
	{
		$listCategory = array();
		$categoryName = '';
		$this->generateList(null, $listCategory, $categoryName);
		if ($this->getRequest()->isPost()) {
			//--- Preparing data ---
			$data = $this->getRequest()->getData('data');
			$colum = ['order_num'];
			$conditions = ['flg_delete' => 0];
			$order = ['order_num' => 'DESC'];
			$limit = 1;
			$maxNum = $this->Categories->where($conditions, $colum, $order, $limit);
			if (empty($maxNum)) {
				$data['order_num'] = 1;
			} else {
				$data['order_num'] = $maxNum[0]['order_num'] + 1;
			}
			$data['flg_delete'] = 0;
			$data['flg_site'] = AppConst::SITE_CONST;
			$data['Seo']['flg_delete'] = 0;

			//======= Validate data ====
			$categoryValid = new CategoriesValidator();
			$seoValid = new SeoValidator();
			$errorsCategory = $categoryValid->errors($data);
			$errorsSeo = $seoValid->errors($data['Seo']);
			//======= Save data ===
			if (empty($errorsCategory) && empty($errorsSeo)) {
				//--- Transaction Begin ---
				$this->connection->begin();
				$seo = $this->Seo->add($data['Seo']);
				if ($seo !== false) {
					$data['seo_id'] = $seo->id;
					$category = $this->Categories->add($data);
					if ($category !== false) {
						$this->connection->commit();
						$this->Flash->success(__('Thêm mới danh mục thành công!'));
						return $this->redirect('/admin/categories/lists');
					}
				}
				$this->connection->rollback();
				//--- Transaction End ---
			} else {
				$this->set('errSeo', $errorsSeo);
				$this->set('errCategory', $errorsCategory);
			}
			$this->Flash->error(__('Thêm mới danh mục không thành công!'));
			$this->set('data', $data);
		}
		$this->set('listCategory', $listCategory);
	}

	/*
	 * Edit category
	 */
	public function edit()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->Categories->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/categories/lists');
			}
			if ($this->getRequest()->isPost()) {
				//--- Preparing data ---
				$data = array_merge($data, $this->getRequest()->getData('data'));
				$data['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['Seo']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));

				//======= Validate data ====
				$categoryValid = new CategoriesValidator();
				$seoValid = new SeoValidator();
				$errorsCategory = $categoryValid->errors($data);
				$errorsSeo = $seoValid->errors($data['Seo']);

				//======= Save data ===
				if (empty($errorsCategory) && empty($errorsSeo)) {
					//--- Transaction Begin ---
					$this->connection->begin();
					$seo = $this->Seo->edit($data['Seo'], $data['seo_id']);
					$category = $this->Categories->edit($data, $id);
					if ($seo !== false && $category !== false) {
						$this->connection->commit();
						$this->Flash->success(__('Chỉnh sửa danh mục thành công!'));
						return $this->redirect('/admin/categories/lists');
					}
					$this->connection->rollback();
					//--- Transaction End ---
				} else {
					$this->set('errSeo', $errorsSeo);
					$this->set('errCategory', $errorsCategory);
				}
				$this->Flash->error(__('Chỉnh sửa danh mục không thành công!'));
			}
			$listCategory = array();
			$categoryName = '';
			$this->generateList(null, $listCategory, $categoryName, null, $id);
			$this->set('data', $data);
			$this->set('listCategory', $listCategory);
			$this->set('id', $id);
		} else {
			$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
			return $this->redirect('/admin/categories/lists');
		}
	}

	/*
	 * Delete category
	 *
	 */
	public function delete()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->Categories->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/categories/lists');
			}
			//--- Transaction Begin ---
			$this->connection->begin();
			$seo = $this->Seo->delete($data['seo_id']);
			$category = $this->Categories->delete($id);
			if ($seo !== false && $category !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Xoá danh mục thành công!'));
				return $this->redirect('/admin/categories/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Xoá danh mục không thành công!'));
			return $this->redirect('/admin/categories/lists');
		} else {
			$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
			return $this->redirect('/admin/categories/lists');
		}
	}

	/*
	 * Change status category
	 *
	 */
	public function changeStatus()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->Categories->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/categories/lists');
			}

			//--- Transaction Begin ---
			$this->connection->begin();
			$category = $this->Categories->changeStatus($id, $data['flg_show']);
			if ($category !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Thay đổi trạng thái danh mục thành công!'));
				return $this->redirect('/admin/categories/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Thay đổi trạng thái danh mục không thành công!'));
			return $this->redirect('/admin/categories/lists');
		} else {
			$this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
			return $this->redirect('/admin/categories/lists');
		}
	}

	/*
	 * Move category
	 *
	 */
	public function move()
	{
		$id = $this->getRequest()->getParam('id');
		$num = $this->getRequest()->getParam('num');
		if (isset($id) && !empty($id)) {
			$categories = $this->Categories->getDataById($id);
			$moveCategory = $this->Categories->getDataById($num);
			if (empty($categories) || empty($moveCategory)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/categories/lists');
			}
			//--- Transaction Begin ---
			$this->connection->begin();
			$data['order_num'] = $moveCategory['order_num'];
			$category = $this->Categories->edit($data, $id);
			$data['order_num'] = $categories['order_num'];
			$moveCategory = $this->Categories->edit($data, $num);
			if ($category !== false && $moveCategory !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Di chuyển danh mục thành công!'));
				return $this->redirect('/admin/categories/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Di chuyển danh mục không thành công!'));
			return $this->redirect('/admin/categories/lists');
		} else {
			$this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
			return $this->redirect('/admin/logout');
		}
	}

	private function generateList($parentId = null, &$listCategory, $categoryName, $conditions = null, $id = null, $level = 0)
	{
		if (!empty($conditions)) {
			if (!isset($conditions['parent_id']) || empty($conditions['parent_id'])) {
				$conditions = array_merge($conditions, ['parent_id is Null']);
			}
			$condition = $conditions;
		} else {
			if (!empty($parentId)) {
				$condition = array('parent_id' => $parentId, 'flg_delete' => 0);
			} else {
				$categoryName = '';
				$condition = array('parent_id is Null', 'flg_delete' => 0);
			}
			if (!empty($id)) {
				$condition = array_merge($condition, ['id <> ' . $id]);
			}
		}
		$condition = array_merge($condition, ['flg_site' => AppConst::SITE_CONST]);
		$order = ['order_num' => 'ASC'];
		$listChild = $this->Categories->where($condition, null, $order);

		if (empty($listChild)) {
			return;
		}
		if (!empty($parentId)) {
			$categoryName .= "|--- ";
		}
		if (isset($conditions[0])) {
			array_splice($conditions, 0);
		}
		$level++;

		foreach ($listChild as $key => $child) {
			$up = 1;
			$down = 1;
			$upNum = 0;
			$downNum = 0;
			if ($key == 0) {
				$up = 0;
			}
			if ($key == (count($listChild) - 1)) {
				$down = 0;
			}
			if ($up) {
				$upNum = $listChild[$key - 1]['id'];
			}
			if ($down) {
				$downNum = $listChild[$key + 1]['id'];
			}
			$listCategory[] = array(
				'id' => $child['id'],
				'title' => $categoryName . $child['title'],
				'type' => $child['type'],
				'slug' => $child['slug'],
				'flg_language' => $child['flg_language'],
				'flg_show' => $child['flg_show'],
				'level' => $level,
				'up' => $up,
				'up_num' => $upNum,
				'down' => $down,
				'down_num' => $downNum
			);
			if (!empty($conditions)) {
				$conditions['parent_id'] = $child['id'];
			}
			$this->generateList($child['id'], $listCategory, $categoryName, $conditions, $id, $level);
		}
	}
}
