<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use App\Model\Validation\EventsValidator;
use App\Model\Validation\SeoValidator;
use Cake\Datasource\ConnectionManager;

class EventsController extends AdminController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Events');
        $this->loadComponent('Seo');
        $this->loadComponent('Function');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    /*--------------------------------------------------------------------*/
    public function index()
    {
        $conditions = [];
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            if ($req["flg_language"] != "") {
                $conditions = array_merge($conditions, ['Events.flg_language' => $req["flg_language"]]);
            }
            if ($req["flg_status"] != "") {
                $conditions = array_merge($conditions, ['Events.flg_status' => $req["flg_status"]]);
            }
            if (!empty(trim($req["keyword"]))) {
                $conditions = array_merge($conditions, ['Events.title like' => "%" . trim($req["keyword"]) . "%"]);
            }
            if ($req["flg_department"] != "") {
                $conditions = array_merge($conditions, ['Events.flg_department' => $req["flg_department"]]);
            }
        }
        $events = $this->Events->all($conditions);
        $this->set(compact('events'));
    }

    /*--------------------------------------------------------------------*/
    public function view($id = null)
    {
        $event = $this->Events->get($id, [
            'contain' => ['Seos'],
        ]);

        $this->set('event', $event);
    }

    /*--------------------------------------------------------------------*/
    public function add()
    {
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();

            $eventsValidator = new EventsValidator();
            $seoValidator = new SeoValidator();
            $errorsEvents = $eventsValidator->addVaild($req);
            $errorsSeo = $seoValidator->errors($req);

            if (!empty($errorsSeo) || !empty($errorsEvents)) {
                $this->set('errSeo', $errorsSeo);
                $this->set('errEvents', $errorsEvents);
                return;
            }
            try {
                $this->connection->begin();
                $resultSeo = $this->Seo->add($req);
                $req["user_id"] = $this->userLogined['id'];
                $req["seo_id"] = $resultSeo->id;
                $resultEvents = $this->Events->add($req);
                if ($resultEvents && $resultSeo) {
                    $this->Flash->success(__('The event has been saved.'));
                    $this->connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->connection->rollback();
                    $this->Flash->error(__('The event could not be saved. Please, try again.'));
                }
            } catch (Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($id = null)
    {
        $event = $this->Events->first(['id' => $id])->toArray();
        $seo = $this->Seo->first(['id' => $event['seo_id']])->toArray();
        if ($this->getRequest()->is('post')) {
            $req = $this->getRequest()->getData();

            $eventsValidator = new EventsValidator();
            $seoValidator = new SeoValidator($req);
            $errorsEvents = $eventsValidator->editVaild($req);
            $errorsSeo = $seoValidator->errors($req);

            if (!empty($errorsSeo) || !empty($errorsEvents)) {
                $this->set('errSeo', $errorsSeo);
                $this->set('errEvents', $errorsEvents);
            } else {
                try {
                    $this->connection->begin();
                    $resultSeo = $this->Seo->edit($req, $seo['id']);
                    $resultEvents = $this->Events->edit($req, $event['id']);
                    if ($resultEvents && $resultSeo) {
                        $this->Flash->success(__('The event has been saved.'));
                        $this->connection->commit();
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->connection->rollback();
                        $this->Flash->error(__('The event could not be saved. Please, try again.'));
                    }
                } catch (Exception $exc) {
                    $this->connection->rollback();
                    $this->Flash->error(__('The event could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('event', 'seo'));
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        if ($id != null) {
            $event = $this->Events->first(['id' => $id]);
            try {
                $this->connection->begin();
                $resultSeo = $this->Seo->delete($event['id']);
                $resultEvents = $this->Events->delete($id);
                if ($resultEvents && $resultSeo) {
                    $this->Flash->success(__('The event has been deleted.'));
                    $this->connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->connection->rollback();
                    $this->Flash->error(__('The event could not be deleted. Please, try again.'));
                }
            } catch (Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error(__('The event could not be deleted. Please, try again.'));
            }
        }
    }
}
