<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use App\Model\Validation\StudentCommentsValidator;

class StudentFeelsController extends AdminController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('StudentFeel');
        $this->loadComponent('Department');
        $this->loadModel('StudentComments');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
        $this->role_id = $this->userLogined['role_id'];
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('StudentFeelSearch')) {
            $this->Session->delete('StudentFeelSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('StudentFeelSearch', $data);
            $inputSearch = $this->Session->read('StudentFeelSearch');
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_language'] = $status;
            }
            if ($data["department_id"] != "") {
                $conditions['department_id'] = $data["department_id"];
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['content like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        if ($this->role_id == 2) {
            array_push($conditions, ['department_id IN' => $this->userLogined['module_id']]);
        }
        $studentfeel = $this->StudentFeel->all($conditions);
        $studentfeel = $this->paginate($studentfeel);
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $this->set(compact('studentfeel', 'inputSearch', 'lstDepartments'));
    }

    public function add()
    {
        $input = "";
        if ($this->Session->check('InputFeel')) {
            $this->Session->delete('InputFeel');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $studentCommentsVaild = new StudentCommentsValidator();
            $errorsStudentComments = $studentCommentsVaild->addVaild($data);
            if (empty($errorsStudentComments)) {
                $this->Session->write('InputFeel', $data);
                $input = $this->Session->read('InputFeel');
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->StudentFeel->add($data);
                    $this->connection->commit();
                    $this->Session->delete('InputFeel');
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errStudentComments', $errorsStudentComments);
            }
        }
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $this->set(compact('input', 'lstDepartments'));
        $this->render('add');
    }

    public function edit($id)
    {
        $studentfeel = $this->StudentFeel->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $studentCommentsVaild = new StudentCommentsValidator();
            $errorsStudentComments = $studentCommentsVaild->editVaild($data);
            if (empty($errorsStudentComments)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->StudentFeel->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errStudentComments', $errorsStudentComments);
            }
        }
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $this->set(compact('studentfeel', 'lstDepartments'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->StudentFeel->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->StudentFeel->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
