<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\DepartmentsValidator;
use App\Model\Validation\SeoValidator;

class DepartmentsController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Department');
        $this->loadComponent('Function');
        $this->loadComponent('ListDepartment');
        $this->loadComponent('Seo');
        $this->loadModel('Departments');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
        $this->role_id = $this->userLogined['role_id'];
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('DepartSearch')) {
            $this->Session->delete('DepartSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('DepartSearch', $data);
            $inputSearch = $this->Session->read('DepartSearch');
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $lang = 0;
                } else {
                    $lang = 1;
                }
                $conditions['Departments.flg_language'] = $lang;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['Departments.title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        if ($this->role_id == 2) {
            array_push($conditions, ['Departments.parent_key IN' => $this->userLogined['module_id']]);
        }
        $departments = $this->Department->all($conditions);
        $departments = $this->paginate($departments);
        $this->set(compact('departments', 'inputSearch'));
    }

    public function add()
    {
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $departments = $this->Department->getListDepartment($conditionsGetListDepartment);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $departmentsValidator = new DepartmentsValidator();
            $seoValidator = new SeoValidator($data);
            $errorsDepartments = $departmentsValidator->addVaild($data);
            $errorsSeo = $seoValidator->errors($data);
            if (empty($errorsDepartments) && empty($errorsSeo)) {
                $flgExist = $this->Department->checkExistLangAdd($data['parent_key'], $data['flg_language']);
                if (isset($flgExist['flg_language'])) {
                    $this->Flash->error('Đã tồn tại hai bản ghi Tiếng Việt hoặc Tiếng Anh');
                } else {
                    try {
                        $this->connection->begin();
                        $data["user_id"] = $this->userLogined['id'];
                        $this->Department->add($data);
                        $this->connection->commit();
                        $this->Flash->success('Dữ liệu được lưu thành công');
                        return $this->redirect(['action' => 'index']);
                    } catch (\Exception $e) {
                        $this->connection->rollback();
                        $this->Flash->error('Dữ liệu lưu không thành công');
                    }
                }
            } else {
                $this->set('errDepartments', $errorsDepartments);
                $this->set('errSeo', $errorsSeo);
            }
        }
        $this->set(compact('departments'));
        $this->render('add');
    }

    public function edit($id)
    {
        $departmests = $this->Department->first(['id' => $id]);
        $conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
        $lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment);
        $seo = $this->Seo->first(['id' => $departmests['seo_id']]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $departmentsValidator = new DepartmentsValidator();
            $seoValidator = new SeoValidator($data);
            $errorsDepartments = $departmentsValidator->editVaild($data);
            $errorsSeo = $seoValidator->errors($data);
            if (empty($errorsDepartments) && empty($errorsSeo)) {
                $countDepart = $this->Department->checkParentDepartment($data['parent_key'], $data['flg_language']);
                $flgExist = $this->Department->checkExistLang($data['parent_key'], $data['flg_language'], $id);
                if ($flgExist['flg_language'] !== (int) $data['flg_language'] && $countDepart['count'] == 2) {
                    $this->Flash->error('Đã tồn tại hai bản ghi Tiếng Việt và Tiếng Anh.');
                } else {
                    try {
                        $this->connection->begin();
                        $data["user_id"] = $this->userLogined['id'];
                        $this->Department->edit($data, $id);
                        $this->connection->commit();
                        $this->Flash->success('Dữ liệu được cập nhật thành công');
                        return $this->redirect(['action' => 'index']);
                    } catch (\Exception $e) {
                        $this->connection->rollback();
                        $this->Flash->error('Dữ liệu cập nhật không thành công');
                    }
                }
            } else {
                $this->set('errDepartments', $errorsDepartments);
                $this->set('errSeo', $errorsSeo);
            }
        }
        $this->set(compact('departmests', 'lstDepartments', 'seo'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Department->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
