<?php

namespace App\Controller;

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\BannerValidator;

class BannerController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Banner');
        $this->loadComponent('Function');
        $this->loadComponent('BannerDetail');
        $this->connection = ConnectionManager::get('default');
        $this->session = $this->getRequest()->getSession();
    }

    /*--------------------------------------------------------------------*/
    public function index()
    {
        $conditions = [];
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            if ($req["flg_language"] != "") {
                $conditions['flg_language'] = $req["flg_language"];
            }
            if ($req["flg_status"] != "") {
                $conditions['flg_status'] = $req["flg_status"];
            }
            if (!empty(trim($req["keyword"]))) {
                $conditions['title like'] = "%" . trim($req["keyword"]) . "%";
            }
            if ($req["mode"] != "") {
                $conditions['mode'] = $req["mode"];
            }
        }
        $banner = $this->Banner->all($conditions);
        $this->set(compact('banner'));
    }

    /*--------------------------------------------------------------------*/
    public function view($id = null)
    {
        $banner = $this->Banner->get($id, [
            'contain' => ['DetailBanner']
        ]);

        $this->set('banner', $banner);
    }

    /*--------------------------------------------------------------------*/
    public function add()
    {
        $lstImg = ($this->session->check('lstImg') === true) ? $this->session->read('lstImg') : [];
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $bannerValidator = new BannerValidator();
            $errorsBanner = $bannerValidator->errors($req);
            if (!empty($errorsBanner)) {
                $this->set('errBanner', $errorsBanner);
            } else {
                try {
                    $this->connection->begin();
                    $resultBanner = $this->Banner->add($req);
                    $data = [];
                    if ($resultBanner) {
                        foreach ($lstImg as $item) {
                            array_push($data, [
                                'title' => $req[str_replace('.', '', $item) . "_title"],
                                'img' => $item,
                                'create_date' => date("Y-m-d H:i:s"),
                                'banner_id' => $resultBanner['id']
                            ]);
                        }
                        $this->BannerDetail->saveMany($data);
                        foreach ($lstImg as $item) {
                            copy('admin/upload/banner/ram/' . $item, 'admin/upload/banner/' . $item);
                        }
                        $this->session->delete('lstImg');
                        $this->Flash->success(__('Lưu banner thành công.'));
                        $this->connection->commit();
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->connection->rollback();
                        $this->Flash->error(__('Đã có lỗi khi lưu. Vui lòng kiểm tra lại.'));
                    }
                } catch (Exception $exc) {
                    $this->connection->rollback();
                    $this->Flash->error(__('Đã có lỗi khi lưu. Vui lòng kiểm tra lại.'));
                }
            }
        }
        $this->set(compact('lstImg'));
    }

    /*--------------------------------------------------------------------*/
    public function edit($id = null)
    {
        $banner = $this->Banner->first(['id' => $id]);
        $lstImg = $this->BannerDetail->all(['banner_id' => $id]);
        if ($this->getRequest()->isPost()) {
            $req = $this->getRequest()->getData();
            $bannerValidator = new BannerValidator();
            $errorsBanner = $bannerValidator->errors($req);

            if (!empty($errorsBanner)) {
                $this->set('errBanner', $errorsBanner);
            } elseif ($this->Banner->edit($req, $id)) {
                $this->Flash->success(__('Lưu banner thành công.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Đã có lỗi khi lưu. Vui lòng kiểm tra lại.'));
        }
        $this->set(compact('banner', 'lstImg'));
    }

    /*--------------------------------------------------------------------*/
    public function editDetail()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            $bannerDetail = $this->BannerDetail->first(['id' => $req['id']]);
            $this->set(compact('bannerDetail'));
        }
        $this->render('edit_detail');
    }

    /*--------------------------------------------------------------------*/
    public function addDetail()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            try {
                $this->connection->begin();
                if (isset($req['file']['error']) && $req['file']['error'] == 0) {
                    $name = $this->Banner->uploadImg($req['file'], 'banner');
                    $req['img'] = $name;
                }
                $result = $this->BannerDetail->add($req);
                if ($result) {
                    $this->connection->commit();
                    $lstImg = $this->BannerDetail->all(['banner_id' => $result['banner_id']]);
                    $this->set(compact('lstImg'));
                } else {
                    $this->connection->rollback();
                }
            } catch (Exception $exc) {
                $this->connection->rollback();
            }
        }
        $this->render('save_detail');
    }

    /*--------------------------------------------------------------------*/
    public function saveDetail()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            try {
                $this->connection->begin();
                $banner_detail = $this->BannerDetail->first(['id' => $req['id']]);
                if (isset($req['file']['error']) && $req['file']['error'] == 0) {
                    $name = $this->Banner->uploadImg($req['file'], 'banner');
                    $req['img'] = $name;
                }
                $result = $this->BannerDetail->edit($req, $req['id']);
                if ($result) {
                    if (isset($req['file']['error']) && file_exists(WWW_ROOT . 'admin/upload/banner/' . $banner_detail['img'])) {
                        unlink(WWW_ROOT . 'admin/upload/banner/' . $banner_detail['img']);
                    }
                    $this->connection->commit();
                    $lstImg = $this->BannerDetail->all(['banner_id' => $result['banner_id']]);
                    $this->set(compact('lstImg'));
                } else {
                    $this->connection->rollback();
                }
            } catch (Exception $exc) {
                $this->connection->rollback();
            }
        }
        $this->render('save_detail');
    }

    /*--------------------------------------------------------------------*/
    public function deleteDetail($id)
    {
        $this->viewBuilder()->setLayout('');
        $this->autoRender = false;
        if ($id != null) {
            $result = $this->BannerDetail->delete($id);
            if ($result) {
                $lstImg = $this->BannerDetail->all(['banner_id' => $result['banner_id']]);
                $this->set(compact('lstImg'));
            } else { }
        }
        $this->render('save_detail');
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $this->autoRender = false;
        if ($id != null) {
            $result = $this->Banner->delete($id);
            if ($result) {
                $this->Flash->success(__('Xóa banner thành công.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Xóa banner không thành công. Vui lòng thử lại.'));
            }
        }
    }

    /*--------------------------------------------------------------------*/
    function saveImgToRam()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            if (!$this->session->check('lstImg')) {
                $this->Banner->removeAllImg();
            }
            if (isset($req['file'])) {
                $reqLstImg = $req['file'];
                $lstImg = ($this->session->check('lstImg') === true) ? $this->session->read('lstImg') : [];
                foreach ($reqLstImg as $item) {
                    if ($item['error'] != 0) {
                        continue;
                    }
                    $name = $this->Banner->uploadImg($item, 'banner/ram');
                    if ($name != "000") {
                        array_push($lstImg, $name);
                    }
                }
                $this->session->write('lstImg', $lstImg);
                $this->set('lstImg', $this->session->read('lstImg'));
            }
        }
        $this->render("save_img_to_ram");
    }

    /*--------------------------------------------------------------------*/
    function removeImgToRam()
    {
        $this->viewBuilder()->setLayout('');
        if ($this->getRequest()->is('ajax')) {
            $req = $this->getRequest()->getData();
            $lstImg = ($this->session->check('lstImg') === true) ? $this->session->read('lstImg') : [];
            $lstImg = array_diff($lstImg, [$req['img_name']]);
            $this->Banner->removeImg($req['img_name']);
            if (count($lstImg) > 0) {
                $this->session->write('lstImg', $lstImg);
                $this->set('lstImg', $this->session->read('lstImg'));
            } else {
                $this->session->delete('lstImg');
            }
        }
        $this->render("save_img_to_ram");
    }

    /*--------------------------------------------------------------------*/
    public function status($id)
    {
        $banner = $this->Banner->first(['id' => $id]);
        $data['flg_status'] = $banner['flg_status'] == 1 ? 0 : 1;
        if ($this->Banner->edit($data, $id)) {
            $this->Flash->success('Bản ghi được đổi trạng thái thành công');
        } else {
            $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
        }
        return $this->redirect(['action' => 'index']);
    }
}
