<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class HomeController extends AdminController
{

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
	}

	public function index()
	{
	    // Tổng số khoa
        $department = TableRegistry::getTableLocator()->get('Departments')->find()->count();
        // Tổng số phòng
        $list_department = TableRegistry::getTableLocator()->get('ListDepartments')->find()->count();
        // Tổng số sự kiện
        $event = TableRegistry::getTableLocator()->get('events')->find()->count();
	    // Tổng số tin tức
        $news = TableRegistry::getTableLocator()->get('News')->find()->count();

        // Gán dữ liệu cho bảng điều khiển
        $this->set('department',$department);
        $this->set('list_department',$list_department);
        $this->set('event',$event);
        $this->set('news',$news);
    }
}
