<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Date;
use Cake\I18n\Time;

class EnrollmentRegistrationsController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('EnrollmentRegistration');
        $this->loadModel('EnrollmentRegistrations');
        $this->connection = ConnectionManager::get('default');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('EnrollmentRegistration')) {
            $this->Session->delete('EnrollmentRegistration');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('EnrollmentRegistration', $data);
            $inputSearch = $this->Session->read('EnrollmentRegistration');
            if ($data["type"] != "") {
                $conditions['cate'] = $data["type"];
            }
            if ($data["flg_read"] != "") {
                if ($data["flg_read"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_read'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['content like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $enroregis = $this->EnrollmentRegistration->all($conditions);
        $enroregis = $this->paginate($enroregis);
        $this->set(compact('enroregis', 'inputSearch'));
    }

    public function view($id)
    {
        $enroregis = $this->EnrollmentRegistration->first(['id' => $id]);
        try {
            $this->connection->begin();
            $this->EnrollmentRegistration->read($id);
            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollback();
        }
        $this->set(compact('enroregis'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->EnrollmentRegistration->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->EnrollmentRegistration->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
