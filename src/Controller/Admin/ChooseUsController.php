<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use App\Model\Validation\ChooseUsValidator;

class ChooseUsController extends AdminController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('WhyChooseUs');
        $this->loadModel('ChooseUs');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('ChooseUsSearch')) {
            $this->Session->delete('ChooseUsSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('ChooseUsSearch', $data);
            $inputSearch = $this->Session->read('ChooseUsSearch');
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_language'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['content like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $chooseus = $this->WhyChooseUs->all($conditions);
        $chooseus = $this->paginate($chooseus);
        $this->set(compact('chooseus', 'inputSearch'));
    }

    public function add()
    {
        $input = "";
        if ($this->Session->check('InputChooseUs')) {
            $this->Session->delete('InputChooseUs');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $chooseUsValidator = new ChooseUsValidator();
            $errorsChooseUs = $chooseUsValidator->addVaild($data);
            if (empty($errorsChooseUs)) {
                $this->Session->write('InputChooseUs', $data);
                $input = $this->Session->read('InputChooseUs');
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->WhyChooseUs->add($data);
                    $this->connection->commit();
                    $this->Session->delete('InputChooseUs');
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errChoose', $errorsChooseUs);
            }
        }
        $this->set(compact('input'));
        $this->render('add');
    }

    public function edit($id)
    {
        $chooseus = $this->WhyChooseUs->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $chooseUsValidator = new ChooseUsValidator();
            $errorsChooseUs = $chooseUsValidator->editVaild($data);
            if (empty($errorsChooseUs)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->WhyChooseUs->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errChoose', $errorsChooseUs);
            }
        }
        $this->set(compact('chooseus'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->WhyChooseUs->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->WhyChooseUs->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
