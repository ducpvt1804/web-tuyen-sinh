<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use \Cake\Datasource\Exception\RecordNotFoundException;
use App\Auth\CustomPasswordHasher;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\LibrariesValidator;
use App\Model\Validation\LibrariesDetailValidator;
use App\Model\Validation\LibrariesMediaValidator;
use App\Model\Validation\SeoValidator;
use AppConst;

/**
 * Class LibrariesController
 *
 * @package App\Controller\Admin
 */
class LibrariesController extends AdminController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Libraries');
		$this->loadComponent('LibrariesDetail');
		$this->loadComponent('LibrariesMedia');
		$this->loadComponent('Seo');
		$this->loadComponent('Department');
		$this->loadComponent('Departmentpb');
		$this->connection = ConnectionManager::get('default');
		$this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
        $this->role_id = $this->userLogined['role_id'];
	}

	/*
	 * Search libraries
	 */
	public function lists()
	{
		$conditions = array();
		$param = array();

		$conditions = array('Libraries.flg_delete' => 0);
		if ($this->request->is('post')) {
			$data = $this->getRequest()->getData('data');
			$this->getRequest()->getSession()->write('Libraries.search', $data);
		} else {
			$data = $this->getRequest()->getSession()->read('Libraries.search');
			if (!isset($data) || empty($data)) {
				$data['name_library'] = '';
				$data['type'] = '';
			}
		}
		if (isset($data['name_library']) && !empty($data['name_library'])) {
			$conditions = array_merge($conditions, ["LibrariesDetail.name_library LIKE '%" . $data['name_library'] . "%'"]);
		}
		if (isset($data['type']) && !empty($data['type'])) {
			$conditions = array_merge($conditions, ['Libraries.type IN ' => $data['type']]);
		}
		if (!empty($conditions)) {
			$libraries = $this->Libraries->where($conditions);
		} else {
			$libraries = $this->Libraries->all();
		}
		// $countArray = $this->Libraries->find()
		// 			->where($conditions)
		// 			->count();
		$this->set('libraries', $libraries);
		$this->set('param', $data);
		// $this->set('countArray', $countArray);
	}

	/*
	 * Add new library
	 */
	public function add()
	{
		if ($this->request->is('post')) {
			//--- Preparing data ---
			$data = $this->getRequest()->getData('data');
			$data['flg_delete'] = 0;
			$data['vi']['LibrariesDetail']['flg_delete'] = 0;
			$data['en']['LibrariesDetail']['flg_delete'] = 0;
			$data['vi']['Seo']['flg_delete'] = 0;
			$data['en']['Seo']['flg_delete'] = 0;
			$data['vi']['LibrariesDetail']['flg_language'] = AppConst::FLG_LANGUAGE_VIETNAM;
			$data['en']['LibrariesDetail']['flg_language'] = AppConst::FLG_LANGUAGE_ENGLISH;

			//======= Validate data ====
			$libraryValid = new LibrariesValidator();
			$libraryDetailValid = new LibrariesDetailValidator();
			$seoValid = new SeoValidator();
			$errorsLibrary = $libraryValid->errors($data);
			$errorsLibraryVi = $libraryDetailValid->errors($data['vi']['LibrariesDetail']);
			$errorsLibraryEn = $libraryDetailValid->errors($data['en']['LibrariesDetail']);
			$errorsSeoVi = $seoValid->errors($data['vi']['Seo']);
			$errorsSeoEn = $seoValid->errors($data['en']['Seo']);

			//======= Save data ===
			if (empty($errorsLibrary) && empty($errorsLibraryVi) && empty($errorsLibraryEn) && empty($errorsSeoVi) && empty($errorsSeoEn)) {
				//--- Transaction Begin ---
				$this->connection->begin();
				$libraries = $this->Libraries->add($data);
				if ($libraries !== false) {
					$seoVi = $this->Seo->add($data['vi']['Seo']);
					$seoEn = $this->Seo->add($data['en']['Seo']);
					if ($seoVi !== false && $seoEn !== false) {
						$data['vi']['LibrariesDetail']['seo_id'] = $seoVi->id;
						$data['en']['LibrariesDetail']['seo_id'] = $seoEn->id;
						$data['vi']['LibrariesDetail']['library_id'] = $libraries->id;
						$data['en']['LibrariesDetail']['library_id'] = $libraries->id;
						$libraryVi = $this->LibrariesDetail->add($data['vi']['LibrariesDetail']);
						$libraryEn = $this->LibrariesDetail->add($data['en']['LibrariesDetail']);
						if ($libraryVi !== false && $libraryEn !== false) {
							$this->connection->commit();
							$this->Flash->success(__('Thêm mới thư viện ảnh/video thành công!'));
							return $this->redirect('/admin/libraries/lists');
						}
					}
				}
				$this->connection->rollback();
				//--- Transaction End ---
			} else {
				$this->set('errorsLibrary', $errorsLibrary);
				$this->set('errorsLibraryVi', $errorsLibraryVi);
				$this->set('errorsLibraryEn', $errorsLibraryEn);
				$this->set('errorsSeoVi', $errorsSeoVi);
				$this->set('errorsSeoEn', $errorsSeoEn);
			}
			$this->Flash->error(__('Thêm mới thư viện ảnh/video không thành công!'));
			$this->set('data', $data);
		}
		$conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
		$lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment)->all()->toArray();
		$lstDepartmentsPB = $this->Departmentpb->getListDepartment($conditionsGetListDepartment)->all()->toArray();
		$this->set('lstDepartments', array_merge($lstDepartments, $lstDepartmentsPB));
	}

	/*
	 * Edit library
	 */
	public function edit()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data['vi'] = $this->Libraries->getLibraryVi($id);
			$data['en'] = $this->Libraries->getLibraryEn($id);
			if (empty($data['vi']) || empty($data['en'])) {
				$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
				return $this->redirect('/admin/libraries/lists');
			}
			if ($this->request->is('post')) {
				//--- Preparing data ---
				$data = array_merge($data, $this->getRequest()->getData('data'));
				$data['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['vi']['LibrariesDetail']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['en']['LibrariesDetail']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['vi']['Seo']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['en']['Seo']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['vi']['LibrariesDetail']['flg_language'] = AppConst::FLG_LANGUAGE_VIETNAM;
				$data['en']['LibrariesDetail']['flg_language'] = AppConst::FLG_LANGUAGE_ENGLISH;

				//======= Validate data ====
				$libraryValid = new LibrariesValidator();
				$libraryDetailValid = new LibrariesDetailValidator();
				$seoValid = new SeoValidator();
				$errorsLibrary = $libraryValid->errors($data);
				$errorsLibraryVi = $libraryDetailValid->errors($data['vi']['LibrariesDetail']);
				$errorsLibraryEn = $libraryDetailValid->errors($data['en']['LibrariesDetail']);
				$errorsSeoVi = $seoValid->errors($data['vi']['Seo']);
				$errorsSeoEn = $seoValid->errors($data['en']['Seo']);

				//======= Save data ===
				if (empty($errorsLibrary) && empty($errorsLibraryVi) && empty($errorsLibraryEn) && empty($errorsSeoVi) && empty($errorsSeoEn)) {
					//--- Transaction Begin ---
					$this->connection->begin();
					$libraries = $this->Libraries->edit($data, $id);
					if ($libraries !== false) {
						$seoVi = $this->Seo->edit($data['vi']['Seo'], $data['vi']['LibrariesDetail']['seo_id']);
						$seoEn = $this->Seo->edit($data['en']['Seo'], $data['en']['LibrariesDetail']['seo_id']);
						if ($seoVi !== false && $seoEn !== false) {
							$libraryVi = $this->LibrariesDetail->edit($data['vi']['LibrariesDetail'], $data['vi']['LibrariesDetail']['id']);
							$libraryEn = $this->LibrariesDetail->edit($data['en']['LibrariesDetail'], $data['en']['LibrariesDetail']['id']);
							if ($libraryVi !== false && $libraryEn !== false) {
								$this->connection->commit();
								$this->Flash->success(__('Chỉnh sửa thư viện ảnh/video thành công!'));
								return $this->redirect('/admin/libraries/lists');
							}
						}
					}
					$this->connection->rollback();
					//--- Transaction End ---
				} else {
					$this->set('errorsLibrary', $errorsLibrary);
					$this->set('errorsLibraryVi', $errorsLibraryVi);
					$this->set('errorsLibraryEn', $errorsLibraryEn);
					$this->set('errorsSeoVi', $errorsSeoVi);
					$this->set('errorsSeoEn', $errorsSeoEn);
				}
				$this->Flash->error(__('Chỉnh sửa thư viện ảnh/video không thành công!'));
			}
			$this->set('data', $data);
			$this->set('id', $id);
			$conditionsGetListDepartment = $this->role_id == 2 ? ['id IN' => $this->userLogined['module_id']] : [];
			$lstDepartments = $this->Department->getListDepartment($conditionsGetListDepartment)->all()->toArray();
			$lstDepartmentsPB = $this->Departmentpb->getListDepartment($conditionsGetListDepartment)->all()->toArray();
			$this->set('lstDepartments', array_merge($lstDepartments, $lstDepartmentsPB));
		} else {
			$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
			return $this->redirect('/admin/libraries/lists');
		}
	}

	/*
	 * detail library
	 */
	public function detail()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			if ($this->request->is('post')) {
				//--- Preparing data ---
				$data = $this->getRequest()->getData('data');
				$errors = [];
				if ($data['type'] == 1) {
					//======= Validate data ====
					if (!isset($data['image'][0]['tmp_name']) || empty($data['image'][0]['tmp_name'])) {
						$errors['image'] = 'Vui lòng chọn ít nhất 1 ảnh để thêm vào album!';
					}
				} else {
					//======= Validate data ====
					if (!isset($data['image'][0]) || empty($data['image'][0])) {
						$errors['image'] = 'Vui lòng điền url video để thêm vào album!';
					}
				}

				//======= Save data ===
				if (empty($errors)) {
					//--- Transaction Begin ---
					$this->connection->begin();
					foreach ($data['image'] as $key => $val) {
						$library = $this->LibrariesMedia->add($val, $id, $data['type']);
						if ($library === false) {
							$this->Flash->error(__('Thêm ảnh/video không thành công!'));
							$this->connection->rollback();
							return $this->redirect('/admin/libraries/detail/' . $id);
						}
					}
					$this->connection->commit();
					$this->Flash->success(__('Thêm ảnh/video thành công!'));
					return $this->redirect('/admin/libraries/detail/' . $id);
					//--- Transaction End ---
				} else {
					$this->Flash->error(__('Thêm ảnh/video không thành công!'));
					$this->set('errors', $errors);
				}
			}
			$data = array();
			$data['vi'] = $this->Libraries->getLibraryVi($id);
			$data['en'] = $this->Libraries->getLibraryEn($id);
			$data['media'] = $this->Libraries->getLibraryMedia($id);
			if (empty($data)) {
				$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
				return $this->redirect('/admin/libraries/lists');
			}
			$this->set('data', $data);
			$this->set('id', $id);
		} else {
			$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
			return $this->redirect('/admin/libraries/lists');
		}
	}

	/*
	 * Delete library
	 *
	 */
	public function delete()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data['vi'] = $this->Libraries->getLibraryVi($id);
			$data['en'] = $this->Libraries->getLibraryEn($id);
			if (empty($data)) {
				$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
				return $this->redirect('/admin/libraries/lists');
			}
			//--- Transaction Begin ---
			$this->connection->begin();
			$libraries = $this->Libraries->delete($id);
			if ($libraries !== false) {
				$seoVi = $this->Seo->delete($data['vi']['LibrariesDetail']['seo_id']);
				$seoEn = $this->Seo->delete($data['en']['LibrariesDetail']['seo_id']);
				if ($seoVi !== false && $seoEn !== false) {
					$libraryVi = $this->LibrariesDetail->delete($data['vi']['LibrariesDetail']['id']);
					$libraryEn = $this->LibrariesDetail->delete($data['en']['LibrariesDetail']['id']);
					if ($libraryVi !== false && $libraryEn !== false) {
						$this->connection->commit();
						$this->Flash->success(__('Xoá thư viện ảnh/video thành công!'));
						return $this->redirect('/admin/libraries/lists');
					}
				}
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Xoá thư viện ảnh/video không thành công!'));
			return $this->redirect('/admin/libraries/lists');
		} else {
			$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
			return $this->redirect('/admin/libraries/lists');
		}
	}

	/*
	 * Delete Image/Video
	 *
	 */
	public function deleteMedia()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->getRequest()->getData('data');
			if (!isset($data['media']) || empty($data['media'])) {
				$this->Flash->error(__('Ảnh/Video bạn chọn không tồn tại!'));
				return $this->redirect('/admin/libraries/detail/' . $id);
			}
			//--- Transaction Begin ---
			$this->connection->begin();
			foreach ($data['media'] as $key => $val) {
				$library = $this->LibrariesMedia->delete($val, $id);
				if ($library === false) {
					$this->Flash->error(__('Xóa ảnh/video không thành công!'));
					$this->connection->rollback();
					return $this->redirect('/admin/libraries/detail/' . $id);
				}
			}
			$this->connection->commit();
			$this->Flash->success(__('Xóa ảnh/video thành công!'));
			return $this->redirect('/admin/libraries/detail/' . $id);
		} else {
			$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
			return $this->redirect('/admin/libraries/lists');
		}
	}

	/*
	 * Change status library
	 *
	 */
	public function changeStatus()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->LibrariesDetail->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Thư viện ảnh/video bạn chọn không tồn tại!'));
				return $this->redirect('/admin/libraries/lists');
			}

			//--- Transaction Begin ---
			$this->connection->begin();
			$library = $this->LibrariesDetail->changeStatus($id, $data['flg_status']);
			if ($library !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Thay đổi trạng thái thư viện ảnh/video thành công!'));
				return $this->redirect('/admin/libraries/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Thay đổi trạng thái thư viện ảnh/video không thành công!'));
			return $this->redirect('/admin/libraries/lists');
		} else {
			$this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
			return $this->redirect('/admin/libraries/lists');
		}
	}
}
