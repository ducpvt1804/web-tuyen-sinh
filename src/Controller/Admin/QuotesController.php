<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\QuotesValidator;

class QuotesController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('Quote');
        $this->loadModel('Quotes');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('QuoteSearch')) {
            $this->Session->delete('QuoteSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('QuoteSearch', $data);
            $inputSearch = $this->Session->read('QuoteSearch');
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $lang = 0;
                } else {
                    $lang = 1;
                }
                $conditions['flg_language'] = $lang;
            }
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if ($data["cate"] != "") {
                if ($data["cate"] == 1) {
                    $cate = 0;
                } else {
                    $cate = 1;
                }
                $conditions['cate'] = $cate;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $quotes = $this->Quote->all($conditions);
        $quotes = $this->paginate($quotes);
        $this->set(compact('quotes', 'inputSearch'));
    }

    public function add()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $quotesValidator = new QuotesValidator();
            $errorsQuotes = $quotesValidator->addVaild($data);
            if (empty($errorsQuotes)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->Quote->add($data);
                    $this->connection->commit();
                    $this->Session->delete('InputQuote');
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errQuotes', $errorsQuotes);
            }
        }
        $this->render('add');
    }

    public function edit($id)
    {
        $quote = $this->Quote->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $quotesValidator = new QuotesValidator();
            $errorsQuotes = $quotesValidator->editVaild($data);
            if (empty($errorsQuotes)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->Quote->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errQuotes', $errorsQuotes);
            }
        }
        $this->set(compact('quote'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Quote->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->Quote->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
