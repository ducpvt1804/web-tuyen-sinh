<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\ConfigsValidator;

class ConfigController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Config');
        $this->loadComponent('Function');
        $this->loadModel('Configs');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $config = $this->Config->all();
        $config = $this->paginate($config);
        $this->set(compact('config'));
    }

    public function edit($id)
    {
        $config = $this->Config->getDataConfig($id);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $configsValidator = new ConfigsValidator();
            $errorsConfig = $configsValidator->errors($data);
            if (empty($errorsConfig)) {
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->Config->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Error');
                }
            } else {
                $this->set('errConfig', $errorsConfig);
            }
        }
        $this->set(compact('config'));
    }
}
