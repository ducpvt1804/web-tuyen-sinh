<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\EnrollmentOptionsValidator;

class EnrollmentOptionsController extends AdminController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Function');
        $this->loadComponent('EnrollmentOption');
        $this->loadModel('EnrollmentOptions');
        $this->connection = ConnectionManager::get('default');
        $this->userLogined = $this->getRequest()->getSession()->read('Auth.User');
    }

    public function index()
    {
        $conditions = [];
        $inputSearch = "";
        if ($this->Session->check('OptionsSearch')) {
            $this->Session->delete('OptionsSearch');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $this->Session->write('OptionsSearch', $data);
            $inputSearch = $this->Session->read('OptionsSearch');
            if ($data["type"] != "") {
                $conditions['type'] = $data["type"];
            }
            if ($data["flg_status"] != "") {
                if ($data["flg_status"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_status'] = $status;
            }
            if ($data["flg_language"] != "") {
                if ($data["flg_language"] == 1) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                $conditions['flg_language'] = $status;
            }
            if (!empty(trim($data["keyword"]))) {
                $conditions['title like'] = "%" . trim($data["keyword"]) . "%";
            }
        }
        $options = $this->EnrollmentOption->all($conditions);
        $options = $this->paginate($options);
        $this->set(compact('options', 'inputSearch'));
    }

    public function add()
    {
        $input = "";
        if ($this->Session->check('InputOption')) {
            $this->Session->delete('InputOption');
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $enrollmentOptionsValidator = new EnrollmentOptionsValidator();
            $errorsEnroll = $enrollmentOptionsValidator->errors($data);
            if (empty($errorsEnroll)) {
                $this->Session->write('InputOption', $data);
                $input = $this->Session->read('InputOption');
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->EnrollmentOption->add($data);
                    $this->connection->commit();
                    $this->Session->delete('InputOption');
                    $this->Flash->success('Dữ liệu được lưu thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu lưu không thành công');
                }
            } else {
                $this->set('errEnroll', $errorsEnroll);
            }
        }
        $list_branch_type1 = $this->EnrollmentOption->getBranchParentType1();
        $list_branch_type2 = $this->EnrollmentOption->getBranchParentType2();
        $this->set(compact('input', 'list_branch_type1', 'list_branch_type2'));
        $this->render('add');
    }

    public function edit($id)
    {
        $options = $this->EnrollmentOption->first(['id' => $id]);
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getData();
            $enrollmentOptionsValidator = new EnrollmentOptionsValidator();
            $errorsEnroll = $enrollmentOptionsValidator->errors($data);
            if (empty($errorsEnroll)) {
                $data = $this->getRequest()->getData();
                try {
                    $this->connection->begin();
                    $data["user_id"] = $this->userLogined['id'];
                    $this->EnrollmentOption->edit($data, $id);
                    $this->connection->commit();
                    $this->Flash->success('Dữ liệu được cập nhật thành công');
                    return $this->redirect(['action' => 'index']);
                } catch (\Exception $e) {
                    $this->connection->rollback();
                    $this->Flash->error('Dữ liệu cập nhật không thành công');
                }
            } else {
                $this->set('errEnroll', $errorsEnroll);
            }
        }
        $list_branch_type1 = $this->EnrollmentOption->getBranchParentType1();
        $list_branch_type2 = $this->EnrollmentOption->getBranchParentType2();
        $this->set(compact('options', 'list_branch_type1', 'list_branch_type2'));
    }

    public function delete($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->EnrollmentOption->delete($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được xóa thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể xóa. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function status($id)
    {
        $this->autoRender = false;
        $this->layout = false;
        if ($id != null) {
            try {
                $this->connection->begin();
                $this->EnrollmentOption->status($id);
                $this->connection->commit();
                $this->Flash->success('Bản ghi được đổi trạng thái thành công');
            } catch (\Exception $exc) {
                $this->connection->rollback();
                $this->Flash->error('Không thể đổi trạng thái. Vui lòng thử lại!');
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
