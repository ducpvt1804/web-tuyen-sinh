<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use \Cake\Datasource\Exception\RecordNotFoundException;
use App\Auth\CustomPasswordHasher;
use Cake\Datasource\ConnectionManager;
use App\Model\Validation\NewsValidator;
use App\Model\Validation\SeoValidator;
use AppConst;

/**
 * Class NewsController
 *
 * @package App\Controller\Admin
 */
class NewsController extends AdminController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('News');
		$this->loadComponent('Seo');
		$this->loadComponent('Categories');
		$this->loadComponent('NewsRelationship');
		$this->connection = ConnectionManager::get('default');
	}

	/*
	 * Search news
	 */
	public function lists()
	{
		$conditions = array();
		$param = array();

		$conditions = array('News.flg_delete' => 0, 'type' => AppConst::FLG_NEWS_TYPE_NORMAL);
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getData('data');
			$this->getRequest()->getSession()->write('News.search', $data);
		} else {
			$data = $this->getRequest()->getSession()->read('News.search');
			if (!isset($data) || empty($data)) {
				$data['title'] = '';
			}
		}
		if (isset($data['title']) && !empty($data['title'])) {
			$conditions = array_merge($conditions, ["News.title LIKE '%" . $data['title'] . "%'"]);
		}
		if (isset($data['flg_language']) && !empty($data['flg_language'])) {
			$conditions = array_merge($conditions, ['News.flg_language IN ' => $data['flg_language']]);
		}
		if (isset($data['flg_status']) && !empty($data['flg_status'])) {
			$conditions = array_merge($conditions, ['News.flg_status IN ' => $data['flg_status']]);
		}
		if (!empty($conditions)) {
			$news = $this->News->where($conditions);
		} else {
			$news = $this->News->all();
		}

		$listCategory = array();
		$categoryName = '';
		$this->generateList(null, $listCategory, $categoryName);
		// $countArray = $this->News->find()
		// 			->where($conditions)
		// 			->count();
		$this->set('news', $news);
		$this->set('param', $data);
		$this->set('listCategory', $listCategory);
	}

	/*
	 * Add new category
	 */
	public function add()
	{
		$listCategory = array();
		$categoryName = '';
		$this->generateList(null, $listCategory, $categoryName);
		if ($this->getRequest()->isPost()) {
			//--- Preparing data ---
			$data = $this->getRequest()->getData('data');
			$data['flg_delete'] = 0;
			$data['flg_site'] = AppConst::SITE_CONST;
			$data['Seo']['flg_delete'] = 0;
			$data['type'] = AppConst::FLG_NEWS_TYPE_NORMAL;

			//======= Validate data ====
			$newValid = new NewsValidator();
			$seoValid = new SeoValidator();
			$errorsNew = $newValid->addVaild($data);
			$errorsSeo = $seoValid->errors($data['Seo']);
			//======= Save data ===
			if (empty($errorsNew) && empty($errorsSeo)) {
				//--- Transaction Begin ---
				$this->connection->begin();
				$seo = $this->Seo->add($data['Seo']);
				if ($seo !== false) {
					$data['seo_id'] = $seo->id;
					$news = $this->News->add($data);
					if ($news !== false) {
						if (is_array($data['cate_id'])) {
							foreach ($data['cate_id'] as $key => $val) {
								$data['NewsRelationship'][$key]['new_id'] = $news->id;
								$data['NewsRelationship'][$key]['cate_id'] = $val;
								$data['NewsRelationship'][$key]['create_date'] = date("Y-m-d H:i:s");
							}
						}
						$newsRelationship = $this->NewsRelationship->addAll($data['NewsRelationship']);
						if ($newsRelationship !== false) {
							$this->connection->commit();
							$this->Flash->success(__('Thêm mới tin tức thành công!'));
							return $this->redirect('/admin/news/lists');
						}
					}
				}
				$this->connection->rollback();
				//--- Transaction End ---
			} else {
				$this->set('errSeo', $errorsSeo);
				$this->set('errNew', $errorsNew);
			}
			$this->Flash->error(__('Thêm mới tin tức không thành công!'));
			$data['img_highlight'] = '';
			$this->set('data', $data);
		}
		$listCategory = array();
		$categoryName = '';
		$this->generateList(null, $listCategory, $categoryName);
		$this->set('listCategory', $listCategory);
	}

	/*
	 * Edit category
	 */
	public function edit()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $dataOld = $this->News->getDataById($id);
			$data['schedule'] = $dataOld['schedule']->format('Y-m-d\TH:i');
			$data['date_edit'] = $dataOld['date_edit']->format('Y-m-d\TH:i');
			if (empty($dataOld)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/news/lists');
			}
			if ($this->getRequest()->isPost()) {
				//--- Preparing data ---
				$data = array_merge($data, $this->getRequest()->getData('data'));
				$data['update_date'] = date("Y-m-d H:i:s", strtotime("now"));
				$data['Seo']['update_date'] = date("Y-m-d H:i:s", strtotime("now"));

				//======= Validate data ====
				$newsValid = new NewsValidator();
				$seoValid = new SeoValidator();
				$errorsNew = $newsValid->editVaild($data);
				$errorsSeo = $seoValid->errors($data['Seo']);

				//======= Save data ===
				if ((empty($errorsNew) || $errorsNew === true) && empty($errorsSeo)) {
					//--- Transaction Begin ---
					$this->connection->begin();
					$seo = $this->Seo->edit($data['Seo'], $data['seo_id']);
					$category = $this->News->edit($data, $id);
					if ($seo !== false && $category !== false) {
						if ($this->NewsRelationship->deletePhysical($id)) {
							if (is_array($data['cate_id'])) {
								foreach ($data['cate_id'] as $key => $val) {
									$data['NewsRelationship'][$key]['new_id'] = $id;
									$data['NewsRelationship'][$key]['cate_id'] = $val;
									$data['NewsRelationship'][$key]['create_date'] = date("Y-m-d H:i:s");
								}
							}
							$newsRelationship = $this->NewsRelationship->addAll($data['NewsRelationship']);
							if ($newsRelationship !== false) {
								$this->connection->commit();
								$this->Flash->success(__('Chỉnh sửa danh mục thành công!'));
								return $this->redirect('/admin/news/lists');
							}
						}
					}
					$this->connection->rollback();
					//--- Transaction End ---
				} else {
					$this->set('errSeo', $errorsSeo);
					$this->set('errNew', $errorsNew);
				}
				$this->Flash->error(__('Chỉnh sửa danh mục không thành công!'));
			}
			$data['img_highlight'] = $dataOld['img_highlight'];
			$listCategory = array();
			$categoryName = '';
			$this->generateList(null, $listCategory, $categoryName);
			$cate = $this->NewsRelationship->getDataById($id);
			$cateId = array();
			foreach ($cate as $key => $val) {
				$cateId[] = $val['cate_id'];
			}
			if (!isset($data['cate_id'])) {
				$data['cate_id'] = $cateId;
			}
			$this->set('data', $data);
			$this->set('listCategory', $listCategory);
			$this->set('id', $id);
		} else {
			$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
			return $this->redirect('/admin/news/lists');
		}
	}

	/*
	 * Delete category
	 *
	 */
	public function delete()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->News->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/news/lists');
			}
			//--- Transaction Begin ---
			$this->connection->begin();
			$seo = $this->Seo->delete($data['seo_id']);
			$category = $this->News->delete($id);
			if ($seo !== false && $category !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Xoá danh mục thành công!'));
				return $this->redirect('/admin/news/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Xoá danh mục không thành công!'));
			return $this->redirect('/admin/news/lists');
		} else {
			$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
			return $this->redirect('/admin/news/lists');
		}
	}

	/*
	 * Change status category
	 *
	 */
	public function changeStatus()
	{
		$id = $this->getRequest()->getParam('id');
		if (isset($id) && !empty($id)) {
			$data = $this->News->getDataById($id);
			if (empty($data)) {
				$this->Flash->error(__('Danh mục bạn chọn không tồn tại!'));
				return $this->redirect('/admin/news/lists');
			}

			//--- Transaction Begin ---
			$this->connection->begin();
			$category = $this->News->changeStatus($id, $data['flg_status']);
			if ($category !== false) {
				$this->connection->commit();
				$this->Flash->success(__('Thay đổi trạng thái danh mục thành công!'));
				return $this->redirect('/admin/news/lists');
			}
			$this->connection->rollback();
			//--- Transaction End ---
			$this->Flash->error(__('Thay đổi trạng thái danh mục không thành công!'));
			return $this->redirect('/admin/news/lists');
		} else {
			$this->Flash->error(__('Đã có truy cập trái phép. Vui lòng đăng nhập lại!.'));
			return $this->redirect('/admin/news/lists');
		}
	}

	private function generateList($parentId = null, &$listCategory, $categoryName, $conditions = null, $id = null, $level = 0)
	{
		if (!empty($conditions)) {
			if (!isset($conditions['parent_id']) || empty($conditions['parent_id'])) {
				$conditions = array_merge($conditions, ['parent_id is Null']);
			}
			$condition = $conditions;
		} else {
			if (!empty($parentId)) {
				$condition = array('parent_id' => $parentId, 'flg_delete' => 0);
			} else {
				$categoryName = '';
				$condition = array('parent_id is Null', 'flg_delete' => 0);
			}
			if (!empty($id)) {
				$condition = array_merge($condition, ['id <> ' . $id]);
			}
		}
		$condition = array_merge($condition, ['flg_site' => AppConst::SITE_CONST]);
		$condition = array_merge($condition, ['type' => AppConst::FLG_CATE_TYPE_NEWS]);
		$condition = array_merge($condition, ['flg_show' => AppConst::FLG_STATUS_SHOW]);
		$order = ['order_num' => 'ASC'];
		$listChild = $this->Categories->where($condition, null, $order);

		if (empty($listChild)) {
			return;
		}
		if (!empty($parentId)) {
			$categoryName .= "|--- ";
		}
		if (isset($conditions[0])) {
			array_splice($conditions, 0);
		}
		$level++;

		foreach ($listChild as $key => $child) {
			$up = 1;
			$down = 1;
			$upNum = 0;
			$downNum = 0;
			if ($key == 0) {
				$up = 0;
			}
			if ($key == (count($listChild) - 1)) {
				$down = 0;
			}
			if ($up) {
				$upNum = $listChild[$key - 1]['id'];
			}
			if ($down) {
				$downNum = $listChild[$key + 1]['id'];
			}
			$listCategory[] = array(
				'id' => $child['id'],
				'title' => $categoryName . $child['title'],
				'type' => $child['type'],
				'slug' => $child['slug'],
				'flg_language' => $child['flg_language'],
				'flg_show' => $child['flg_show'],
				'level' => $level,
				'up' => $up,
				'up_num' => $upNum,
				'down' => $down,
				'down_num' => $downNum
			);
			if (!empty($conditions)) {
				$conditions['parent_id'] = $child['id'];
			}
			$this->generateList($child['id'], $listCategory, $categoryName, $conditions, $id, $level);
		}
	}
}
