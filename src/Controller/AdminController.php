<?php

/**
 * Created by PhpStorm.
 * User: TienPV
 * Date: 2/22/2019
 * Time: 9:02 AM
 */

namespace App\Controller;

use App\Controller\AuthController;
use Cake\Event\Event;

class AdminController extends AuthController
{

	/**
	 * Initialization hook method.
	 *
	 * Use this method to add common initialization code like loading components.
	 *
	 * e.g. `$this->loadComponent('Security');`
	 *
	 * @return void
	 */
	public function initialize()
	{
		parent::initialize();
		// Load model

		$this->loadComponent('Paginator');
		$this->loadComponent('Contact');

		// Set the layout.
		$this->viewBuilder()->setLayout('admin');

		$this->Session = $this->getRequest()->getSession();

		$data = $this->Contact->notification();
		$allMess = $this->Contact->allMess();
		$this->set(compact('data', 'allMess'));
	}

	public function beforeFilter(Event $event)
	{
		$userLogined = $this->getRequest()->getSession()->read('Auth.User');
		$this->set(compact('userLogined'));
		$controller_current = $this->getRequest()->getParam('controller');
		$action_current = $this->getRequest()->getParam('action');
		if ($userLogined['role_id'] == 1) {
			$check = in_array($controller_current, $userLogined['modules']);
			if ($check == false && $controller_current !== "Home" && $controller_current !== "Users" && $action_current !== "editProfile") {
				return $this->viewBuilder()->setLayout('error');
			}
		} elseif ($userLogined['role_id'] == 2) {
			if (!in_array($controller_current, ['Departments', 'CateDepartments', 'StudentFeels', 'NewsDepartments', 'Departmentspb', 'CateDepartmentspb', 'NewsDepartmentspb', 'Home', 'Users'])) {
				return $this->viewBuilder()->setLayout('error');
			}
			if ($controller_current == "Users" && $action_current != "editProfile") {
				return $this->viewBuilder()->setLayout('error');
			}
			if (!in_array('0', $userLogined['type']) && in_array($controller_current, ['Departments', 'CateDepartments', 'StudentFeels', 'NewsDepartments'])) {
				return $this->viewBuilder()->setLayout('error');
			}
			if (!in_array(1, $userLogined['type']) && in_array($controller_current, ['Departmentspb', 'CateDepartmentspb', 'NewsDepartmentspb'])) {
				return $this->viewBuilder()->setLayout('error');
			}
		}
	}
}
