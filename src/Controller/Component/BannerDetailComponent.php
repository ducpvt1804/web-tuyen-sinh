<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * DetailBanner component
 */
class BannerDetailComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->DetailBanner = TableRegistry::getTableLocator()->get('DetailBanner');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->DetailBanner
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->DetailBanner
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->DetailBanner
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function saveMany($data)
    {
        $entities = $this->DetailBanner->newEntities($data);
        $result = $this->DetailBanner->saveMany($entities);
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $banner = $this->DetailBanner->newEntity();
        $this->assignValue($banner, $req);
        $banner->create_date = date("Y-m-d H:i:s");
        return $this->DetailBanner->save($banner);
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $banner = $this->first(['id' => $id]);
        $this->assignValue($banner, $req);
        $banner->update_date = date("Y-m-d H:i:s");
        return $this->DetailBanner->save($banner);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($banner, $req)
    {
        $banner->title = isset($req['title']) ? $req['title'] : $banner['title'];
        $banner->img = isset($req['img']) ? $req['img'] : $banner['img'];
        $banner->banner_id = isset($req['banner_id']) ? $req['banner_id'] : $banner['banner_id'];
    }

    public function delete($id = null)
    {
        $banner = $this->DetailBanner->get($id);
        $banner->update_date = date("Y-m-d H:i:s");
        $banner->flg_delete = 1;
        return $this->DetailBanner->save($banner);
    }
}
