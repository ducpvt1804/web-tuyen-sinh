<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * NewsDepartment component
 */
class NewsDepartmentpbComponent extends Component
{

    public $components = ['Function'];
    protected $_TYPE;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->News = TableRegistry::getTableLocator()->get('News');
        $this->_TYPE = 2;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions)
    {
        $conditions = array_merge($conditions, ['News.type' => $this->_TYPE, 'News.flg_delete' => 0]);
        $data = $this->News
            ->find()
            ->select([
                'News.id',
                'News.seo_id',
                'News.title',
                'News.slug',
                'News.cate_id',
                'News.img_highlight',
                'News.cate_depart_id',
                'News.schedule',
                'News.short_description',
                'News.content',
                'News.date_edit',
                'News.view',
                'News.flg_language',
                'News.flg_status',
                'cate_title' => 'CateDepartments.title'
            ])
            ->join([
                'CateDepartments' => [
                    'table' => 'cate_departments',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'News.cate_depart_id = CateDepartments.id',
                        1 => 'CateDepartments.flg_delete = 0',
                    ],
                ],
            ])
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->News
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getDataById($id)
    {
        $data = $this->News
            ->find()
            ->select([
                'News.id',
                'News.seo_id',
                'News.title',
                'News.slug',
                'News.cate_id',
                'News.img_highlight',
                'News.cate_depart_id',
                'News.schedule',
                'News.short_description',
                'News.content',
                'News.date_edit',
                'News.view',
                'News.flg_language',
                'News.flg_status',
                'Seo.meta_title',
                'Seo.meta_description',
                'Seo.meta_keywords',
                'Seo.tags',
            ])
            ->join([
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'News.seo_id = Seo.id',
                        1 => 'Seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['News.id' => $id, 'News.flg_delete' => 0])
            ->first()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [], $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'type' => $this->_TYPE]);
        $data = $this->News
            ->find();
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $news = $this->News->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'news');
        if ($uploadImg != "000") {
            $news->img_highlight = $uploadImg;
            $this->assignValue($news, $req);
            $news->create_date = date("Y-m-d H:i:s");
            $news->type = $this->_TYPE;
            return $this->News->save($news);
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $news = $this->News->get($id);
        $this->assignValue($news, $req);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'news');
            if ($uploadImg != "000") {
                $imgOld = $news->img_highlight;
                $news->img_highlight = $uploadImg;
                $this->assignValue($news, $req);
                $result = $this->News->save($news);
                if ($result && file_exists(WWW_ROOT . "admin/upload/news/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/news/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $news->update_date = date("Y-m-d H:i:s");
        return $this->News->save($news);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $news = $this->News->get($id);
        $news->update_date = date("Y-m-d H:i:s");
        $news->flg_delete = 1;
        return $this->News->save($news);
    }

    /*--------------------------------------------------------------------*/
    public function changeStatus($id = null, $status)
    {
        $news = $this->News->get($id);
        if ($status == 0) {
            $news->flg_status = 1;
        } else {
            $news->flg_status = 0;
        }
        $news->update_date = date("Y-m-d H:i:s");
        if ($this->News->save($news)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($news, $req)
    {
        $news->user_id = isset($req['user_id']) ? $req['user_id'] : $news['user_id'];
        $news->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $news['seo_id'];
        $news->cate_depart_id = isset($req['cate_depart_id']) ? $req['cate_depart_id'] : $news['cate_depart_id'];
        $news->cate_id = NULL;
        $news->title = isset($req['title']) ? $req['title'] : $news['title'];
        $news->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $news['slug'];
        $news->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $news['flg_status'];
        $news->schedule = isset($req['schedule']) ? date('Y-m-d H:i:s', strtotime($req['schedule'])) : $news['schedule'];
        $news->date_edit = isset($req['date_edit']) ? date('Y-m-d H:i:s', strtotime($req['date_edit'])) : $news['date_edit'];
        $news->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $news['flg_language'];
        $news->short_description = isset($req['short_description']) ? $req['short_description'] : $news['short_description'];
        $news->view = isset($req['view']) ? $req['view'] : $news['view'];
        $news->content = isset($req['content']) ? htmlentities($req['content']) : $news['content'];
        $news->type = $this->_TYPE;
    }
}
