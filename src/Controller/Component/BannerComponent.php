<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use AppConst;

/**
 * Banner component
 */
class BannerComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Banner = TableRegistry::getTableLocator()->get('Banner');
        $this->siteType = AppConst::SITE_TYPE;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Banner
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Banner
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Banner
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $check = $this->first(['flg_language' => $req['flg_language'], 'mode' => $req['mode'], 'flg_status' => 1]);
        if (empty($check) || $req['flg_status'] == 0) {
            $banner = $this->Banner->newEntity();
            $this->assignValue($banner, $req);
            $banner->create_date = date("Y-m-d H:i:s");
            return $this->Banner->save($banner);
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $banner = $this->first(['id' => $id]);
        $flg_language = isset($req['flg_language']) ? $req['flg_language'] : $banner['flg_language'];
        $mode = isset($req['mode']) ? $req['mode'] : $banner['mode'];
        $check = $this->first(['id <>' => $id, 'flg_language' => $flg_language, 'mode' => $mode, 'flg_status' => 1]);
        if (empty($check) || $req['flg_status'] == 0) {
            $this->assignValue($banner, $req);
            $banner->update_date = date("Y-m-d H:i:s");
            return $this->Banner->save($banner);
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $banner = $this->Banner->get($id);
        $banner->update_date = date("Y-m-d H:i:s");
        $banner->flg_delete = 1;
        return $this->Banner->save($banner);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($banner, $req)
    {
        $banner->title = isset($req['title']) ? $req['title'] : $banner['title'];
        $banner->mode = isset($req['mode']) ? $req['mode'] : $banner['mode'];
        $banner->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $banner['flg_status'];
        $banner->flg_site = $this->siteType;
        $banner->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $banner['flg_language'];
    }

    /*--------------------------------------------------------------------*/
    public function uploadImg($req, $folder)
    {
        $target_dir = "admin/upload/" . $folder . "/";
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($req['name'], PATHINFO_EXTENSION));
        $file_name = $this->Function->generateName() . "." . $imageFileType;
        $target_file = $target_dir . $file_name;
        if (isset($_POST["submit"])) {
            $check = getimagesize($req["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
        if (file_exists($target_file)) {
            $uploadOk = 0;
        }
        // if ($req["size"] > 500000) {
        //     $uploadOk = 0;
        // }
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            $uploadOk = 0;
        }
        if ($uploadOk == 1) {
            move_uploaded_file($req["tmp_name"], $target_file);
            return $file_name;
        } else {
            return "000";
        }
    }

    /*--------------------------------------------------------------------*/
    public function removeAllImg()
    {
        $files = scandir(WWW_ROOT . 'admin/upload/banner/ram/');
        foreach ($files as $item) {
            if ($item !== '.' && $item !== '..' && file_exists(WWW_ROOT . 'admin/upload/banner/ram/' . $item)) {
                unlink(WWW_ROOT . 'admin/upload/banner/ram/' . $item);
            }
        }
    }

    /*--------------------------------------------------------------------*/
    public function removeImg($name)
    {
        if (file_exists(WWW_ROOT . 'admin/upload/banner/ram/' . $name)) {
            unlink(WWW_ROOT . 'admin/upload/banner/ram/' . $name);
        }
    }
}
