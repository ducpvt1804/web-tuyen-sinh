<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use AppConst;

/**
 * Users component
 */
class UsersComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $this->siteType = AppConst::SITE_TYPE;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Users
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Users
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => $this->siteType]);
        $data = $this->Users
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $users = $this->Users->newEntity();
        $uploadImg = $this->Function->upload($req['avarta'], 'users');
        if ($uploadImg != "000") {
            $users->avarta = $uploadImg;
            $this->assignValue($users, $req);
            $users->create_date = date("Y-m-d H:i:s");
            return $this->Users->save($users);
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $users = $this->first(['id' => $id]);
        $this->assignValue($users, $req);
        if (!empty($req["avarta"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['avarta'], 'users');
            if ($uploadImg != "000") {
                $imgOld = $users->avarta;
                $users->avarta = $uploadImg;
                $result = $this->Users->save($users);
                if ($result && !empty($imgOld) && file_exists(WWW_ROOT . "admin/upload/Users/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/Users/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $users->update_date = date("Y-m-d H:i:s");
        return $this->Users->save($users);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $users = $this->Users->get($id);
        $users->update_date = date("Y-m-d H:i:s");
        $users->flg_delete = 1;
        return $this->Users->save($users);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($users, $req)
    {
        $users->user_name = isset($req['user_name']) ? $req['user_name'] : $users['user_name'];
        $users->email = isset($req['email']) ? $req['email'] : $users['email'];
        $users->password = (isset($req['password']) && !empty($req['password'])) ? md5(trim($req['password'])) : $users['password'];
        $users->fullname = isset($req['fullname']) ? $req['fullname'] : $users['fullname'];
        $users->telno = isset($req['telno']) ? $req['telno'] : $users['telno'];
        $users->sex = isset($req['sex']) ? $req['sex'] : $users['sex'];
        $users->flg_site = $this->siteType;
        $users->address = isset($req['address']) ? $req['address'] : $users['address'];
        $users->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $users['flg_status'];
        $users->birth_date = isset($req['birth_date']) ? date('Y-m-d H:i:s', strtotime($req['birth_date'])) : $users['birth_date'];
        $users->remindpw_key = isset($req['remindpw_key']) ? $req['remindpw_key'] : $users['remindpw_key'];
        $users->time_out = isset($req['time_out']) ? $req['time_out'] : $users['time_out'];
    }

    /*--------------------------------------------------------------------*/
    public function validation($req)
    {
        $validation = $this->Users->newEntity($req, ['validate' => 'Users']);
        return $validation->getErrors();
    }

    /*--------------------------------------------------------------------*/
    public function validationAdd($req)
    {
        $validation = $this->Users->newEntity($req, ['validate' => 'AddUsers']);
        return $validation->getErrors();
    }
}
