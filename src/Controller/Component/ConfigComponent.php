<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use AppConst;

/**
 * Events component
 */
class ConfigComponent extends Component
{
    public $components = array('Function');
    /**
     * Default configuration.
     *
     * @var array
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Config = TableRegistry::getTableLocator()->get('Configs');
        $this->Seo = TableRegistry::getTableLocator()->get('Seo');
    }

    public function all()
    {
        $data = $this->Config
            ->find('all')
            ->where(['flg_delete' => 0, 'flg_site' => AppConst::SITE_TYPE]);
        return $data;
    }

    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => AppConst::SITE_TYPE]);
        $data = $this->Config
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => AppConst::SITE_TYPE]);
        $data = $this->Config
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    public function getDataConfig($id)
    {
        $data = $this->Config
            ->find('all')
            ->select($this->Config)
            ->select([
                'meta_title' => 'seo.meta_title',
                'meta_description' => 'seo.meta_description',
                'meta_keywords' => 'seo.meta_keywords',
            ])
            ->join([
                'seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'Configs.seo_id = seo.id',
                        1 => 'seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Configs.id' => $id, 'Configs.flg_delete' => 0, 'Configs.flg_site' => AppConst::SITE_TYPE])
            ->first();
        return $data;
    }

    public function edit($req, $id = null)
    {
        if (!empty($req['logo_header']['tmp_name'])) {
            $file_header = $this->Function->upload($req['logo_header'], "logo");
        }
        if (!empty($req['logo_footer']['tmp_name'])) {
            $file_footer = $this->Function->upload($req['logo_footer'], "logo");
        }
        $result = true;
        $config = $this->Config->get($id);
        if (!empty($config['seo_id'])) {
            $seo = $this->Seo->get($config['seo_id']);
            $seo->update_date = date('Y-m-d H:i:s');
        } else {
            $seo = $this->Seo->newEntity();
            $seo->create_date = date('Y-m-d H:i:s');
        }
        $seo->meta_title = !empty($req['meta_title']) ? $req['meta_title'] : NULL;
        $seo->meta_description = !empty($req['meta_description']) ? $req['meta_description'] : NULL;
        $seo->meta_keywords = !empty($req['meta_keywords']) ? $req['meta_keywords'] : NULL;

        $saveSeo = $this->Seo->save($seo);
        if ($saveSeo) {
            $req['seo_id'] = $saveSeo->id;
            $req['update_date'] = date('Y-m-d H:i:s');
            $config = $this->Config->patchEntity($config, $req);
            $config->logo_header = isset($file_header) ? $file_header : $config['logo_header'];
            $config->logo_footer = isset($file_footer) ? $file_footer : $config['logo_footer'];
            $config->about_title = isset($req['about_title']) ? $req['about_title'] : $config['logo_footer'];
            $config->about_desc = isset($req['about_desc']) ? $req['about_desc'] : $config['about_desc'];
            $config->establish = isset($req['establish']) ? $req['establish'] : $config['establish'];
            $config->total_student = isset($req['total_student']) ? $req['total_student'] : $config['total_student'];
            $config->percent_have_job = isset($req['percent_have_job']) ? $req['percent_have_job'] : $config['percent_have_job'];
            $config->user_id = isset($req['user_id']) ? $req['user_id'] : $config['user_id'];
            $this->Config->save($config);
        }
        if (!$saveSeo || !$this->Config->save($config)) {
            $result = false;
            if (file_exists(WWW_ROOT . "admin/upload/logo/" . $file_header)) {
                unlink(WWW_ROOT . "admin/upload/logo/" . $file_header);
            }
            if (file_exists(WWW_ROOT . "admin/upload/logo/" . $file_footer)) {
                unlink(WWW_ROOT . "admin/upload/logo/" . $file_footer);
            }
        }
        return $result;
    }
}
