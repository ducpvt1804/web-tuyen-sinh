<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Mailer\Email;

class MailComponent extends Component
{

    public function sendMail($toEmail, $subject, $viewVar, $template)
    {
        $mail = new Email('default');
        $mail->setFrom('hoanghung888@gmail.com')
            ->setTo($toEmail)
            ->setSubject($subject)
            ->setEmailFormat('html')
            ->setViewVars($viewVar)
            ->viewBuilder()
            ->setTemplate($template);
        return $mail->send();
    }
}
