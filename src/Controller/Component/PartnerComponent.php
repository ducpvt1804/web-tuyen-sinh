<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class PartnerComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Partners = TableRegistry::getTableLocator()->get('Partners');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Partners
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Partners
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Partners
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $partner = $this->Partners->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'partner');
        if ($uploadImg != "000") {
            $partner->img = $uploadImg;
            $this->assignValue($partner, $req);
            $partner->create_date = date("Y-m-d H:i:s");
            if ($this->Partners->save($partner)) {
                return true;
            } else {
                if (file_exists(WWW_ROOT . "admin/upload/partner/" . $uploadImg)) {
                    unlink(WWW_ROOT . "admin/upload/partner/" . $uploadImg);
                }
                return false;
            }
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $partner = $this->Partners->get($id);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'partner');
            if ($uploadImg != "000") {
                $imgOld = $partner->img;
                $this->assignValue($partner, $req);
                $partner->img = $uploadImg;
                $result = $this->Partners->save($partner);
                if ($result && file_exists(WWW_ROOT . "admin/upload/partner/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/partner/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $this->assignValue($partner, $req);
        $partner->update_date = date("Y-m-d H:i:s");
        return $this->Partners->save($partner);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $partner = $this->Partners->get($id);
        $partner->update_date = date("Y-m-d H:i:s");
        $partner->flg_delete = 1;
        return $this->Partners->save($partner);
    }

    /*--------------------------------------------------------------------*/
    public function status($id = null)
    {
        $partner = $this->Partners->get($id);
        $partner->update_date = date("Y-m-d H:i:s");
        if ($partner->flg_status == 0) {
            $partner->flg_status = 1;
        } else {
            $partner->flg_status = 0;
        }
        return $this->Partners->save($partner);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($partner, $req)
    {
        $partner->user_id = isset($req['user_id']) ? $req['user_id'] : $partner['user_id'];
        $partner->title = isset($req['title']) ? $req['title'] : $partner['title'];
        $partner->description = isset($req['description']) ? $req['description'] : $partner['description'];
        $partner->link = isset($req['link']) ? $req['link'] : $partner['link'];
        $partner->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $partner['flg_status'];
    }
}
