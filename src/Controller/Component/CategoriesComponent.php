<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Categories component
 */
class CategoriesComponent extends Component
{

    public $components = ['Function', 'Department', 'Departmentpb'];
    private $cateHome;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Categories = TableRegistry::getTableLocator()->get('Categories');
        $this->cateHome = Configure::read('HOME');
    }

    /*--------------------------------------------------------------------*/
    public function all()
    {
        $data = $this->Categories
            ->find('all')
            ->where(['flg_delete' => 0])
            ->order(['order_num' => 'ASC'])
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Categories
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getDataById($id)
    {
        $data = $this->Categories
            ->find()
            ->select([
                'Categories.id',
                'Categories.seo_id',
                'Categories.title',
                'Categories.slug',
                'Categories.parent_id',
                'Categories.url',
                'Categories.order_num',
                'Categories.type',
                'Categories.flg_site',
                'Categories.flg_language',
                'Categories.flg_show',
                'Seo.meta_title',
                'Seo.meta_description',
                'Seo.meta_keywords',
            ])
            ->join([
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'Categories.seo_id = Seo.id',
                        1 => 'Seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Categories.id' => $id, 'Categories.flg_delete' => 0, 'Categories.flg_site' => AppConst::SITE_CONST])
            ->first()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions, $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Categories
            ->find();
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $categories = $this->Categories->newEntity();
        $this->assignValue($categories, $req);
        $categories->create_date = date("Y-m-d H:i:s");
        if ($this->Categories->save($categories)) {
            return $categories;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $categories = $this->Categories->get($id);
        $this->assignValue($categories, $req);
        $categories->update_date = date("Y-m-d H:i:s");
        if ($this->Categories->save($categories)) {
            return $categories;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $categories = $this->Categories->get($id);
        $categories->update_date = date("Y-m-d H:i:s");
        $categories->flg_delete = 1;
        if ($this->Categories->save($categories)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function changeStatus($id = null, $status)
    {
        $categories = $this->Categories->get($id);
        if ($status == 0) {
            $categories->flg_show = 1;
        } else {
            $categories->flg_show = 0;
        }
        $categories->update_date = date("Y-m-d H:i:s");
        if ($this->Categories->save($categories)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($categories, $req)
    {
        $categories->user_id = isset($req['user_id']) ? $req['user_id'] : $categories['user_id'];
        $categories->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $categories['seo_id'];
        $categories->title = isset($req['title']) ? $req['title'] : $categories['title'];
        $categories->url = isset($req['url']) ? $req['url'] : $categories['url'];
        $categories->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $categories['slug'];
        $categories->parent_id = isset($req['parent_id']) ? $req['parent_id'] : $categories['parent_id'];
        $categories->type = isset($req['type']) ? (int) $req['type'] : $categories['type'];
        $categories->order_num = isset($req['order_num']) ? $req['order_num'] : $categories['order_num'];
        $categories->flg_site = AppConst::SITE_CONST;
        $categories->flg_language = isset($req['flg_language']) ? (int) $req['flg_language'] : $categories['flg_language'];
        $categories->flg_show = isset($req['flg_show']) ? (int) $req['flg_show'] : $categories['flg_show'];
    }

    /*--------------------------------------------------------------------*/
    public function getMenus($lang)
    {
        $conditionsMenu = [
            'parent_id is' => null,
            'type' => 0,
            'flg_show' => 0,
            'flg_language' => $lang,
            'flg_site' => AppConst::SITE_CONST
        ];
        $slugMenu = [
            'header_top' => $lang === 0 ? $this->cateHome['VI']['HEADER_TOP'] : $this->cateHome['EN']['HEADER_TOP'],
            'intro' => $lang === 0 ? $this->cateHome['VI']['INTRO'] : $this->cateHome['EN']['INTRO'],
            'department' => $lang === 0 ? $this->cateHome['VI']['DEPARTMENT'] : $this->cateHome['EN']['DEPARTMENT'],
            'training' => $lang === 0 ? $this->cateHome['VI']['TRAINING'] : $this->cateHome['EN']['TRAINING'],
            'khcn_htdt' => $lang === 0 ? $this->cateHome['VI']['KHCN_HTĐT'] : $this->cateHome['EN']['KHCN_HTĐT'],
            'student' => $lang === 0 ? $this->cateHome['VI']['STUDENT'] : $this->cateHome['EN']['STUDENT'],
            'three_public' => $lang === 0 ? $this->cateHome['VI']['THREE_PUBLIC'] : $this->cateHome['EN']['THREE_PUBLIC'],
            'contact' => $lang === 0 ? $this->cateHome['VI']['CONTACT'] : $this->cateHome['EN']['CONTACT'],
            'extend' => $lang === 0 ? $this->cateHome['VI']['EXTEND'] : $this->cateHome['EN']['EXTEND']
        ];
        $menus = [
            'header_top' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['header_top']])), $slugMenu),
            'intro' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['intro']])), $slugMenu),
            'department' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['department']])), $slugMenu),
            'training' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['training']])), $slugMenu),
            'khcn_htdt' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['khcn_htdt']])), $slugMenu),
            'student' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['student']])), $slugMenu),
            'three_public' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['three_public']])), $slugMenu),
            'contact' => $this->getSubMenus($this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['contact']])), $slugMenu),
            'extend' => $this->getSubMenus(['slug' => $slugMenu['extend'], 'title' => $lang == 1 ? 'Extend' : 'Mở Rộng', 'flg_language' => $lang], $slugMenu)
        ];
        return $menus;
    }

    /*--------------------------------------------------------------------*/
    public function getSubMenus($menu, $slugMenu)
    {
        if (isset($menu['slug'])) {
            $infoMenu = [
                'slug' => $menu['slug'],
                'title' => $menu['title'],
                'sub_menu' => []
            ];
            $conditionsMenu = [
                'type' => 0,
                'flg_show' => 0,
                'flg_language' => $menu['flg_language'],
                'flg_site' => AppConst::SITE_CONST
            ];

            if ($menu['slug'] == $slugMenu['extend']) {
                $conditionsMenu = array_merge($conditionsMenu, [
                    'parent_id is' => null,
                    'slug not in' => [
                        $slugMenu['intro'],
                        $slugMenu['department'],
                        $slugMenu['training'],
                        $slugMenu['khcn_htdt'],
                        $slugMenu['student'],
                        $slugMenu['three_public'],
                        $slugMenu['contact'],
                        $slugMenu['header_top']
                    ]
                ]);
            } else {
                $conditionsMenu = array_merge($conditionsMenu, ['parent_id' => $menu['id']]);
            }

            $subMenu = [];

            if ($menu['slug'] == $slugMenu['department']) {
                //Get list department
                $department = $this->Department->all([
                    'Departments.flg_language' => $menu['flg_language'],
                    'ListDepartments.flg_language' => $menu['flg_language'],
                ])->toArray();
                $departmentPB = $this->Departmentpb->all([
                    'Departments.flg_language' => $menu['flg_language'],
                    'ListDepartments.flg_language' => $menu['flg_language']
                ])->toArray();
                $subMenuDepartment = array_merge($department, $departmentPB);
                foreach ($subMenuDepartment as $item) {
                    $_subMenu = [
                        'slug' => $item['ListDepartments']['slug'],
                        'title' => $item['shot_title']
                    ];
                    array_push($infoMenu['sub_menu'], $_subMenu);
                }
            } else {
                $subMenu = $this->allSubMenu($conditionsMenu);
            }

            if (is_array($subMenu) && count($subMenu) > 0) {
                foreach ($subMenu as $item) {
                    $_subMenu = $this->getSubMenus($item, $slugMenu);
                    array_push($infoMenu['sub_menu'], $_subMenu);
                }
            }
            return $infoMenu;
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function allSubMenu($conditions =  [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Categories
            ->find('all')
            ->where($conditions)
            ->order(['order_num' => 'ASC'])
            ->toArray();
        return $data;
    }
}
