<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class ListDepartmentComponent extends Component
{
    public $components = array('Function', 'Flash');
    protected $_TYPE;

    /**
     * Default configuration.
     *
     * @var array
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $this->Departments = TableRegistry::getTableLocator()->get('Departments');
        $this->_TYPE = 0;
    }

    /*--------------------------------------------------------------------*/
    public function getData($slug, $lang)
    {
        $data = $this->ListDepartments
            ->find('all')
            ->select($this->ListDepartments)
            ->select($this->Departments)
            ->join([
                'Departments' => [
                    'table' => 'departments',
                    'type' => 'LEFT',
                    'conditions' => 'Departments.parent_key = ListDepartments.id and Departments.flg_delete = 0 and ListDepartments.flg_delete = 0'
                ]
            ])
            ->where([
                'ListDepartments.type' => $this->_TYPE, 
                'ListDepartments.slug' => $slug, 
                'ListDepartments.flg_language' => $lang,
                'Departments.type' => $this->_TYPE,
                'Departments.flg_language' => $lang
            ])
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'type' => $this->_TYPE]);
        $data = $this->ListDepartments
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'type' => $this->_TYPE]);
        $data = $this->ListDepartments
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $lstDepart = $this->ListDepartments->newEntity();
        $this->assignValue($lstDepart, $req);
        $lstDepart->create_date = date("Y-m-d H:i:s");
        return $this->ListDepartments->save($lstDepart);
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $lstDepart = $this->ListDepartments->get($id);
        $this->assignValue($lstDepart, $req);
        $lstDepart = $this->ListDepartments->patchEntity($lstDepart, $req);
        $lstDepart->update_date = date("Y-m-d H:i:s");

        return $this->ListDepartments->save($lstDepart);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $lstDepart = $this->ListDepartments->get($id);
        $lstDepart->update_date = date("Y-m-d H:i:s");
        $lstDepart->flg_delete = 1;
        return $this->ListDepartments->save($lstDepart);
    }

    /*--------------------------------------------------------------------*/
    public function swstatus($id = null)
    {
        $lstDepart = $this->ListDepartments->get($id);
        $lstDepart->update_date = date("Y-m-d H:i:s");
        if($lstDepart->flg_status == 0){
            $lstDepart->flg_status = 1;
        } else {
            $lstDepart->flg_status = 0;
        }
        return $this->ListDepartments->save($lstDepart);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($lstDepart, $req)
    {
        $lstDepart->flg_status = isset($req['flg_status']) ? (int)$req['flg_status'] : $lstDepart['flg_status'];
        $lstDepart->flg_language = isset($req['flg_language']) ? (int)$req['flg_language'] : $lstDepart['flg_language'];
        $lstDepart->title = isset($req['title']) ? $req['title'] : $lstDepart['title'];
        $lstDepart->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $lstDepart['slug'];
        $lstDepart->type = 0;
        $lstDepart->user_id = isset($req['user_id']) ? $req['user_id'] : $lstDepart['user_id'];
    }
}
