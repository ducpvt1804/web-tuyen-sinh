<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class StudentFeelComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->StudentComments = TableRegistry::getTableLocator()->get('StudentComments');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->StudentComments
            ->find('all')
            ->where($conditions)
            ->order(['create_date' => 'DESC']);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->StudentComments
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->StudentComments
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $studentfeel = $this->StudentComments->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'student_feel');
        if ($uploadImg != "000") {
            $studentfeel->img_highlight = $uploadImg;
            $this->assignValue($studentfeel, $req);
            $studentfeel->create_date = date("Y-m-d H:i:s");
            if($this->StudentComments->save($studentfeel)){
                return true;
            } else {
                if (file_exists(WWW_ROOT . "admin/upload/student_feel/" . $uploadImg)) {
                    unlink(WWW_ROOT . "admin/upload/student_feel/" . $uploadImg);
                }
                return false;
            }
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $studentfeel = $this->StudentComments->get($id);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'student_feel');
            if ($uploadImg != "000") {
                $imgOld = $studentfeel->img_highlight;
                $this->assignValue($studentfeel, $req);
                $studentfeel->img_highlight = $uploadImg;
                $result = $this->StudentComments->save($studentfeel);
                if ($result && file_exists(WWW_ROOT . "admin/upload/student_feel/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/student_feel/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $this->assignValue($studentfeel, $req);
        $studentfeel->update_date = date("Y-m-d H:i:s");
        return $this->StudentComments->save($studentfeel);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $studentfeel = $this->StudentComments->get($id);
        $studentfeel->update_date = date("Y-m-d H:i:s");
        $studentfeel->flg_delete = 1;
        return $this->StudentComments->save($studentfeel);
    }

    /*--------------------------------------------------------------------*/
    public function status($id = null)
    {
        $studentfeel = $this->StudentComments->get($id);
        $studentfeel->update_date = date("Y-m-d H:i:s");
        if($studentfeel->flg_status == 0){
            $studentfeel->flg_status = 1;
        } else {
            $studentfeel->flg_status = 0;
        }
        return $this->StudentComments->save($studentfeel);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($studentfeel, $req)
    {
        $studentfeel->user_id = isset($req['user_id']) ? $req['user_id'] : $studentfeel['user_id'];
        $studentfeel->department_id = isset($req['department_id']) ? $req['department_id'] : $studentfeel['department_id'];
        $studentfeel->fullname = isset($req['fullname']) ? $req['fullname'] : $studentfeel['fullname'];
        $studentfeel->class = isset($req['class']) ? $req['class'] : $studentfeel['class'];
        $studentfeel->content = isset($req['content']) ? $req['content'] : $studentfeel['content'];
        $studentfeel->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $studentfeel['flg_language'];
        $studentfeel->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $studentfeel['flg_status'];
    }
}
