<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class DepartmentpbComponent extends Component
{
    public $components = array('Function');
    protected $_TYPE;
    /**
     * Default configuration.
     *
     * @var array
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Departments = TableRegistry::getTableLocator()->get('Departments');
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $this->Seo = TableRegistry::getTableLocator()->get('Seo');
        $this->_TYPE = 1;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['Departments.flg_delete' => 0, 'Departments.type' => $this->_TYPE]);
        $data = $this->Departments
            ->find('all')
            ->select($this->Departments)
            ->select($this->ListDepartments)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    //                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'ListDepartments.id = Departments.parent_key',
                        1 => 'ListDepartments.flg_delete = 0',
                        2 => 'ListDepartments.flg_status = 0',
                    ],
                ],
            ])
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'type' => $this->_TYPE,]);
        $data = $this->Departments
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $result = true;
        $department = $this->Departments->newEntity();
        $seo = $this->Seo->newEntity();
        if ($req['banner']) {
            $banner = $this->Function->upload($req['banner'], "departments");
        }
        if ($req['img_highlight']) {
            $img_highlight = $this->Function->upload($req['img_highlight'], "departments");
        }
        if ($banner != "000" && $img_highlight != "000") {
            $seo->create_date = date('Y-m-d H:i:s');
            $seo->meta_title = !empty($req['meta_title']) ? $req['meta_title'] : NULL;
            $seo->meta_description = !empty($req['meta_description']) ? $req['meta_description'] : NULL;
            $seo->meta_keywords = !empty($req['meta_keywords']) ? $req['meta_keywords'] : NULL;
            $saveSeo = $this->Seo->save($seo);
            if ($saveSeo) {
                $department->user_id = $req['user_id'];
                $department->banner = $banner;
                $department->img_highlight = $img_highlight;
                $department->seo_id = $saveSeo->id;
                $department->parent_key = $req['parent_key'];
                $department->flg_language = (int) $req['flg_language'];
                $department->title = $req['title'];
                $department->shot_title = $req['shot_title'];
                $department->description = $req['description'];
                $department->type = $this->_TYPE;
                $department->create_date = date("Y-m-d H:i:s");
                if (!$this->Departments->save($department)) {
                    if (file_exists(WWW_ROOT . "admin/upload/departments/" . $banner)) {
                        unlink(WWW_ROOT . "admin/upload/departments/" . $banner);
                    }
                    if (file_exists(WWW_ROOT . "admin/upload/departments/" . $img_highlight)) {
                        unlink(WWW_ROOT . "admin/upload/departments/" . $img_highlight);
                    }
                    $result = false;
                }
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
        return $result;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        if ($req['banner']['tmp_name']) {
            $banner = $this->Function->upload($req['banner'], "departments");
        }
        if ($req['img_highlight']['tmp_name']) {
            $img_highlight = $this->Function->upload($req['img_highlight'], "departments");
        }
        $result = true;
        $departments = $this->Departments->get($id);
        if (!empty($departments['seo_id'])) {
            $seo = $this->Seo->get($departments['seo_id']);
            $seo->update_date = date('Y-m-d H:i:s');
        } else {
            $seo = $this->Seo->newEntity();
            $seo->create_date = date('Y-m-d H:i:s');
        }
        $seo->meta_title = !empty($req['meta_title']) ? $req['meta_title'] : NULL;
        $seo->meta_description = !empty($req['meta_description']) ? $req['meta_description'] : NULL;
        $seo->meta_keywords = !empty($req['meta_keywords']) ? $req['meta_keywords'] : NULL;

        $saveSeo = $this->Seo->save($seo);
        if ($saveSeo) {
            $department = $this->Departments->patchEntity($departments, $req);

            $department->user_id = $req['user_id'];
            $department->banner = isset($banner) ? $banner : $department['banner'];
            $department->img_highlight = isset($img_highlight) ? $img_highlight : $department['img_highlight'];
            $department->seo_id = $saveSeo->id;
            $department->parent_key = $req['parent_key'];
            $department->flg_language = (int) $req['flg_language'];
            $department->title = $req['title'];
            $department->shot_title = $req['shot_title'];
            $department->description = $req['description'];
            $department->type = $this->_TYPE;
            $department->create_date = date("Y-m-d H:i:s");
        }
        if (!$saveSeo || !$this->Departments->save($department)) {
            $result = false;
            if (file_exists(WWW_ROOT . "admin/upload/departments/" . $banner)) {
                unlink(WWW_ROOT . "admin/upload/departments/" . $banner);
            }
            if (file_exists(WWW_ROOT . "admin/upload/departments/" . $img_highlight)) {
                unlink(WWW_ROOT . "admin/upload/departments/" . $img_highlight);
            }
        }
        return $result;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $depart = $this->Departments->get($id);
        $depart->update_date = date("Y-m-d H:i:s");
        $depart->flg_delete = 1;
        return $this->Departments->save($depart);
    }

    /*--------------------------------------------------------------------*/
    public function getListDepartment($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'type' => $this->_TYPE, 'flg_status' => 0]);
        $data = $this->ListDepartments
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function checkParentDepartment($parentId)
    {
        $data = $this->Departments
            ->find('all')
            ->select(['count' => 'count(id)'])
            ->where(['flg_delete' => 0, 'type' => $this->_TYPE, 'parent_key' => $parentId])
            ->group('parent_key')
            ->first();
        return $data;
    }

    public function checkExistLangAdd($flg_lang, $parentId)
    {
        $data = $this->Departments
            ->find('all')
            ->select(['flg_language'])
            ->where(['flg_delete' => 0, 'type' => $this->_TYPE, 'flg_language' => $flg_lang, 'parent_key' => $parentId])
            ->first();
        return $data;
    }

    public function checkExistLang($flg_lang, $parentId, $id)
    {
        $data = $this->Departments
            ->find('all')
            ->select(['flg_language'])
            ->where(['flg_delete' => 0, 'type' => $this->_TYPE, 'flg_language' => $flg_lang, 'parent_key' => $parentId, 'id' => $id])
            ->first();
        return $data;
    }
}
