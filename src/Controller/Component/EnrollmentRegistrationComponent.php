<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class EnrollmentRegistrationComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->EnrollmentRegistration = TableRegistry::getTableLocator()->get('EnrollmentRegistrations');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentRegistration
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function allMess($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_read' => 0]);
        $data = $this->EnrollmentRegistration
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentRegistration
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    public function notification(){
        $data = $this->EnrollmentRegistration
            ->find('all')
            ->select(['notification' => 'count(flg_read)', 'id'])
            ->where(['flg_delete' => 0, 'flg_read' => 0])
            ->group(['flg_read'])
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentRegistration
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $enroRegis = $this->EnrollmentRegistration->newEntity();
        $this->assignValue($enroRegis, $req);
        $enroRegis->create_date = date("Y-m-d H:i:s");
        if($this->EnrollmentRegistration->save($enroRegis)){
            return true;
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $enroRegis = $this->EnrollmentRegistration->get($id);
        $enroRegis->update_date = date("Y-m-d H:i:s");
        $enroRegis->flg_delete = 1;
        return $this->EnrollmentRegistration->save($enroRegis);
    }

    /*--------------------------------------------------------------------*/
    public function read($id = null)
    {
        $enroRegis = $this->EnrollmentRegistration->get($id);
        $enroRegis->update_date = date("Y-m-d H:i:s");
        $enroRegis->flg_read = 1;
        return $this->EnrollmentRegistration->save($enroRegis);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($enroRegis, $req)
    {
        $enroRegis->name = isset($req['name']) ? $req['name'] : $enroRegis['name'];
        $enroRegis->email = isset($req['email']) ? $req['email'] : $enroRegis['email'];
        $enroRegis->title = isset($req['title']) ? $req['title'] : $enroRegis['title'];
        $enroRegis->content = isset($req['content']) ? $req['content'] : $enroRegis['content'];
        $enroRegis->flg_site = 0;
        $enroRegis->flg_read = isset($req['flg_read']) ? $req['flg_read'] : $enroRegis['flg_read'];
        $enroRegis->flg_reply = isset($req['flg_reply']) ? $req['flg_reply'] : $enroRegis['flg_reply'];
    }
}
