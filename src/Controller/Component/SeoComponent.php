<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class SeoComponent extends Component
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Seo = TableRegistry::getTableLocator()->get('Seo');
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Seo
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Seo
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $seo = $this->Seo->newEntity();
        $this->assignValue($seo, $req);
        $seo->create_date = date("Y-m-d H:i:s");
        if ($this->Seo->save($seo)) {
            return $seo;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $seo = $this->Seo->get($id);
        $this->assignValue($seo, $req);
        $seo->update_date = date("Y-m-d H:i:s");
        if ($this->Seo->save($seo)) {
            return $seo;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $seo = $this->Seo->get($id);
        $seo->update_date = date("Y-m-d H:i:s");
        $seo->flg_delete = 1;
        if ($this->Seo->save($seo)) {
            return $seo;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($seo, $req)
    {
        $seo->meta_title = isset($req['meta_title']) ? $req['meta_title'] : $seo['meta_title'];
        $seo->meta_description = isset($req['meta_description']) ? $req['meta_description'] : $seo['meta_description'];
        $seo->meta_keywords = isset($req['meta_keywords']) ? $req['meta_keywords'] : $seo['meta_keywords'];
        $seo->tags = isset($req['tags']) ? $req['tags'] : $seo['tags'];
    }

    /*--------------------------------------------------------------------*/
    public function validationSeoVsEvent($req)
    {
        $validation = $this->Seo->newEntity($req, ['validate' => 'EventVsSeo']);
        return $validation->getErrors();
    }
    /*--------------------------------------------------------------------*/
    public function validationAddSeoVsEvent($req)
    {
        $validation = $this->Seo->newEntity($req, ['validate' => 'AddEventVsSeo']);
        return $validation->getErrors();
    }
}
