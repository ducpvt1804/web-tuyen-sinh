<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * LibrariesMedia component
 */
class LibrariesMediaComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->LibrariesMedia = TableRegistry::getTableLocator()->get('LibrariesMedia');
    }

    /*--------------------------------------------------------------------*/
    public function all()
    {
        $data = $this->LibrariesMedia
            ->find('all')
            ->where(['flg_delete' => 0])
            ->all()
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->LibrariesMedia
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions, $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->LibrariesMedia
            ->find();
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function add($req, $id, $type)
    {
        $libraryMedia = $this->LibrariesMedia->newEntity();
        if ($type == 1) {
            $uploadImg = $this->Function->upload($req, 'libraries');        
            if ($uploadImg != "000") {
                $libraryMedia->path = $uploadImg;
                $libraryMedia->library_id = $id;
                $libraryMedia->create_date = date("Y-m-d H:i:s");
                if ($this->LibrariesMedia->save($libraryMedia)) {
                    return $libraryMedia;
                }
            } else {
                return false;
            }
        } else {
            $libraryMedia->path = $req;
            $libraryMedia->library_id = $id;
            $libraryMedia->create_date = date("Y-m-d H:i:s");
            if ($this->LibrariesMedia->save($libraryMedia)) {
                return $libraryMedia;
            }
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $libraryMedia = $this->LibrariesMedia->get($id);
        $this->assignValue($libraryMedia, $req);
        $libraryMedia->update_date = date("Y-m-d H:i:s");
        if ($this->LibrariesMedia->save($libraryMedia)) {
            return $libraryMedia;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($idMedia = null, $id)
    {
        $libraryMedia = $this->LibrariesMedia->get($idMedia);
        $libraryMedia->update_date = date("Y-m-d H:i:s");
        $libraryMedia->flg_delete = 1;
        if ($this->LibrariesMedia->save($libraryMedia)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($libraryMedia, $req)
    {
        $libraryMedia->library_id = isset($req['library_id']) ? $req['library_id'] : $libraryMedia['library_id'];
    }
}
