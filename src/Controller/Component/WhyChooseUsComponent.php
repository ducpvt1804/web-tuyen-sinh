<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class WhyChooseUsComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->ChooseUs = TableRegistry::getTableLocator()->get('ChooseUs');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->ChooseUs
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getAllChooseUs($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->ChooseUs
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->ChooseUs
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->ChooseUs
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $chooseus = $this->ChooseUs->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'whychooseus');
        if ($uploadImg != "000") {
            $chooseus->img_highlight = $uploadImg;
            $this->assignValue($chooseus, $req);
            $chooseus->create_date = date("Y-m-d H:i:s");
            if ($this->ChooseUs->save($chooseus)) {
                return true;
            } else {
                if (file_exists(WWW_ROOT . "admin/upload/whychooseus/" . $uploadImg)) {
                    unlink(WWW_ROOT . "admin/upload/whychooseus/" . $uploadImg);
                }
                return false;
            }
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $chooseus = $this->ChooseUs->get($id);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'whychooseus');
            if ($uploadImg != "000") {
                $imgOld = $chooseus->img_highlight;
                $this->assignValue($chooseus, $req);
                $chooseus->img = $uploadImg;
                $result = $this->ChooseUs->save($chooseus);
                if ($result && file_exists(WWW_ROOT . "admin/upload/whychooseus/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/whychooseus/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $this->assignValue($chooseus, $req);
        $chooseus->update_date = date("Y-m-d H:i:s");
        return $this->ChooseUs->save($chooseus);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $chooseus = $this->ChooseUs->get($id);
        $chooseus->update_date = date("Y-m-d H:i:s");
        $chooseus->flg_delete = 1;
        return $this->ChooseUs->save($chooseus);
    }

    /*--------------------------------------------------------------------*/
    public function status($id = null)
    {
        $chooseus = $this->ChooseUs->get($id);
        $chooseus->update_date = date("Y-m-d H:i:s");
        if ($chooseus->flg_status == 0) {
            $chooseus->flg_status = 1;
        } else {
            $chooseus->flg_status = 0;
        }
        return $this->ChooseUs->save($chooseus);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($chooseus, $req)
    {
        $chooseus->user_id = isset($req['user_id']) ? $req['user_id'] : $chooseus['user_id'];
        $chooseus->title = isset($req['title']) ? $req['title'] : $chooseus['title'];
        $chooseus->description = isset($req['description']) ? $req['description'] : $chooseus['description'];
        $chooseus->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $chooseus['flg_language'];
        $chooseus->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $chooseus['flg_status'];
    }
}
