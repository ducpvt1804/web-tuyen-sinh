<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use AppConst;

/**
 * RoleDetail component
 */
class RoleDetailComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->RoleDetail = TableRegistry::getTableLocator()->get('RoleDetail');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->RoleDetail
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->RoleDetail
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->RoleDetail
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $data = [];
        if (isset($req['role_detail']) && in_array($req['role_id'], [1, 2])) {
            foreach ($req['role_detail'] as $item) {
                array_push($data, $this->assignValue($req, $item));
            }
        } else {
            $data = [$this->assignValue($req)];
        }
        if (isset($req['user_id'])) {
            $this->RoleDetail->deleteAll(['user_id' => $req['user_id']]);
        }
        $entities = $this->RoleDetail->newEntities($data);
        $result = $this->RoleDetail->saveMany($entities);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($req, $module_id = "")
    {
        return [
            'role_id' => isset($req['role_id']) ? $req['role_id'] : '',
            'user_id' => isset($req['user_id']) ? $req['user_id'] : '',
            'module_id' => $module_id,
            'create_date' => date("Y-m-d H:i:s"),
            'update_date' => date("Y-m-d H:i:s")
        ];
    }

    /*--------------------------------------------------------------------*/
    public function getRole($user_id)
    {
        $data = $this->RoleDetail
            ->find('all')
            ->select(['RoleDetail.role_id', 'list_departments.id', 'list_departments.type'])
            ->join([
                'list_departments' => [
                    'table' => 'list_departments',
                    'type' => 'LEFT',
                    'conditions' => 'RoleDetail.module_id = list_departments.id and RoleDetail.flg_delete = 0 and list_departments.flg_delete = 0'
                ]
            ])
            ->where([
                'RoleDetail.user_id' => $user_id
            ])
            ->toArray();
        if (count($data) > 0) {
            $role = $data[0]['role_id'];
            $module_id = [];
            $type = [];
            foreach ($data as $item) {
                array_push($module_id, $item['list_departments']['id']);
                array_push($type, $item['list_departments']['type']);
            }
            return ['role_id' => $role, 'module_id' => $module_id, 'type' => $type];
        } else {
            return [];
        }
    }

    /*--------------------------------------------------------------------*/
    public function getModules($user_id)
    {
        $result = $this->RoleDetail
            ->find('all')
            ->select(['RoleDetail.role_id', 'modules.id', 'modules.controller_name'])
            ->join([
                'modules' => [
                    'table' => 'modules',
                    'type' => 'INNER',
                    'conditions' => 'RoleDetail.module_id = modules.id and RoleDetail.flg_delete = 0 and modules.flg_delete = 0'
                ]
            ])
            ->where([
                'modules.flg_site' => AppConst::SITE_TYPE,
                'RoleDetail.user_id' => $user_id
            ])
            ->toArray();
        $modules = [];
        if ($result[0]['role_id'] != 1) {
            return $modules;
        }
        if (count($result) > 0) {
            foreach ($result as $item) {
                array_push($modules, $item['modules']['controller_name']);
            }
        }
        return ['modules' => $modules];
    }
}
