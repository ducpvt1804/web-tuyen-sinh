<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * NewsRelationship component
 */
class NewsRelationshipComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->NewsRelationship = TableRegistry::getTableLocator()->get('NewsRelationship');
    }

    /*--------------------------------------------------------------------*/
    public function all()
    {
        $data = $this->NewsRelationship
            ->find('all')
            ->where(['flg_delete' => 0])
            ->all()
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->NewsRelationship
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions, $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->NewsRelationship
            ->find();
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function getDataById($id)
    {
        $data = $this->NewsRelationship
            ->find()
            ->select([
                'NewsRelationship.cate_id',
            ])
            ->where(['NewsRelationship.new_id' => $id, 'NewsRelationship.flg_delete' => 0])
            ->all()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function addAll($req)
    {
        $newsRelationship = $this->NewsRelationship->newEntities($req);
        if ($this->NewsRelationship->saveMany($newsRelationship)) {
            return $newsRelationship;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $newsRelationship = $this->NewsRelationship->get($id);
        $this->assignValue($newsRelationship, $req);
        $newsRelationship->update_date = date("Y-m-d H:i:s");
        if ($this->NewsRelationship->save($newsRelationship)) {
            return $newsRelationship;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function deletePhysical($id = null)
    {
        $newsRelationship = $this->NewsRelationship->query();
        $newsRelationship->delete();
        $result = $newsRelationship->where(['new_id' => $id])->execute();
        if ($result->rowCount() > 0) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function deleteAll($newsId = null)
    {
        $newsRelationship = $this->NewsRelationship->query();
        $newsRelationship->update_date()
            ->set(['flg_delete' => 1]);
        $result = $newsRelationship->where(['new_id' => $newsId])->execute();
        if ($result->rowCount() > 0) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function changeStatus($id = null, $status)
    {
        $newsRelationship = $this->NewsRelationship->get($id);
        if ($status == 0) {
            $newsRelationship->flg_show = 1;
        } else {
            $newsRelationship->flg_show = 0;
        }
        $newsRelationship->update_date = date("Y-m-d H:i:s");
        if ($this->NewsRelationship->save($newsRelationship)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($newsRelationship, $req)
    {
        $newsRelationship->user_id = isset($req['user_id']) ? $req['user_id'] : $newsRelationship['user_id'];
        $newsRelationship->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $newsRelationship['seo_id'];
        $newsRelationship->title = isset($req['title']) ? $req['title'] : $newsRelationship['title'];
        $newsRelationship->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $newsRelationship['slug'];
        $newsRelationship->parent_id = isset($req['parent_id']) ? $req['parent_id'] : $newsRelationship['parent_id'];
        $newsRelationship->type = isset($req['type']) ? (int)$req['type'] : $newsRelationship['type'];
        $newsRelationship->order_num = isset($req['order_num']) ? $req['order_num'] : $newsRelationship['order_num'];
        $newsRelationship->flg_site = AppConst::SITE_CONST;
        $newsRelationship->flg_language = isset($req['flg_language']) ? (int)$req['flg_language'] : $newsRelationship['flg_language'];
        $newsRelationship->flg_show = isset($req['flg_show']) ? (int)$req['flg_show'] : $newsRelationship['flg_show'];
    }
}
