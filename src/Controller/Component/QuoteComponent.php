<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Quotes component
 */
class QuoteComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Quotes = TableRegistry::getTableLocator()->get('Quotes');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Quotes
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Quotes
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Quotes
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $quote = $this->Quotes->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'quote');
        if ($uploadImg != "000") {
            $quote->img_highlight = $uploadImg;
            $this->assignValue($quote, $req);
            $quote->create_date = date("Y-m-d H:i:s");
            if ($this->Quotes->save($quote)) {
                return true;
            } else {
                if (file_exists(WWW_ROOT . "admin/upload/quote/" . $uploadImg)) {
                    unlink(WWW_ROOT . "admin/upload/quote/" . $uploadImg);
                }
                return false;
            }
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $quote = $this->Quotes->get($id);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'quote');
            if ($uploadImg != "000") {
                $imgOld = $quote->img_highlight;
                $this->assignValue($quote, $req);
                $quote->img_highlight = $uploadImg;
                $result = $this->Quotes->save($quote);
                if ($result && file_exists(WWW_ROOT . "admin/upload/quote/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/quote/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $this->assignValue($quote, $req);
        $quote->update_date = date("Y-m-d H:i:s");
        return $this->Quotes->save($quote);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $quote = $this->Quotes->get($id);
        $quote->update_date = date("Y-m-d H:i:s");
        $quote->flg_delete = 1;
        return $this->Quotes->save($quote);
    }

    /*--------------------------------------------------------------------*/
    public function status($id = null)
    {
        $quote = $this->Quotes->get($id);
        $quote->update_date = date("Y-m-d H:i:s");
        if ($quote->flg_status == 0) {
            $quote->flg_status = 1;
        } else {
            $quote->flg_status = 0;
        }
        return $this->Quotes->save($quote);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($quote, $req)
    {
        $quote->user_id = isset($req['user_id']) ? $req['user_id'] : $quote['user_id'];
        $quote->title = isset($req['title']) ? $req['title'] : $quote['title'];
        $quote->position = isset($req['position']) ? $req['position'] : $quote['position'];
        $quote->content = isset($req['content']) ? $req['content'] : $quote['content'];
        $quote->cate = isset($req['cate']) ? $req['cate'] : $quote['cate'];
        $quote->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $quote['flg_status'];
        $quote->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $quote['flg_language'];
    }
}
