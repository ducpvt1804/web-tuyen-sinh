<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class CateDepartmentComponent extends Component
{
    public $components = array('Function', 'Flash');
    protected $_TYPE;
    private $cate;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $this->Departments = TableRegistry::getTableLocator()->get('Departments');
        $this->CateDepartments = TableRegistry::getTableLocator()->get('CateDepartments');
        $this->_TYPE = 0;
        $this->cate = Configure::read('CATE_DAPARTMENT');
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->CateDepartments
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions)
    {
        $conditions = array_merge($conditions, ['CateDepartments.flg_delete' => 0]);
        $data = $this->CateDepartments
            ->find('all')
            ->select($this->CateDepartments)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    //                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'CateDepartments.department_id = ListDepartments.id',
                        1 => 'ListDepartments.flg_delete = 0',
                        2 => 'ListDepartments.type = 0',
                    ],
                ],
            ])
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getList($conditions)
    {
        $conditions = array_merge($conditions, ['CateDepartments.flg_delete' => 0, 'CateDepartments.flg_status' => 0]);
        $data = $this->CateDepartments
            ->find('all')
            ->select($this->CateDepartments)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'conditions' => [
                        0 => 'CateDepartments.department_id = ListDepartments.id',
                        1 => 'ListDepartments.flg_delete = 0',
                        2 => 'ListDepartments.type = 0',
                    ],
                ],
            ])
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $cateDepart = $this->CateDepartments->newEntity();
        $this->assignValue($cateDepart, $req);
        $cateDepart->create_date = date("Y-m-d H:i:s");
        return $this->CateDepartments->save($cateDepart);
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $cateDepart = $this->CateDepartments->get($id);
        $this->assignValue($cateDepart, $req);
        $cateDepart = $this->CateDepartments->patchEntity($cateDepart, $req);
        $cateDepart->update_date = date("Y-m-d H:i:s");
        return $this->CateDepartments->save($cateDepart);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $cateDepart = $this->CateDepartments->get($id);
        $cateDepart->update_date = date("Y-m-d H:i:s");
        $cateDepart->flg_delete = 1;
        return $this->CateDepartments->save($cateDepart);
    }

    /*--------------------------------------------------------------------*/
    public function swstatus($id = null)
    {
        $cateDepart = $this->CateDepartments->get($id);
        $cateDepart->update_date = date("Y-m-d H:i:s");
        if ($cateDepart->flg_status == 0) {
            $cateDepart->flg_status = 1;
        } else {
            $cateDepart->flg_status = 0;
        }
        return $this->CateDepartments->save($cateDepart);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($cateDepart, $req)
    {
        $cateDepart->department_id = isset($req['department_id']) ? (int) $req['department_id'] : $cateDepart['department_id'];
        $cateDepart->flg_language = isset($req['flg_language']) ? (int) $req['flg_language'] : $cateDepart['flg_language'];
        $cateDepart->flg_showheader = isset($req['flg_showheader']) ? (int) $req['flg_showheader'] : $cateDepart['flg_showheader'];
        $cateDepart->flg_status = isset($req['flg_status']) ? (int) $req['flg_status'] : $cateDepart['flg_status'];
        $cateDepart->department_id = isset($req['department_id']) ? (int) $req['department_id'] : $cateDepart['department_id'];
        $cateDepart->title = isset($req['title']) ? $req['title'] : $cateDepart['title'];
        $cateDepart->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $cateDepart['slug'];
        $cateDepart->user_id = isset($req['user_id']) ? $req['user_id'] : $cateDepart['user_id'];
        $cateDepart->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $cateDepart['seo_id'];
        $cateDepart->type = 0;
    }

    /*--------------------------------------------------------------------*/
    public function getMenus($deparment_id, $lang)
    {
        $conditionsMenu = [
            'department_id' => $deparment_id,
            'flg_showheader' => 0,
            'flg_status' => 0,
            'flg_language' => $lang
        ];
        $slugMenu = [
            'intro' => $lang === 0 ? $this->cate['VI']['INTRO'] : $this->cate['EN']['INTRO'],
            'new' => $lang === 0 ? $this->cate['VI']['NEW'] : $this->cate['EN']['NEW'],
            'action' => $lang === 0 ? $this->cate['VI']['ACTION'] : $this->cate['EN']['ACTION'],
            'contact' => $lang === 0 ? $this->cate['VI']['STUDENT'] : $this->cate['EN']['STUDENT'],
            'student' => $lang === 0 ? $this->cate['VI']['CONTACT'] : $this->cate['EN']['CONTACT']
        ];
        $menus = [
            'intro' => $this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['intro']])),
            'new' => $this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['new']])),
            'action' => $this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['action']])),
            'contact' => $this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['contact']])),
            'student' => $this->first(array_merge($conditionsMenu, ['slug' => $slugMenu['student']])),
        ];
        return $menus;
    }
}
