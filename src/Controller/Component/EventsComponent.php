<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Events component
 */
class EventsComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Events = TableRegistry::getTableLocator()->get('Events');
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $this->Seo = TableRegistry::getTableLocator()->get('Seo');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['Events.flg_delete' => 0]);
        $data = $this->Events
            ->find('all')
            ->select($this->Events)
            ->select($this->ListDepartments)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Events.flg_department and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ]
            ])
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getAllView($conditions = [])
    {
        $conditions = array_merge($conditions, ['Events.flg_delete' => 0]);
        $data = $this->Events
            ->find('all')
            ->select($this->Events)
            ->select($this->ListDepartments)
            ->select($this->Seo)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Events.flg_department and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ],
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'INNER',
                    'conditions' => 'Seo.id = Events.seo_id and Seo.flg_delete = 0'
                ]
            ])
            ->where($conditions);
        return $data;
    }

    public function etDetail($conditions){
        $conditions = array_merge($conditions, ['Events.flg_delete' => 0]);
        $data = $this->Events
            ->find('all')
            ->select($this->Events)
            ->select($this->ListDepartments)
            ->select($this->Seo)
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Events.flg_department and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ],
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'INNER',
                    'conditions' => 'Seo.id = Events.seo_id and Seo.flg_delete = 0'
                ]
            ])
            ->where($conditions)
            ->first()
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Events
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Events
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $event = $this->Events->newEntity();
        $uploadImg = $this->Function->upload($req['img_highlight'], 'events');
        if ($uploadImg != "000") {
            $event->img_highlight = $uploadImg;
            $this->assignValue($event, $req);
            $event->create_date = date("Y-m-d H:i:s");
            return $this->Events->save($event);
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $event = $this->Events->get($id);
        $this->assignValue($event, $req);
        if (!empty($req["img_highlight"]['tmp_name'])) {
            $uploadImg = $this->Function->upload($req['img_highlight'], 'events');
            if ($uploadImg != "000") {
                $imgOld = $event->img_highlight;
                $event->img_highlight = $uploadImg;
                $result = $this->Events->save($event);
                if ($result && !empty($imgOld) && file_exists(WWW_ROOT . "admin/upload/events/" . $imgOld)) {
                    unlink(WWW_ROOT . "admin/upload/events/" . $imgOld);
                }
                return $result;
            } else {
                return false;
            }
        }
        $event->update_date = date("Y-m-d H:i:s");
        return $this->Events->save($event);
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $event = $this->Events->get($id);
        $event->update_date = date("Y-m-d H:i:s");
        $event->flg_delete = 1;
        return $this->Events->save($event);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($event, $req)
    {
        $event->user_id = isset($req['user_id']) ? $req['user_id'] : $event['user_id'];
        $event->mail = isset($req['mail']) ? $req['mail'] : $event['mail'];
        $event->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $event['seo_id'];
        $event->flg_department = isset($req['flg_department']) ? $req['flg_department'] : $event['flg_department'];
        $event->title = isset($req['title']) ? $req['title'] : $event['title'];
        $event->slug = isset($req['title']) ? $this->Function->convertSlug($req['title']) : $event['slug'];
        $event->address = isset($req['address']) ? $req['address'] : $event['address'];
        $event->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $event['flg_status'];
        $event->start_date = isset($req['start_date']) ? date('Y-m-d H:i:s', strtotime($req['start_date'])) : $event['start_date'];
        $event->end_date = isset($req['end_date']) ? date('Y-m-d H:i:s', strtotime($req['end_date'])) : $event['end_date'];
        $event->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $event['flg_language'];
        $event->map = isset($req['map']) ? $req['map'] : $event['map'];
        $event->telno = isset($req['telno']) ? $req['telno'] : $event['telno'];
        $event->position = isset($req['position']) ? $req['position'] : $event['position'];
        $event->cost = isset($req['cost']) ? $req['cost'] : $event['cost'];
        $event->content = isset($req['content']) ? htmlentities($req['content']) : $event['content'];
    }
}
