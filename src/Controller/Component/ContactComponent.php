<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class ContactComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Contacts = TableRegistry::getTableLocator()->get('Contacts');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Contacts
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function allMess($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_read' => 0]);
        $data = $this->Contacts
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Contacts
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    public function notification(){
        $data = $this->Contacts
            ->find('all')
            ->select(['notification' => 'count(flg_read)', 'id'])
            ->where(['flg_delete' => 0, 'flg_read' => 0])
            ->group(['flg_read'])
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Contacts
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $contact = $this->Contacts->newEntity();
        $this->assignValue($contact, $req);
        $contact->create_date = date("Y-m-d H:i:s");
        if($this->Contacts->save($contact)){
            return true;
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $contact = $this->Contacts->get($id);
        $contact->update_date = date("Y-m-d H:i:s");
        $contact->flg_delete = 1;
        return $this->Contacts->save($contact);
    }

    /*--------------------------------------------------------------------*/
    public function read($id = null)
    {
        $contact = $this->Contacts->get($id);
        $contact->update_date = date("Y-m-d H:i:s");
        $contact->flg_read = 1;
        return $this->Contacts->save($contact);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($contact, $req)
    {
        $contact->name = isset($req['name']) ? $req['name'] : $contact['name'];
        $contact->email = isset($req['email']) ? $req['email'] : $contact['email'];
        $contact->title = isset($req['title']) ? $req['title'] : $contact['title'];
        $contact->content = isset($req['content']) ? htmlentities($req['content']) : $contact['content'];
        $contact->telno = isset($req['telno']) ? $req['telno'] : $contact['telno'];
        $contact->flg_site = isset($req['flg_site']) ? $req['flg_site'] : $contact['flg_site'];
        $contact->flg_read = isset($req['flg_read']) ? $req['flg_read'] : 0;
        $contact->flg_reply = isset($req['flg_reply']) ? $req['flg_reply'] : $contact['flg_reply'];
    }
}
