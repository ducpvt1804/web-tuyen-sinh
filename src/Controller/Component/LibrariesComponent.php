<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Libraries component
 */
class LibrariesComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Libraries = TableRegistry::getTableLocator()->get('Libraries');
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
    }

    /*--------------------------------------------------------------------*/
    public function all()
    {
        $data = $this->Libraries
            ->find('all')
            ->where(['flg_delete' => 0])
            ->all()
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->Libraries
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getLibraryVi($id)
    {
        $data = $this->Libraries
            ->find()
            ->select([
                'Libraries.id',
                'Libraries.department_id',
                'Libraries.type',
                'LibrariesDetail.name_library',
                'LibrariesDetail.slug',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.id',
                'Seo.meta_title',
                'Seo.meta_description',
                'Seo.meta_keywords',
                'Seo.tags',
                'department_title' => 'ListDepartments.title',
            ])
            ->join([
                'LibrariesDetail' => [
                    'table' => 'libraries_detail',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'Libraries.id = LibrariesDetail.library_id',
                        1 => 'LibrariesDetail.flg_delete = 0',
                    ],
                ],
            ])
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'ListDepartments.id = Libraries.department_id',
                        1 => 'ListDepartments.flg_delete = 0',
                    ],
                ],
            ])
            ->join([
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'LibrariesDetail.seo_id = Seo.id',
                        1 => 'Seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Libraries.id' => $id, 'Libraries.flg_delete' => 0, 'LibrariesDetail.flg_language' => AppConst::FLG_LANGUAGE_VIETNAM])
            ->first()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getLibraryEn($id)
    {
        $data = $this->Libraries
            ->find()
            ->select([
                'Libraries.id',
                'Libraries.department_id',
                'Libraries.type',
                'LibrariesDetail.name_library',
                'LibrariesDetail.slug',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.id',
                'Seo.meta_title',
                'Seo.meta_description',
                'Seo.meta_keywords',
                'Seo.tags',
                'department_title' => 'ListDepartments.title',
            ])
            ->join([
                'LibrariesDetail' => [
                    'table' => 'libraries_detail',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'Libraries.id = LibrariesDetail.library_id',
                        1 => 'LibrariesDetail.flg_delete = 0',
                    ],
                ],
            ])
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'ListDepartments.id = Libraries.department_id',
                        1 => 'ListDepartments.flg_delete = 0',
                    ],
                ],
            ])
            ->join([
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'LibrariesDetail.seo_id = Seo.id',
                        1 => 'Seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Libraries.id' => $id, 'Libraries.flg_delete' => 0, 'LibrariesDetail.flg_language' => AppConst::FLG_LANGUAGE_ENGLISH])
            ->first()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getLibraryMedia($id)
    {
        $data = $this->Libraries
            ->find()
            ->select([
                'Libraries.id',
                'LibrariesMedia.id',
                'LibrariesMedia.path',
            ])
            ->join([
                'LibrariesMedia' => [
                    'table' => 'libraries_media',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'LibrariesMedia.library_id = Libraries.id',
                        1 => 'LibrariesMedia.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Libraries.id' => $id, 'Libraries.flg_delete' => 0])
            ->all()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions, $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['Libraries.flg_delete' => 0]);
        $data = $this->Libraries
        ->find()
        ->select([
            'Libraries.id',
            'Libraries.department_id',
            'Libraries.type',
            'LibrariesDetail.name_library',
            'LibrariesDetail.slug',
            'LibrariesDetail.flg_language',
            'LibrariesDetail.flg_status',
            'LibrariesDetail.seo_id',
            'LibrariesDetail.id',
        ])
        ->join([
            'LibrariesDetail' => [
                'table' => 'libraries_detail',
                'type' => 'LEFT',
                'conditions' => [
                    0 => 'Libraries.id = LibrariesDetail.library_id',
                    1 => 'LibrariesDetail.flg_delete = 0',
                ],
            ],
        ]);
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $libraries = $this->Libraries->newEntity();
        $this->assignValue($libraries, $req);
        $libraries->create_date = date("Y-m-d H:i:s");
        if ($this->Libraries->save($libraries)) {
            return $libraries;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $libraries = $this->Libraries->get($id);
        $this->assignValue($libraries, $req);
        $libraries->update_date = date("Y-m-d H:i:s");
        if ($this->Libraries->save($libraries)) {
            return $libraries;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $libraries = $this->Libraries->get($id);
        $libraries->update_date = date("Y-m-d H:i:s");
        $libraries->flg_delete = 1;
        if ($this->Libraries->save($libraries)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function changeStatus($id = null, $status)
    {
        $libraries = $this->Libraries->get($id);
        if ($status == 0) {
            $libraries->flg_status = 1;
        } else {
            $libraries->flg_status = 0;
        }
        $libraries->update_date = date("Y-m-d H:i:s");
        if ($this->Libraries->save($libraries)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($libraries, $req)
    {
        $libraries->department_id = isset($req['department_id']) ? (int)$req['department_id'] : $libraries['department_id'];
        $libraries->type = isset($req['type']) ? (int)$req['type'] : $libraries['type'];
        $libraries->flg_delete = isset($req['flg_delete']) ? (int)$req['flg_delete'] : $libraries['flg_delete'];
    }

    public function getListLibraryImages($conditions, $limit = null, $offset = 1){
        $conditions = array_merge($conditions, ['Libraries.flg_delete' => 0]);
        $data = $this->Libraries
            ->find('all')
            ->select([
                'Libraries.id',
                'Libraries.department_id',
                'Libraries.type',
                'LibrariesDetail.name_library',
                'LibrariesDetail.slug',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.id',
                'department_title' => 'ListDepartments.title'
            ])
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Libraries.department_id and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ]
            ])
            ->join([
                'LibrariesDetail' => [
                    'table' => 'libraries_detail',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'Libraries.id = LibrariesDetail.library_id',
                        1 => 'LibrariesDetail.flg_delete = 0',
                    ],
                ],
            ])
            ->where($conditions);
            if($limit){
                $data = $data
                    ->order(['Libraries.create_date' => 'DESC'])
                    ->limit($limit)
                    ->page($offset);
            }
        return $data;
    }

    public function getListLibraryVideos($conditions, $limit = null, $offset = 1){
        $conditions = array_merge($conditions, ['Libraries.flg_delete' => 0]);
        $data = $this->Libraries
            ->find('all')
            ->select([
                'Libraries.id',
                'Libraries.department_id',
                'Libraries.type',
                'LibrariesDetail.name_library',
                'LibrariesDetail.slug',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.id',
                'department_title' => 'ListDepartments.title'
            ])
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Libraries.department_id and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ]
            ])
            ->join([
                'LibrariesDetail' => [
                    'table' => 'libraries_detail',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'Libraries.id = LibrariesDetail.library_id',
                        1 => 'LibrariesDetail.flg_delete = 0',
                    ],
                ],
            ])
            ->where($conditions);
        if($limit){
            $data = $data
                ->order(['Libraries.create_date' => 'DESC'])
                ->limit($limit)
                ->page($offset);
        }
        return $data;
    }

    public function getDetailLibrary($conditions){
        $conditions = array_merge($conditions, ['Libraries.flg_delete' => 0]);
        $data = $this->Libraries
            ->find('all')
            ->select([
                'Libraries.id',
                'Libraries.department_id',
                'Libraries.type',
                'Libraries.create_date',
                'LibrariesDetail.name_library',
                'LibrariesDetail.slug',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.id',
                'department_title' => 'ListDepartments.title',
                'Seo.meta_title',
                'Seo.meta_description',
                'Seo.meta_keywords',
                'Seo.tags',
            ])
            ->join([
                'ListDepartments' => [
                    'table' => 'list_departments',
                    'type' => 'INNER',
                    'conditions' => 'ListDepartments.id = Libraries.department_id and ListDepartments.flg_delete = 0 and ListDepartments.flg_status = 0'
                ]
            ])
            ->join([
                'LibrariesDetail' => [
                    'table' => 'libraries_detail',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'Libraries.id = LibrariesDetail.library_id',
                        1 => 'LibrariesDetail.flg_delete = 0',
                    ],
                ],
            ])
            ->join([
                'Seo' => [
                    'table' => 'seo',
                    'type' => 'LEFT',
                    'conditions' => [
                        0 => 'LibrariesDetail.seo_id = Seo.id',
                        1 => 'Seo.flg_delete = 0',
                    ],
                ],
            ])
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getDetailLibraryMedia($id)
    {
        $data = $this->Libraries
            ->find('all')
            ->select([
                'Libraries.id',
                'LibrariesMedia.id',
                'LibrariesMedia.path',
            ])
            ->join([
                'LibrariesMedia' => [
                    'table' => 'libraries_media',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'LibrariesMedia.library_id = Libraries.id',
                        1 => 'LibrariesMedia.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Libraries.id' => $id, 'Libraries.flg_delete' => 0]);
        return $data;
    }
}
