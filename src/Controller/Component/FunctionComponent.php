<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Events component
 */
class FunctionComponent extends Component
{
    public $components = ['Flash'];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /*--------------------------------------------------------------------*/
    public function convertSlug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        $str = str_replace('---', '-', $str);
        return $str;
    }

    /*--------------------------------------------------------------------*/
    public function upload($data, $folder)
    {
        $file = $data['tmp_name'];
        $sourceProperties = getimagesize($file);
        $fileNewName = $this->generateName();
        $folderPath = WWW_ROOT . "admin/upload/" . $folder . "/";
        $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];

        if ($folder == 'libraries') {
            if (move_uploaded_file($file, $folderPath . $fileNewName . "." . $ext)) {
                return $fileNewName . "." . $ext;
            }
            return '000';
        } else {
            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], $imageType);
                    imagepng($targetLayer, $folderPath . $fileNewName . "_thump." . $ext);
                    break;


                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], $imageType);
                    imagegif($targetLayer, $folderPath . $fileNewName . "_thump." . $ext);
                    break;


                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], $imageType);
                    imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext);
                    break;

                default:
                    return "000"; // error extension
                    break;
            }

            if (move_uploaded_file($file, $folderPath . $fileNewName . "." . $ext)) {
                if (file_exists($folderPath . $fileNewName . "." . $ext)) {
                    unlink($folderPath . $fileNewName . "." . $ext);
                }
            }
            return $fileNewName . "_thump." . $ext;
        }
    }

    /*--------------------------------------------------------------------*/
    public function generateName($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . date("YmdHis");
    }

    /*--------------------------------------------------------------------*/
    public function imageResize($imageResourceId, $width, $height, $imageType = null)
    {
        $targetWidth = $width;
        $targetHeight = $height;
        if ($width > 1500) {
            $targetWidth = (int) $width / 2;
        }
        if ($width > 1500) {
            $targetHeight = (int) $height / 2;
        }
        $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
        switch ($imageType) {

            case 'gif':
            case IMAGETYPE_PNG:
                // integer representation of the color black (rgb: 0,0,0)
                $background = imagecolorallocate($targetLayer , 0, 0, 0);
                // removing the black from the placeholder
                imagecolortransparent($targetLayer, $background);

                // turning off alpha blending (to ensure alpha channel information
                // is preserved, rather than removed (blending with the rest of the
                // image in the form of black))
                imagealphablending($targetLayer, false);

                // turning on alpha channel information saving (to ensure the full range
                // of transparency is preserved)
                imagesavealpha($targetLayer, true);
                break;
        
            default:
                break;
        }

        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }

    /*--------------------------------------------------------------------*/
    public function showMess($data)
    {
        foreach ($data as $item) {
            foreach ($item as $k => $message) {
                $this->Flash->error(__($message));
            }
        }
    }
}
