<?php

namespace App\Controller\Component;

use AppConst;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * LibrariesDetail component
 */
class LibrariesDetailComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->LibrariesDetail = TableRegistry::getTableLocator()->get('LibrariesDetail');
    }

    /*--------------------------------------------------------------------*/
    public function all()
    {
        $data = $this->LibrariesDetail
            ->find('all')
            ->where(['flg_delete' => 0])
            ->all()
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->LibrariesDetail
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions, $colums = null, $order = null, $limit = null)
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->LibrariesDetail
            ->find();
        if (!empty($colums)) {
            $data->select($colums);
        }
        $data->where($conditions);
        if (!empty($order)) {
            $data->order($order);
        }
        if (!empty($limit)) {
            $data->limit($limit);
        }
        $datas = $data->all()->toArray();
        return $datas;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $librariesDetail = $this->LibrariesDetail->newEntity();
        $this->assignValue($librariesDetail, $req);
        $librariesDetail->create_date = date("Y-m-d H:i:s");
        if ($this->LibrariesDetail->save($librariesDetail)) {
            return $librariesDetail;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $librariesDetail = $this->LibrariesDetail->get($id);
        $this->assignValue($librariesDetail, $req);
        $librariesDetail->update_date = date("Y-m-d H:i:s");
        if ($this->LibrariesDetail->save($librariesDetail)) {
            return $librariesDetail;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $librariesDetail = $this->LibrariesDetail->get($id);
        $librariesDetail->update_date = date("Y-m-d H:i:s");
        $librariesDetail->flg_delete = 1;
        if ($this->LibrariesDetail->save($librariesDetail)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function changeStatus($id = null, $status)
    {
        $librariesDetail = $this->LibrariesDetail->get($id);
        if ($status == 0) {
            $librariesDetail->flg_status = 1;
        } else {
            $librariesDetail->flg_status = 0;
        }
        $librariesDetail->update_date = date("Y-m-d H:i:s");
        if ($this->LibrariesDetail->save($librariesDetail)) {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------------*/
    public function getDataById($id)
    {
        $data = $this->LibrariesDetail
            ->find()
            ->select([
                'LibrariesDetail.id',
                'LibrariesDetail.seo_id',
                'LibrariesDetail.flg_language',
                'LibrariesDetail.flg_status',
            ])
            ->where(['LibrariesDetail.id' => $id, 'LibrariesDetail.flg_delete' => 0])
            ->first()->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($librariesDetail, $req)
    {
        $librariesDetail->library_id = isset($req['library_id']) ? $req['library_id'] : $librariesDetail['library_id'];
        $librariesDetail->seo_id = isset($req['seo_id']) ? $req['seo_id'] : $librariesDetail['seo_id'];
        $librariesDetail->name_library = isset($req['name_library']) ? $req['name_library'] : $librariesDetail['name_library'];
        $librariesDetail->slug = isset($req['name_library']) ? $this->Function->convertSlug($req['name_library']) : $categories['slug'];
        $librariesDetail->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $librariesDetail['flg_status'];
        $librariesDetail->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $librariesDetail['flg_language'];
        $librariesDetail->flg_delete = isset($req['flg_delete']) ? $req['flg_delete'] : $librariesDetail['flg_delete'];
    }
}
