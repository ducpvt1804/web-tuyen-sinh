<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Partner component
 */
class EnrollmentOptionComponent extends Component
{

    public $components = ['Function'];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->EnrollmentOptions = TableRegistry::getTableLocator()->get('EnrollmentOptions');
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentOptions
            ->find('all')
            ->where($conditions);
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function first($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentOptions
            ->find('all')
            ->where($conditions)
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getBranchParentType1()
    {
        $data = $this->EnrollmentOptions
            ->find('all')
            ->where(['flg_delete' => 0, 'branch_parent' => 0, 'flg_status' => 0, 'type' => 1])
            ->all();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function getBranchParentType2()
    {
        $data = $this->EnrollmentOptions
            ->find('all')
            ->where(['flg_delete' => 0, 'branch_parent' => 0, 'flg_status' => 0, 'type' => 2])
            ->all();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function where($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0]);
        $data = $this->EnrollmentOptions
            ->find('all')
            ->where($conditions)
            ->toArray();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function add($req)
    {
        $enrollment_options = $this->EnrollmentOptions->newEntity();
        $this->assignValue($enrollment_options, $req);
        $enrollment_options->create_date = date("Y-m-d H:i:s");
        if($req['branch_parent'] == ""){
            $enrollment_options->branch_parent = 0;
        }
        if ($this->EnrollmentOptions->save($enrollment_options)) {
            return true;
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function edit($req, $id = null)
    {
        $enrollment_options = $this->EnrollmentOptions->get($id);
        $this->assignValue($enrollment_options, $req);
        $enrollment_options->update_date = date("Y-m-d H:i:s");
        if($req['branch_parent'] == ""){
            $enrollment_options->branch_parent = 0;
        }
        if ($this->EnrollmentOptions->save($enrollment_options)) {
            return true;
        } else {
            return false;
        }
    }

    /*--------------------------------------------------------------------*/
    public function delete($id = null)
    {
        $enrollment_options = $this->EnrollmentOptions->get($id);
        $enrollment_options->update_date = date("Y-m-d H:i:s");
        $enrollment_options->flg_delete = 1;
        return $this->EnrollmentOptions->save($enrollment_options);
    }

    /*--------------------------------------------------------------------*/
    public function status($id = null)
    {
        $enrollment_options = $this->EnrollmentOptions->get($id);
        $enrollment_options->update_date = date("Y-m-d H:i:s");
        if ($enrollment_options->flg_status == 0) {
            $enrollment_options->flg_status = 1;
        } else {
            $enrollment_options->flg_status = 0;
        }
        return $this->EnrollmentOptions->save($enrollment_options);
    }

    /*--------------------------------------------------------------------*/
    public function assignValue($enrollment_options, $req)
    {
        $enrollment_options->type = isset($req['type']) ? $req['type'] : $enrollment_options['type'];
        $enrollment_options->user_id = isset($req['user_id']) ? $req['user_id'] : $enrollment_options['user_id'];
        $enrollment_options->branch_name = isset($req['branch_name']) ? $req['branch_name'] : $enrollment_options['branch_name'];
        $enrollment_options->branch_code = isset($req['branch_code']) ? $req['branch_code'] : $enrollment_options['branch_code'];
        $enrollment_options->combination_code = isset($req['combination_code']) ? $req['combination_code'] : $enrollment_options['combination_code'];
        $enrollment_options->branch_parent = isset($req['branch_parent']) ? $req['branch_parent'] : $enrollment_options['branch_parent'];
        $enrollment_options->branch_update_name = isset($req['branch_update_name']) ? $req['branch_update_name'] : $enrollment_options['branch_update_name'];
        $enrollment_options->flg_language = isset($req['flg_language']) ? $req['flg_language'] : $enrollment_options['flg_language'];
        $enrollment_options->flg_status = isset($req['flg_status']) ? $req['flg_status'] : $enrollment_options['flg_status'];
    }
}
