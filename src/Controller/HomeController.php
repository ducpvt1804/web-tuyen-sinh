<?php


namespace App\Controller;

use Cake\Controller\Controller;


class HomeController extends Controller
{
     public function index(){
         $this->redirect('/vi');
     }
}
