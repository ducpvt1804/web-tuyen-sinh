<!DOCTYPE html>
<html lang="">

<head>
  <meta charset="utf-8" />
  <title><?= isset($title) ? $title : __('Trường Đại Học Đại Nam | Cổng thông Tin chính thức Đại Học Đại Nam') ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?= isset($seo['meta_description']) ? $seo['meta_description'] : 'Đại học Đại Nam' ?>">
  <meta name="keywords" content="<?= isset($seo['meta_keywords']) ? $seo['meta_keywords'] : 'Đại học Đại Nam' ?>">
  <meta name="title" content="<?= isset($seo['meta_title']) ? $seo['meta_title'] : 'Đại học Đại Nam' ?>">
  <link rel="icon" type="image/png" href="favicon.png" />

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link type="text/css" href="/front/stylesheets/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="/front/stylesheets/owl.carousel.css" rel="stylesheet" />
  <link type="text/css" href="/front/stylesheets/owl.theme.default.css" rel="stylesheet" />
  <link rel="stylesheet" href="/front/stylesheets/font-awesome.css">
  <link type="text/css" href="/front/stylesheets/magnific-popup.css" rel="stylesheet" />
  <link type="text/css" href="/front/stylesheets/bootstrap-datepicker.min.css" rel="stylesheet" />
  <link type="text/css" href="/front/stylesheets/style.css" rel="stylesheet" />

</head>

<body>

  <div class="wrapper">
    <?= $this->fetch('header') ?>

    <?= $this->fetch('menu') ?>

    <?= $this->fetch('banner') ?>

    <main class="main-content">
      <?= $this->Flash->render() ?>
      <?= $this->fetch('content') ?>
    </main>

    <?= $this->element('Front/footer') ?>
  </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="/front/javascripts/popper.min.js"></script>
  <script src="/front/javascripts/bootstrap.min.js"></script>
  <script src="/front/javascripts/owl.carousel.js"></script>
  <script src="/front/javascripts/jquery.dotdotdot.js"></script>
  <script src="/front/javascripts/jquery.magnific-popup.min.js"></script>
  <script src="/front/javascripts/bootstrap-datepicker.min.js"></script>
  <script src="/front/javascripts/customs.js"></script>

  <?= $this->fetch('script_footer') ?>
  <script>
    $(document).ready(function() {

    });
  </script>
</body>

</html>
