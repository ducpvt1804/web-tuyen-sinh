<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?=$this->Html->charset() ?>
    <title>Error page</title>
    <?=$this->Html->meta('icon') ?>

    <?=$this->Html->css('error/base.css') ?>
    <?=$this->Html->css('error/style.css') ?>
    <?=$this->Html->css('error/error_page.css') ?>

    <?=$this->fetch('meta') ?>
    <?=$this->fetch('css') ?>
    <?=$this->fetch('script') ?>
</head>
<body>
<div id="container">
    <div id="content">

        <div id="clouds">
            <div class="cloud x1"></div>
            <div class="cloud x1_5"></div>
            <div class="cloud x2"></div>
            <div class="cloud x3"></div>
            <div class="cloud x4"></div>
            <div class="cloud x5"></div>
        </div>
        <!--`-->
        <div class="c">
            <div class="_404">404</div>
            <hr>
            <div class="_1">THE PAGE</div>
            <div class="_2">WAS NOT FOUND</div>
            <a class="btn" href="/">BACK TO HOMEPAGE</a>
        </div>
</div>
</body>
</html>
