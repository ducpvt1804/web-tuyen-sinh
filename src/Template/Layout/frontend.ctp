<!DOCTYPE html>
<html lang="<?= $lang ?>">

<head>
    <title><?= isset($title) ? $title : __('Trường Đại Học Đại Nam | Cổng thông Tin chính thức Đại Học Đại Nam') ?></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= isset($seo['meta_description']) ? $seo['meta_description'] : 'Đại học Đại Nam' ?>">
    <meta name="keywords" content="<?= isset($seo['meta_keywords']) ? $seo['meta_keywords'] : 'Đại học Đại Nam' ?>">
    <meta name="title" content="<?= isset($seo['meta_title']) ? $seo['meta_title'] : 'Đại học Đại Nam' ?>">
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <link type="text/css" href="/front/stylesheets/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="/front/stylesheets/owl.carousel.css" rel="stylesheet" />
    <link type="text/css" href="/front/stylesheets/owl.theme.default.css" rel="stylesheet" />
    <link rel="stylesheet" href="/front/stylesheets/font-awesome.css">
    <link type="text/css" href="/front/stylesheets/magnific-popup.css" rel="stylesheet" />
    <link type="text/css" href="/front/stylesheets/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link type="text/css" href="/front/stylesheets/style.css" rel="stylesheet" />

</head>

<body>
    <div class="wrapper">
        <!-- header -->
        <?= $this->element('Front/header') ?>

        <div class="sidebar-menu-bg"></div>

        <?= $this->element('Front/sidebar') ?>
        <!--/.sidebar-menu -->
        <!-- e: header -->
        <?= $this->fetch('banner') ?>
        <?php $current_action = $this->getRequest()->getParam('action'); ?>
        <!-- main content -->
        <main class="main-content <?php if ($current_controller != "Home" && $current_controller != "Contacts" && $current_controller != "Library" && $current_controller == "News" && $current_action == 'detail') { ?> main-content-gray <?php } ?>">
            <!-- start section-announce -->
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </main>
        <!-- e: main content -->

        <!-- footer -->
        <?= $this->element('Front/footer') ?>
        <!-- footer -->
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/front/javascripts/popper.min.js"></script>
    <script src="/front/javascripts/bootstrap.min.js"></script>
    <script src="/front/javascripts/owl.carousel.js"></script>
    <script src="/front/javascripts/jquery.dotdotdot.js"></script>
    <script src="/front/javascripts/jquery.magnific-popup.min.js"></script>
    <script src="/front/javascripts/bootstrap-datepicker.min.js"></script>
    <script src="/front/javascripts/customs.js"></script>


    <script>
        $(document).ready(function() {

        });
    </script>
    <?= $this->fetch('script_footer') ?>
</body>

</html>
