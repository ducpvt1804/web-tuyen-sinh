<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Sự kiện',
    'url' => [
        'Sự kiện' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Danh sách sự kiện' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm sự kiện</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->Form->create() ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "flg_language",
                                "default" => "Ngôn ngữ",
                                "options" => $this->Language->all(),
                                "selected" => isset($req['flg_language']) ? $req['flg_language'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "flg_status",
                                "default" => "Trạng thái sự kiện",
                                "options" => $this->StatusEvent->all(),
                                "selected" => isset($req['flg_status']) ? $req['flg_status'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/selectDepartment", [
                                "class" => "form-control custom-select",
                                "name" => "flg_department",
                                "default" => "Khoa",
                                "options" => $this->ListDepartment->all(),
                                "selected" => isset($req['flg_department']) ? $req['flg_department'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" name="keyword" value="<?= isset($req['keyword']) ? $req['keyword'] : '' ?>" class="form-control" placeholder="Nhập từ khóa cần tìm kiếm...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'add']) ?>" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách sự kiện</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="card-body no-padding">
            <table id="example2" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Khoa</th>
                        <th>Tên sự kiện</th>
                        <th>Ngôn ngữ</th>
                        <th>Trạng thái</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($events) && count($events) > 0) :
                        foreach ($events as $key => $row) :
                            ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $row['ListDepartments']['title'] ?></td>
                                <td><?= $row['title'] ?></td>
                                <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $row['flg_language']) ?></td>
                                <td><?= $this->StatusEvent->getTitle($row['flg_status']) ?></td>
                                <td style="text-align: left; vertical-align: middle">
                                    <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'Events', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                    </button>
                                </td>
                            </tr>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa bản ghi này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/categories.js"></script>
<?php $this->start('script_footer'); ?>
<script>
    $(function() {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
