<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Chỉnh sửa sự kiện',
    'url' => [
        'Sự kiện' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Chỉnh sửa sự kiện' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Thông tin sự kiện</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select" . (isset($errEvents['flg_status']) ? ' err-ctrl' : ''),
                                "name" => "flg_status",
                                "label" => "Tình trạng:",
                                "options" => $this->StatusEvent->all(),
                                "selected" => isset($req['flg_status']) ? $req['flg_status'] : (isset($event['flg_status']) ? $event['flg_status'] : '')
                            ]) ?>
                            <?= isset($errEvents['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['flg_status'], 'name' => 'flg_status']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->element("Admin/Form/selectDepartment", [
                                "class" => "form-control custom-select" . (isset($errEvents['flg_department']) ? ' err-ctrl' : ''),
                                "name" => "flg_department",
                                "label" => "Tổ chức bởi:",
                                "options" => $this->ListDepartment->all(),
                                "selected" => isset($req['flg_department']) ? $req['flg_department'] : (isset($event['flg_department']) ? $event['flg_department'] : '')
                            ]) ?>
                            <?= isset($errEvents['flg_department']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['flg_department'], 'name' => 'flg_department']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('title', [
                                'class' => 'form-control' . (isset($errEvents['title']) ? ' err-ctrl' : ''),
                                'label' => 'Tên sự kiện:',
                                "value" => isset($req['title']) ? $req['title'] : (isset($event['title']) ? $event['title'] : ''),
                                'placeholder' => 'Nhập tên sự kiện...'
                            ]) ?>
                            <?= isset($errEvents['title']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['title'], 'name' => 'title']) : '' ?>
                        </div>
                        <div class="form-group">
                            <label for="input_img_highlight">Ảnh Đại Diện:</label>
                            <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->events($event['img_highlight']), 'name' => 'img_highlight']) ?>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="img_highlight" type="file" id="input_img_highlight" class="custom-file-input input_file" accept="image/jpeg, image/png">
                                    <label class="custom-file-label<?= (isset($errEvents['img_highlight']) ? ' err-ctrl' : '') ?>" id="img_highlight" for="input_img_highlight">Chọn ảnh...</label>
                                </div>
                            </div>
                            <?= isset($errEvents['img_highlight']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['img_highlight'], 'name' => 'img_highlight']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('address', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['address']) ? ' err-ctrl' : ''),
                                'label' => 'Địa chỉ diễn ra sự kiện:',
                                "value" => isset($req['address']) ? $req['address'] : (isset($event['address']) ? $event['address'] : ''),
                                'placeholder' => 'Nhập địa chỉ diễn ra sự kiện...'
                            ]) ?>
                            <?= isset($errEvents['address']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['address'], 'name' => 'address']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('map', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['map']) ? ' err-ctrl' : ''),
                                'label' => 'Link địa chỉ map:',
                                "value" => isset($req['map']) ? $req['map'] : (isset($event['map']) ? $event['map'] : ''),
                                'placeholder' => 'Nhập link địa chỉ map...'
                            ]) ?>
                            <?= isset($errEvents['map']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['map'], 'name' => 'map']) : '' ?>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="start_date">Thời gian bắt đầu:</label>
                            <input type="datetime-local" class="form-control<?= (isset($errEvents['start_date']) ? ' err-ctrl' : '') ?>" name="start_date" value="<?= isset($req['start_date']) ?  $req['start_date'] : (isset($event['start_date']) ? $this->Time->format($event['start_date'], 'Y-MM-dd') . 'T' . $this->Time->format($event['start_date'], 'HH:mm') : '') ?>">
                            <?= isset($errEvents['start_date']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['start_date'], 'name' => 'start_date']) : '' ?>
                        </div>
                        <div class="form-group">
                            <label for="end_date">Thời gian kết thúc</label>
                            <input type="datetime-local" class="form-control <?= (isset($errEvents['end_date']) ? ' err-ctrl' : '') ?>" name="end_date" value="<?= isset($req['end_date']) ? $req['end_date'] : (isset($event['end_date']) ? $this->Time->format($event['end_date'], 'Y-MM-dd') . 'T' . $this->Time->format($event['end_date'], 'HH:mm') : '') ?>">
                            <?= isset($errEvents['end_date']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['end_date'], 'name' => 'end_date']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'class' => (isset($errEvents['flg_language']) ? ' err-ctrl' : ''),
                                'name' => 'flg_language',
                                'label' => 'Ngôn ngữ',
                                'options' => $this->Language->all(),
                                'checked' => isset($req['flg_language']) ? $req['flg_language'] : (isset($event['flg_language']) ? $event['flg_language'] : ''),
                            ]) ?>
                            <?= isset($errEvents['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['flg_language'], 'name' => 'flg_language']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('mail', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['mail']) ? ' err-ctrl' : ''),
                                "value" => isset($req['mail']) ? $req['mail'] : (isset($event['mail']) ? $event['mail'] : ''),
                                'label' => 'Email ban tổ chức:',
                                'placeholder' => 'Nhập email...',
                            ]) ?>
                            <?= isset($errEvents['mail']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['mail'], 'name' => 'mail']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('telno', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['telno']) ? ' err-ctrl' : ''),
                                'label' => 'Số điện thoại:',
                                "value" => isset($req['telno']) ? $req['telno'] : (isset($event['telno']) ? $event['telno'] : ''),
                                'placeholder' => 'Nhập số điện thoại...'
                            ]) ?>
                            <?= isset($errEvents['telno']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['telno'], 'name' => 'telno']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('position', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['position']) ? ' err-ctrl' : ''),
                                'label' => 'Đối tượng có thể tham gia:',
                                "value" => isset($req['position']) ? $req['position'] : (isset($event['position']) ? $event['position'] : ''),
                                'placeholder' => 'Nhập đối tượng có thể tham gia...'
                            ]) ?>
                            <?= isset($errEvents['position']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['position'], 'name' => 'position']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('cost', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errEvents['cost']) ? ' err-ctrl' : ''),
                                'label' => 'Phí tham gia:',
                                "value" => isset($req['cost']) ? $req['cost'] : (isset($event['cost']) ? $event['cost'] : ''),
                                'placeholder' => 'Nhập phí tham gia...'
                            ]) ?>
                            <?= isset($errEvents['cost']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['cost'], 'name' => 'cost']) : '' ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="content">Chi tiết sự kiện:</label>
                            <?= $this->Form->textarea('content', [
                                'class' => 'form-control' . (isset($errEvents['flg_status']) ? ' err-ctrl' : ''),
                                'id' => 'content',
                                "value" => isset($req['content']) ? $req['content'] : (isset($event['content']) ? html_entity_decode($event['content']) : '')
                            ]) ?>
                            <?= isset($errEvents['content']) ? $this->element('Admin/errmess', ['opptions' => $errEvents['content'], 'name' => 'content']) : '' ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Thông tin seo</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <?= $this->Form->control('meta_title', [
                        'class' => 'form-control' . (isset($errSeo['meta_title']) ? ' err-ctrl' : ''),
                        'label' => 'Meta Title:',
                        "value" => isset($req['meta_title']) ? $req['meta_title'] : (isset($seo['meta_title']) ? $seo['meta_title'] : ''),
                        'placeholder' => 'Nhập meta title...'
                    ]) ?>
                    <?= isset($errSeo['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_title'], 'name' => 'meta_title']) : '' ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('meta_description', [
                        'class' => 'form-control' . (isset($errSeo['meta_description']) ? ' err-ctrl' : ''),
                        'label' => 'Meta Description:',
                        "value" => isset($req['meta_description']) ? $req['meta_description'] : (isset($seo['meta_description']) ? $seo['meta_description'] : ''),
                        'placeholder' => 'Nhập meta description...'
                    ]) ?>
                    <?= isset($errSeo['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_description'], 'name' => 'meta_description']) : '' ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('meta_keywords', [
                        'class' => 'form-control' . (isset($errSeo['meta_keywords']) ? ' err-ctrl' : ''),
                        'label' => 'Meta Keywords:',
                        "value" => isset($req['meta_keywords']) ? $req['meta_keywords'] : (isset($seo['meta_keywords']) ? $seo['meta_keywords'] : ''),
                        'placeholder' => 'Nhập meta keywords...'
                    ]) ?>
                    <?= isset($errSeo['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_keywords'], 'name' => 'meta_keywords']) : '' ?>
                </div>
                <div class="form-group">
                    <label for="tags">Thẻ Tags:</label><br />
                    <input name='tags' class="<?= (isset($errSeo['meta_keywords']) ? ' err-ctrl' : '') ?>" value="<?= isset($req['tags']) ? $req['tags'] : (isset($seo['tags']) ? $seo['tags'] : '') ?>" data-role="tagsinput" placeholder='Nhập thẻ tags...' value=''>
                    <?= isset($errSeo['tags']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['tags'], 'name' => 'tags']) : '' ?>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer" style="text-align: right">
                <a href="/admin/event/index" class="btn btn-info">Quay lại</a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu</button>
            </div>
        </div>

        <!-- /.card -->
    </div>

</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn lưu dữ liệu này không？
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

<?php $this->start('script_footer'); ?>
<script src="/admin/plugins/ckeditor/ckeditor.js"></script>
<script>
    //editor
    $(function() {
        // instance, using default configuration.
        CKEDITOR.config.height = 600;
        CKEDITOR.config.extraPlugins = 'imageuploader';
        CKEDITOR.replace('content');
    });

    //tags
    $('[name=tags]').tagify();
</script>
<?php $this->end(); ?>