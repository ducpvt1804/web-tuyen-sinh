<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Add new Event',
    'url' => [
        'Event' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Add New Event' => null
    ]
]) ?>
<?php $this->end(); ?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Event</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <?= $this->element("Admin/Form/select", ["class" => "form-control custom-select", "name" => "flg_department", "label" => "Tổ chức bởi:", "options" => $this->Department->all()]) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('title', ['class' => 'form-control', 'label' => 'Tên sự kiện:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('img_highlight', ['type' => 'file', 'class' => 'form-control', 'label' => 'Ảnh đại diện:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('address', ['type' => 'text', 'class' => 'form-control', 'label' => 'Địa chỉ diễn ra sự kiện:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('map', ['type' => 'text', 'class' => 'form-control', 'label' => 'Link Địa chỉ map:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->element("Admin/Form/select", ["class" => "form-control custom-select", "name" => "flg_status", "label" => "Tình trạng:", "options" => $this->StatusEvent->all()]) ?>
                </div>
                <div class="form-group float-left">
                    <label for="start_date">Thời gian bắt đầu:</label>
                    <input type="datetime-local" class="form-control" name="start_date">
                </div>
                <div class="form-group float-left ml-10">
                    <label for="end_date">Thời gian kết thúc</label>
                    <input type="datetime-local" class="form-control" name="end_date">
                </div>
                <div class="form-group float-left ml-10">
                    <?= $this->element("Admin/Form/select", ["class" => "form-control custom-select", "name" => "flg_language", "label" => "Ngôn ngữ:", "options" => $this->Language->all()]) ?>
                </div>
                <div class="form-group float-left">
                    <?= $this->Form->control('telno', ['type' => 'text', 'class' => 'form-control', 'label' => 'Số điện thoại:']) ?>
                </div>
                <div class="form-group float-left ml-10">
                    <?= $this->Form->control('position', ['type' => 'text', 'class' => 'form-control', 'label' => 'Đối tượng tham gia:']) ?>
                </div>
                <div class="form-group float-left ml-10">
                    <?= $this->Form->control('cost', ['type' => 'text', 'class' => 'form-control', 'label' => 'Phí tham gia:']) ?>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Seo</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <?= $this->Form->control('meta_title', ['class' => 'form-control', 'label' => 'Meta Title:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('meta_description', ['class' => 'form-control', 'label' => 'Meta Description:']) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('meta_keywords', ['class' => 'form-control', 'label' => 'Meta Keywords:']) ?>
                </div>
                <div class="form-group">
                    <label for="end_date">Thẻ Tags:</label><br />
                    <input name='tags' data-role="tagsinput" placeholder='write some tags' value=''>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Chi tiết sự kiện:</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <?= $this->Form->textarea('content', ['class' => 'form-control textarea']) ?>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
<div class="row pb-20">
    <div class="col-12">
        <a href="#" class="btn btn-secondary">Cancel</a>
        <input type="submit" value="Save" class="btn btn-success float-right">
    </div>
</div>
<?= $this->Form->end() ?>
<?php $this->start('script_footer'); ?>
<script>
    $('[name=tags]').tagify();
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<?php $this->end(); ?>