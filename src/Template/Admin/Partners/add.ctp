<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Partners' => $this->Url->build(['controller' => 'Partners', 'action' => 'index'])
  ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'Partners', 'action' => 'add'],
  'id' => 'quote-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thêm mới đối tác</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Trạng thái") ?>:</label>
        <div class="custom-control custom-radio">
          <input value="0" class="custom-control-input<?= (isset($errQuotes['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="flg_status" <?= isset($req["flg_status"]) && $req["flg_status"] == 0 ? "checked" : "" ?>>
          <label for="active" class="custom-control-label">Hoạt động</label>
        </div>
        <div class="custom-control custom-radio">
          <input value="1" class="custom-control-input<?= (isset($errQuotes['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="notactive" name="flg_status" <?= isset($req["flg_status"]) && $req["flg_status"] == 1 ? "checked" : "" ?>>
          <label for="notactive" class="custom-control-label">Không hoạt động</label>
        </div>
        <?= isset($errQuotes['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errQuotes['flg_status'], 'name' => 'flg_status']) : '' ?>
      </div>
      <div class="form-group">
        <label for="InputFileLG"><?= __("Hình ảnh") ?>:</label>
        <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'img_highlight']) ?>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="img_highlight" placeholder="Chọn ảnh" class="custom-file-input input_file" id="InputFileLG" accept="image/jpeg, image/png">
            <label class="custom-file-label<?= (isset($errQuotes['img_highlight']) ? ' err-ctrl' : '') ?>" id="img_highlight" for="InputFileLG">Chọn file</label>
          </div>
        </div>
        <?= isset($errQuotes['img_highlight']) ? $this->element('Admin/errmess', ['opptions' => $errQuotes['img_highlight'], 'name' => 'img_highlight']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Tiêu đề") ?>:</label>
        <input type="text" name="title" placeholder="Tiêu đề" value="<?= isset($req["title"]) ? $req["title"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errQuotes['title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errQuotes['title']) ? $this->element('Admin/errmess', ['opptions' => $errQuotes['title'], 'name' => 'title']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Liên kết") ?>:</label>
        <input type="text" name="link" placeholder="Liên kết" value="<?= isset($req["link "]) ? $req["link"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errQuotes['link']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errQuotes['link']) ? $this->element('Admin/errmess', ['opptions' => $errQuotes['link'], 'name' => 'link']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Mô tả") ?>:</label>
        <textarea class="form-control<?= (isset($errQuotes['description']) ? ' err-ctrl' : '') ?>" name="description" rows="5" placeholder="Mô tả" value="<?= isset($req["description"]) ? $req["description"] : "" ?>"><?= isset($req["description"]) ? $req["description"] : "" ?></textarea>
        <?= isset($errQuotes['description']) ? $this->element('Admin/errmess', ['opptions' => $errQuotes['description'], 'name' => 'description']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
    <div class="card-footer" style="text-align: right">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>

  <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>