<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Config' => null
  ]
]) ?>
<?php $this->end(); ?>
<div class="card">
  <div class="card-header">
    <h3 class="box-title">Cấu hình hệ thống</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example2" class="table table-striped">
      <thead>
        <tr>
          <th>STT</th>
          <th>Page title</th>
          <th>Title</th>
          <th>Email</th>
          <th>Address</th>
          <th>Language</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php

        if (isset($config) && count($config) > 0) :
          foreach ($config as $key => $row) :
            ?>
            <tr>
              <td><?= $key + 1 ?></td>
              <td><?= $row['page_title'] ?></td>
              <td><?= $row['name'] ?></td>
              <td><?= $row['email'] ?></td>
              <td><?= $row['address_1'] ?></td>
              <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage($row['flg_language']) ?></td>
              <td style="text-align: left; vertical-align: middle">
                <a href="<?= $this->Url->build(['controller' => 'Config', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
              </td>
            </tr>
        <?php
          endforeach;
        endif;
        ?>
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
<?php $this->start('script_footer'); ?>
<script>
  $(function() {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<?php $this->end(); ?>
