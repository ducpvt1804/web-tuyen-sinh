<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => 'Config',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Config' => null
  ]
]) ?>
<?php $this->end();
$logo_footer = $config['logo_footer'];
$logo_header = $config['logo_header'];
$req = $this->getRequest()->getData();
$config = array_merge($config->toArray(), $req); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'Config', 'action' => 'edit', $config["id"]],
  'id' => 'config-input-form'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Sửa cấu hình chung</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label><?= __("Tên trường") ?>:</label>
            <input type="text" name="name" placeholder="Tên trường" value="<?= isset($config["name"]) ? $config["name"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['name']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['name']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['name'], 'name' => 'name']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Tiêu đề trang") ?>:</label>
            <input type="text" name="page_title" placeholder="Nhập tiêu đề của trang" value="<?= isset($config["page_title"]) ? $config["page_title"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['page_title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['page_title']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['page_title'], 'name' => 'page_title']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Email") ?>:</label>
            <input type="email" name="email" placeholder="Địa chỉ email" value="<?= isset($config["email"]) ? $config["email"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['email']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['email']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['email'], 'name' => 'email']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Địa chỉ 1") ?>:</label>
            <input type="text" name="address_1" placeholder="Địa chỉ cơ sở 1" value="<?= isset($config["address_1"]) ? $config["address_1"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['address_1']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['address_1']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['address_1'], 'name' => 'address_1']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Địa chỉ 2") ?>:</label>
            <input type="text" name="address_2" placeholder="Địa chỉ cơ sở 2" value="<?= isset($config["address_2"]) ? $config["address_2"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['address_2']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['address_2']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['address_2'], 'name' => 'address_2']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Số điện thoại") ?>:</label>
            <input type="text" name="telno" placeholder="Số điện thoại" value="<?= isset($config["telno"]) ? $config["telno"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['telno']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['telno']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['telno'], 'name' => 'telno']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Fax") ?>:</label>
            <input type="text" name="fax" placeholder="Fax" value="<?= isset($config["fax"]) ? $config["fax"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['fax']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['fax']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['fax'], 'name' => 'fax']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Bản đồ") ?>:</label>
            <textarea class="form-control<?= (isset($errConfig['map']) ? ' err-ctrl' : '') ?>" name="map" value="<?= isset($config["map"]) ? h($config["map"]) : "" ?>" rows="3" placeholder="Link Google map"><?= isset($config["map"]) ? $config["map"] : "" ?></textarea>
            <?= isset($errConfig['map']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['map'], 'name' => 'map']) : '' ?>
          </div>
          <!-- /.form-group -->
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label><?= __("Ngôn ngữ") ?>:</label>
            <div class="custom-control custom-radio">
              <input disabled="disabled" class="custom-control-input<?= (isset($errConfig['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($config["flg_language"]) && $config["flg_language"] == 0 ? "checked" : "" ?>>
              <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
            </div>
            <div class="custom-control custom-radio">
              <input disabled="disabled" class="custom-control-input<?= (isset($errConfig['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($config["flg_language"]) && $config["flg_language"] == 1 ? "checked" : "" ?>>
              <label for="english" class="custom-control-label">Tiếng Anh</label>
            </div>
            <?= isset($errConfig['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['flg_language'], 'name' => 'flg_language']) : '' ?>
          </div>
          <div class="form-group">
            <label for="InputFileLG"><?= __("Logo header") ?>:</label>
            <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->logo($logo_header), 'name' => 'logo_header']) ?>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" name="logo_header" placeholder="Chọn ảnh" <?= isset($config["logo_header"]) ? "" : "" ?> class="custom-file-input input_file" id="InputFileLG" accept="image/jpeg, image/png">
                <label class="custom-file-label<?= (isset($errConfig['logo_header']) ? ' err-ctrl' : '') ?>" id="logo_header" for="InputFileLG">Chọn file</label>
              </div>
            </div>
            <?= isset($errConfig['logo_header']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['logo_header'], 'name' => 'logo_header']) : '' ?>
          </div>
          <div class="form-group">
            <label for="InputFileFT"><?= __("Logo footer") ?>:</label>
            <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->logo($logo_footer), 'name' => 'logo_footer']) ?>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" name="logo_footer" placeholder="Chọn ảnh" <?= isset($config["logo_footer"]) ? "" : "" ?> class="custom-file-input input_file" id="InputFileFT" accept="image/jpeg, image/png">
                <label class="custom-file-label<?= (isset($errConfig['logo_footer']) ? ' err-ctrl' : '') ?>" id="logo_footer" for="InputFileFT">Chọn file</label>
              </div>
            </div>
            <?= isset($errConfig['logo_footer']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['logo_footer'], 'name' => 'logo_footer']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Link Facebook") ?>:</label>
            <input type="text" name="facebook_address" placeholder="Nhập địa chỉ Facebook" value="<?= isset($config["facebook_address"]) ? $config["facebook_address"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['facebook_address']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['facebook_address']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['facebook_address'], 'name' => 'facebook_address']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Link Twitter") ?>:</label>
            <input type="text" name="twitter_address" placeholder="Nhập địa chỉ Twitter" value="<?= isset($config["twitter_address"]) ? $config["twitter_address"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['twitter_address']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['twitter_address']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['twitter_address'], 'name' => 'twitter_address']) : '' ?>
          </div>
          <div class="form-group">
            <label><?= __("Link Pinterest") ?>:</label>
            <input type="text" name="pinterest_address" placeholder="Nhập địa chỉ Pinterest" value="<?= isset($config["pinterest_address"]) ? $config["pinterest_address"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['pinterest_address']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
            <?= isset($errConfig['pinterest_address']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['pinterest_address'], 'name' => 'pinterest_address']) : '' ?>
          </div>
          <!-- /.form-group -->
        </div>
      </div>
      <div>
        <?= isset($config["map"]) ? $config["map"] : "" ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.card-body -->
</div>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">About</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Tiêu đề") ?>:</label>
        <input type="text" name="about_title" placeholder="Về Trường Đại Học Đại Nam" value="<?= isset($config["about_title"]) ? $config["about_title"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['about_title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errConfig['about_title']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['about_title'], 'name' => 'about_title']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Mô tả") ?>:</label>
        <textarea class="form-control<?= (isset($errConfig['about_desc']) ? ' err-ctrl' : '') ?>" name="about_desc" rows="3" placeholder="Mô tả" value="<?= isset($config["about_desc"]) ? $config["about_desc"] : "" ?>"><?= isset($config["about_desc"]) ? $config["about_desc"] : "" ?></textarea>
        <?= isset($errConfig['about_desc']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['about_desc'], 'name' => 'about_desc']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Thành lập") ?>:</label>
        <input type="number" name="establish" placeholder="Năm thành lập" value="<?= isset($config["establish"]) ? $config["establish"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['establish']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errConfig['establish']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['establish'], 'name' => 'establish']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Sinh viên") ?>:</label>
        <input type="number" name="total_student" placeholder="Số sinh viên của trường" value="<?= isset($config["total_student"]) ? $config["total_student"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['total_student']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errConfig['total_student']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['total_student'], 'name' => 'total_student']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Phần trăm sinh viên có việc làm") ?>:</label>
        <input type="number" name="percent_have_job" placeholder="Phần trăm sinh viên có việc làm" value="<?= isset($config["percent_have_job"]) ? $config["percent_have_job"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errConfig['percent_have_job']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errConfig['percent_have_job']) ? $this->element('Admin/errmess', ['opptions' => $errConfig['percent_have_job'], 'name' => 'percent_have_job']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thông tin SEO</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Meta title") ?>:</label>
        <input type="text" name="meta_title" placeholder="Nhập Meta title" value="<?= isset($config["meta_title"]) ? $config["meta_title"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['flg_status']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errSeo['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['flg_status'], 'name' => 'flg_status']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Meta description") ?>:</label>
        <input type="text" name="meta_description" placeholder="Nhập Meta description" value="<?= isset($config["meta_description"]) ? $config["meta_description"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['flg_status']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errSeo['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['flg_status'], 'name' => 'flg_status']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Meta keywords") ?>:</label>
        <textarea class="form-control<?= (isset($errSeo['flg_status']) ? ' err-ctrl' : '') ?>" name="meta_keywords" rows="3" placeholder="Nhập Meta keywords" value="<?= isset($config["meta_keywords"]) ? $config["meta_keywords"] : "" ?>"><?= isset($config["meta_keywords"]) ? $config["meta_keywords"] : "" ?></textarea>
        <?= isset($errSeo['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['flg_status'], 'name' => 'flg_status']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="card-footer" style="text-align: right">
      <a href="javascript:history.back();" class="btn btn-info">Trờ về</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>
  <!-- /.row -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>