<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'ListDepartmentspb' => $this->Url->build(['controller' => 'ListDepartmentspb', 'action' => 'index'])
  ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'post',
  'url' => ['controller' => 'ListDepartmentspb', 'action' => 'add'],
  'id' => 'lstdepartments-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thêm mới Khoa</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label><?= __("Ngôn ngữ") ?>:</label>
                    <div class="custom-control custom-radio">
                        <input value="0" checked <?= isset($req["flg_language"]) && $req["flg_language"] == 0 ? "checked" : "" ?> class="custom-control-input <?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($inputDepartment["flg_language"]) && $inputDepartment["flg_language"] == 0 ? "checked" : "" ?>>
                        <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input value="1" <?= isset($req["flg_language"]) && $req["flg_language"] == 1 ? "checked" : "" ?> class="custom-control-input <?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($inputDepartment["flg_language"]) && $inputDepartment["flg_language"] == 1 ? "checked" : "" ?>>
                        <label for="english" class="custom-control-label">Tiếng Anh</label>
                    </div>
                    <?= isset($errDepartments['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_language'], 'name' => 'flg_language']) : '' ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label><?= __("Trạng thái") ?>:</label>
                    <div class="custom-control custom-radio">
                        <input value="0" checked <?= isset($req["flg_status"]) && $req["flg_status"] == 0 ? "checked" : '' ?> class="custom-control-input<?= (isset($errListDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="flg_status">
                        <label for="active" class="custom-control-label">Hoạt động</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input value="1" <?= isset($req["flg_status"]) && $req["flg_status"] == 0 ? "checked" : '' ?> class="custom-control-input<?= (isset($errListDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="notactive" name="flg_status">
                        <label for="notactive" class="custom-control-label">Không hoạt động</label>
                    </div>
                    <?= isset($errListDepartments['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errListDepartments['flg_status'], 'name' => 'flg_status']) : '' ?>
                </div>
            </div>
        </div>
      <div class="form-group">
        <label><?= __("Tiêu đề") ?>:</label>
        <input type="text" name="title" placeholder="Tiêu đề" value="<?= isset($req["title"]) ? $req["title"] : '' ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errListDepartments['title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errListDepartments['title']) ? $this->element('Admin/errmess', ['opptions' => $errListDepartments['title'], 'name' => 'title']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
    <div class="card-footer" style="text-align: right">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>

  <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>
