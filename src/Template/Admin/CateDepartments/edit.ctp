<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => '',
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'CateDepartments' => $this->Url->build(['controller' => 'CateDepartments', 'action' => 'index'])
    ]
]) ?>
<?php $this->end();
$cateDepartmests = array_merge($cateDepartmests->toArray(), $this->getRequest()->getData());
$seo = array_merge($seo->toArray(), $this->getRequest()->getData());
?>
<?= $this->AppForm->create(null, [
    'type' => 'file',
    'url' => ['controller' => 'CateDepartments', 'action' => 'edit', $cateDepartmests['id']],
    'id' => 'departments-add'
]) ?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Thêm mới danh mục tin</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i>
                </button>
            </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <div class="form-group">
                <label><?= __("Khoa") ?></label>
                <select name="department_id" class="custom-select<?= (isset($errDepartments['department_id']) ? ' err-ctrl' : '') ?>">
                    <option>Chọn Khoa</option>
                    <?php foreach ($lstDepartments as $value) { ?>
                        <option <?= isset($cateDepartmests["department_id"]) && $cateDepartmests["department_id"] == $value['id'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
                    <?php } ?>
                </select>
                <?= isset($errDepartments['department_id']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['department_id'], 'name' => 'department_id']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Tiêu đề") ?>:</label>
                <input type="text" name="title" value="<?= isset($cateDepartmests["title"]) ? $cateDepartmests["title"] : "" ?>" placeholder="Tiêu đề" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errDepartments['title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                <?= isset($errDepartments['title']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['title'], 'name' => 'title']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Ngôn ngữ") ?>:</label>
                <div class="custom-control custom-radio">
                    <input value="0" class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($cateDepartmests["flg_language"]) && $cateDepartmests["flg_language"] == 0 ? "checked" : "" ?>>
                    <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
                </div>
                <div class="custom-control custom-radio">
                    <input value="1" class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($cateDepartmests["flg_language"]) && $cateDepartmests["flg_language"] == 1 ? "checked" : "" ?>>
                    <label for="english" class="custom-control-label">Tiếng Anh</label>
                </div>
                <?= isset($errDepartments['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_language'], 'name' => 'flg_language']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Hiển thị trên menu header") ?>:</label>
                <div class="custom-control custom-radio">
                    <input value="0" class="custom-control-input<?= (isset($errDepartments['flg_showheader']) ? ' err-ctrl' : '') ?>" type="radio" id="showheader" name="flg_showheader" <?= isset($cateDepartmests["flg_showheader"]) && $cateDepartmests["flg_showheader"] == 0 ? "checked" : "" ?>>
                    <label for="showheader" class="custom-control-label">Hiển thị</label>
                </div>
                <div class="custom-control custom-radio">
                    <input value="1" class="custom-control-input<?= (isset($errDepartments['flg_showheader']) ? ' err-ctrl' : '') ?>" type="radio" id="notshowheader" name="flg_showheader" <?= isset($cateDepartmests["flg_showheader"]) && $cateDepartmests["flg_showheader"] == 1 ? "checked" : "" ?>>
                    <label for="notshowheader" class="custom-control-label">Không hiển thị</label>
                </div>
                <?= isset($errDepartments['flg_showheader']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_showheader'], 'name' => 'flg_showheader']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Trạng thái") ?>:</label>
                <div class="custom-control custom-radio">
                    <input value="0" <?= isset($cateDepartmests["flg_status"]) && $cateDepartmests["flg_status"] == 0 ? "checked" : "" ?> class="custom-control-input<?= (isset($errDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="flg_status">
                    <label for="active" class="custom-control-label">Hoạt động</label>
                </div>
                <div class="custom-control custom-radio">
                    <input value="1" <?= isset($cateDepartmests["flg_status"]) && $cateDepartmests["flg_status"] == 1 ? "checked" : "" ?> class="custom-control-input<?= (isset($errDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="notactive" name="flg_status">
                    <label for="notactive" class="custom-control-label">Không hoạt động</label>
                </div>
                <?= isset($errDepartments['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_status'], 'name' => 'flg_status']) : '' ?>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.card-body -->
</div>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Thông tin SEO</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="form-group">
                <label><?= __("Meta title") ?>:</label>
                <input type="text" name="meta_title" value="<?= isset($seo["meta_title"]) ? $seo["meta_title"] : "" ?>" placeholder="Nhập Meta title" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['meta_title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                <?= isset($errSeo['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_title'], 'name' => 'meta_title']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Meta description") ?>:</label>
                <input type="text" name="meta_description" value="<?= isset($seo["meta_description"]) ? $seo["meta_description"] : "" ?>" placeholder="Nhập Meta description" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['meta_description']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                <?= isset($errSeo['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_description'], 'name' => 'meta_description']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Meta keywords") ?>:</label>
                <textarea class="form-control<?= (isset($errSeo['meta_keywords']) ? ' err-ctrl' : '') ?>" name="meta_keywords" rows="3" placeholder="Nhập Meta keywords"><?= isset($seo["meta_keywords"]) ? $seo["meta_keywords"] : "" ?></textarea>
                <?= isset($errSeo['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_keywords'], 'name' => 'meta_keywords']) : '' ?>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col -->
        <div class="card-footer" style="text-align: right">
            <a href="javascript:history.back();" class="btn btn-info">Quay lại</a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
            </button>
        </div>
    </div>
    <!-- /.row -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn lưu dữ liệu này không？
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>