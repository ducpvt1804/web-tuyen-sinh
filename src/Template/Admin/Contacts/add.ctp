<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Quotes' => $this->Url->build(['controller' => 'Quotes', 'action' => 'index'])
  ]
]) ?>
<?php $this->end(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'Quotes', 'action' => 'add'],
  'id' => 'quote-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thêm mới Quote</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Ngôn ngữ") ?>:</label>
        <div class="custom-control custom-radio">
          <input value="0" required class="custom-control-input" type="radio" id="vietnamese" name="flg_language" <?= isset($input["flg_language"]) && $input["flg_language"] == 0 ? "checked" : "" ?>>
          <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
        </div>
        <div class="custom-control custom-radio">
          <input value="1" required class="custom-control-input" type="radio" id="english" name="flg_language" <?= isset($input["flg_language"]) && $input["flg_language"] == 1 ? "checked" : "" ?>>
          <label for="english" class="custom-control-label">Tiếng Anh</label>
        </div>
      </div>
      <div class="form-group">
        <label><?= __("Trạng thái") ?>:</label>
        <div class="custom-control custom-radio">
          <input value="0" required class="custom-control-input" type="radio" id="active" name="flg_status" <?= isset($input["flg_status"]) && $input["flg_status"] == 0 ? "checked" : "" ?>>
          <label for="active" class="custom-control-label">Hoạt động</label>
        </div>
        <div class="custom-control custom-radio">
          <input value="1" required class="custom-control-input" type="radio" id="notactive" name="flg_status" <?= isset($input["flg_status"]) && $input["flg_status"] == 1 ? "checked" : "" ?>>
          <label for="notactive" class="custom-control-label">Không hoạt động</label>
        </div>
      </div>
      <div class="form-group">
        <label><?= __("Loại") ?>:</label>
        <div class="custom-control custom-radio">
          <input value="0" required class="custom-control-input" type="radio" id="teacher" name="cate" <?= isset($input["cate"]) && $input["cate"] == 0 ? "checked" : "" ?>>
          <label for="teacher" class="custom-control-label">Giảng Viên</label>
        </div>
        <div class="custom-control custom-radio">
          <input value="1" required class="custom-control-input" type="radio" id="student" name="cate" <?= isset($input["cate"]) && $input["cate"] == 1 ? "checked" : "" ?>>
          <label for="student" class="custom-control-label">Sinh viên</label>
        </div>
      </div>
      <div class="form-group">
        <label for="InputFileLG"><?= __("Hình ảnh") ?>:</label>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="img_highlight" placeholder="Chọn ảnh" required class="custom-file-input" id="InputFileLG" accept="image/jpeg, image/png">
            <label class="custom-file-label" for="InputFileLG">Chọn file</label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label><?= __("Tiêu đề") ?>:</label>
        <input type="text" name="title" placeholder="Tiêu đề" value="<?= isset($input["title"]) ? $input["title"] : "" ?>" required class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title="">
      </div>
      <div class="form-group">
        <label><?= __("Chức danh") ?>:</label>
        <input type="text" name="position" placeholder="Chức danh" value="<?= isset($input["position"]) ? $input["position"] : "" ?>" required class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title="">
      </div>
      <div class="form-group">
        <label><?= __("Nội dung") ?>:</label>
        <textarea class="form-control" name="content" rows="5" placeholder="Nội dung" value="<?= isset($input["content"]) ? $input["content"] : "" ?>"><?= isset($input["content"]) ? $input["content"] : "" ?></textarea>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
    <div class="card-footer" style="text-align: right">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>

  <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>