<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Contacts' => $this->Url->build(['controller' => 'Contacts', 'action' => 'index'])
  ]
]) ?>
<?php $this->end(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'Contacts', 'action' => 'view', $contact['id']],
  'id' => 'quote-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Chi tiết liên hệ - góp ý</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label style="width: 100px; margin-right: 10px"><?= __("Người gửi") ?>: </label><?= isset($contact["name"]) ? $contact["name"] : "" ?>
      </div>
      <div class="form-group">
        <label style="width: 100px; margin-right: 10px"><?= __("Email") ?>: </label><?= isset($contact["email"]) ? $contact["email"] : "" ?>
      </div>
    <div class="form-group">
        <label style="width: 100px; margin-right: 10px"><?= __("Số điện thoại") ?>: </label><?= isset($contact["telno"]) ? $contact["telno"] : "" ?>
      </div>
      <div class="form-group">
        <label style="width: 100px; margin-right: 10px"><?= __("Tiêu đề") ?>: </label><?= isset($contact["title"]) ? $contact["title"] : "" ?>
      </div>
      <div class="form-group">
        <label style="width: 100px; margin-right: 10px"><?= __("Ngày gửi") ?>: </label><?= isset($contact["create_date"]) ? $this->Time->format($contact["create_date"], 'HH:mm:ss dd-MM-yyyy') : "" ?>
      </div>
      <div class="form-group">
        <label><?= __("Nội dung") ?>:</label>
        <div class="input-group">
          <?= isset($contact["content"]) ? html_entity_decode($contact["content"]) : "" ?>
        </div>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
    <div class="card-footer" style="text-align: right">
      <a href="javascript:history.back();" class="btn btn-info">Quay lại</a>
      <!--          <a class="btn btn-primary" href="">Trả lời</a>-->
      <!--          = $this->Url->build(['controller' => 'Contacts', 'action' => 'view', $contact['id']])-->
    </div>
  </div>

  <!-- /.card-body -->
</div>
<?= $this->Form->end() ?>
