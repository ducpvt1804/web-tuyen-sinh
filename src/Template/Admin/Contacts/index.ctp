<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'Contacts' => $this->Url->build(['controller' => 'Contacts', 'action' => 'index'])
    ]
]) ?>
<?php $this->end(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm liên hệ - góp ý</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->AppForm->create(null, [
                'type' => 'post',
                'url' => ['controller' => 'Contacts', 'action' => 'index'],
                'id' => 'search-quotes'
            ]) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <select name="flg_read" class="custom-select">
                                <option value="">Trạng thái</option>
                                <option <?= isset($inputSearch["flg_read"]) && $inputSearch["flg_read"] == 1 ? "selected=\"selected\"" : "" ?> value="1">Đã đọc
                                </option>
                                <option <?= isset($inputSearch["flg_read"]) && $inputSearch["flg_read"] == 2 ? "selected=\"selected\"" : "" ?> value="2">Chưa đọc
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <input value="<?= isset($inputSearch["keyword"]) ? $inputSearch["keyword"] : "" ?>" name="keyword" type="text" class="form-control" placeholder="Nhập từ khóa tìm kiếm...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách liên hệ - góp ý</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body no-padding">
        <table id="example2" class="table table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Người gửi</th>
                    <th>Tiêu đề</th>
                    <th>Nội dung</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php

                if (isset($contact) && count($contact) > 0) :
                    foreach ($contact as $key => $row) :
                        ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><?= $row['name'] ?></td>
                            <td><?= $row['title'] ?></td>
                            <td width="40%"><?= mb_strimwidth($row['content'], 0, 120, "...") ?></td>
                            <td style="text-align: left; vertical-align: middle;"><?= $this->Common->getStatusRead($row['flg_read']) ?></td>
                            <td style="text-align: left; vertical-align: middle">
                                <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'view', $row['id']]) ?>" class="btn btn-primary btn-sm">View</a>
                                <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                </button>
                            </td>
                        </tr>
                <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>
<!-- /.card-body -->

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa bản ghi này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/categories.js"></script>
<?php $this->start('script_footer'); ?>
<script>
    $(function() {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
