<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Banner',
    'url' => [
        'Banner' => $this->Url->build(['controller' => 'Banner', 'action' => 'index']),
        'Danh sách banner' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->Form->create() ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "flg_language",
                                "default" => "Ngôn ngữ",
                                "options" => $this->Language->all(),
                                "selected" => isset($req['flg_language']) ? $req['flg_language'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "flg_status",
                                "default" => "Trạng thái",
                                "options" => $this->StatusBanner->all(),
                                "selected" => isset($req['flg_status']) ? $req['flg_status'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "mode",
                                "default" => "Giao diện hiển thị",
                                "options" => $this->ModeBanner->all(),
                                "selected" => isset($req['mode']) ? $req['mode'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" name="keyword" value="<?= isset($req['keyword']) ? $req['keyword'] : '' ?>" class="form-control" placeholder="Nhập từ khóa cần tìm kiếm...">
                        </div>
                    </div>

                </div>

            </div>
            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'add']) ?>" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách banner</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="card-body no-padding">
            <table id="example2" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Title</th>
                        <th>Giao diện hiển thị</th>
                        <th>Ngôn ngữ</th>
                        <th>Hiển thị</th>
                        <th>Số lượng ảnh</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($banner) && count($banner) > 0) :
                        foreach ($banner as $key => $row) :
                            ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $row['title'] ?></td>
                                <td><?= $this->ModeBanner->getTitle($row['mode']) ?></td>
                                <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $row['flg_language']) ?></td>
                                <td style="text-align: left; vertical-align: middle">
                                    <label class="switch ">
                                        <input type="checkbox" class="success status" recordId="<?= $row['id'] ?>" <?php if ($row['flg_status'] == 1) { ?> checked='checked' <?php } ?> />
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td><?= $this->Common->countBanner($row['id']) ?></td>
                                <td style="text-align: left; vertical-align: middle">
                                    <a href="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                    </button>
                                </td>
                            </tr>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa quote này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/categories.js"></script>
<?php $this->start('script_footer'); ?>
<script>
    $(".status").change(function() {
        let id = $(this).attr("recordId")
        location.href = "/admin/banner/status/" + id;
    });
    $(function() {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
