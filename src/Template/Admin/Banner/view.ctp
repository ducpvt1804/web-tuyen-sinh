<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Banner $banner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Banner'), ['action' => 'edit', $banner->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Banner'), ['action' => 'delete', $banner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $banner->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Banner'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Banner'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Detail Banner'), ['controller' => 'DetailBanner', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Detail Banner'), ['controller' => 'DetailBanner', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="banner view large-9 medium-8 columns content">
    <h3><?= h($banner->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($banner->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($banner->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mode') ?></th>
            <td><?= $this->Number->format($banner->mode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Status') ?></th>
            <td><?= $this->Number->format($banner->flg_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Site') ?></th>
            <td><?= $this->Number->format($banner->flg_site) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Language') ?></th>
            <td><?= $this->Number->format($banner->flg_language) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Delete') ?></th>
            <td><?= $this->Number->format($banner->flg_delete) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Date') ?></th>
            <td><?= h($banner->create_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Date') ?></th>
            <td><?= h($banner->update_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Detail Banner') ?></h4>
        <?php if (!empty($banner->detail_banner)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Banner Id') ?></th>
                <th scope="col"><?= __('Img') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($banner->detail_banner as $detailBanner): ?>
            <tr>
                <td><?= h($detailBanner->id) ?></td>
                <td><?= h($detailBanner->banner_id) ?></td>
                <td><?= h($detailBanner->img) ?></td>
                <td><?= h($detailBanner->create_date) ?></td>
                <td><?= h($detailBanner->update_date) ?></td>
                <td><?= h($detailBanner->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DetailBanner', 'action' => 'view', $detailBanner->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DetailBanner', 'action' => 'edit', $detailBanner->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DetailBanner', 'action' => 'delete', $detailBanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailBanner->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
