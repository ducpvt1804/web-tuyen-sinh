<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Thêm mới banner',
    'url' => [
        'Banner' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Thêm mới banner' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Thông tin banner</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'class' => (isset($errBanner['mode']) ? ' err-ctrl' : ''),
                                'name' => 'mode',
                                'label' => 'Giao diện hiển thị:',
                                'options' => $this->ModeBanner->all(),
                                'checked' => isset($req['mode']) ? $req['mode'] : ''
                            ]) ?>
                            <?= isset($errBanner['mode']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['mode'], 'name' => 'mode']) : '' ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'class' => (isset($errBanner['flg_status']) ? ' err-ctrl' : ''),
                                'name' => 'flg_status',
                                'label' => 'Trạng thái:',
                                'options' => $this->StatusBanner->all(),
                                'checked' => isset($req['flg_status']) ? $req['flg_status'] : ''
                            ]) ?>
                            <?= isset($errBanner['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['flg_status'], 'name' => 'flg_status']) : '' ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'class' => (isset($errBanner['flg_language']) ? ' err-ctrl' : ''),
                                'name' => 'flg_language',
                                'label' => 'Ngôn Ngữ:',
                                'options' => $this->Language->all(),
                                'checked' => isset($req['flg_language']) ? $req['flg_language'] : ''
                            ]) ?>
                            <?= isset($errBanner['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['flg_language'], 'name' => 'flg_language']) : '' ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <?= $this->Form->control('title', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errBanner['title']) ? ' err-ctrl' : ''),
                                'label' => 'Tên banner: ',
                                'placeholder' => 'Nhập tên banner...'
                            ]) ?>
                            <?= isset($errBanner['title']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['title'], 'name' => 'title']) : '' ?>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label>Ảnh:</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input name="addimg" type="file" id="input_adding" multiple="multiple" class="custom-file-input" accept="image/jpeg, image/png">
                                        <label class="custom-file-label <?= (isset($errBanner['addimg']) ? ' err-ctrl' : '') ?>" id="addimg" for="addimg">Chọn ảnh...</label>
                                    </div>
                                </div>
                                <img id="img_highlight" style="max-height: 200px; margin-top: 10px; display: none" src="" alt="your image" />
                                <?= isset($errBanner['addimg']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['addimg'], 'name' => 'addimg']) : '' ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Hình ảnh</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body" id="list-img">
                <?php
                if (isset($lstImg) && count($lstImg) > 0) {
                    foreach ($lstImg as $item) {
                        echo $this->element("Admin/bannerImg", ['img_name' => $item, 'folder' => 'ram/']);
                    }
                } else {
                    echo "Chưa có ảnh nào.";
                }
                ?>
            </div>
            <div class="card-footer" style="text-align: right">
                <a href="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
                <button type="submit" class="btn btn-primary" onclick="return confirm('Bạn có chắc chắn muốn lưu bản ghi không？')">Lưu</button>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

</div>
<?= $this->Form->end() ?>



<?php $this->start('script_footer'); ?>
<script src="/admin/plugins/ckeditor/ckeditor.js"></script>
<script>
    $("#input_adding").change(function() {
        let frm = new FormData();
        let countfile = $("input[name='addimg']")[0].files.length;
        for (let i = 0; i < countfile; i++) {
            let file = $("input[name='addimg']")[0].files[i];
            frm.append('file[]', file);
        }
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_csrfToken"]').val()
            },
            url: '<?= $this->Url->build(['controller' => 'Banner', 'action' => 'saveImgToRam']) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(function(rp) {
            if (rp) {
                $('#list-img').html(rp);
            } else {
                alert("Không load được một số hình ảnh. vui lòng thử lại.");
            }
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        });
    });

    function imgClose(name) {
        let frm = new FormData();
        frm.append('img_name', name);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_csrfToken"]').val()
            },
            url: '<?= $this->Url->build(['controller' => 'Banner', 'action' => 'removeImgToRam']) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(function(rp) {
            $('#list-img').html(rp);
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        });
    }
</script>
<?php $this->end(); ?>