<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Chỉnh sửa banner',
    'url' => [
        'Banner' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Chỉnh sửa banner' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData();
?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Tên banner:</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'name' => 'mode',
                                'label' => 'Giao diện hiển thị:',
                                'options' => $this->ModeBanner->all(),
                                'checked' => isset($req['mode']) ? $req['mode'] : (isset($banner['mode']) ? $banner['mode'] : '')
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'name' => 'flg_status',
                                'label' => 'Trạng thái:',
                                'options' => $this->StatusBanner->all(),
                                'checked' => isset($req['flg_status']) ? $req['flg_status'] : (isset($banner['flg_status']) ? $banner['flg_status'] : '')
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'name' => 'flg_language',
                                'label' => 'Ngôn Ngữ:',
                                'options' => $this->Language->all(),
                                'checked' => isset($req['flg_language']) ? $req['flg_language'] : (isset($banner['flg_language']) ? $banner['flg_language'] : '')
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <?= $this->Form->control('title', [
                                'type' => 'text',
                                'class' => 'form-control' . (isset($errBanner['title']) ? ' err-ctrl' : ''),
                                'label' => 'Title: ',
                                'placeholder' => 'Nhập title banner...',
                                'value' => isset($req['title']) ? $req['title'] : (isset($banner['title']) ? $banner['title'] : '')
                            ]) ?>
                            <?= isset($errBanner['title']) ? $this->element('Admin/errmess', ['opptions' => $errBanner['title'], 'name' => 'title']) : '' ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="card-footer" style="text-align: left">
                            <a href="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
                            <button type="submit" class="btn btn-primary" onclick="return confirm('Bạn có chắc chắn muốn lưu bản ghi không？')">Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Hình ảnh</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body" id="list-img">
                <?php
                if (isset($lstImg) && count($lstImg) > 0) {
                    foreach ($lstImg as $item) {
                        echo $this->element("Admin/bannerImg", ['img_name' => $item['img'], 'item' => $item]);
                    }
                } else {
                    echo "Chưa có ảnh nào.";
                }
                ?>
            </div>
            <div class="card-footer" style="text-align: left">
                <button type="button" data-toggle="modal" data-target="#add_detailModal" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</button>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

</div>
<?= $this->Form->end() ?>

<div class="modal fade bd-example-modal-lg" id="add_detailModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Chỉnh sửa banner detail </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mess_notifi_add">

                </div>
                <div class="form-group">
                    <?= $this->Form->control('title', [
                        'banner_id' => $banner['id'],
                        'id' => 'title_add',
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'placeholder' => 'Nhập alt seo banner...',
                    ]) ?>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="custom-file">
                            <input name="img2" type="file" class="custom-file-input input_file" id="file_add" accept="image/jpeg, image/png">
                            <label class="custom-file-label" for="file_add">Thay ảnh...</label>
                        </div>
                    </div>
                    <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'img2']) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="adbanner_detail" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="edit_detailModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Chỉnh sửa banner detail </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal_content">

            </div>
            <div class="modal-footer">
                <button type="button" id="upbanner_detail" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa không?
            </div>
            <div class="modal-footer">
                <button type="button" onclick="deleteDetail()" class="btn btn-danger mr-2 btn-yes" id="urlDelete">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<?php $this->start('script_footer'); ?>
<script>
    $(document).on('click', '#adbanner_detail', function() {
        let title = $('#title_add').val();
        let banner_id = $('#title_add').attr('banner_id');
        let file = document.getElementById('file_add').files[0];
        let frm = new FormData();
        frm.append('title', title);
        frm.append('file', file);
        frm.append('banner_id', banner_id);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_csrfToken"]').val()
            },
            url: '<?= $this->Url->build(['controller' => 'Banner', 'action' => 'add_detail']) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(async function(rp) {
            await new Promise((resolve, reject) => {
                resolve(rp);
            }).then(res => {
                if (res) {
                    $("#list-img").html(res);
                    $(".mess_notifi_add").html('<div class="message success" onload="setTimeout(function(){this.style.display = "none";}, 3000)" onclick="this.style.display = "none"">Thêm banner detail thành công.</div>')
                } else {
                    $(".mess_notifi_add").html('<div class="message error" onload="setTimeout(function(){this.style.display = "none";}, 3000)" onclick="this.style.display = "none"">Thêm banner detail không thành công. Vui lòng kiểm tra lại</div>');
                }
            });
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        });
    });

    $(document).on('click', '#upbanner_detail', function() {
        let title = $('#title_update').val();
        let id = $('#title_update').attr('detail_id');
        let file = document.getElementById('file_update').files[0];
        let frm = new FormData();
        frm.append('id', id);
        frm.append('title', title);
        frm.append('file', file);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_csrfToken"]').val()
            },
            url: '<?= $this->Url->build(['controller' => 'Banner', 'action' => 'save_detail']) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(async function(rp) {
            await new Promise((resolve, reject) => {
                resolve(rp);
            }).then(res => {
                if (res) {
                    $("#list-img").html(res);
                    $(".mess_notifi_update").html('<div class="message success" onload="setTimeout(function(){this.style.display = "none";}, 3000)" onclick="this.style.display = "none"">Lưu sửa đổi thành công.</div>')
                } else {
                    $(".mess_notifi_update").html('<div class="message error" onload="setTimeout(function(){this.style.display = "none";}, 3000)" onclick="this.style.display = "none"">Lưu sửa đổi không thành công. Vui lòng kiểm tra lại</div>');
                }
            });
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        });
    });

    async function deleteCf(_url) {
        await new Promise((resolve, reject) => {
            resolve(_url);
        }).then(res => {
            $('#urlDelete').attr('_url', _url);
        });
        $("#confirmModal").modal('show');
    }

    $(document).on('click', "#urlDelete", function() {
        let _this = $(this);
        let _url = _this.attr("_url");
        if (_url) {
            $.ajax({
                url: _url,
                type: 'get',
                contentType: false,
                processData: false,
                cache: false
            }).done(async function(rp) {
                await new Promise((resolve, reject) => {
                    resolve(rp);
                }).then(res => {
                    if (res) {
                        $("#list-img").html(res);
                        $("#confirmModal").modal('hide');
                    } else {
                        alert("Đã có lỗi xảy ra. vui lòng thử lại.");
                    }
                });
            }).fail(function() {
                alert("Đã có lỗi xảy ra. vui lòng thử lại.");
            });
        } else {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        }

    });


    function showEdit(id) {
        let frm = new FormData();
        frm.append('id', id);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_csrfToken"]').val()
            },
            url: '<?= $this->Url->build(['controller' => 'Banner', 'action' => 'edit_detail']) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(async function(rp) {
            await new Promise((resolve, reject) => {
                resolve(rp);
            }).then(res => {
                $("#modal_content").html(res);
            });
            $("#edit_detailModal").modal('show');
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        });
    }
</script>
<?php $this->end(); ?>