<div class="mess_notifi_update">

</div>
<div class="form-group">
    <?= $this->Form->control('title', [
        'detail_id' => isset($bannerDetail['id']) ? $bannerDetail['id'] : '',
        'id' => 'title_update',
        'type' => 'text',
        'class' => 'form-control',
        'label' => false,
        'placeholder' => 'Sửa alt seo banner...',
        'value' => isset($bannerDetail['title']) ? $bannerDetail['title'] : ''
    ]) ?>
</div>
<div class="form-group">
    <div class="input-group">
        <div class="custom-file">
            <input name="img" type="file" class="custom-file-input input_file" id="file_update" accept="image/jpeg, image/png">
            <label class="custom-file-label" for="file_update">Thay ảnh...</label>
        </div>
    </div>
    <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->banner($bannerDetail['img']), 'name' => 'img']) ?>
</div>