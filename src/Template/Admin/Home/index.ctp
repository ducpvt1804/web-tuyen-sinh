<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => 'Dashboard',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Dashboard' => null
  ]
]) ?>
<?php $this->end(); ?>

<div class="row">
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
          <?php if ($department > 0 && isset($department)): ?>
              <h3><?= $department ?></h3>
          <?php else: ?>
              <h3>0</h3>
          <?php endif; ?>
        <p>Số khoa</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?= $this->Url->build(['controller' => 'ListDepartments', 'action' => 'index']) ?>" class="small-box-footer">Thông tin thêm <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
          <?php if ($list_department > 0 && isset($list_department)): ?>
              <h3><?= $list_department ?></h3>
          <?php else: ?>
              <h3>0</h3>
          <?php endif; ?>
        <p>Số phòng</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="<?= $this->Url->build(['controller' => 'ListDepartmentspb', 'action' => 'index']) ?>" class="small-box-footer">Thông tin thêm <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-blue">
      <div class="inner">
          <?php if ($event > 0 && isset($event)): ?>
              <h3><?= $event ?></h3>
          <?php else: ?>
              <h3>0</h3>
          <?php endif; ?>

        <p>Số sự kiện</p>
      </div>
      <div class="icon">
          <i class="fa fa-calendar-week"></i>
      </div>
      <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'index']) ?>" class="small-box-footer">Thông tin thêm <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-danger">
      <div class="inner">
          <?php if ($news > 0 && isset($news)): ?>
              <h3><?= $news ?></h3>
          <?php else: ?>
              <h3>0</h3>
          <?php endif; ?>

        <p>Số tin tức</p>
      </div>
      <div class="icon">
          <i class="fa fa-newspaper"></i>
      </div>
      <a href="javascript:void(0)" class="small-box-footer">Thông tin thêm <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
</div>
<!-- /.row -->

