<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => '',
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'EnrollmentOptions' => $this->Url->build(['controller' => 'EnrollmentOptions', 'action' => 'index'])
    ]
]) ?>
<?php $this->end(); ?>
<?= $this->AppForm->create(null, [
    'type' => 'post',
    'url' => ['controller' => 'EnrollmentOptions', 'action' => 'edit', $options["id"]],
    'id' => 'quote-add'
]) ?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Thêm mới</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">
                    <label><?= __("Danh mục") ?>:</label>
                    <select id="optionselector" name="type" class="custom-select<?= (isset($errEnroll['type']) ? ' err-ctrl' : '') ?>">
                        <option value="">Loại đăng ký</option>
                        <option <?= isset($options["type"]) && $options["type"] == 1 ? "selected=\"selected\"" : "" ?> value="1">Đại học chính quy</option>
                        <option <?= isset($options["type"]) && $options["type"] == 2 ? "selected=\"selected\"" : "" ?> value="2">Liên thông</option>
                        <option <?= isset($options["type"]) && $options["type"] == 3 ? "selected=\"selected\"" : "" ?> value="3">Cập nhật kiến thức chuyên môn về dược</option>
                    </select>
                    <?= isset($errEnroll['type']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['type'], 'name' => 'type']) : '' ?>
                </div>
            </div>
            <div class="form-group">
                <label><?= __("Trạng thái") ?>:</label>
                <div class="custom-control custom-radio">
                    <input value="0" required class="custom-control-input<?= (isset($errEnroll['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="flg_status" <?= isset($options["flg_status"]) && $options["flg_status"] == 0 ? "checked" : "" ?>>
                    <label for="active" class="custom-control-label">Hoạt động</label>
                </div>
                <div class="custom-control custom-radio">
                    <input value="1" required class="custom-control-input<?= (isset($errEnroll['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="notactive" name="flg_status" <?= isset($options["flg_status"]) && $options["flg_status"] == 1 ? "checked" : "" ?>>
                    <label for="notactive" class="custom-control-label">Không hoạt động</label>
                </div>
                <?= isset($errEnroll['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['flg_status'], 'name' => 'flg_status']) : '' ?>
            </div>
            <div class="form-group">
                <label><?= __("Ngôn ngữ") ?>:</label>
                <div class="custom-control custom-radio">
                    <input value="0" required class="custom-control-input<?= (isset($errEnroll['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($options["flg_language"]) && $options["flg_language"] == 0 ? "checked" : "" ?>>
                    <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
                </div>
                <div class="custom-control custom-radio">
                    <input value="1" required class="custom-control-input<?= (isset($errEnroll['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($options["flg_language"]) && $options["flg_language"] == 1 ? "checked" : "" ?>>
                    <label for="english" class="custom-control-label">Tiếng Anh</label>
                </div>
                <?= isset($errEnroll['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['flg_language'], 'name' => 'flg_language']) : '' ?>
            </div>
            <div id="1" class="option_enrollment" style="display:none">
                <div class="form-group">
                    <label><?= __("Chọn ngành") ?>:</label>
                    <select id="branch" name="branch_parent" class="custom-select<?= (isset($errEnroll['branch_parent']) ? ' err-ctrl' : '') ?>">
                        <option value="">Chọn Nghành</option>
                        <?php foreach ($list_branch_type1 as $value) { ?>
                            <option <?= $options["type"] == 1 && isset($options["branch_parent"]) && $options["branch_parent"] == $value['id'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->branch_name ?></option>
                        <?php } ?>
                    </select>
                    <?= isset($errEnroll['branch_parent']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_parent'], 'name' => 'branch_parent']) : '' ?>
                </div>
                <div class="form-group">
                    <label><?= __("Tiêu đề") ?>:</label>
                    <input type="text" name="branch_name" placeholder="Tên ngành / Tổ hợp môn" value="<?= $options["type"] == 1 && isset($options["branch_name"]) ? $options["branch_name"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['branch_name']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['branch_name']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_name'], 'name' => 'branch_name']) : '' ?>
                </div>
                <div id="code_nganh" class="form-group" style="display: none">
                    <label><?= __("Mã nghành") ?>:</label>
                    <input type="text" name="branch_code" placeholder="Mã ngành" value="<?= $options["type"] == 1 && isset($options["branch_code"]) ? $options["branch_code"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['branch_code']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['branch_code']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_code'], 'name' => 'branch_code']) : '' ?>
                </div>
                <div id="code" class="form-group" style="display: none">
                    <label><?= __("Mã tổ hợp môn") ?>:</label>
                    <input type="text" name="combination_code" placeholder="Mã tổ hợp môn" value="<?= $options["type"] == 1 && isset($options["combination_code"]) ? $options["combination_code"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['combination_code']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['combination_code']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['combination_code'], 'name' => 'combination_code']) : '' ?>
                </div>
            </div>
            <div id="2" class="option_enrollment" style="display:none">
                <div class="form-group">
                    <label><?= __("Chọn ngành") ?>:</label>
                    <select id="branch1" name="branch_parent" class="custom-select<?= (isset($errEnroll['branch_parent']) ? ' err-ctrl' : '') ?>">
                        <option value="">Chọn Nghành</option>
                        <?php foreach ($list_branch_type2 as $value) { ?>
                            <option <?= $options["type"] == 2 && isset($options["branch_parent"]) && $options["branch_parent"] == $value['branch_parent'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->branch_name ?></option>
                        <?php } ?>
                    </select>
                    <?= isset($errEnroll['branch_parent']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_parent'], 'name' => 'branch_parent']) : '' ?>
                </div>
                <div class="form-group">
                    <label><?= __("Tiêu đề") ?>:</label>
                    <input type="text" name="branch_name" placeholder="Tên ngành liên thông / Hệ liên thông" value="<?= $options["type"] == 2 && isset($options["branch_name"]) ? $options["branch_name"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['branch_name']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['branch_name']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_name'], 'name' => 'branch_name']) : '' ?>
                </div>
                <div id="code_nganh1" class="form-group" style="display: none">
                    <label><?= __("Mã nghành liên thông") ?>:</label>
                    <input type="text" name="branch_code" placeholder="Mã ngành liên thông" value="<?= $options["type"] == 2 && isset($options["branch_code"]) ? $options["branch_code"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['branch_code']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['branch_code']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_code'], 'name' => 'branch_code']) : '' ?>
                </div>
                <div id="code1" class="form-group" style="display: none">
                    <label><?= __("Mã hệ liên thông") ?>:</label>
                    <input type="text" name="combination_code" placeholder="Mã tổ hợp môn" value="<?= $options["type"] == 2 && isset($options["combination_code"]) ? $options["combination_code"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['combination_code']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                    <?= isset($errEnroll['combination_code']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['combination_code'], 'name' => 'combination_code']) : '' ?>
                </div>
            </div>
            <div id="3" class="option_enrollment" style="display:none">
                <label><?= __("Ngành nghề đăng ký cập nhật") ?>:</label>
                <input type="text" name="branch_update_name" placeholder="Tên ngành nghề đăng ký cập nhật" value="<?= isset($options["branch_update_name"]) ? $options["branch_update_name"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errEnroll['branch_update_name']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
                <?= isset($errEnroll['branch_update_name']) ? $this->element('Admin/errmess', ['opptions' => $errEnroll['branch_update_name'], 'name' => 'branch_update_name']) : '' ?>
            </div>
        </div>
        <!-- /.row -->
        <div class="card-footer" style="text-align: right">
            <a href="<?= $this->Url->build(['controller' => 'EnrollmentOptions', 'action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
            </button>
        </div>
    </div>

    <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn lưu dữ liệu này không？
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?php $this->start('script_footer'); ?>
<script>
    $('#optionselector').change(function() {
        $('.option_enrollment').hide();
        $('.option_enrollment :input').attr('disabled', 'disabled');
        $('#' + $(this).val()).show();
        $('#' + $(this).val() + ' :input').removeAttr('disabled');
        if ($('select[name = "type"]').val() == 1) {
            changeBranch();
        } else if ($('select[name = "type"]').val() == 2) {
            changeBranch1();
        }
    });

    function changeBranch() {
        if ($('#branch').val() == "") {
            $('#code_nganh').show();
            $('#code').hide();
        } else {
            $('#code').show();
            $('#code_nganh').hide();
        }
    }

    function changeBranch1() {
        if ($('#branch1').val() == "") {
            $('#code_nganh1').show();
            $('#code1').hide();
        } else {
            $('#code1').show();
            $('#code_nganh1').hide();
        }
    }

    $('#branch').change(function() {
        changeBranch();
    });
    $('#branch1').change(function() {
        changeBranch1();
    });

    toggleOptions();

    function toggleOptions() {
        if ($('select[name = "type"]').val() == 1) {
            $('.option_enrollment').hide();
            $('.option_enrollment :input').attr('disabled', 'disabled');
            $('#1 :input').removeAttr('disabled');
            $('#1').show();
            changeBranch();
        }
        if ($('select[name = "type"]').val() == 2) {
            $('.option_enrollment').hide();
            $('.option_enrollment :input').attr('disabled', 'disabled');
            $('#2 :input').removeAttr('disabled');
            $('#2').show();
            changeBranch1();
        }
        if ($('select[name = "type"]').val() == 3) {
            $('.option_enrollment').hide();
            $('.option_enrollment :input').attr('disabled', 'disabled');
            $('#3 :input').removeAttr('disabled');
            $('#3').show();
        }
    }
</script>
<?php $this->end(); ?>