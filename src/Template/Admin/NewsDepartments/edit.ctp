<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
	'title' => 'News',
	'url' => [
		'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
		'NewsDepartments' => $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'lists']),
	],
]) ?>
<?php $this->end();
$imgOld = $data['img_highlight'];
$req = $this->getRequest()->getData();
if (isset($req['data'])) {
	$data = array_merge($data, $req['data']);
}
?>
<?= $this->AppForm->create(null, [
	'type' => 'file',
	'url' => ['controller' => 'NewsDepartments', 'action' => 'edit', $id],
	'id' => 'quote-add'
]) ?>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Tin tức</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="card-body">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group ">
						<label for="type">Tiêu đề tin tức</label>
						<input type="text" name="data[title]" placeholder="Nhập tiêu đề tin tức" value="<?= isset($data['title']) ? $data['title'] : '' ?>" class="form-control<?= (isset($errDepartments['title']) ? ' err-ctrl' : '') ?>">
						<?= isset($errDepartments['title']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['title'], 'name' => 'data[title]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="type">Khoa</label>
						<select name="data[type]" class="form-control<?= (isset($errDepartments['type']) ? ' err-ctrl' : '') ?>" id="type">
							<option value="">Chọn Khoa</option>
							<?php foreach ($lstDepartments as $value) { ?>
								<option <?= isset($data["department_id"]) && $data["department_id"] == $value['id'] ? "selected" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
							<?php } ?>
						</select>
						<?= isset($errDepartments['type']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['type'], 'name' => 'data[type]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="parentId">Danh mục tin</label>
						<select name="data[parent_id]" class="form-control<?= (isset($errDepartments['parent_id']) ? ' err-ctrl' : '') ?>" id="parentId">
							<option value="">Please choose a category</option>
						</select>
						<?= isset($errDepartments['parent_id']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['parent_id'], 'name' => 'data[parent_id]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="exampleInputFile">Ảnh nổi bật:</label>
						<?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->news($imgOld), 'name' => 'data[img_highlight]']) ?>
						<div class="input-group">
							<div class="custom-file">
								<input name="data[img_highlight]" type="file" id="input_img_highlight" class="custom-file-input input_file" id="exampleInputFile">
								<label class="custom-file-label<?= (isset($errDepartments['img_highlight']) ? ' err-ctrl' : '') ?>" id=dataimg_highlight for="exampleInputFile">Chọn ảnh...</label>
							</div>
						</div>
						<?= isset($errDepartments['img_highlight']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['img_highlight'], 'name' => 'data[img_highlight]']) : '' ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label><?= __("Ngôn ngữ") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="data[flg_language]" value="0" <?php if (!isset($data['flg_language']) || $data['flg_language'] === 0) { ?> checked <?php } ?>>
							<label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="data[flg_language]" value="1" <?php if (isset($data['flg_language']) && $data['flg_language'] == 1) { ?> checked <?php } ?>>
							<label for="english" class="custom-control-label">Tiếng Anh</label>
						</div>
						<?= isset($errDepartments['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_language'], 'name' => 'data[flg_language]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="date_edit">Thời gian viết bài:</label>
						<input type="datetime-local" class="form-control<?= (isset($errDepartments['date_edit']) ? ' err-ctrl' : '') ?>" name="data[date_edit]" value="<?= isset($data['date_edit']) ? $data['date_edit'] : '' ?>">
						<?= isset($errDepartments['date_edit']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['date_edit'], 'name' => 'data[date_edit]']) : '' ?>
					</div>
					<div class="form-group">
						<label><?= __("Trạng thái") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input<?= (isset($errDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="data[flg_status]" value="0" <?php if (!isset($data['flg_status']) || $data['flg_status'] === 0) { ?> checked <?php } ?>>
							<label for="active" class="custom-control-label">Publish</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input<?= (isset($errDepartments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="deactive" name="data[flg_status]" value="1" <?php if (isset($data['flg_status']) && $data['flg_status'] == 1) { ?> checked <?php } ?>>
							<label for="deactive" class="custom-control-label">Draft</label>
						</div>
						<?= isset($errDepartments['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_status'], 'name' => 'data[flg_status]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="schedule">Thời gian publish:</label>
						<input type="datetime-local" class="form-control<?= (isset($errDepartments['schedule']) ? ' err-ctrl' : '') ?>" name="data[schedule]" value="<?= isset($data['schedule']) ? $data['schedule'] : '' ?>">
						<?= isset($errDepartments['schedule']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['schedule'], 'name' => 'data[schedule]']) : '' ?>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group ">
						<label for="short_description">Tóm tắt nội dung</label>
						<textarea style="width: 100%;" name="data[short_description]" type="text" class="form-control<?= (isset($errDepartments['short_description']) ? ' err-ctrl' : '') ?>" id="short_description" placeholder="Tóm tắt nội dung" value=""><?= isset($data['short_description']) ? $data['short_description'] : '' ?></textarea>
						<?= isset($errDepartments['short_description']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['short_description'], 'name' => 'data[short_description]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="content">Nội dung chi tiết</label>
						<textarea style="width: 100%;" name="data[content]" type="text" class="form-control<?= (isset($errDepartments['content']) ? ' err-ctrl' : '') ?>" id="content" placeholder="Nội dung chi tiết" value=""><?= isset($data['content']) ? $data['content'] : '' ?></textarea>
						<?= isset($errDepartments['content']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['content'], 'name' => 'data[content]']) : '' ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin SEO</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<div class="card-body">
			<div class="col-md-12">
				<div class="form-group">
					<label for="title">SEO title</label>
					<input name="data[Seo][meta_title]" type="text" class="form-control<?= (isset($errSeo['meta_title']) ? ' err-ctrl' : '') ?>" id="title" placeholder="SEO title" value="<?= isset($data['Seo']['meta_title']) ? $data['Seo']['meta_title'] : '' ?>">
					<?= isset($errSeo['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_title'], 'name' => 'data[Seo][meta_title]']) : '' ?>
				</div>
				<div class="form-group ">
					<label for="description">SEO description</label>
					<input name="data[Seo][meta_description]" type="text" class="form-control<?= (isset($errSeo['meta_description']) ? ' err-ctrl' : '') ?>" id="description" placeholder="SEO description" value="<?= isset($data['Seo']['meta_description']) ? $data['Seo']['meta_description'] : '' ?>">
					<?= isset($errSeo['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_description'], 'name' => 'data[Seo][meta_description]']) : '' ?>
				</div>
				<div class="form-group ">
					<label for="keywords">SEO keywords</label>
					<textarea style="width: 100%;" name="data[Seo][meta_keywords]" type="text" class="form-control<?= (isset($errSeo['meta_keywords']) ? ' err-ctrl' : '') ?>" id="keywords" placeholder="SEO keywords" value=""><?= isset($data['Seo']['meta_keywords']) ? $data['Seo']['meta_keywords'] : '' ?></textarea>
					<?= isset($errSeo['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_keywords'], 'name' => 'data[Seo][meta_keywords]']) : '' ?>
				</div>
				<div class="form-group">
					<label for="tags">Tags:</label><br />
					<input class="<?= (isset($errSeo['tags']) ? ' err-ctrl' : '') ?>" name='data[Seo][tags]' value="<?= isset($data['Seo']['tags']) ? $data['Seo']['tags'] : '' ?>" data-role="tagsinput" placeholder='Nhập thẻ tags...'>
					<?= isset($errSeo['tags']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['tags'], 'name' => 'data[Seo][tags]']) : '' ?>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="card-footer" style="text-align: right">
			<a href="<?= $this->Url->build(['action' => 'lists']) ?>" class="btn btn-info">Quay lại</a>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
			</button>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Bạn có chắc chắn muốn lưu dữ liệu này không？
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<?= $this->AppForm->end() ?>
<div style="display: none;">
	<select id="parentIdDefault">
		<option value="">Please choose a category</option>
	</select>
	<select class="form-control" id="parentIdList">
		<?php foreach ($listCategory as $category) { ?>
			<?php if (isset($data['cate_depart_id']) && $category['id'] == $data['cate_depart_id']) { ?>
				<option language="<?= $category['flg_language'] ?>" typecate="<?= $category['department_id'] ?>" value="<?= $category['id'] ?>" selected="selected"><?= $category['title'] ?></option>
			<?php } else { ?>
				<option language="<?= $category['flg_language'] ?>" typecate="<?= $category['department_id'] ?>" value="<?= $category['id'] ?>"><?= $category['title'] ?></option>
			<?php } ?>
		<?php } ?>
	</select>
</div>
<!-- jQuery -->
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/news-department.js"></script>
<script>
	var val = "<?= isset($data['cate_depart_id']) ? $data['cate_depart_id'] : "" ?>"
	$("input[name='data[flg_language]']").change(function() {
		generateParentAdd(val);
	});

	$(document).ready(function() {
		generateParentAdd(val);
	});
	$("#type").change(function() {
		generateParentList(val);
	});
</script>
<?php $this->start('script_footer'); ?>
<script src="/admin/plugins/ckeditor/ckeditor.js"></script>
<script>
	//editor
	$(function() {
		// instance, using default configuration.
		CKEDITOR.config.height = 600;
		CKEDITOR.config.extraPlugins = 'imageuploader';
		CKEDITOR.replace('content');
	});

	//tags
	$('[name="data[Seo][tags]"]').tagify();
</script>
<?php $this->end(); ?>