<link rel="stylesheet" href="/admin/css/categories.css">
<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'News',
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'NewsDepartments' => $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'lists']),
    ],
]) ?>
<?php $this->end(); ?>

<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm danh mục</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= $this->AppForm->create(null) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group <?php if (isset($errors['title']) && !empty($errors['title'])) { ?> has-error <?php } ?>">
                            <label for="type">Tiêu đề tin tức</label>
                            <input type="text" name="data[title]" placeholder="Nhập tiêu đề danh mục" value="<?= isset($param['title']) ? $param['title'] : '' ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?php if (isset($errors['type']) && !empty($errors['type'])) { ?> has-error <?php } ?>">
                            <label for="type">Khoa</label>
                            <select name="data[type]" class="form-control" id="type">
                                <option value="">Chọn Khoa</option>
                                <?php foreach ($lstDepartments as $value) { ?>
                                    <option required <?= isset($param["type"]) && $param["type"] == $value['id'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?= __("Ngôn ngữ") ?>:</label>
                            <div class="row">
                                <div class="col-12 icheck-primary d-inline">
                                    <input type="checkbox" id="vietnamese" name="data[flg_language][vi]" value="0" <?php if (isset($param['flg_language']['vi']) && $param['flg_language']['vi'] === "0") { ?> checked <?php } ?>>
                                    <label for="vietnamese">Tiếng Việt</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 icheck-primary d-inline">
                                    <input type="checkbox" id="english" name="data[flg_language][en]" value="1" <?php if (isset($param['flg_language']['en']) && $param['flg_language']['en'] == 1) { ?> checked <?php } ?>>
                                    <label for="english">Tiếng Anh</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group <?php if (isset($errors['cate_id']) && !empty($errors['cate_id'])) { ?> has-error <?php } ?>">
                            <label for="parentId">Danh mục tin</label>
                            <select name="data[parent_id]" class="form-control" id="parentId">
                                <option value="">Please choose a category</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?= __("Trạng thái") ?>:</label>
                            <div class="row">
                                <div class=" col-12 icheck-primary d-inline">
                                    <input type="checkbox" id="active" name="data[flg_status][show]" value="0" <?php if (isset($param['flg_status']['show']) && $param['flg_status']['show'] === "0") { ?> checked <?php } ?>>
                                    <label for="active">Hoạt động</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-12 icheck-primary d-inline">
                                    <input type="checkbox" id="deactive" name="data[flg_status][hidden]" value="1" <?php if (isset($param['flg_status']['hidden']) && $param['flg_status']['hidden'] == 1) { ?> checked <?php } ?>>
                                    <label for="deactive">Không hoạt động</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'add']) ?>" class="btn btn-success btn-fw">
                    <i class="fas fa-plus-square"></i>&nbsp;&nbsp; Thêm tin tức
                </a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </div>
            <?= $this->AppForm->end() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Danh sách tin tức</h3>
            </div>
            <!-- /.box-header -->
            <div class="card-body no-padding">
                <table id="listNews" class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 2%">
                                STT
                            </th>
                            <th style="width: 15%">
                                Tên danh mục
                            </th>
                            <th style="width: 25%">
                                Tiêu đề
                            </th>
                            <th style="width: 13%">
                                Ngôn ngữ
                            </th>
                            <th style="width: 10%">
                                Trạng thái
                            </th>
                            <th style="width: 15%">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($news as $key => $record) { ?>
                            <tr>
                                <td class="py-1">
                                    <?= $key + 1 ?>
                                </td>
                                <td>
                                    <?= $record['cate_title'] ?>
                                </td>
                                <td>
                                    <?= $record['title'] ?>
                                </td>
                                <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $record['flg_language']) ?></td>
                                <td>
                                    <label class="switch ">
                                        <input type="checkbox" class="success status" recordId="<?= $record['id'] ?>" <?php if ($record['flg_status'] === 0) { ?> checked='checked' <?php } ?> />
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td>
                                    <a href="<?= $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'edit', $record['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'delete', $record['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa bản ghi này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <select id="parentIdDefault">
        <option value="">Please choose a category</option>
    </select>
    <select class="form-control" id="parentIdList">
        <?php foreach ($listCategory as $category) { ?>
            <?php if (isset($param['parent_id']) && $category['id'] == $param['parent_id']) { ?>
                <option typecate="<?= $category['department_id'] ?>" value="<?= $category['id'] ?>" selected="selected"><?= $category['title'] ?></option>
            <?php } else { ?>
                <option typecate="<?= $category['department_id'] ?>" value="<?= $category['id'] ?>"><?= $category['title'] ?></option>
            <?php } ?>
        <?php } ?>
    </select>
</div>

<!-- jQuery -->
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/news-department.js"></script>
<script>
    var val = "<?= isset($param['parent_id']) ? $param['parent_id'] : "" ?>"
    $(".status").change(function() {
        let id = $(this).attr("recordId")
        location.href = "/admin/faculty_news/change-status/" + id;
    });
    $(document).ready(function() {
        generateParentList(val);
        $("#type").change(function() {
            generateParentList(val);
        });
    });
</script>
<?php $this->start('script_footer'); ?>
<!-- DataTables -->
<script src="/admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="/admin/dist/js/adminlte.min.js"></script>
<script>
    $(function() {
        $('#listNews').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
