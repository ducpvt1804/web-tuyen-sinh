<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Seo $seo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Seo'), ['action' => 'edit', $seo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Seo'), ['action' => 'delete', $seo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $seo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Seo'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Seo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cate Departments'), ['controller' => 'CateDepartments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cate Department'), ['controller' => 'CateDepartments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Configs'), ['controller' => 'Configs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Config'), ['controller' => 'Configs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="seo view large-9 medium-8 columns content">
    <h3><?= h($seo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Meta Tile') ?></th>
            <td><?= h($seo->meta_tile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Description') ?></th>
            <td><?= h($seo->meta_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Keywords') ?></th>
            <td><?= h($seo->meta_keywords) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tags') ?></th>
            <td><?= h($seo->tags) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($seo->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Language') ?></th>
            <td><?= $this->Number->format($seo->flg_language) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Delete') ?></th>
            <td><?= $this->Number->format($seo->flg_delete) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created Date') ?></th>
            <td><?= h($seo->created_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Date') ?></th>
            <td><?= h($seo->update_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Cate Departments') ?></h4>
        <?php if (!empty($seo->cate_departments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Department Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Flg Showheader') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->cate_departments as $cateDepartments): ?>
            <tr>
                <td><?= h($cateDepartments->id) ?></td>
                <td><?= h($cateDepartments->seo_id) ?></td>
                <td><?= h($cateDepartments->department_id) ?></td>
                <td><?= h($cateDepartments->title) ?></td>
                <td><?= h($cateDepartments->slug) ?></td>
                <td><?= h($cateDepartments->flg_showheader) ?></td>
                <td><?= h($cateDepartments->flg_language) ?></td>
                <td><?= h($cateDepartments->created_date) ?></td>
                <td><?= h($cateDepartments->update_date) ?></td>
                <td><?= h($cateDepartments->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CateDepartments', 'action' => 'view', $cateDepartments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CateDepartments', 'action' => 'edit', $cateDepartments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CateDepartments', 'action' => 'delete', $cateDepartments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cateDepartments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Categories') ?></h4>
        <?php if (!empty($seo->categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Flg Site') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Flg Showheader') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->categories as $categories): ?>
            <tr>
                <td><?= h($categories->id) ?></td>
                <td><?= h($categories->seo_id) ?></td>
                <td><?= h($categories->title) ?></td>
                <td><?= h($categories->slug) ?></td>
                <td><?= h($categories->parent_id) ?></td>
                <td><?= h($categories->flg_site) ?></td>
                <td><?= h($categories->flg_language) ?></td>
                <td><?= h($categories->flg_showheader) ?></td>
                <td><?= h($categories->created_date) ?></td>
                <td><?= h($categories->update_date) ?></td>
                <td><?= h($categories->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $categories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $categories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $categories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Configs') ?></h4>
        <?php if (!empty($seo->configs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Flg Site') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Logo') ?></th>
                <th scope="col"><?= __('Address 1') ?></th>
                <th scope="col"><?= __('Address 2') ?></th>
                <th scope="col"><?= __('Telno') ?></th>
                <th scope="col"><?= __('Fax') ?></th>
                <th scope="col"><?= __('Map') ?></th>
                <th scope="col"><?= __('Facebook Address') ?></th>
                <th scope="col"><?= __('Twitter Address') ?></th>
                <th scope="col"><?= __('Pinterest Address') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->configs as $configs): ?>
            <tr>
                <td><?= h($configs->id) ?></td>
                <td><?= h($configs->seo_id) ?></td>
                <td><?= h($configs->flg_site) ?></td>
                <td><?= h($configs->name) ?></td>
                <td><?= h($configs->email) ?></td>
                <td><?= h($configs->logo) ?></td>
                <td><?= h($configs->address_1) ?></td>
                <td><?= h($configs->address_2) ?></td>
                <td><?= h($configs->telno) ?></td>
                <td><?= h($configs->fax) ?></td>
                <td><?= h($configs->map) ?></td>
                <td><?= h($configs->facebook_address) ?></td>
                <td><?= h($configs->twitter_address) ?></td>
                <td><?= h($configs->pinterest_address) ?></td>
                <td><?= h($configs->flg_language) ?></td>
                <td><?= h($configs->created_date) ?></td>
                <td><?= h($configs->update_date) ?></td>
                <td><?= h($configs->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Configs', 'action' => 'view', $configs->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Configs', 'action' => 'edit', $configs->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Configs', 'action' => 'delete', $configs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $configs->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Departments') ?></h4>
        <?php if (!empty($seo->departments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Cate Id') ?></th>
                <th scope="col"><?= __('Banner') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Shot Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->departments as $departments): ?>
            <tr>
                <td><?= h($departments->id) ?></td>
                <td><?= h($departments->seo_id) ?></td>
                <td><?= h($departments->cate_id) ?></td>
                <td><?= h($departments->banner) ?></td>
                <td><?= h($departments->img_highlight) ?></td>
                <td><?= h($departments->flg_language) ?></td>
                <td><?= h($departments->title) ?></td>
                <td><?= h($departments->shot_title) ?></td>
                <td><?= h($departments->description) ?></td>
                <td><?= h($departments->created_date) ?></td>
                <td><?= h($departments->update_date) ?></td>
                <td><?= h($departments->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Departments', 'action' => 'view', $departments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Departments', 'action' => 'edit', $departments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Departments', 'action' => 'delete', $departments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $departments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Events') ?></h4>
        <?php if (!empty($seo->events)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Department Name') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Schedule') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Flg Status') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Map') ?></th>
                <th scope="col"><?= __('Telno') ?></th>
                <th scope="col"><?= __('Position') ?></th>
                <th scope="col"><?= __('Cost') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->events as $events): ?>
            <tr>
                <td><?= h($events->id) ?></td>
                <td><?= h($events->department_name) ?></td>
                <td><?= h($events->seo_id) ?></td>
                <td><?= h($events->title) ?></td>
                <td><?= h($events->img_highlight) ?></td>
                <td><?= h($events->schedule) ?></td>
                <td><?= h($events->address) ?></td>
                <td><?= h($events->flg_status) ?></td>
                <td><?= h($events->date) ?></td>
                <td><?= h($events->flg_language) ?></td>
                <td><?= h($events->map) ?></td>
                <td><?= h($events->telno) ?></td>
                <td><?= h($events->position) ?></td>
                <td><?= h($events->cost) ?></td>
                <td><?= h($events->content) ?></td>
                <td><?= h($events->created_date) ?></td>
                <td><?= h($events->update_date) ?></td>
                <td><?= h($events->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Events', 'action' => 'view', $events->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Events', 'action' => 'edit', $events->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Events', 'action' => 'delete', $events->id], ['confirm' => __('Are you sure you want to delete # {0}?', $events->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related News') ?></h4>
        <?php if (!empty($seo->news)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Cate Id') ?></th>
                <th scope="col"><?= __('Cate Depart Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Flg Status') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Schedule') ?></th>
                <th scope="col"><?= __('Short Description') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('View') ?></th>
                <th scope="col"><?= __('Date Edit') ?></th>
                <th scope="col"><?= __('Created Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($seo->news as $news): ?>
            <tr>
                <td><?= h($news->id) ?></td>
                <td><?= h($news->seo_id) ?></td>
                <td><?= h($news->cate_id) ?></td>
                <td><?= h($news->cate_depart_id) ?></td>
                <td><?= h($news->title) ?></td>
                <td><?= h($news->img_highlight) ?></td>
                <td><?= h($news->slug) ?></td>
                <td><?= h($news->flg_status) ?></td>
                <td><?= h($news->flg_language) ?></td>
                <td><?= h($news->schedule) ?></td>
                <td><?= h($news->short_description) ?></td>
                <td><?= h($news->content) ?></td>
                <td><?= h($news->view) ?></td>
                <td><?= h($news->date_edit) ?></td>
                <td><?= h($news->created_date) ?></td>
                <td><?= h($news->update_date) ?></td>
                <td><?= h($news->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'News', 'action' => 'view', $news->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'News', 'action' => 'edit', $news->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'News', 'action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
