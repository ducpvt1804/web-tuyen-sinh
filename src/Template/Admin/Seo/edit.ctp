<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Seo $seo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $seo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $seo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Seo'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cate Departments'), ['controller' => 'CateDepartments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cate Department'), ['controller' => 'CateDepartments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Configs'), ['controller' => 'Configs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Config'), ['controller' => 'Configs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="seo form large-9 medium-8 columns content">
    <?= $this->Form->create($seo) ?>
    <fieldset>
        <legend><?= __('Edit Seo') ?></legend>
        <?php
            echo $this->Form->control('meta_tile');
            echo $this->Form->control('meta_description');
            echo $this->Form->control('meta_keywords');
            echo $this->Form->control('tags');
            echo $this->Form->control('flg_language');
            echo $this->Form->control('created_date', ['empty' => true]);
            echo $this->Form->control('update_date', ['empty' => true]);
            echo $this->Form->control('flg_delete');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
