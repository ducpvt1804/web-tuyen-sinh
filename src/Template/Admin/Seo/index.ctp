<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Seo[]|\Cake\Collection\CollectionInterface $seo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Seo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cate Departments'), ['controller' => 'CateDepartments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cate Department'), ['controller' => 'CateDepartments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Configs'), ['controller' => 'Configs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Config'), ['controller' => 'Configs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="seo index large-9 medium-8 columns content">
    <h3><?= __('Seo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('meta_tile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('meta_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('meta_keywords') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tags') ?></th>
                <th scope="col"><?= $this->Paginator->sort('flg_language') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('update_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('flg_delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($seo as $seo): ?>
            <tr>
                <td><?= $this->Number->format($seo->id) ?></td>
                <td><?= h($seo->meta_tile) ?></td>
                <td><?= h($seo->meta_description) ?></td>
                <td><?= h($seo->meta_keywords) ?></td>
                <td><?= h($seo->tags) ?></td>
                <td><?= $this->Number->format($seo->flg_language) ?></td>
                <td><?= h($seo->created_date) ?></td>
                <td><?= h($seo->update_date) ?></td>
                <td><?= $this->Number->format($seo->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $seo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $seo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $seo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $seo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
