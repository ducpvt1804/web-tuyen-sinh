<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailBanner[]|\Cake\Collection\CollectionInterface $detailBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Detail Banner'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="detailBanner index large-9 medium-8 columns content">
    <h3><?= __('Detail Banner') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('banner_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('img') ?></th>
                <th scope="col"><?= $this->Paginator->sort('create_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('update_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('flg_delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detailBanner as $detailBanner): ?>
            <tr>
                <td><?= $this->Number->format($detailBanner->id) ?></td>
                <td><?= $this->Number->format($detailBanner->banner_id) ?></td>
                <td><?= h($detailBanner->img) ?></td>
                <td><?= h($detailBanner->create_date) ?></td>
                <td><?= h($detailBanner->update_date) ?></td>
                <td><?= $this->Number->format($detailBanner->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $detailBanner->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $detailBanner->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $detailBanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailBanner->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
