<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailBanner $detailBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Detail Banner'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="detailBanner form large-9 medium-8 columns content">
    <?= $this->Form->create($detailBanner) ?>
    <fieldset>
        <legend><?= __('Add Detail Banner') ?></legend>
        <?php
            echo $this->Form->control('banner_id');
            echo $this->Form->control('img');
            echo $this->Form->control('create_date', ['empty' => true]);
            echo $this->Form->control('update_date', ['empty' => true]);
            echo $this->Form->control('flg_delete');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
