<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailBanner $detailBanner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Detail Banner'), ['action' => 'edit', $detailBanner->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Detail Banner'), ['action' => 'delete', $detailBanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailBanner->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Detail Banner'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Detail Banner'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="detailBanner view large-9 medium-8 columns content">
    <h3><?= h($detailBanner->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Img') ?></th>
            <td><?= h($detailBanner->img) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($detailBanner->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Banner Id') ?></th>
            <td><?= $this->Number->format($detailBanner->banner_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Delete') ?></th>
            <td><?= $this->Number->format($detailBanner->flg_delete) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Date') ?></th>
            <td><?= h($detailBanner->create_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Date') ?></th>
            <td><?= h($detailBanner->update_date) ?></td>
        </tr>
    </table>
</div>
