<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'Departmentspb' => null
    ]
]) ?>
<?php $this->end(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm Phòng ban</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->AppForm->create(null, [
                'type' => 'post',
                'url' => ['controller' => 'Departmentspb', 'action' => 'index'],
                'id' => 'search-departments'
            ]) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <select name="flg_language" class="custom-select">
                                <option value="">Ngôn ngữ</option>
                                <option <?= isset($inputSearch["flg_language"]) && $inputSearch["flg_language"] == 1 ? "selected=\"selected\"" : "" ?> value="1">Tiếng Việt</option>
                                <option <?= isset($inputSearch["flg_language"]) && $inputSearch["flg_language"] == 2 ? "selected=\"selected\"" : "" ?> value="2">Tiếng Anh</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <input value="<?= isset($inputSearch["keyword"]) ? $inputSearch["keyword"] : "" ?>" name="keyword" type="text" class="form-control" placeholder="Nhập từ khóa tìm kiếm...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'Departmentspb', 'action' => 'add']) ?>" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách Phòng ban</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="card-body no-padding">
            <table id="lstDepartments" class="table table-striped department-list">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên phòng ban</th>
                        <th>Mô tả</th>
                        <th>Ảnh</th>
                        <th>Ngôn ngữ</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    if (isset($departments) && count($departments) > 0) :
                        foreach ($departments as $key => $row) :
                            ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $row['title'] ?></td>
                                <td><?= $row['description'] ?></td>
                                <td>
                                    <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->departments($row["img_highlight"]), 'name' => 'banner']) ?>
                                </td>
                                <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $row['flg_language']) ?></td>
                                <td style="text-align: left; vertical-align: middle">
                                    <a href="<?= $this->Url->build(['controller' => 'Departmentspb', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'Departmentspb', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                    </button>
                                </td>
                            </tr>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Bạn có chắc chắn muốn xóa bản ghi này không?
                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <script src="/admin/plugins/jquery/jquery.min.js"></script>
    <script src="/admin/js/categories.js"></script>
    <?php $this->start('script_footer'); ?>
    <script>
        $(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
    <?php $this->end(); ?>
