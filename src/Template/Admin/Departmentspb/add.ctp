<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'Departmentspb' => $this->Url->build(['controller' => 'Departmentspb', 'action' => 'index'])
  ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'Departmentspb', 'action' => 'add'],
  'id' => 'departments-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thêm mới thông tin Phòng ban</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>

    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Khoa") ?></label>
        <select name="parent_key" class="custom-select<?= (isset($errDepartments['parent_key']) ? ' err-ctrl' : '') ?>">
          <option value="">Chọn Phòng ban</option>
          <?php foreach ($departments as $value) { ?>
            <option <?= isset($req["parent_key"]) && $req["parent_key"] == $value['id'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
          <?php } ?>
        </select>
        <?= isset($errDepartments['parent_key']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['parent_key'], 'name' => 'parent_key']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Ngôn ngữ") ?>:</label>
        <div class="custom-control custom-radio">
          <input value="0" checked class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($req["flg_language"]) && $req["flg_language"] == 0 ? "checked" : "" ?>>
          <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
        </div>
        <div class="custom-control custom-radio">
          <input value="1" class="custom-control-input<?= (isset($errDepartments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($req["flg_language"]) && $req["flg_language"] == 1 ? "checked" : "" ?>>
          <label for="english" class="custom-control-label">Tiếng Anh</label>
        </div>
        <?= isset($errDepartments['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['flg_language'], 'name' => 'flg_language']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Tên đầy đủ") ?>:</label>
        <input type="text" name="title" value="<?= isset($req["title"]) ? $req["title"] : "" ?>" placeholder="Tên đầy đủ (VD: Khoa Dược Học)" value="" class="form-control my-colorpicker1 colorpicker-element <?= (isset($errDepartments['title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errDepartments['title']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['title'], 'name' => 'title']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Tên ngắn") ?>:</label>
        <input type="text" name="shot_title" value="<?= isset($req["shot_title"]) ? $req["shot_title"] : "" ?>" placeholder="Tên ngắn (VD: Khoa Dược)" value="" class="form-control my-colorpicker1 colorpicker-element <?= (isset($errDepartments['shot_title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errDepartments['shot_title']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['shot_title'], 'name' => 'shot_title']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Mô tả") ?>:</label>
        <textarea class="form-control<?= (isset($errDepartments['description']) ? ' err-ctrl' : '') ?>" name="description" rows="3" placeholder="Mô tả" value=""><?= isset($req["shot_title"]) ? $req["shot_title"] : "" ?></textarea>
        <?= isset($errDepartments['description']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['description'], 'name' => 'description']) : '' ?>
      </div>
      <!-- /.form-group -->
      <div class="form-group">
        <label for="InputFileLG"><?= __("Ảnh banner") ?>:</label>
        <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'banner']) ?>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="banner" placeholder="Chọn ảnh" class="custom-file-input input_file" id="InputFileLG" accept="image/jpeg, image/png">
            <label class="custom-file-label<?= (isset($errDepartments['banner']) ? ' err-ctrl' : '') ?>" id="banner" for="InputFileLG">Chọn file</label>
          </div>
        </div>
        <?= isset($errDepartments['banner']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['banner'], 'name' => 'banner']) : '' ?>
      </div>
      <div class="form-group">
        <label for="InputFileFT"><?= __("Ảnh tiêu điểm") ?>:</label>
        <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'img_highlight']) ?>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="img_highlight" placeholder="Chọn ảnh" class="custom-file-input input_file" id="InputFileFT" accept="image/jpeg, image/png">
            <label class="custom-file-label<?= (isset($errDepartments['img_highlight']) ? ' err-ctrl' : '') ?>" id=img_highlight for="InputFileFT">Chọn file</label>
          </div>
        </div>
        <?= isset($errDepartments['img_highlight']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['img_highlight'], 'name' => 'img_highlight']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.card-body -->
</div>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thông tin SEO</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Meta title") ?>:</label>
        <input type="text" name="meta_title" value="<?= isset($req["meta_title"]) ? $req["meta_title"] : "" ?>" placeholder="Nhập Meta title" value="" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['meta_title']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errSeo['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_title'], 'name' => 'meta_title']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Meta description") ?>:</label>
        <input type="text" name="meta_description" value="<?= isset($req["meta_description"]) ? $req["meta_description"] : "" ?>" placeholder="Nhập Meta description" value="" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errSeo['meta_description']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errSeo['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_description'], 'name' => 'meta_description']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Meta keywords") ?>:</label>
        <textarea class="form-control<?= (isset($errSeo['meta_keywords']) ? ' err-ctrl' : '') ?>" name="meta_keywords" value="" rows="3" placeholder="Nhập Meta keywords" value=""><?= isset($req["meta_keywords"]) ? $req["meta_keywords"] : "" ?></textarea>
        <?= isset($errSeo['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_keywords'], 'name' => 'meta_keywords']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
    <div class="card-footer" style="text-align: right">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>
  <!-- /.row -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>