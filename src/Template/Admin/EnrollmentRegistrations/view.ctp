<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'EnrollmentRegistrations' => $this->Url->build(['controller' => 'EnrollmentRegistrations', 'action' => 'index'])
  ]
]) ?>
<?php $this->end(); ?>
<?= $this->AppForm->create(null, [
    'type' => 'file',
    'url' => ['controller' => 'EnrollmentRegistrations', 'action' => 'view', $enroregis['id']],
    'id' => 'quote-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Chi tiết đăng ký tuyển sinh</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
          <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Danh mục đăng ký") ?>: </label><?= isset($enroregis["cate"]) ? $this->Common->getCategoriesRegister($enroregis['cate']) : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Người gửi") ?>: </label><?= isset($enroregis["fullname"]) ? $enroregis["fullname"] : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Ngày tháng năm sinh") ?>: </label><?= isset($enroregis["dob"]) ? $enroregis["dob"] : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Giới tính") ?>: </label><?= isset($enroregis["sex"]) ? $this->Sex->getTitle($enroregis["sex"]) : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Số điện thoại") ?>: </label><?= isset($enroregis["telno"]) ? $enroregis["telno"] : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Email") ?>: </label><?= isset($enroregis["email"]) ? $enroregis["email"] : ""?>
          </div>
        <div class="form-group">
            <label style="width: 250px; margin-right: 10px"><?= __("Địa chỉ") ?>: </label><?= isset($enroregis["address"]) ? $enroregis["address"] : ""?>
        </div>
        <?php if($enroregis['cate'] == 1){?>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Ngành xét tuyển") ?>: </label><?= isset($enroregis["admission"]) ? $this->common->getBranchTitle($enroregis["admission"]) : ""?>
            </div>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Tổ hợp môn xét tuyển") ?>: </label><?= isset($enroregis["sub_admission"]) ? $this->common->getBranchTitle($enroregis["sub_admission"]) : ""?>
            </div>
        <?php } elseif ($enroregis['cate'] == 2){?>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Ngành xét liên thông") ?>: </label><?= isset($enroregis["admission"]) ? $this->common->getBranchTitle($enroregis["admission"]) : ""?>
            </div>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Hệ liên thông") ?>: </label><?= isset($enroregis["sub_admission"]) ? $this->common->getBranchTitle($enroregis["sub_admission"]) : ""?>
            </div>
        <?php } else {?>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Số CMTND/ Thẻ căn cước/ Hộ chiếu") ?>: </label><?= isset($enroregis["person_id"]) ? $enroregis["person_id"] : ""?>
            </div>
            <div class="form-group">
                <label style="width: 250px; margin-right: 10px"><?= __("Ngành nghề đăng ký cập nhật") ?>: </label><?= isset($enroregis["admission"]) ? $this->common->getBranchTitle($enroregis["admission"]) : ""?>
            </div>
        <?php }?>
          <!-- /.form-group -->
        </div>
    <!-- /.row -->
      <div class="card-footer" style="text-align: right">
          <a href="javascript:history.back();" class="btn btn-info">Quay lại</a>
<!--          <a class="btn btn-primary" href="">Trả lời</a>-->
<!--          = $this->Url->build(['controller' => 'EnrollmentRegistrations', 'action' => 'view', $enroregis['id']])-->
      </div>
  </div>

  <!-- /.card-body -->
</div>
<?= $this->Form->end() ?>
