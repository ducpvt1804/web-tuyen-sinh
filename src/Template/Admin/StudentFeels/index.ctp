<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'StudentFeels' => $this->Url->build(['controller' => 'StudentFeels', 'action' => 'index'])
    ]
]) ?>
<?php $this->end(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm cảm nhận của sinh viên</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->AppForm->create(null, [
                'type' => 'post',
                'url' => ['controller' => 'StudentFeels', 'action' => 'index'],
                'id' => 'search-quotes'
            ]) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <select name="flg_language" class="custom-select">
                                <option value="">Ngôn ngữ</option>
                                <option <?= isset($inputSearch["flg_language"]) && (int) $inputSearch["flg_language"] == 1 ? "selected=\"selected\"" : "" ?> value="1">Tiếng Việt
                                </option>
                                <option <?= isset($inputSearch["flg_language"]) && $inputSearch["flg_language"] == 2 ? "selected=\"selected\"" : "" ?> value="2">Tiếng Anh
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <select name="flg_status" class="custom-select">
                                <option value="">Trạng thái</option>
                                <option <?= isset($inputSearch["flg_status"]) && $inputSearch["flg_status"] == 1 ? "selected=\"selected\"" : "" ?> value="1">Hoạt động
                                </option>
                                <option <?= isset($inputSearch["flg_status"]) && $inputSearch["flg_status"] == 2 ? "selected=\"selected\"" : "" ?> value="2">Không hoạt động
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <select name="department_id" class="custom-select">
                                <option value="">Chọn Khoa</option>
                                <?php foreach ($lstDepartments as $value) { ?>
                                    <option <?= isset($inputSearch["department_id"]) && $inputSearch["department_id"] == $value->id ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <input value="<?= isset($inputSearch["keyword"]) ? $inputSearch["keyword"] : "" ?>" name="keyword" type="text" class="form-control" placeholder="Nhập từ khóa tìm kiếm...">
                        </div>
                    </div>

                </div>

            </div>
            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'StudentFeels', 'action' => 'add']) ?>" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách cảm nhận</h3>
    </div>
    <div class="card-body no-padding">
        <table id="example2" class="table table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên sinh viên</th>
                    <th>Hình ảnh</th>
                    <th>Cảm nhận</th>
                    <th>Ngôn ngữ</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php

                if (isset($studentfeel) && count($studentfeel) > 0) :
                    foreach ($studentfeel as $key => $row) :
                        ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><?= $row['fullname'] ?></td>
                            <td>
                                <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->student_feel($row["img_highlight"]), 'name' => 'img_highlight']) ?>
                            </td>
                            <td><?= mb_strimwidth($row['content'], 0, 40, "...") ?></td>
                            <td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $row['flg_language']) ?></td>
                            <td style="text-align: left; vertical-align: middle">
                                <label class="switch ">
                                    <input type="checkbox" class="success status" recordId="<?= $row['id'] ?>" <?php if ($row['flg_status'] === 0) { ?> checked='checked' <?php } ?> />
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td style="text-align: left; vertical-align: middle">
                                <a href="<?= $this->Url->build(['controller' => 'StudentFeels', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                <button type="button" class="btn btn-danger btn-sm delete" link="<?= $this->Url->build(['controller' => 'StudentFeels', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                </button>
                            </td>
                        </tr>
                <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa bản ghi này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<script src="/admin/js/categories.js"></script>
<script>
    $(".status").change(function() {
        let id = $(this).attr("recordId")
        location.href = "/admin/student_feels/status/" + id;
    });
</script>
<?php $this->start('script_footer'); ?>
<script>
    $(function() {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
