<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
  'title' => '',
  'url' => [
    'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
    'StudentFeels' => $this->Url->build(['controller' => 'StudentFeels', 'action' => 'index'])
  ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->AppForm->create(null, [
  'type' => 'file',
  'url' => ['controller' => 'StudentFeels', 'action' => 'add'],
  'id' => 'quote-add'
]) ?>
<div class="container-fluid">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Thêm mới cảm nhận</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group">
        <label><?= __("Khoa") ?></label>
        <select name="department_id" class="custom-select <?= (isset($errStudentComments['department_id']) ? ' err-ctrl' : '') ?>">
          <option>Chọn Khoa</option>
          <?php foreach ($lstDepartments as $value) { ?>
            <option <?= isset($req["department_id"]) && $req["department_id"] == $value['id'] ? "selected=\"selected\"" : "" ?> value="<?= $value->id ?>"><?= $value->title ?></option>
          <?php } ?>
        </select>
        <?= isset($errStudentComments['department_id']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['department_id'], 'name' => 'department_id']) : '' ?>
      </div>
        <div class="row">
            <div class="col-3 col-sm-4">
                <div class="form-group">
                    <label><?= __("Ngôn ngữ") ?>:</label>
                    <div class="custom-control custom-radio">
                        <input value="0" class="custom-control-input<?= (isset($errStudentComments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="flg_language" <?= isset($req["flg_language"]) && $req["flg_language"] == 0 ? "checked" : "" ?>>
                        <label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input value="1" class="custom-control-input<?= (isset($errStudentComments['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="flg_language" <?= isset($req["flg_language"]) && $req["flg_language"] == 1 ? "checked" : "" ?>>
                        <label for="english" class="custom-control-label">Tiếng Anh</label>
                    </div>
                    <?= isset($errStudentComments['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['flg_language'], 'name' => 'flg_language']) : '' ?>
                </div>
            </div>
            <div class="col-3 col-sm-4">
                <div class="form-group">
                    <label><?= __("Trạng thái") ?>:</label>
                    <div class="custom-control custom-radio">
                        <input value="0" class="custom-control-input<?= (isset($errStudentComments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="flg_status" <?= isset($req["flg_status"]) && $req["flg_status"] == 0 ? "checked" : "" ?>>
                        <label for="active" class="custom-control-label">Hoạt động</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input value="1" class="custom-control-input<?= (isset($errStudentComments['flg_status']) ? ' err-ctrl' : '') ?>" type="radio" id="notactive" name="flg_status" <?= isset($req["flg_status"]) && $req["flg_status"] == 1 ? "checked" : "" ?>>
                        <label for="notactive" class="custom-control-label">Không hoạt động</label>
                    </div>
                    <?= isset($errStudentComments['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['flg_status'], 'name' => 'flg_status']) : '' ?>
                </div>
            </div>
        </div>
      <div class="form-group">
        <label for="InputFileLG"><?= __("Hình ảnh") ?>:</label>
        <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'img_highlight']) ?>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="img_highlight" placeholder="Chọn ảnh" class="custom-file-input input_file" id="InputFileLG" accept="image/jpeg, image/png">
            <label class="custom-file-label <?= (isset($errStudentComments['img_highlight']) ? ' err-ctrl' : '') ?>" id="img_highlight" for="InputFileLG">Chọn file</label>
          </div>
        </div>
        <?= isset($errStudentComments['img_highlight']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['img_highlight'], 'name' => 'img_highlight']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Họ tên sinh viên") ?>:</label>
        <input type="text" name="fullname" placeholder="Họ tên sinh viên" value="<?= isset($req["fullname"]) ? $req["fullname"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errStudentComments['fullname']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errStudentComments['fullname']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['fullname'], 'name' => 'fullname']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Lớp - Khóa") ?>:</label>
        <input type="text" name="class" placeholder="Lớp - Khóa" value="<?= isset($req["class"]) ? $req["class"] : "" ?>" class="form-control my-colorpicker1 colorpicker-element<?= (isset($errStudentComments['class']) ? ' err-ctrl' : '') ?>" data-colorpicker-id="1" data-original-title="" title="">
        <?= isset($errStudentComments['class']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['class'], 'name' => 'class']) : '' ?>
      </div>
      <div class="form-group">
        <label><?= __("Nội dung cảm nhận") ?>:</label>
        <textarea class="form-control<?= (isset($errStudentComments['content']) ? ' err-ctrl' : '') ?>" name="content" rows="5" placeholder="Nội dung cảm nhận"><?= isset($req["content"]) ? $req["content"] : "" ?></textarea>
        <?= isset($errStudentComments['content']) ? $this->element('Admin/errmess', ['opptions' => $errStudentComments['content'], 'name' => 'content']) : '' ?>
      </div>
      <!-- /.form-group -->
    </div>
    <!-- /.row -->
    <div class="card-footer" style="text-align: right">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-info">Quay lại</a>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
      </button>
    </div>
  </div>

  <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn có chắc chắn muốn lưu dữ liệu này không？
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?= $this->Form->end() ?>
