<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
	'title' => 'Libraries',
	'url' => [
		'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
		'Libraries' => $this->Url->build(['controller' => 'Libraries', 'action' => 'lists']),
	],
]) ?>
<?php $this->end(); ?>
<?= $this->Form->create(null, ['type' => "file", "url" => "/admin/libraries/edit/$id"]) ?>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin chung</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<div class="card-body">
			<div class="form-group ">
				<label for="type">Loại thư viện ảnh/video</label>
				<div class="custom-control custom-radio">
					<input class="custom-control-input" type="radio" id="image" name="data[type]" value="1" <?php if (!isset($data['vi']['type']) || $data['vi']['type'] == 1) { ?> checked <?php } ?>>
					<label for="image" class="custom-control-label">Ảnh</label>
				</div>
				<div class="custom-control custom-radio">
					<input class="custom-control-input" type="radio" id="video" name="data[type]" value="2" <?php if (isset($data['vi']['type']) && $data['vi']['type'] == 2) { ?> checked <?php } ?>>
					<label for="video" class="custom-control-label">Video</label>
				</div>
			</div>
			<div class="form-group">
				<label for="type">Khoa</label>
				<select name="data[department_id]" class="form-control" id="department_id">
					<option value="">Chọn Khoa</option>
					<?php foreach ($lstDepartments as $value) { ?>
						<option <?= (isset($data['vi']['department_id']) && $data['vi']['department_id'] == $value->id) ? 'selected'  : '' ?> value="<?= $value->id ?>"><?= $value->title ?></option>
					<?php } ?>
				</select>
				<?= isset($errDepartments['type']) ? $this->element('Admin/errmess', ['opptions' => $errDepartments['type'], 'name' => 'data[type]']) : '' ?>
			</div>
		</div>
	</div>
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin Libraries</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name_library">Tên thư viện ảnh/video (Việt Nam)</label>
						<input type="text" name="data[vi][LibrariesDetail][name_library]" placeholder="Nhập tiêu đề danh mục" value="<?= isset($data['vi']['LibrariesDetail']['name_library']) ? $data['vi']['LibrariesDetail']['name_library'] : '' ?>" class="form-control <?= (isset($errorsLibraryVi['name_library']) ? ' err-ctrl' : '') ?>">
						<?= isset($errorsLibraryVi['name_library']) ? $this->element('Admin/errmess', ['opptions' => $errorsLibraryVi['name_library'], 'name' => 'errorsLibraryVi[name_library]']) : '' ?>
					</div>
					<div class="form-group">
						<label><?= __("Ngôn ngữ") ?>: Việt Nam - <?= $this->Common->getTitleLanguage((int) $data['vi']['LibrariesDetail']['flg_language']) ?></label>
					</div>
					<div class="form-group">
						<label><?= __("Trạng thái (Việt Nam)") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input" type="radio" id="vi_active" name="data[vi][LibrariesDetail][flg_status]" value="0" <?php if (isset($data['vi']['LibrariesDetail']['flg_status']) && $data['vi']['LibrariesDetail']['flg_status'] === '0') { ?> checked <?php } ?>>
							<label for="vi_active" class="custom-control-label">Hoạt động</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input" type="radio" id="vi_deactive" name="data[vi][LibrariesDetail][flg_status]" value="1" <?php if (isset($data['vi']['LibrariesDetail']['flg_status']) && $data['vi']['LibrariesDetail']['flg_status'] == 1) { ?> checked <?php } ?>>
							<label for="vi_deactive" class="custom-control-label">Không hoạt động</label>
						</div>
					</div>
					<input type="hidden" name="data[vi][LibrariesDetail][id]" value="<?= isset($data['vi']['LibrariesDetail']['id']) ? $data['vi']['LibrariesDetail']['id'] : '' ?>" />
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label for="name_library">Tên thư viện ảnh/video (English)</label>
						<input type="text" name="data[en][LibrariesDetail][name_library]" placeholder="Nhập tiêu đề danh mục" value="<?= isset($data['en']['LibrariesDetail']['name_library']) ? $data['en']['LibrariesDetail']['name_library'] : '' ?>" class="form-control <?= (isset($errorsLibraryEn['name_library']) ? ' err-ctrl' : '') ?>">
						<?= isset($errorsLibraryEn['name_library']) ? $this->element('Admin/errmess', ['opptions' => $errorsLibraryEn['name_library'], 'name' => 'errorsLibraryEn[name_library]']) : '' ?>
					</div>
					<div class="form-group">
						<label><?= __("Ngôn ngữ") ?>: English - <?= $this->Common->getTitleLanguage((int) $data['en']['LibrariesDetail']['flg_language']) ?></label>
					</div>
					<div class="form-group">
						<label><?= __("Trạng thái (English)") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input" type="radio" id="en_active" name="data[en][LibrariesDetail][flg_status]" value="0" <?php if (isset($data['en']['LibrariesDetail']['flg_status']) && $data['en']['LibrariesDetail']['flg_status'] === '0') { ?> checked <?php } ?>>
							<label for="en_active" class="custom-control-label">Hoạt động</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input" type="radio" id="en_deactive" name="data[en][LibrariesDetail][flg_status]" value="1" <?php if (isset($data['en']['LibrariesDetail']['flg_status']) && $data['en']['LibrariesDetail']['flg_status'] == 1) { ?> checked <?php } ?>>
							<label for="en_deactive" class="custom-control-label">Không hoạt động</label>
						</div>
					</div>
					<input type="hidden" name="data[en][LibrariesDetail][id]" value="<?= isset($data['en']['LibrariesDetail']['id']) ? $data['en']['LibrariesDetail']['id'] : '' ?>" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin SEO</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group ">
						<label for="title">SEO title(Việt Nam)</label>
						<input name="data[vi][Seo][meta_title]" type="text" class="form-control <?= (isset($errorsSeoVi['meta_title']) ? ' err-ctrl' : '') ?>" id="title" placeholder="SEO title" value="<?= isset($data['vi']['Seo']['meta_title']) ? $data['vi']['Seo']['meta_title'] : '' ?>">
						<?= isset($errorsSeoVi['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoVi['meta_title'], 'name' => 'errorsSeoVi[meta_title]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="description">SEO description(Việt Nam)</label>
						<input name="data[vi][Seo][meta_description]" type="text" class="form-control <?= (isset($errorsSeoVi['meta_description']) ? ' err-ctrl' : '') ?>" id="description" placeholder="SEO description" value="<?= isset($data['vi']['Seo']['meta_description']) ? $data['vi']['Seo']['meta_description'] : '' ?>">
						<?= isset($errorsSeoVi['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoVi['meta_description'], 'name' => 'errorsSeoVi[meta_description]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="keywords">SEO keywords(Việt Nam)</label>
						<textarea style="width: 100%;" name="data[vi][Seo][meta_keywords]" type="text" class="form-control <?= (isset($errorsSeoVi['meta_keywords']) ? ' err-ctrl' : '') ?>" id="keywords" placeholder="SEO keywords" value=""><?= isset($data['vi']['Seo']['meta_keywords']) ? $data['vi']['Seo']['meta_keywords'] : '' ?></textarea>
						<?= isset($errorsSeoVi['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoVi['meta_keywords'], 'name' => 'errorsSeoVi[meta_keywords]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="tags">Tags(Việt Nam)</label><br />
						<input name='data[vi][Seo][tags]' class="<?= (isset($errorsSeoVi['tags']) ? ' err-ctrl' : '') ?>" value="<?= isset($data['vi']['Seo']['tags']) ? $data['vi']['Seo']['tags'] : '' ?>" data-role="tagsinput" placeholder='Nhập thẻ tags...'>
						<?= isset($errorsSeoVi['tags']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoVi['tags'], 'name' => 'errorsSeoVi[tags]']) : '' ?>
					</div>
					<input type="hidden" name="data[vi][LibrariesDetail][seo_id]" value="<?= isset($data['vi']['LibrariesDetail']['seo_id']) ? $data['vi']['LibrariesDetail']['seo_id'] : '' ?>" />
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="title">SEO title(English)</label>
						<input name="data[en][Seo][meta_title]" type="text" class="form-control <?= (isset($errorsSeoEn['meta_title']) ? ' err-ctrl' : '') ?>" id="title" placeholder="SEO title" value="<?= isset($data['en']['Seo']['meta_title']) ? $data['en']['Seo']['meta_title'] : '' ?>">
						<?= isset($errorsSeoEn['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoEn['meta_title'], 'name' => 'errorsSeoEn[meta_title]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="description">SEO description(English)</label>
						<input name="data[en][Seo][meta_description]" type="text" class="form-control <?= (isset($errorsSeoEn['meta_description']) ? ' err-ctrl' : '') ?>" id="description" placeholder="SEO description" value="<?= isset($data['en']['Seo']['meta_description']) ? $data['en']['Seo']['meta_description'] : '' ?>">
						<?= isset($errorsSeoEn['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoEn['meta_description'], 'name' => 'errorsSeoEn[meta_description]']) : '' ?>
					</div>
					<div class="form-group ">
						<label for="keywords">SEO keywords(English)</label>
						<textarea style="width: 100%;" name="data[en][Seo][meta_keywords]" type="text" class="form-control <?= (isset($errorsSeoEn['meta_keywords']) ? ' err-ctrl' : '') ?>" id="keywords" placeholder="SEO keywords" value=""><?= isset($data['en']['Seo']['meta_keywords']) ? $data['en']['Seo']['meta_keywords'] : '' ?></textarea>
						<?= isset($errorsSeoEn['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoEn['meta_keywords'], 'name' => 'errorsSeoEn[meta_keywords]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="tags">Tags(English)</label><br />
						<input name='data[en][Seo][tags]' class="<?= (isset($errorsSeoEn['tags']) ? ' err-ctrl' : '') ?>" value="<?= isset($data['en']['Seo']['tags']) ? $data['en']['Seo']['tags'] : '' ?>" data-role="tagsinput" placeholder='Nhập thẻ tags...'>
						<?= isset($errorsSeoEn['tags']) ? $this->element('Admin/errmess', ['opptions' => $errorsSeoEn['tags'], 'name' => 'errorsSeoEn[tags]']) : '' ?>
					</div>
					<input type="hidden" name="data[en][LibrariesDetail][seo_id]" value="<?= isset($data['en']['LibrariesDetail']['seo_id']) ? $data['en']['LibrariesDetail']['seo_id'] : '' ?>" />
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="card-footer" style="text-align: right">
			<a href="/admin/libraries/lists" class="btn btn-info">Quay lại</a>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
			</button>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Bạn có chắc chắn muốn lưu dữ liệu này không？
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<?= $this->Form->end() ?>
<?php $this->start('script_footer'); ?>
<script>
	//tags
	$('[name="data[vi][Seo][tags]"]').tagify();
	$('[name="data[en][Seo][tags]"]').tagify();
</script>
<?php $this->end(); ?>