<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Libraries',
    'url' => [
        'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
        'Libraries' => $this->Url->build(['controller' => 'Libraries', 'action' => 'lists']),
    ],
]) ?>
<?php $this->end(); ?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Thông tin Libraries</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
            <table id="libraries" class="table table-striped" style="margin-bottom: 30px">
                <tbody>
                    <tr>
                        <th class="table-primary" style="width: 30%">
                            Tên thư viện ảnh/video(Tiếng Việt):
                        </th>
                        <td>
                            <?= $data['vi']['LibrariesDetail']['name_library'] ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Loại thư viện ảnh/video:
                        </th>
                        <td>
                            <?php if ($data['vi']['type'] == 1) { ?>Ảnh <?php } ?>
                        <?php if ($data['vi']['type'] == 2) { ?>Video <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Khoa/Phòng ban:
                        </th>
                        <td>
                            <?= $data['vi']['department_title'] ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Ngôn ngữ:
                        </th>
                        <td>
                            Việt Nam - <?= $this->Common->getTitleLanguage((int) $data['vi']['LibrariesDetail']['flg_language']) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Trạng thái(Tiếng Việt):
                        </th>
                        <td>
                            <?php if ($data['vi']['LibrariesDetail']['flg_status'] === '0') { ?>Hoạt động <?php } ?>
                        <?php if ($data['vi']['LibrariesDetail']['flg_status'] == 1) { ?>Không hoạt động <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table id="libraries" class="table table-striped">
                <tbody>
                    <tr>
                        <th class="table-primary" style="width: 30%">
                            Tên thư viện ảnh/video(Tiếng Anh):
                        </th>
                        <td>
                            <?= $data['en']['LibrariesDetail']['name_library'] ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Loại thư viện ảnh/video:
                        </th>
                        <td>
                            <?php if ($data['en']['type'] == 1) { ?>Ảnh <?php } ?>
                        <?php if ($data['en']['type'] == 2) { ?>Video <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Khoa/Phòng ban:
                        </th>
                        <td>
                            <?= $data['vi']['department_title'] ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Ngôn ngữ:
                        </th>
                        <td>
                            English - <?= $this->Common->getTitleLanguage((int) $data['en']['LibrariesDetail']['flg_language']) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="table-primary">
                            Trạng thái(Tiếng Anh):
                        </th>
                        <td>
                            <?php if ($data['en']['LibrariesDetail']['flg_status'] === '0') { ?>Hoạt động <?php } ?>
                        <?php if ($data['en']['LibrariesDetail']['flg_status'] == 1) { ?>Không hoạt động <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer" style="text-align: right">
            <a href="/admin/libraries/edit/<?= $id ?>" class="btn btn-info">Edit</a>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Thêm ảnh/video</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
        </div>
        <?= $this->Form->create(null, ['type' => "file", "url" => "/admin/libraries/detail/$id"]) ?>
        <!-- /.box-header -->
        <div class="card-body">
            <?php if ($data['en']['type'] == 1 || $data['vi']['type'] == 1) { ?>
                <div class="form-group">
                    <div class="input-group">
                        <input name="" placeholder="Chọn ảnh..." type="text" id="input_text" style="cursor: pointer;" class="form-control" readonly="readonly">
                        <span class="input-group-append">
                            <button type="button" id="btn_file" class="btn bg-purple">Browser</button>
                        </span>
                        <input style="display: none;" name="data[image][]" type="file" id="input_img" class="custom-file-input" multiple>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <label for="type">Id Video của youbtube</label>
                    <input type="text" name="data[image][0]" placeholder="Nhập url video" value="" class="form-control">
                </div>
            <?php } ?>
        </div>
        <div class="card-footer" style="text-align: right">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Thêm
            </button>
        </div>
        <input type="hidden" name="data[type]" value="<?= $data['en']['type'] ?>" />
        <!-- Modal -->
        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Bạn có chắc chắn muốn lưu dữ liệu này không？
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Album ảnh/video</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create(null, ['type' => "file", "url" => "/admin/libraries/delete-media/$id"]) ?>
            <div class="row">
                <?php if (!isset($data['media']) || empty($data['media'])) { ?>
                    <div class="col-12">
                        <h4> Album chưa có ảnh/video!</h4>
                    </div>
                <?php } else { ?>
                    <?php foreach ($data['media'] as $key => $value) { ?>
                        <div class="col-sm-2">
                            <div class=" col-12 icheck-primary d-inline" style="padding: 0px;">
                                <input type="checkbox" id="media-<?= $value['LibrariesMedia']['id'] ?>" name="data[media][<?= $value['LibrariesMedia']['id'] ?>]" value="<?= $value['LibrariesMedia']['id'] ?>">
                                <label for="media-<?= $value['LibrariesMedia']['id'] ?>"></label>
                            </div>
                            <?php if ($data['en']['type'] == 1 || $data['vi']['type'] == 1) { ?>
                                <a href="/admin/upload/libraries/<?= $value['LibrariesMedia']['path'] ?>" data-toggle="lightbox" data-title="Image Gallery" data-gallery="gallery">
                                    <img src="/admin/upload/libraries/<?= $value['LibrariesMedia']['path'] ?>" class="img-fluid mb-2" alt="Image Gallery" />
                                </a>
                            <?php } else { ?>
                                <a href="https://www.youtube.com/watch?v=<?= $value['LibrariesMedia']['path'] ?>" data-toggle="lightbox" data-title="Video Gallery" data-gallery="gallery">
                                    <img src="https://img.youtube.com/vi/<?= $value['LibrariesMedia']['path'] ?>/0.jpg" class="img-fluid mb-2" alt="Video Gallery" />
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="card-footer" style="text-align: right">
            <a href="/admin/libraries/lists" class="btn btn-info">Quay lại</a>
            <button type="submit" class="btn btn-primary" onclick="return confirm('Bạn có chắc chắn muốn lưu dữ liệu này không？')">Xóa</button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <!-- /.row -->
</div>
<?php $this->start('script_footer'); ?>
<!-- jQuery -->
<script src="/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Ekko Lightbox -->
<script src="/admin/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script>
    $(function() {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });
    })
    $('#btn_file').click(function() {
        $('#input_img').click();
    });
    $('#input_text').click(function() {
        $('#input_img').click();
    });
    $("#input_img").change(function(e) {
        let path = "";
        for (let i = 0; i < e.target.files.length; i++) {
            console.log(e.target.files[i]);
            path += e.target.files[i].name;
            path += "  ||  ";
        };
        $('#input_text').val(path);
    });
</script>
<?php $this->end(); ?>