<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
	'title' => 'Libraries',
	'url' => [
		'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
		'Libraries' => $this->Url->build(['controller' => 'Libraries', 'action' => 'lists']),
	],
]) ?>
<?php $this->end(); ?>

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="box-title">Tìm kiếm thư viện ảnh/video</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<?= $this->AppForm->create(null) ?>
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<input type="text" name="data[name_library]" placeholder="Nhập tên thư viện ảnh/video" value="<?= isset($param['name_library']) ? $param['name_library'] : '' ?>" class="form-control">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<div class="row">
								<div class=" col-12 icheck-primary d-inline">
									<input type="checkbox" id="image" name="data[type][image]" value="1" <?php if (isset($param['type']['image']) && $param['type']['image'] == 1) { ?> checked <?php } ?>>
									<label for="image">Ảnh</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="row">
								<div class=" col-12 icheck-primary d-inline">
									<input type="checkbox" id="video" name="data[type][video]" value="2" <?php if (isset($param['type']['video']) && $param['type']['video'] == 2) { ?> checked <?php } ?>>
									<label for="video">Video</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="card-footer">
				<a href="/admin/libraries/add" class="btn btn-success btn-fw">
					<i class="fas fa-folder-plus"></i>&nbsp;&nbsp; Thêm thư viện
				</a>
				<button type="submit" class="btn btn-primary">Tìm kiếm</button>
			</div>
			<?= $this->AppForm->end() ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="box-title">Danh sách thư viện ảnh/video</h3>
			</div>
			<!-- /.box-header -->
			<div class="card-body no-padding">
				<table id="listLibraries" class="table table-striped">
					<thead>
						<tr>
							<th style="width: 2%">
								STT
							</th>
							<th style="width: 35%">
								Tên thư viện
							</th>
							<th style="width: 13%">
								Ngôn ngữ
							</th>
							<th style="width: 15%">
								Loại thư viện
							</th>
							<th style="width: 10%">
								Trạng thái
							</th>
							<th style="width: 20%">
								Actions
							</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($libraries as $key => $record) { ?>
							<tr>
								<td class="py-1">
									<?= $key + 1 ?>
								</td>
								<td>
									<?= $record['LibrariesDetail']['name_library'] ?>
								</td>
								<td style="text-align: left; vertical-align: middle"><?= $this->Common->getTitleLanguage((int) $record['LibrariesDetail']['flg_language']) ?></td>
								<td>
									<?php if ($record['type'] === 1) { ?>Thư viện Ảnh <?php } ?>
								<?php if ($record['type'] === 2) { ?>Thư viện Video <?php } ?>
								</td>
								<td>
									<label class="switch ">
										<input type="checkbox" class="success status" recordId="<?= $record['LibrariesDetail']['id'] ?>" <?php if ($record['LibrariesDetail']['flg_status'] == '0') { ?> checked='checked' <?php } ?> />
										<span class="slider round"></span>
									</label>
								</td>
                                <style>
                                    @media screen and (max-width: 1230px) {
                                        .with-1230{
                                            padding: 15px 0px!important;
                                        }
                                    }
                                </style>
								<td class="with-1230">
									<a href="/admin/libraries/edit/<?= $record['id'] ?>" class="btn btn-primary btn-sm">Edit</a>
									<a href="/admin/libraries/detail/<?= $record['id'] ?>" class="btn btn-success btn-sm">Detail</a>
									<button type="button" class="btn btn-danger btn-sm delete" link="/admin/libraries/delete/<?= $record['id'] ?>" data-toggle="modal" data-target="#confirmModal">Delete</button>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Bạn có chắc chắn muốn xóa danh mục này không?
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<div style="display: none;">
	<select id="parentIdDefault">
		<option value="">Please choose a library</option>
	</select>
	<select class="form-control" id="parentIdList">
		<?php foreach ($listLibrary as $library) { ?>
			<?php if (isset($param['parent_id']) && $library['id'] == $param['parent_id']) { ?>
				<option typecate="<?= $library['type'] ?>" value="<?= $library['id'] ?>" selected="selected"><?= $library['title'] ?></option>
			<?php } else { ?>
				<option typecate="<?= $library['type'] ?>" value="<?= $library['id'] ?>"><?= $library['title'] ?></option>
			<?php } ?>
		<?php } ?>
	</select>
</div>
<?php $this->start('script_footer'); ?>
<!-- DataTables -->
<script src="/admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="/admin/dist/js/adminlte.min.js"></script>
<script src="/admin/js/libraries.js"></script>
<script>
	$(function() {
		$('#listLibraries').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
		});
	});
	$(".status").change(function() {
		let id = $(this).attr("recordId");
		location.href = "/admin/libraries/change-status/" + id;
	});
</script>
<?php $this->end(); ?>
