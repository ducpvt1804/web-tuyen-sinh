<?= $this->element('Admin/content_header', [
    'title' => 'Thêm mới tài khoản',
    'url' => [
        'Tài khoản' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Thêm mới' => null,
    ],
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Thông Tài Khoản</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('user_name', [
                                        'class' => 'form-control' . (isset($err['user_name']) ? ' err-ctrl' : ''),
                                        'label' => 'Tên đăng nhập:',
                                        'placeholder' => 'Nhập tên đăng nhập',
                                    ]) ?>
                                    <?= isset($err['user_name']) ? $this->element('Admin/errmess', ['opptions' => $err['user_name'], 'name' => 'user_name']) : '' ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('password', [
                                        'class' => 'form-control' . (isset($err['password']) ? ' err-ctrl' : ''),
                                        'label' => 'Mật khẩu:',
                                        'placeholder' => 'Nhập mật khẩu',
                                    ]) ?>
                                    <?= isset($err['password']) ? $this->element('Admin/errmess', ['opptions' => $err['password'], 'name' => 'password']) : '' ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('password_confirm', [
                                        'type' => 'password',
                                        'class' => 'form-control' . (isset($err['password_confirm']) ? ' err-ctrl' : ''),
                                        'label' => 'Xác nhận mật khẩu:',
                                        'placeholder' => 'Nhập lại mật khẩu',
                                    ]) ?>
                                    <?= isset($err['password_confirm']) ? $this->element('Admin/errmess', ['opptions' => $err['password_confirm'], 'name' => 'password_confirm']) : '' ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('fullname', [
                                'class' => 'form-control' . (isset($err['fullname']) ? ' err-ctrl' : ''),
                                'label' => 'Họ và tên:',
                                'placeholder' => 'Nhập họ và tên',
                            ]) ?>
                            <?= isset($err['fullname']) ? $this->element('Admin/errmess', ['opptions' => $err['fullname'], 'name' => 'fullname']) : '' ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="end_date">Ngày sinh:</label>
                                    <input type="date" class="form-control<?= (isset($err['birth_date']) ? ' err-ctrl' : '') ?>" name="birth_date" value="<?= isset($req['birth_date']) ? $req['birth_date'] : '' ?>">
                                    <?= isset($err['birth_date']) ? $this->element('Admin/errmess', ['opptions' => $err['birth_date'], 'name' => 'birth_date']) : '' ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->element("Admin/Form/select", [
                                        "class" => "form-control custom-select" . (isset($err['sex']) ? ' err-ctrl' : ''),
                                        "name" => "sex",
                                        "label" => "Giới tính:",
                                        "options" => $this->Sex->all(),
                                        "selected" => isset($req['sex']) ? $req['sex'] : '',
                                    ]) ?>
                                    <?= isset($err['sex']) ? $this->element('Admin/errmess', ['opptions' => $err['sex'], 'name' => 'sex']) : '' ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('telno', [
                                        'type' => 'text',
                                        'class' => 'form-control' . (isset($err['telno']) ? ' err-ctrl' : ''),
                                        'label' => 'Số điện thoại:',
                                        'placeholder' => 'Nhập số điện thoại',
                                    ]) ?>
                                    <?= isset($err['telno']) ? $this->element('Admin/errmess', ['opptions' => $err['telno'], 'name' => 'telno']) : '' ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('email', [
                                'class' => 'form-control' . (isset($err['email']) ? ' err-ctrl' : ''),
                                'label' => 'Email:',
                                'placeholder' => 'Nhập email',
                            ]) ?>
                            <?= isset($err['email']) ? $this->element('Admin/errmess', ['opptions' => $err['email'], 'name' => 'email']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('address', [
                                'class' => 'form-control' . (isset($err['address']) ? ' err-ctrl' : ''),
                                'label' => 'Địa chỉ:',
                                'placeholder' => 'Nhập địa chỉ',
                            ]) ?>
                            <?= isset($err['address']) ? $this->element('Admin/errmess', ['opptions' => $err['address'], 'name' => 'address']) : '' ?>
                        </div>
                        <div class="form-group">
                            <label for="input_avarta">Hình ảnh:</label>
                            <?= $this->element('Admin/Form/img_preview', ['path' => '', 'name' => 'avarta']) ?>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="avarta" type="file" id="input_avarta" class="custom-file-input input_file" accept="image/jpeg, image/png">
                                    <label class="custom-file-label<?= (isset($err['avarta']) ? ' err-ctrl' : '') ?>" id="avarta" for="input_avarta">Chọn ảnh...</label>
                                </div>
                            </div>
                            <?= isset($err['avarta']) ? $this->element('Admin/errmess', ['opptions' => $err['avarta'], 'name' => 'avarta']) : '' ?>
                        </div>
                        <div class="form-group">
                            <?= $this->element('Admin/Form/radio', [
                                'class' => (isset($err['flg_status']) ? ' err-ctrl' : ''),
                                'name' => 'flg_status',
                                'label' => 'Trạng thái:',
                                'options' => $this->StatusUsers->all(),
                                'checked' => isset($req['flg_status']) ? $req['flg_status'] : '',
                            ]) ?>
                            <?= isset($err['flg_status']) ? $this->element('Admin/errmess', ['opptions' => $err['flg_status'], 'name' => 'flg_status']) : '' ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Quyền hạn</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <?= $this->element("Admin/Form/select", [
                        "class" => "form-control custom-select" . (isset($err['role_id']) ? ' err-ctrl' : ''),
                        "name" => "role_id",
                        "label" => "Quền hạn",
                        "options" => $this->Role->all(),
                        "selected" => isset($req['role_id']) ? $req['role_id'] : '',
                    ]) ?>
                    <?= isset($err['role_id']) ? $this->element('Admin/errmess', ['opptions' => $err['role_id'], 'name' => 'role_id']) : '' ?>
                </div>
                <div class="form-group" id="list_role" style="display: none;">
                    <label><?= __("Khoa") ?>:</label>
                    <?= $this->element("Admin/Form/list_role", [
                        'options' => $this->ListDepartment->all(['type' => 0]),
                        'req' => $req,
                        'name' => 'role'
                    ]) ?>
                    <label style="margin-top: 10px"><?= __("Phòng ban") ?>:</label>
                    <?= $this->element("Admin/Form/list_role", [
                        'options' => $this->ListDepartment->all(['type' => 1]),
                        'req' => $req,
                        'name' => 'role'
                    ]) ?>
                </div>
                <div class="form-group" id="list_modules" style="display: none;">
                    <label><?= __("Modules") ?>:</label>
                    <?= $this->element("Admin/Form/list_role", [
                        'options' => $this->Modules->all(),
                        'req' => $req,
                        'name' => 'modules'
                    ]) ?>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer" style="text-align: right">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'index']) ?>" class="btn btn-info">Trờ về</a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu</button>
            </div>
        </div>
        <!-- /.card -->
    </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn lưu dữ liệu này không？
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

<?php $this->start('script_footer'); ?>
<script>
    $('select[name = "role_id"]').change(function() {
        toggleListRole();
    });

    toggleListRole();

    function toggleListRole() {

        if ($('select[name = "role_id"]').val() == 2) {
            $("#list_role").show();
            $("#list_modules").hide();
        } else if ($('select[name = "role_id"]').val() == 1) {
            $("#list_modules").show();
            $("#list_role").hide();
        } else {
            $("#list_modules").hide();
            $("#list_role").hide();
        }
    }
</script>
<?php $this->end(); ?>