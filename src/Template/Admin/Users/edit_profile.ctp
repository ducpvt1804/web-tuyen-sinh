<?= $this->element('Admin/content_header', [
    'title' => 'Chỉnh sửa tài khoản',
    'url' => [
        'Tài khoản' => $this->Url->build(['controller' => 'Events', 'action' => 'index']),
        'Chỉnh sửa' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<?= $this->Form->create(null, ['enctype' => 'multipart/form-data']) ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Thông tin tài khoản</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="input_avarta">Hình ảnh:</label>
                            <?= $this->element('Admin/Form/img_preview', ['path' => $this->Root->users($user['avarta']), 'name' => 'avarta']) ?>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="avarta" type="file" id="input_avarta" class="custom-file-input input_file" accept="image/jpeg, image/png">
                                    <label class="custom-file-label" for="input_avarta">Đổi ảnh...</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('', [
                                        'class' => 'form-control',
                                        'label' => 'Tên đăng nhập:',
                                        'disabled' => 'disabled',
                                        'placeholder' => 'Nhập tên đăng nhập',
                                        "value" => isset($user['user_name']) ? $user['user_name'] : ''
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('password', [
                                        'class' => 'form-control',
                                        'label' => 'Mật khẩu:',
                                        'placeholder' => 'Nhập mật khẩu'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('password_confirm', [
                                        'type' => 'password',
                                        'class' => 'form-control',
                                        'label' => 'Xác nhận mật khẩu:',
                                        'placeholder' => 'Nhập lại mật khẩu'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('fullname', [
                                'class' => 'form-control',
                                'label' => 'Họ và tên:',
                                'placeholder' => 'Nhập họ và tên',
                                "value" => isset($req['fullname']) ? $req['fullname'] : (isset($user['fullname']) ? $user['fullname'] : '')
                            ]) ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="end_date">Ngày sinh:</label>
                                    <input type="date" class="form-control" name="birth_date" value="<?= isset($req['birth_date']) ? $req['birth_date'] : (isset($user['birth_date']) ? $this->Time->format($user['birth_date'], 'Y-MM-dd') : '') ?>">
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->element("Admin/Form/select", [
                                        "class" => "form-control custom-select",
                                        "name" => "sex",
                                        "label" => "Giới tính:",
                                        "options" => $this->Sex->all(),
                                        "selected" => isset($req['sex']) ? $req['sex'] : (isset($user['sex']) ? $user['sex'] : '')
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?= $this->Form->control('telno', [
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => 'Số điện thoại:',
                                        'placeholder' => 'Nhập số điện thoại',
                                        "value" => isset($req['telno']) ? $req['telno'] : (isset($user['telno']) ? $user['telno'] : '')
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('', [
                                'class' => 'form-control',
                                'disabled' => 'disabled',
                                'label' => 'Email:',
                                'placeholder' => 'Nhập email',
                                "value" => isset($user['email']) ? $user['email'] : ''
                            ]) ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('address', [
                                'class' => 'form-control',
                                'label' => 'Địa chỉ:',
                                'placeholder' => 'Nhập địa chỉ',
                                "value" => isset($req['address']) ? $req['address'] : (isset($user['address']) ? $user['address'] : '')
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer" style="text-align: right">
                <a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index']) ?>" class="btn btn-info">Trờ về</a>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
        <!-- /.card -->
    </div>

</div>
<?= $this->Form->end() ?>