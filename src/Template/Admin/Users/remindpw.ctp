<?= $this->Form->create(null, ['style' => 'padding: 40px 0px;']) ?>
<p>Bạn có thể đặt lại mật khẩu từ email của bạn. Vui lòng nhập tài khoản hoặc email của bạn.</p>
<div class="form-group">
    <?= $this->Form->control('user_name', [
        'class' => 'form-control',
        'label' => false,
        'placeholder' => 'Nhập tên tài khoản hoặc email',
        'required' => 'required'
    ]) ?>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0 auto;">
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Tôi đồng ý</button>
        </div>
        <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'login']) ?>">Quay lại đăng nhập.</a>
    </div>
</div>
<?= $this->Form->end() ?>