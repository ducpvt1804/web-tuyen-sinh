<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Choose Us'), ['controller' => 'ChooseUs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Choose Us'), ['controller' => 'ChooseUs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List List Departments'), ['controller' => 'ListDepartments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New List Department'), ['controller' => 'ListDepartments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Partners'), ['controller' => 'Partners', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Partner'), ['controller' => 'Partners', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Quotes'), ['controller' => 'Quotes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Quote'), ['controller' => 'Quotes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Role Detail'), ['controller' => 'RoleDetail', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role Detail'), ['controller' => 'RoleDetail', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Student Comments'), ['controller' => 'StudentComments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student Comment'), ['controller' => 'StudentComments', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Name') ?></th>
            <td><?= h($user->user_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fullname') ?></th>
            <td><?= h($user->fullname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telno') ?></th>
            <td><?= h($user->telno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($user->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avarta') ?></th>
            <td><?= h($user->avarta) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Site') ?></th>
            <td><?= $this->Number->format($user->flg_site) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Flg Delete') ?></th>
            <td><?= $this->Number->format($user->flg_delete) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birth Date') ?></th>
            <td><?= h($user->birth_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Date') ?></th>
            <td><?= h($user->create_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Update Date') ?></th>
            <td><?= h($user->update_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Categories') ?></h4>
        <?php if (!empty($user->categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Order Num') ?></th>
                <th scope="col"><?= __('Flg Site') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Flg Show') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->categories as $categories): ?>
            <tr>
                <td><?= h($categories->id) ?></td>
                <td><?= h($categories->seo_id) ?></td>
                <td><?= h($categories->user_id) ?></td>
                <td><?= h($categories->title) ?></td>
                <td><?= h($categories->slug) ?></td>
                <td><?= h($categories->parent_id) ?></td>
                <td><?= h($categories->type) ?></td>
                <td><?= h($categories->order_num) ?></td>
                <td><?= h($categories->flg_site) ?></td>
                <td><?= h($categories->flg_language) ?></td>
                <td><?= h($categories->flg_show) ?></td>
                <td><?= h($categories->create_date) ?></td>
                <td><?= h($categories->update_date) ?></td>
                <td><?= h($categories->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $categories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $categories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $categories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Choose Us') ?></h4>
        <?php if (!empty($user->choose_us)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->choose_us as $chooseUs): ?>
            <tr>
                <td><?= h($chooseUs->id) ?></td>
                <td><?= h($chooseUs->user_id) ?></td>
                <td><?= h($chooseUs->title) ?></td>
                <td><?= h($chooseUs->description) ?></td>
                <td><?= h($chooseUs->img_highlight) ?></td>
                <td><?= h($chooseUs->create_date) ?></td>
                <td><?= h($chooseUs->update_date) ?></td>
                <td><?= h($chooseUs->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ChooseUs', 'action' => 'view', $chooseUs->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ChooseUs', 'action' => 'edit', $chooseUs->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ChooseUs', 'action' => 'delete', $chooseUs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chooseUs->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Departments') ?></h4>
        <?php if (!empty($user->departments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Cate Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Parent Key') ?></th>
                <th scope="col"><?= __('Banner') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Shot Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->departments as $departments): ?>
            <tr>
                <td><?= h($departments->id) ?></td>
                <td><?= h($departments->seo_id) ?></td>
                <td><?= h($departments->cate_id) ?></td>
                <td><?= h($departments->user_id) ?></td>
                <td><?= h($departments->parent_key) ?></td>
                <td><?= h($departments->banner) ?></td>
                <td><?= h($departments->img_highlight) ?></td>
                <td><?= h($departments->flg_language) ?></td>
                <td><?= h($departments->title) ?></td>
                <td><?= h($departments->shot_title) ?></td>
                <td><?= h($departments->description) ?></td>
                <td><?= h($departments->type) ?></td>
                <td><?= h($departments->create_date) ?></td>
                <td><?= h($departments->update_date) ?></td>
                <td><?= h($departments->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Departments', 'action' => 'view', $departments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Departments', 'action' => 'edit', $departments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Departments', 'action' => 'delete', $departments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $departments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Events') ?></h4>
        <?php if (!empty($user->events)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Flg Department') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Flg Status') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Map') ?></th>
                <th scope="col"><?= __('Telno') ?></th>
                <th scope="col"><?= __('Position') ?></th>
                <th scope="col"><?= __('Cost') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->events as $events): ?>
            <tr>
                <td><?= h($events->id) ?></td>
                <td><?= h($events->user_id) ?></td>
                <td><?= h($events->seo_id) ?></td>
                <td><?= h($events->flg_department) ?></td>
                <td><?= h($events->title) ?></td>
                <td><?= h($events->slug) ?></td>
                <td><?= h($events->img_highlight) ?></td>
                <td><?= h($events->address) ?></td>
                <td><?= h($events->flg_status) ?></td>
                <td><?= h($events->start_date) ?></td>
                <td><?= h($events->end_date) ?></td>
                <td><?= h($events->flg_language) ?></td>
                <td><?= h($events->map) ?></td>
                <td><?= h($events->telno) ?></td>
                <td><?= h($events->position) ?></td>
                <td><?= h($events->cost) ?></td>
                <td><?= h($events->content) ?></td>
                <td><?= h($events->create_date) ?></td>
                <td><?= h($events->update_date) ?></td>
                <td><?= h($events->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Events', 'action' => 'view', $events->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Events', 'action' => 'edit', $events->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Events', 'action' => 'delete', $events->id], ['confirm' => __('Are you sure you want to delete # {0}?', $events->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related List Departments') ?></h4>
        <?php if (!empty($user->list_departments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Flg Status') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->list_departments as $listDepartments): ?>
            <tr>
                <td><?= h($listDepartments->id) ?></td>
                <td><?= h($listDepartments->user_id) ?></td>
                <td><?= h($listDepartments->title) ?></td>
                <td><?= h($listDepartments->flg_status) ?></td>
                <td><?= h($listDepartments->url) ?></td>
                <td><?= h($listDepartments->type) ?></td>
                <td><?= h($listDepartments->create_date) ?></td>
                <td><?= h($listDepartments->update_date) ?></td>
                <td><?= h($listDepartments->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ListDepartments', 'action' => 'view', $listDepartments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ListDepartments', 'action' => 'edit', $listDepartments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ListDepartments', 'action' => 'delete', $listDepartments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $listDepartments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related News') ?></h4>
        <?php if (!empty($user->news)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seo Id') ?></th>
                <th scope="col"><?= __('Cate Id') ?></th>
                <th scope="col"><?= __('Cate Depart Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Flg Status') ?></th>
                <th scope="col"><?= __('Flg Language') ?></th>
                <th scope="col"><?= __('Schedule') ?></th>
                <th scope="col"><?= __('Short Description') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('View') ?></th>
                <th scope="col"><?= __('Date Edit') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->news as $news): ?>
            <tr>
                <td><?= h($news->id) ?></td>
                <td><?= h($news->seo_id) ?></td>
                <td><?= h($news->cate_id) ?></td>
                <td><?= h($news->cate_depart_id) ?></td>
                <td><?= h($news->user_id) ?></td>
                <td><?= h($news->title) ?></td>
                <td><?= h($news->img_highlight) ?></td>
                <td><?= h($news->slug) ?></td>
                <td><?= h($news->flg_status) ?></td>
                <td><?= h($news->flg_language) ?></td>
                <td><?= h($news->schedule) ?></td>
                <td><?= h($news->short_description) ?></td>
                <td><?= h($news->content) ?></td>
                <td><?= h($news->view) ?></td>
                <td><?= h($news->date_edit) ?></td>
                <td><?= h($news->create_date) ?></td>
                <td><?= h($news->update_date) ?></td>
                <td><?= h($news->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'News', 'action' => 'view', $news->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'News', 'action' => 'edit', $news->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'News', 'action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Partners') ?></h4>
        <?php if (!empty($user->partners)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Img') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Link') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->partners as $partners): ?>
            <tr>
                <td><?= h($partners->id) ?></td>
                <td><?= h($partners->user_id) ?></td>
                <td><?= h($partners->title) ?></td>
                <td><?= h($partners->img) ?></td>
                <td><?= h($partners->description) ?></td>
                <td><?= h($partners->link) ?></td>
                <td><?= h($partners->create_date) ?></td>
                <td><?= h($partners->update_date) ?></td>
                <td><?= h($partners->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Partners', 'action' => 'view', $partners->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Partners', 'action' => 'edit', $partners->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Partners', 'action' => 'delete', $partners->id], ['confirm' => __('Are you sure you want to delete # {0}?', $partners->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Quotes') ?></h4>
        <?php if (!empty($user->quotes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Position') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Cate') ?></th>
                <th scope="col"><?= __('Flg Laguage') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->quotes as $quotes): ?>
            <tr>
                <td><?= h($quotes->id) ?></td>
                <td><?= h($quotes->user_id) ?></td>
                <td><?= h($quotes->title) ?></td>
                <td><?= h($quotes->position) ?></td>
                <td><?= h($quotes->img_highlight) ?></td>
                <td><?= h($quotes->content) ?></td>
                <td><?= h($quotes->cate) ?></td>
                <td><?= h($quotes->flg_laguage) ?></td>
                <td><?= h($quotes->create_date) ?></td>
                <td><?= h($quotes->update_date) ?></td>
                <td><?= h($quotes->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Quotes', 'action' => 'view', $quotes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Quotes', 'action' => 'edit', $quotes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Quotes', 'action' => 'delete', $quotes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $quotes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Role Detail') ?></h4>
        <?php if (!empty($user->role_detail)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Role Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Module Id') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->role_detail as $roleDetail): ?>
            <tr>
                <td><?= h($roleDetail->id) ?></td>
                <td><?= h($roleDetail->role_id) ?></td>
                <td><?= h($roleDetail->user_id) ?></td>
                <td><?= h($roleDetail->module_id) ?></td>
                <td><?= h($roleDetail->create_date) ?></td>
                <td><?= h($roleDetail->update_date) ?></td>
                <td><?= h($roleDetail->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'RoleDetail', 'action' => 'view', $roleDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'RoleDetail', 'action' => 'edit', $roleDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'RoleDetail', 'action' => 'delete', $roleDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $roleDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Student Comments') ?></h4>
        <?php if (!empty($user->student_comments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Img Highlight') ?></th>
                <th scope="col"><?= __('Fullname') ?></th>
                <th scope="col"><?= __('Class') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Create Date') ?></th>
                <th scope="col"><?= __('Update Date') ?></th>
                <th scope="col"><?= __('Flg Delete') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->student_comments as $studentComments): ?>
            <tr>
                <td><?= h($studentComments->id) ?></td>
                <td><?= h($studentComments->user_id) ?></td>
                <td><?= h($studentComments->img_highlight) ?></td>
                <td><?= h($studentComments->fullname) ?></td>
                <td><?= h($studentComments->class) ?></td>
                <td><?= h($studentComments->content) ?></td>
                <td><?= h($studentComments->create_date) ?></td>
                <td><?= h($studentComments->update_date) ?></td>
                <td><?= h($studentComments->flg_delete) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StudentComments', 'action' => 'view', $studentComments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StudentComments', 'action' => 'edit', $studentComments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StudentComments', 'action' => 'delete', $studentComments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentComments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
