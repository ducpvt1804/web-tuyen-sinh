<?= $this->Form->create(null, ['style' => 'padding: 40px 0px;']) ?>
<div class="form-group">
    <?= $this->Form->control('user_name', [
        'class' => 'form-control',
        'label' => 'Tên tài khoản',
        'placeholder' => 'Nhập tên tài khoản',
        'required' => 'required'
    ]) ?>
</div>
<div class="form-group">
    <?= $this->Form->control('password', [
        'class' => 'form-control',
        'label' => 'Mật khẩu',
        'placeholder' => 'Nhập mật khẩu',
        'required' => 'required'
    ]) ?>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0 auto;">
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Đăng nhập</button>
        </div>
        <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'remindpw']) ?>">Quên mật khẩu?</a>
    </div>
</div>
<?= $this->Form->end() ?>