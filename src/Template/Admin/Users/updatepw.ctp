<?= $this->Form->create(null, ['style' => 'padding: 40px 0px;']) ?>
<div class="form-group">
    <?= $this->Form->control('password', [
        'class' => 'form-control',
        'label' => 'Mật khẩu mới:',
        'placeholder' => 'Nhập mật khẩu mới',
        'required' => 'required'
    ]) ?>
</div>
<div class="form-group">
    <?= $this->Form->control('password_confirm', [
        'type' => 'password',
        'class' => 'form-control',
        'label' => 'Nhập lại mật khẩu',
        'placeholder' => 'Nhập lại mật khẩu',
        'required' => 'required'
    ]) ?>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0 auto;">
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Tôi đồng ý</button>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>