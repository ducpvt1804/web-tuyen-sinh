<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
    'title' => 'Tài Khoản',
    'url' => [
        'Tài khoản' => $this->Url->build(['controller' => 'Users', 'action' => 'index']),
        'Danh sách tài khoản' => null
    ]
]) ?>
<?php $this->end();
$req = $this->getRequest()->getData(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="box-title">Tìm kiếm tài khoản</h3>
            </div>
            <!-- /.box-header -->
            <?= $this->Form->create() ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "role_id",
                                "default" => "Quyền hạn",
                                "options" => $this->Role->all(),
                                "selected" => isset($req['role_id']) ? $req['role_id'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "flg_status",
                                "default" => "Trạng thái",
                                "options" => $this->StatusUsers->all(),
                                "selected" => isset($req['flg_status']) ? $req['flg_status'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <?= $this->element("Admin/Form/select", [
                                "class" => "form-control custom-select",
                                "name" => "sex",
                                "default" => "Giới tính",
                                "options" => $this->Sex->all(),
                                "selected" => isset($req['sex']) ? $req['sex'] : ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" name="keyword" value="<?= isset($req['keyword']) ? $req['keyword'] : '' ?>" class="form-control" placeholder="Enter key search...">
                        </div>
                    </div>

                </div>

            </div>
            <div class="card-footer">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'add']) ?>" class="btn btn-success btn-fw"><i class="fas fa-folder-plus"></i> Thêm mới</a>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="box-title">Danh sách tài khoản</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body no-padding">
        <?= $this->Form->end() ?>
        <table id="example2" class="table table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Ảnh</th>
                    <th>Tên đăng nhập</th>
                    <th>Email</th>
                    <th>Họ và tên</th>
                    <th>Số điện thoại</th>
                    <th>Quyền hạn</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($users) && count($users) > 0) :
                    foreach ($users as $key => $row) :
                        if ((isset($this->Role->getRole($row['id'])['value'], $req['role_id']) && $req['role_id'] != "" && $this->Role->getRole($row['id'])['value'] != $req['role_id'])) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><img style="max-height: 100px; margin: 5px;" src="<?= (isset($row['avarta']) && !empty($row['avarta'])) ? '/admin/upload/users/' . $row['avarta'] : '/img/system/no-image.png' ?>" alt="your image" /></td>
                            <td><?= $row['user_name'] ?></td>
                            <td><?= $row['email'] ?></td>
                            <td><?= $row['fullname'] ?></td>
                            <td><?= $row['telno'] ?></td>
                            <td><?= isset($this->Role->getRole($row['id'])['title']) ? $this->Role->getRole($row['id'])['title'] : "" ?></td>
                            <td style="text-align: left; vertical-align: middle"><?= $this->StatusUsers->first($row['flg_status']) ?></td>
                            <td style="text-align: left; vertical-align: middle">
                                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'edit', $row['id']]) ?>" class="btn btn-primary btn-sm">Edit</a>
                                <button type="button" class="btn btn-danger btn-sm mr-2 delete" link="<?= $this->Url->build(['controller' => 'Users', 'action' => 'delete', $row['id']]) ?>" data-toggle="modal" data-target="#confirmModal">Delete
                                </button>
                            </td>
                        </tr>
                <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bạn có chắc chắn muốn xóa quote này không?
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-danger mr-2 btn-yes">Yes</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<?php $this->start('script_footer'); ?>
<script src="/admin/js/categories.js"></script>
<script>
    $(function() {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
</script>
<?php $this->end(); ?>
