<link rel="stylesheet" href="/admin/css/categories.css">
<?php $this->start('content_header'); ?>
<?= $this->element('Admin/content_header', [
	'title' => 'Categories',
	'url' => [
		'Home' => $this->Url->build(['controller' => 'Home', 'action' => 'index']),
		'Categories' => $this->Url->build(['controller' => 'Categories', 'action' => 'lists']),
	],
]) ?>
<?php $this->end(); ?>
<?= $this->Form->create(null, ['type' => "POST", "url" => "/admin/categories/edit/$id"]) ?>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin Categories</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="card-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="type">Tiêu đề danh mục</label>
						<input type="text" name="data[title]" placeholder="Nhập tiêu đề danh mục" value="<?= isset($data['title']) ? $data['title'] : '' ?>" class="form-control<?= (isset($errCategory['title']) ? ' err-ctrl' : '') ?>">
						<?= isset($errCategory['title']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['title'], 'name' => 'data[title]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="type">Loại danh mục</label>
						<select name="data[type]" class="form-control<?= (isset($errCategory['type']) ? ' err-ctrl' : '') ?>" id="type">
							<option value="">Please choose a type</option>
							<option value="0" <?php if (isset($data['type']) && (int) $data['type'] === 0) { ?> selected='selected' <?php } ?>>Menu Header</option>
							<option value="1" <?php if (isset($data['type']) && $data['type'] == 1) { ?> selected='selected' <?php } ?>>Menu Footer</option>
							<option value="2" <?php if (isset($data['type']) && $data['type'] == 2) { ?> selected='selected' <?php } ?>>Danh mục tin tức</option>
						</select>
						<?= isset($errCategory['type']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['type'], 'name' => 'data[type]']) : '' ?>
					</div>
					<div class="form-group">
						<label><?= __("Ngôn ngữ") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input <?= (isset($errCategory['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="vietnamese" name="data[flg_language]" value="0" <?php if (!isset($data['flg_language']) || (int) $data['flg_language'] === 0) { ?> checked <?php } ?>>
							<label for="vietnamese" class="custom-control-label">Tiếng Việt</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input <?= (isset($errCategory['flg_language']) ? ' err-ctrl' : '') ?>" type="radio" id="english" name="data[flg_language]" value="1" <?php if (isset($data['flg_language']) && $data['flg_language'] == 1) { ?> checked <?php } ?>>
							<label for="english" class="custom-control-label">Tiếng Anh</label>
						</div>
						<?= isset($errCategory['flg_language']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['flg_language'], 'name' => 'data[flg_language]']) : '' ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="parentId">Danh mục cha</label>
						<select name="data[parent_id]" class="form-control <?= (isset($errCategory['parent_id']) ? ' err-ctrl' : '') ?>" id="parentId">
							<option value="">Please choose a category</option>
						</select>
						<?= isset($errCategory['parent_id']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['parent_id'], 'name' => 'data[parent_id]']) : '' ?>
					</div>
					<div class="form-group">
						<label for="type">Url</label>
						<input type="text" name="data[url]" placeholder="Nhập url menu" value="<?= isset($data['url']) ? $data['url'] : '' ?>" class="form-control <?= (isset($errCategory['url']) ? ' err-ctrl' : '') ?>">
						<?= isset($errCategory['url']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['url'], 'name' => 'data[url]']) : '' ?>
					</div>
					<div class="form-group">
						<label><?= __("Trạng thái") ?>:</label>
						<div class="custom-control custom-radio">
							<input class="custom-control-input <?= (isset($errCategory['flg_show']) ? ' err-ctrl' : '') ?>" type="radio" id="active" name="data[flg_show]" value="0" <?php if (!isset($data['flg_show']) || (int) $data['flg_show'] === 0) { ?> checked <?php } ?>>
							<label for="active" class="custom-control-label">Hoạt động</label>
						</div>
						<div class="custom-control custom-radio">
							<input class="custom-control-input <?= (isset($errCategory['flg_show']) ? ' err-ctrl' : '') ?>" type="radio" id="deactive" name="data[flg_show]" value="1" <?php if (isset($data['flg_show']) && $data['flg_show'] == 1) { ?> checked <?php } ?>>
							<label for="deactive" class="custom-control-label">Không hoạt động</label>
						</div>
						<?= isset($errCategory['flg_show']) ? $this->element('Admin/errmess', ['opptions' => $errCategory['flg_show'], 'name' => 'data[flg_show]']) : '' ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Thông tin SEO</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
				<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
			</div>
		</div>
		<div class="card-body">
			<div class="col-md-12">
				<div class="form-group">
					<label for="title">SEO title</label>
					<input name="data[Seo][meta_title]" type="text" class="form-control <?= (isset($errSeo['meta_title']) ? ' err-ctrl' : '') ?>" id="title" placeholder="SEO title" value="<?= isset($data['Seo']['meta_title']) ? $data['Seo']['meta_title'] : '' ?>">
					<?= isset($errSeo['meta_title']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_title'], 'name' => 'data[Seo][meta_title]']) : '' ?>
				</div>
				<div class="form-group">
					<label for="description">SEO description</label>
					<input name="data[Seo][meta_description]" type="text" class="form-control <?= (isset($errSeo['meta_description']) ? ' err-ctrl' : '') ?>" id="description" placeholder="SEO description" value="<?= isset($data['Seo']['meta_description']) ? $data['Seo']['meta_description'] : '' ?>">
					<?= isset($errSeo['meta_description']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_description'], 'name' => 'data[Seo][meta_description]']) : '' ?>
				</div>
				<div class="form-group">
					<label for="keywords">SEO keywords</label>
					<textarea style="width: 100%;" name="data[Seo][meta_keywords]" type="text" class="form-control <?= (isset($errSeo['meta_keywords']) ? ' err-ctrl' : '') ?>" id="keywords" placeholder="SEO keywords" value=""><?= isset($data['Seo']['meta_keywords']) ? $data['Seo']['meta_keywords'] : '' ?></textarea>
					<?= isset($errSeo['meta_keywords']) ? $this->element('Admin/errmess', ['opptions' => $errSeo['meta_keywords'], 'name' => 'data[Seo][meta_keywords]']) : '' ?>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="card-footer" style="text-align: right">
			<a href="/admin/categories/lists" class="btn btn-info">Quay lại</a>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lưu
			</button>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="confirmModalTitle">Dialog Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Bạn có chắc chắn muốn lưu dữ liệu này không？
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger mr-2 btn-yes">Yes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<?= $this->AppForm->end() ?>
<div style="display: none;">
	<select id="parentIdDefault">
		<option value="">Please choose a category</option>
	</select>
	<select class="form-control" id="parentIdList">
		<?php foreach ($listCategory as $category) { ?>
			<?php if (isset($data['parent_id']) && $category['id'] == $data['parent_id']) { ?>
				<option language="<?= $category['flg_language'] ?>" typecate="<?= $category['type'] ?>" value="<?= $category['id'] ?>" selected="selected"><?= $category['title'] ?></option>
			<?php } else { ?>
				<option language="<?= $category['flg_language'] ?>" typecate="<?= $category['type'] ?>" value="<?= $category['id'] ?>"><?= $category['title'] ?></option>
			<?php } ?>
		<?php } ?>
	</select>
</div>
<?php $this->start('script_footer'); ?>
<script src="/admin/js/categories.js"></script>
<script>
	var val = "<?= isset($data['parent_id']) ? $data['parent_id'] : "" ?>"
	$("#type").change(function() {
		generateParentAdd(val);
	});

	$("input[name='data[flg_language]']").change(function() {
		generateParentAdd(val);
	});

	$(document).ready(function() {
		generateParentAdd(val);
	});
</script>
<?php $this->end(); ?>