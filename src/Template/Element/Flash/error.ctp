<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger alert-dismissible" id="errorCollapse" aria-expanded="true" onload="setTimeout(function(){this.style.display = 'none';}, 5000)" onclick="this.style.display = 'none'">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fas fa-ban"></i> Fail!</h5>
    <?= $message ?>
</div>
