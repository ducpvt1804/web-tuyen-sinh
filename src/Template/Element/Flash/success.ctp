<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success alert-dismissible" onload="setTimeout(function(){this.style.display = 'none';}, 5000)" onclick="this.style.display = 'none'">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fas fa-check"></i> Success! </h5>
    <?= $message ?>
</div>
