<header>
    <div class="header-top">
        <div class="container d-flex flex-wrap justify-content-between align-items-center">
            <div class="logo-header-mb d-flex d-lg-none">
                <a href="/<?= $lang ?>"><img src="/front/images/logo-header-mb.png" alt="<?= $config['name'] ?>"></a>
                <h1 class="name-site"><?= __('Đại Nam University') ?></h1>
            </div>
            <div class="social-hd d-none d-lg-block">
                <ul class="list-inline">
                    <li class="list-inline-item"><a target="_blank" href="<?= $config['facebook_address'] ?>"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a target="_blank" href="<?= $config['twitter_address'] ?>"><i class="fa fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a target="_blank" href="<?= $config['pinterest_address'] ?>"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div>

            <div class="header-top-right d-flex align-items-center">
                <ul class="nav-header">
                    <?php foreach ($menu_top_header as $key => $value) { ?>
                        <li>
                            <?php if (empty($value['url'])) { ?>
                                <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                <?php } else { ?>
                                    <a target="_blank" href="<?= $value['url'] ?>">
                                    <?php } ?>
                                    <?= $value['title'] ?>
                                    </a>
                        </li>
                    <?php } ?>
                </ul>

                <div class="header-func d-flex align-items-center">
                    <div class="search">
                        <div class="sear-boder"></div>
                        <?= $this->Form->create('', ['url' => ['controller' => 'Home', 'action' => 'search', 'lang' => $lang]]) ?>
                        <div class="search-wrapper">
                            <div class="input-holder">
                                <input type="text" class="form-control search-input" name="" placeholder="<?= __("Nhập từ khóa tìm kiếm") ?>">
                                <button class="btn-clear btn-search-submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="language dropdown">
                        <button class="btn-clear dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/front/images/<?= $lang ?>.png" alt="">
                            <span class="d-none d-lg-inline"><?= $nameLang ?></span>
                        </button>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?php if ($lang == "vi") { ?>
                                <li><a class="dropdown-item" href="/en"><?= __('Tiếng Anh') ?></a></li>
                            <?php } else { ?>
                                <li><a class="dropdown-item" href="/vi"><?= __('Tiếng Việt') ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- /.language -->

                    <div class="menu-btn d-lg-none">
                        <button class="btn-clear btn-toggle-menu"><i class="ico-4"></i></button>
                    </div>
                </div>
                <!-- /.header-func-->
            </div>
        </div>
    </div>
    <!-- /.header-top-->

    <div class="header-middle d-none d-lg-block">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <div class="logo-header">
                    <a class="navbar-brand" href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index', 'lang' => $lang]) ?>"><img src="<?= $this->Root->logo(isset($config['logo_header']) ? $config['logo_header'] : '') ?>" alt="<?= $config['name'] ?>"></a>
                    <div class="addmissions text-uppercase"><a href="/adasddadad">Tuyển sinh 2019</a></div>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <?= $this->element('Front/menu') ?>
                    </ul>
                </div>
        </nav>
    </div>
    <!-- /.header-middle-->
</header>
