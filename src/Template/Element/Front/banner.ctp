<div class="banner-fixed">
    <section class="panel" data-type="splash-image" data-modular-content data-js="panel" data-index="0" data-scroll-type="parallax" data-logo-loc-v="bottom" data-logo-loc-h="center" data-ga-category="Splash Panel">
        <img src="/front/images/upload/banner-home-height.png" alt="" class="cover-height d-none d-lg-block">
        <img src="/front/images/upload/banner-home-height-mb.png" alt="" class="cover-height-mb d-lg-none">
        <div class="inner-panel">
            <?php if(isset($bannerPCDetail) && !empty($bannerPCDetail)) { ?>
            <div class="banner-slider owl-carousel owl-theme d-lg-block">
                <?php foreach($bannerPCDetail as $key => $value) { ?>
                <div class="banner-item">
                    <img alt="<?= $value['title'] ?>" src="<?= $this->Root->banner(isset($value['img']) ? $value['img'] : '') ?>" class="d-none d-lg-block">
                </div>
                <?php } ?>
            </div>
            <?php } ?>
            <?php if(isset($bannerSPDetail) && !empty($bannerSPDetail)) { ?>
            <div class="banner-slider owl-carousel owl-theme d-lg-none">
                <?php foreach($bannerSPDetail as $key => $value) { ?>
                <div class="banner-item">
                    <div class="banner-caption">
                        <h2 class="d-lg-none title-mobile"><?= $value['title'] ?></h2>
                    </div>
                    <img alt="<?= $value['title'] ?>" src="<?= $this->Root->banner(isset($value['img']) ? $value['img'] : '') ?>" class="d-lg-none">
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </section>
</div>
