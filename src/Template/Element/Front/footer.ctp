<footer>
    <div class="footer-top">
        <div class="container d-flex flex-wrap align-items-center justify-content-between">
            <div class="time">
                <div class='time-frame'>
                    <span id='time-part'></span>
                    <span id='date-part'></span>
                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/vi.js"></script>
            </div>
            <div class="social-ft">
                <a target="_blank" href="<?= $config['facebook_address'] ?>"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="<?= $config['twitter_address'] ?>"><i class="fa fa-twitter"></i></a>
                <a target="_blank" href="<?= $config['pinterest_address'] ?>"><i class="fa fa-pinterest-p"></i></a>
            </div>
        </div>
    </div>
    <!-- /.footer-top -->

    <div class="footer-middle">
        <div class="container">
            <!-- menu for mobile -->
            <div class="d-md-none">
                <div class="row">
                    <div class="col-7">
                        <div class="box-ft">
                            <h3><?= __('Đào tạo chính quy') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($listDTCQ as $key => $value) { ?>
                                    <li>
                                        <?php if (empty($value['url'])) { ?>
                                            <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                            <?php } else { ?>
                                                <a target="_blank" href="<?= $value['url'] ?>">
                                                <?php } ?>
                                                <?= $value['title'] ?>
                                                </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                    <div class="col-5">
                        <div class="box-ft">
                            <h3><?= __('Khoa') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($faculty as $key => $department) : ?>
                                    <li><a href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'index',
                                                            'lang' => $department['ListDepartments']['flg_language'] == 1 ? 'en' : 'vi',
                                                            'slug_faculty' => $department['ListDepartments']['slug']
                                                        ]) ?>"><?= $department['title'] ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-7">
                        <div class="box-ft">
                            <h3><?= __('Đào tạo liên thông') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($listDTLT as $key => $value) : ?>
                                    <li>
                                        <?php if (empty($value['url'])) : ?>
                                            <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                            <?php else : ?>
                                                <a target="_blank" href="<?= $value['url'] ?>"> <?= $value['title'] ?> </a>
                                            <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                    <div class="col-5">
                        <div class="box-ft">
                            <h3><?= __('Phòng ban') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($departmentPB as $key => $department) : ?>
                                    <li><a href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'index',
                                                            'lang' => $department['ListDepartments']['flg_language'] == 1 ? 'en' : 'vi',
                                                            'slug_faculty' => $department['ListDepartments']['slug']
                                                        ]) ?>"><?= $department['title'] ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-7">
                        <div class="box-ft">
                            <h3><?= __('Download') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($menu_download as $key => $value) { ?>
                                    <li>
                                        <?php if (empty($value['url'])) { ?>
                                            <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                            <?php } else { ?>
                                                <a target="_blank" href="<?= $value['url'] ?>"><?= $value['title'] ?> </a>
                                            <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                    <div class="col-5">
                        <div class="box-ft">
                            <h3><?= __('Thư viện') ?></h3>
                            <ul class="list-links">
                                <?php foreach ($thuvien as $key => $value) { ?>
                                    <li>
                                        <?php if (empty($value['url'])) { ?>
                                            <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                            <?php } else { ?>
                                                <a target="_blank" href="<?= $value['url'] ?>">
                                                <?php } ?>
                                                <?= $value['title'] ?>
                                                </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- /.box-ft-->
                    </div>
                </div>
            </div>
            <!-- e: menu for mobile -->
            <div class="row">
                <!-- menu for desktop -->
                <div class="col-lg-2 col-6 d-none d-md-block">
                    <div class="box-ft">
                        <h3><?= __('Khoa') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($faculty as $key => $department) : ?>
                                <li><a href="<?= $this->Url->build([
                                                        'controller' => 'Faculty',
                                                        'action' => 'index',
                                                        'lang' => $department['ListDepartments']['flg_language'] == 1 ? 'en' : 'vi',
                                                        'slug_faculty' => $department['ListDepartments']['slug']
                                                    ]) ?>"><?= $department['title'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->
                </div>

                <div class="col-lg-2 col-6 d-none d-md-block">
                    <div class="box-ft">
                        <h3><?= __('Phòng ban') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($departmentPB as $key => $department) : ?>
                                <li><a href="<?= $this->Url->build([
                                                        'controller' => 'Faculty',
                                                        'action' => 'index',
                                                        'lang' => $department['ListDepartments']['flg_language'] == 1 ? 'en' : 'vi',
                                                        'slug_faculty' => $department['ListDepartments']['slug']
                                                    ]) ?>"><?= $department['title'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->

                    <div class="box-ft">
                        <h3><?= __('Thư viện') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($thuvien as $key => $value) { ?>
                                <li>
                                    <?php if (empty($value['url'])) { ?>
                                        <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                        <?php } else { ?>
                                            <a target="_blank" href="<?= $value['url'] ?>">
                                            <?php } ?>
                                            <?= $value['title'] ?>
                                            </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->
                </div>

                <div class="col-lg-2 col-6 d-none d-md-block">
                    <div class="box-ft">
                        <h3><?= __('Đào tạo chính quy') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($listDTCQ as $key => $value) { ?>
                                <li>
                                    <?php if (empty($value['url'])) { ?>
                                        <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                        <?php } else { ?>
                                            <a target="_blank" href="<?= $value['url'] ?>">
                                            <?php } ?>
                                            <?= $value['title'] ?>
                                            </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->
                </div>

                <div class="col-lg-2 col-6 d-none d-md-block">
                    <div class="box-ft">
                        <h3><?= __('Đào tạo liên thông') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($listDTLT as $key => $value) { ?>
                                <li>
                                    <?php if (empty($value['url'])) { ?>
                                        <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                        <?php } else { ?>
                                            <a target="_blank" href="<?= $value['url'] ?>">
                                            <?php } ?>
                                            <?= $value['title'] ?>
                                            </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->

                    <div class="box-ft">
                        <h3><?= __('Download') ?></h3>
                        <ul class="list-links">
                            <?php foreach ($menu_download as $key => $value) { ?>
                                <li>
                                    <?php if (empty($value['url'])) { ?>
                                        <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                        <?php } else { ?>
                                            <a target="_blank" href="<?= $value['url'] ?>">
                                            <?php } ?>
                                            <?= $value['title'] ?>
                                            </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- /.box-ft-->
                </div>
                <!-- e: menu for desktop -->

                <div class="col-lg-2">
                    <div class="nav-footer">
                        <ul>
                            <?php foreach ($menuFooter as $key => $value) { ?>
                                <li>
                                    <?php if (empty($value['url'])) { ?>
                                        <a href="/<?= $lang ?>/<?= $value['slug'] ?>">
                                        <?php } else { ?>
                                            <a target="_blank" href="<?= $value['url'] ?>">
                                            <?php } ?>
                                            <?= $value['title'] ?>
                                            </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!--/.nav-footer-->
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer-middle -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 text-center text-lg-left">
                    <div class="about-us-ft">
                        <div class="img">
                            <img src="<?= $this->Root->logo(isset($config['logo_footer']) ? $config['logo_footer'] : '') ?>" alt="<?= $config['name'] ?>" class="d-none d-lg-inline-block">
                            <img src="/front/images/logo-footer-mb.png" alt="<?= $config['name'] ?>" class="d-lg-none">
                        </div>
                        <div class="text">
                            <h3><?= __('Trường Đại học Đại Nam') ?></h3>
                            <ul>
                                <li><i class="ico-1"></i> <?= __('Cơ sở chính') ?>: <?= $config['address_1'] ?></li>
                                <li><i class="ico-1"></i> <?= __('Cơ sở 1') ?>: <?= $config['address_1'] ?></li>
                                <li><i class="ico-2"></i> <?= __('Điện thoại') ?>: <?= $config['telno'] ?> - <?= __('Fax') ?>: <?= $config['fax'] ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-none d-lg-block">
                    <div class="footer-right">
                        <div class="map">
                            <img src="/front/images/upload/map.png" alt="">
                        </div>
                        <div class="copyright">Copyright @2019 Đại học Đại Nam</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<!-- Your customer chat code -->
<div class="fb-customerchat"
attribution=setup_tool
page_id="102727291224429"
logged_in_greeting="Hi! How can we help you?"
logged_out_greeting="Hi! How can we help you?">
</div>
</footer>
