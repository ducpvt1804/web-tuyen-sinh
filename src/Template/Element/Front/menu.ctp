<li class="nav-item active">
    <a class="nav-link" href="/<?= $lang ?>"><i class="fa fa-home"></i></a>
</li>
<?php if ($menu['intro']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['intro']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['department']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['department']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['training']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['training']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['khcn_htdt']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['khcn_htdt']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['student']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['student']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['three_public']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['three_public']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['contact']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['contact']]) ?>
    </li>
<?php endif; ?>

<?php if ($menu['extend']) : ?>
    <li class="nav-item dropdown">
        <?= $this->element('Front/sub_menu', ['sub_menu' => $menu['extend'], 'extend' => '<i class="fa fa-angle-double-down"></i>']) ?>
    </li>
<?php endif; ?>