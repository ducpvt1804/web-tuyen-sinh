<div class="banner-fixed">
    <section class="panel" data-type="splash-image" data-modular-content data-js="panel" data-index="0" data-scroll-type="parallax" data-logo-loc-v="bottom" data-logo-loc-h="center" data-ga-category="Splash Panel">
        <img src="/front/images/upload/banner-home-height.png" alt="" class="cover-height d-none d-lg-block">
        <img src="/front/images/upload/banner-khoa-height-mb.png" alt="" class="cover-height-mb d-lg-none">
        <div class="inner-panel">
            <img alt="" src="<?= $banner ?>" class="d-none d-lg-block">
            <img alt="" src="<?= $banner ?>" class="d-lg-none">
        </div>
    </section>
</div>