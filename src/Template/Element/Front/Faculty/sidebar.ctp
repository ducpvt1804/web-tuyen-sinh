<div class="sidebar-menu-bg"></div>
<div class="sidebar-menu">
    <div class="sidebar-menu-header">
        <h2 class="name-site"><?= __('Đại Nam University') ?></h2>
        <button class="btn-clear btn-close-menu"><i class="ico-5"></i></button>
    </div>

    <ul class="main-menu">
        <li class="nav-item active dropdown">
            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-home"></i></a>
            <ul class="dropdown-menu style-2" aria-labelledby="dropdownMenuButton">
                <li><a class="dropdown-item" href="#"><?= __('Khoa') ?></a></li>
                <li><a class="dropdown-item" href="#"><?= __('Đại Nam') ?></a></li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'list',
                                            'lang' => $slugLang,
                                            'slug_faculty' => $slugFaculty,
                                            'slug_faculty_list' => $menus['intro']['slug']
                                        ]) ?>"><?= $menus['intro']['title'] ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'list',
                                            'lang' => $slugLang,
                                            'slug_faculty' => $slugFaculty,
                                            'slug_faculty_list' => $menus['new']['slug']
                                        ]) ?>"><?= $menus['new']['title'] ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'list',
                                            'lang' => $slugLang,
                                            'slug_faculty' => $slugFaculty,
                                            'slug_faculty_list' => $menus['action']['slug']
                                        ]) ?>"><?= $menus['action']['title'] ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'list',
                                            'lang' => $slugLang,
                                            'slug_faculty' => $slugFaculty,
                                            'slug_faculty_list' => $menus['student']['slug']
                                        ]) ?>"><?= $menus['student']['title'] ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'list',
                                            'lang' => $slugLang,
                                            'slug_faculty' => $slugFaculty,
                                            'slug_faculty_list' => $menus['contact']['slug']
                                        ]) ?>"><?= $menus['contact']['title'] ?></a>
        </li>
    </ul>
</div>