<header>
    <div class="header-top">
        <div class="container d-flex flex-wrap justify-content-between align-items-center">
            <div class="logo-header-mb d-flex d-lg-none">
                <a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index', 'lang' => $slugLang]) ?>"><img src="/front/images/logo-header-mb.png" alt=""></a>
                <h1 class="name-site"><?= __('Đại Nam University') ?></h1>
            </div>
            <div class="social-hd d-none d-lg-block">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div>

            <div class="header-top-right d-flex align-items-center">
                <ul class="nav-header">
                    <li><a href="#"><?= __('Thông tin tuyển sinh') ?></a></li>
                    <li><a href="#"><?= __('Thư viện số') ?></a></li>
                    <li><a href="#"><?= __('Email') ?></a></li>
                </ul>

                <div class="header-func d-flex align-items-center">
                    <div class="search">
                        <form>
                            <div class="search-wrapper">
                                <div class="input-holder">
                                    <input type="text" class="form-control search-input" name="" placeholder="Type to search">
                                    <button class="btn-clear btn-search-submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="language dropdown">
                        <button class="btn-clear dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/front/images/vi.png" alt="">
                            <span class="d-none d-lg-inline"><?= __('Tiếng việt') ?></span>
                        </button>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <li><a class="dropdown-item" href="#"><?= __('English') ?></a></li>
                            <li><a class="dropdown-item" href="#"><?= __('Vietnames') ?></a></li>
                            <li><a class="dropdown-item" href="#"><?= __('France') ?></a></li>
                        </ul>
                    </div><!-- /.language -->

                    <div class="menu-btn d-lg-none">
                        <button class="btn-clear btn-toggle-menu"><i class="ico-4"></i></button>
                    </div>
                </div><!-- /.header-func-->
            </div>
        </div>
    </div><!-- /.header-top-->

    <div class="header-middle d-none d-lg-block">
        <nav class="navbar navbar-expand-lg navbar-faculty">
            <div class="container">
                <div class="logo-header">
                    <a class="navbar-brand" href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index', 'lang' => $slugLang]) ?>"><img src="/front/images/logo.png" alt=""></a>
                    <div class="addmissions text-uppercase"><a href="<?= $this->Url->build([
                                                                            'controller' => 'Faculty',
                                                                            'action' => 'index',
                                                                            'lang' => $slugLang,
                                                                            'slug_faculty' => $slugFaculty
                                                                        ]) ?>"><?= __(isset($data['Departments']['title']) ? $data['Departments']['title'] : '') ?></a></div>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active dropdown">
                            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-home"></i></a>
                            <ul class="dropdown-menu style-2" aria-labelledby="dropdownMenuButton">
                                <li><a class="dropdown-item" href="#"><?= __('Khoa') ?></a></li>
                                <li><a class="dropdown-item" href="#"><?= __('Đại Nam') ?></a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'list',
                                                            'lang' => $slugLang,
                                                            'slug_faculty' => $slugFaculty,
                                                            'slug_faculty_list' => $menus['intro']['slug']
                                                        ]) ?>"><?= $menus['intro']['title'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'list',
                                                            'lang' => $slugLang,
                                                            'slug_faculty' => $slugFaculty,
                                                            'slug_faculty_list' => $menus['new']['slug']
                                                        ]) ?>"><?= $menus['new']['title'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'list',
                                                            'lang' => $slugLang,
                                                            'slug_faculty' => $slugFaculty,
                                                            'slug_faculty_list' => $menus['action']['slug']
                                                        ]) ?>"><?= $menus['action']['title'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'list',
                                                            'lang' => $slugLang,
                                                            'slug_faculty' => $slugFaculty,
                                                            'slug_faculty_list' => $menus['student']['slug']
                                                        ]) ?>"><?= $menus['student']['title'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $this->Url->build([
                                                            'controller' => 'Faculty',
                                                            'action' => 'list',
                                                            'lang' => $slugLang,
                                                            'slug_faculty' => $slugFaculty,
                                                            'slug_faculty_list' => $menus['contact']['slug']
                                                        ]) ?>"><?= $menus['contact']['title'] ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div><!-- /.header-middle-->
</header>