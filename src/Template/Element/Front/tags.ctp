<?php
if (isset($tags) && count($tags) > 0) :
    foreach ($tags as $tag) :
        ?>
        <a href="/<?=$lang?>/tags/<?= $this->Common->convertSlug($tag)?>"><?= $tag?></a>
    <?php
    endforeach;
endif;
?>
