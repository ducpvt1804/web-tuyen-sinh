<a class="nav-link" href="<?= empty($sub_menu['sub_menu']) ? "/$lang/" . $sub_menu['slug'] : '' ?>" role="button" <?= empty($sub_menu['sub_menu']) ? '' : 'data-toggle="dropdown"' ?> aria-haspopup="true" aria-expanded="false"><?= $sub_menu['title'] ?><?= isset($extend) ? $extend : '' ?></a>
<?php if (count($sub_menu['sub_menu'])) : ?>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php foreach ($sub_menu['sub_menu'] as $item) : ?>
            <li><a class="dropdown-item" href="<?= "/$lang/" . $item['slug'] ?>"><?= $item['title'] ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>