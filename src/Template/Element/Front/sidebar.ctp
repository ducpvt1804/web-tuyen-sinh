<div class="sidebar-menu">
    <div class="sidebar-menu-header">
        <h2 class="name-site"><?= __('Đại Nam University') ?></h2>
        <button class="btn-clear btn-close-menu"><i class="ico-5"></i></button>
    </div>

    <ul class="main-menu">
        <?= $this->element('Front/menu') ?>
    </ul>
</div>