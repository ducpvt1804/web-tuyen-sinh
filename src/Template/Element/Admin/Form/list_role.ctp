<div class="row">
    <?php foreach ($options as $item) : ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="custom-control custom-checkbox">
                <input class="custom-control-input" id="<?= $name ?>[<?= $item['id'] ?>]" <?= (isset($req[$name]) && in_array($item['id'], $req[$name])) ? "checked" : "" ?> type="checkbox" name="<?= $name ?>[]" value="<?= $item["id"] ?>">
                <label for="<?= $name ?>[<?= $item['id'] ?>]" class="custom-control-label"><?= $item["title"] ?></label>
            </div>
        </div>
    <?php endforeach; ?>
</div>