<?php if (isset($label)) : ?>
    <label <?= isset($name) ? 'for="' . $name . '"' : "" ?>><?= $label ?></label>
<?php endif; ?>

<select <?= isset($class) ? 'class="' . $class . '"' : "" ?> <?= isset($name) ? 'name="' . $name . '"' : "" ?>>
    <option value=""><?= isset($default) ? $default : "Chọn item..." ?></option>
    <?php if (isset($options) && count($options) > 0) :
        foreach ($options as $item) : ?>
            <option value="<?= $item['id'] ?>" <?= (isset($selected) && $selected !== '' && $selected == $item['id']) ? "selected" : "" ?>><?= $item['title'] ?></option>
    <?php endforeach;
    endif; ?>
</select>