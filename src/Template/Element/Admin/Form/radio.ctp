<?php if (isset($label)) : ?>
    <label <?= isset($name) ? 'for="' . $name . '"' : "" ?>><?= $label ?></label>
<?php endif; ?>
<?php if (isset($options) && count($options) > 0) :
    $index = 0;
    foreach ($options as $item) : ?>
        <div class="custom-control custom-radio">
            <input class="custom-control-input <?= isset($class) ? $class : '' ?>" value="<?= $item['value'] ?>" <?= (isset($checked) && $checked !== '' && $checked == $item['value']) ? 'checked' : (($index == 0 && (!isset($checked) || $checked == '')) ? 'checked' : '') ?> type="radio" id="<?= $name . $item['value'] ?>" name="<?= $name ?>">
            <label for="<?= $name . $item['value'] ?>" class="custom-control-label"><?= $item['title'] ?></label>
        </div>
<?php $index = 1;
    endforeach;
endif; ?>