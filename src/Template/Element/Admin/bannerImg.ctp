<div class="card card-default">
    <div class="card-header">
        <div class="row">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <?php if (isset($item)) : ?>
                <button type="button" class="btn btn-tool" onclick="deleteCf('<?= $this->Url->build(['controller' => 'Banner', 'action' => 'delete_detail', $item['id']]) ?>')" data-toggle="modal" data-target="#confirmModal" style="color: red" title="delete">
                    <i class="fas fa-times"></i></button>
                <span><?= !empty($item['title']) ? $item['title'] : '<span style="color: #adb5bd">Chưa có title.</span>' ?></span>&nbsp;<span class="span-link" onclick="showEdit(<?= $item['id'] ?>)">chỉnh sửa</span>
            <?php else : ?>
                <button type="button" class="btn btn-tool" onclick="imgClose('<?= $img_name ?>')" style="color: red" title="delete">
                    <i class="fas fa-times"></i></button>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <?= $this->Form->control(str_replace('.', '', $img_name) . "_title", [
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => 'Nhập alt seo banner...'
                        ]) ?>
                </div>
            <?php endif; ?>
        </div>

    </div>
    <div class="card-body" style="min-height: 250px;">
        <img src="/admin/upload/banner/<?= isset($folder) ? $folder : '' ?><?= $img_name ?>" style="max-height: 250px;" class="img-responsive" alt="Image">
    </div>
</div>