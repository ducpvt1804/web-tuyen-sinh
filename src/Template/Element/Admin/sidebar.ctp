<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/admin" class="brand-link">
    <img src="/img/system/logo.png" alt="Trường Đại Học Đại Nam | Quản lý hệ thống" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Quản lý hệ thống</span>
  </a>
  <style>
    .dropdown {
      position: relative;
      display: inline-block;
    }

    .dropdown-content {
      display: none;
      position: fixed;
      background-color: #343a40;
      min-width: 160px;
      font-size: 14px;
      padding: 12px 16px;
      z-index: 1;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }

    .custom {
      border: 1px solid #fff;
      padding: 1px;
      border-radius: 2px;
    }
    .mn i{
      min-width: 20px;
    }
  </style>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/admin/upload/users/<?= $userLogined['avarta'] ?>" class="elevation-2 custom" alt="User Image">
      </div>
      <div class="info dropdown">
        <a href="" class="d-block"><?= empty($userLogined['fullname']) ? $userLogined['fullname'] : $userLogined['user_name'] ?></a>
        <div class="dropdown-content">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
              <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'edit_profile']) ?>" class="nav-link">
                <i class="far fa-id-card"></i>
                <p>Đổi thông tin</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column mn" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <?php
        $controller_current = $this->getRequest()->getParam('controller');
        $action_current = $this->getRequest()->getParam('action'); ?>


        <li class="nav-item has-treeview">
          <a href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Home' && $action_current == 'index') ? 'active' : '' ?>">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Trang chủ</p>
          </a>
        </li>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && (in_array('Departments', $userLogined['modules']) || in_array('ListDepartments', $userLogined['modules'])  || in_array('CateDepartments', $userLogined['modules']) || in_array('StudentFeels', $userLogined['modules']) || in_array('NewsDepartments', $userLogined['modules']))) || ($userLogined['role_id'] == 2 && in_array('0', $userLogined['type']))) : ?>
              <li class="nav-item has-treeview <?= ($controller_current == 'Departments' || $controller_current == 'ListDepartments' || $controller_current == 'CateDepartments' || $controller_current == 'NewsDepartments') || $controller_current == 'StudentFeels' ? 'menu-open' : '' ?>">
                  <a href="#" class="nav-link <?= ($controller_current == 'Departments' || $controller_current == 'ListDepartments' || $controller_current == 'CateDepartments' || $controller_current == 'NewsDepartments') || $controller_current == 'StudentFeels' ? 'active' : '' ?>">
                      <i class="fas fa-book-reader"></i>
                      <p>
                          Khoa
                          <i class="right fas fa-angle-left"></i>
                      </p>
                  </a>
                  <ul class="nav nav-treeview" style="<?= ($controller_current == 'Departments' || $controller_current == 'ListDepartments' || $controller_current == 'CateDepartments' || $controller_current == 'NewsDepartments') || $controller_current == 'StudentFeels' ? 'display: block' : 'display: none' ?>;">

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if (($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('ListDepartments', $userLogined['modules']))) && $userLogined['role_id'] != 2) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'ListDepartments', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'ListDepartments') ? 'active' : '' ?>">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Danh sách khoa</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Departments', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('0', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'Departments', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Departments') ? 'active' : '' ?>">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Cấu hình Khoa</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('CateDepartments', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('0', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'CateDepartments', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'CateDepartments') ? 'active' : '' ?>"">
                              <i class=" far fa-circle nav-icon"></i>
                              <p>Danh mục tin</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('NewsDepartments', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('0', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'NewsDepartments', 'action' => 'lists']) ?>" class="nav-link <?= ($controller_current == 'NewsDepartments') ? 'active' : '' ?>"">
                              <i class=" far fa-circle nav-icon"></i>
                              <p>Tin tức</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('StudentFeels', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('0', $userLogined['type']))) : ?>
                          <li class="nav-item has-treeview">
                              <a href="<?= $this->Url->build(['controller' => 'StudentFeels', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'StudentFeels') ? 'active' : '' ?>">
                                  <i class=" far fa-circle nav-icon"></i>
                                  <p>Cảm nhận sinh viên</p>
                              </a>
                          </li>
                      <?php endif; ?>
                  </ul>
              </li>
          <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && (in_array('ListDepartmentspb', $userLogined['modules']) || in_array('Departmentspb', $userLogined['modules'])  || in_array('CateDepartmentspb', $userLogined['modules']) || in_array('NewsDepartmentspb', $userLogined['modules']))) || ($userLogined['role_id'] == 2 && in_array('1', $userLogined['type']))) : ?>
              <li class="nav-item has-treeview <?= ($controller_current == 'Departmentspb' || $controller_current == 'ListDepartmentspb' || $controller_current == 'CateDepartmentspb' || $controller_current == 'NewsDepartmentspb') ? 'menu-open' : '' ?>">
                  <a href="#" class="nav-link <?= ($controller_current == 'Departmentspb' || $controller_current == 'ListDepartmentspb' || $controller_current == 'CateDepartmentspb' || $controller_current == 'NewsDepartmentspb') ? 'active' : '' ?>">
                      <i class="far fa-building"></i>
                      <p>
                          Phòng ban
                          <i class="right fas fa-angle-left"></i>
                      </p>
                  </a>
                  <ul class="nav nav-treeview" style="<?= ($controller_current == 'Departmentspb' || $controller_current == 'ListDepartmentspb' || $controller_current == 'CateDepartmentspb' || $controller_current == 'NewsDepartmentspb') ? 'display: block' : 'display: none' ?>;">

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if (($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('ListDepartmentspb', $userLogined['modules']))) && $userLogined['role_id'] != 2) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'ListDepartmentspb', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'ListDepartmentspb') ? 'active' : '' ?>">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Danh sách phòng ban</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Departmentspb', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('1', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'Departmentspb', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Departmentspb') ? 'active' : '' ?>">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Cấu hình phòng ban</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('CateDepartmentspb', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('1', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'CateDepartmentspb', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'CateDepartmentspb') ? 'active' : '' ?>"">
                              <i class=" far fa-circle nav-icon"></i>
                              <p>Danh mục tin</p>
                              </a>
                          </li>
                      <?php endif; ?>

                      <?php /*------------------------------------------------------------------------------------------*/ ?>
                      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('NewsDepartmentspb', $userLogined['modules'])) || ($userLogined['role_id'] == 2 && in_array('1', $userLogined['type']))) : ?>
                          <li class="nav-item">
                              <a href="<?= $this->Url->build(['controller' => 'NewsDepartmentspb', 'action' => 'lists']) ?>" class="nav-link <?= ($controller_current == 'NewsDepartmentspb') ? 'active' : '' ?>"">
                              <i class=" far fa-circle nav-icon"></i>
                              <p>Tin tức</p>
                              </a>
                          </li>
                      <?php endif; ?>
                  </ul>
              </li>
          <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('News', $userLogined['modules']))) : ?>
              <li class="nav-item has-treeview">
                  <a href="<?= $this->Url->build(['controller' => 'News', 'action' => 'lists']) ?>" class="nav-link <?= ($controller_current == 'News') ? 'active' : '' ?>">
                      <i class="fas fa-newspaper"></i>
                      <p>Tin tức</p>
                  </a>
              </li>
        <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Categories', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Categories', 'action' => 'lists']) ?>" class="nav-link <?= ($controller_current == 'Categories') ? 'active' : '' ?>">
              <i class="fas fa-folder-open"></i>
              <p>Danh mục tin</p>
            </a>
          </li>
        <?php endif; ?>



        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Libraries', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Libraries', 'action' => 'lists']) ?>" class="nav-link <?= ($controller_current == 'Libraries') ? 'active' : '' ?>">
              <i class="fas fa-photo-video"></i>
              <p>Thư viện ảnh/video</p>
            </a>
          </li>
        <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Events', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Events') ? 'active' : '' ?>">
              <i class="fas fa-calendar-alt"></i>
              <p>Sự kiện</p>
            </a>
          </li>
        <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Users', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Users' && $action_current != "editProfile") ? 'active' : '' ?>">
              <i class="fas fa-users"></i>
              <p>Tài khoản</p>
            </a>
          </li>
        <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Quotes', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Quotes') ? 'active' : '' ?>">
              <i class="fas fa-quote-left"></i>
              <p>Quản lý Quote</p>
            </a>
          </li>
        <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Partners', $userLogined['modules']))) : ?>
          <li class="nav-item has-treeview">
            <a href="<?= $this->Url->build(['controller' => 'Partners', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Partners') ? 'active' : '' ?>">
              <i class="far fa-handshake"></i>
              <p>Quản lý đối tác</p>
            </a>
          </li>
        <?php endif; ?>


        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && (in_array('EnrollmentOptions', $userLogined['modules']) || in_array('EnrollmentRegistrations', $userLogined['modules'])))) : ?>
          <li class="nav-item has-treeview <?= in_array($controller_current, ['EnrollmentOptions', 'EnrollmentRegistrations'])  ? 'menu-open' : '' ?>">
            <a href="#" class="nav-link <?= in_array($controller_current, ['EnrollmentOptions', 'EnrollmentRegistrations']) ? 'active' : '' ?>">
              <i class="fas fa-book-reader"></i>
              <p>
                Đơn đăng ký
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="<?= in_array($controller_current, ['EnrollmentOptions', 'EnrollmentRegistrations']) ? 'display: block' : 'display: none' ?>;">

              <?php /*------------------------------------------------------------------------------------------*/ ?>
              <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('EnrollmentOptions', $userLogined['modules']))) : ?>
                <li class="nav-item has-treeview">
                  <a href="<?= $this->Url->build(['controller' => 'EnrollmentOptions', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'EnrollmentOptions') ? 'active' : '' ?>">
                    <i class="fas fa-file-alt"></i>
                    <p>Nghành - Môn</p>
                  </a>
                </li>
              <?php endif; ?>

              <?php /*------------------------------------------------------------------------------------------*/ ?>
              <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('EnrollmentRegistrations', $userLogined['modules']))) : ?>
                <li class="nav-item has-treeview">
                  <a href="<?= $this->Url->build(['controller' => 'EnrollmentRegistrations', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'EnrollmentRegistrations') ? 'active' : '' ?>">
                    <i class="fas fa-file-contract"></i>
                    <p>Đơn đăng ký tuyển sinh</p>
                  </a>
                </li>
              <?php endif; ?>
            </ul>
          </li>
        <?php endif; ?>

      <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
      <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && (in_array('Config', $userLogined['modules']) || in_array('Banner', $userLogined['modules'])  || in_array('Contacts', $userLogined['modules']) || in_array('ChooseUs', $userLogined['modules']) ))) : ?>
      <li class="nav-item has-treeview <?= in_array($controller_current, ['Config', 'Banner','Contacts','ChooseUs'])  ? 'menu-open' : '' ?>">
              <a href="#" class="nav-link <?= in_array($controller_current, ['Config', 'Banner','Contacts','ChooseUs']) ? 'active' : '' ?>">
                  <i class="fas fa-book-reader"></i>
                  <p>
                      Cấu hình chung
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview" style="<?= in_array($controller_current, ['Config', 'Banner','Contacts','ChooseUs']) ? 'display: block' : 'display: none' ?>;">

                  <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
                  <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Config', $userLogined['modules']))) : ?>
                      <li class="nav-item has-treeview">
                          <a href="<?= $this->Url->build(['controller' => 'Config', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Config') ? 'active' : '' ?>">
                              <i class="fas fa-cogs"></i>
                              <p>Cấu hình hệ thống</p>
                          </a>
                      </li>
                  <?php endif; ?>

                  <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
                  <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Banner', $userLogined['modules']))) : ?>
                      <li class="nav-item has-treeview">
                          <a href="<?= $this->Url->build(['controller' => 'Banner', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Banner') ? 'active' : '' ?>">
                              <i class="far fa-image"></i>
                              <p>Banner</p>
                          </a>
                      </li>
                  <?php endif; ?>

                  <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
                  <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('Contacts', $userLogined['modules']))) : ?>
                      <li class="nav-item has-treeview">
                          <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'Contacts') ? 'active' : '' ?>">
                              <i class="far fa-id-card"></i>
                              <p>Quản lý liên hệ - góp ý</p>
                          </a>
                      </li>
                  <?php endif; ?>

                  <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
                  <?php if ($userLogined['role_id'] == 0 || ($userLogined['role_id'] == 1 && in_array('ChooseUs', $userLogined['modules']))) : ?>
                      <li class="nav-item has-treeview">
                          <a href="<?= $this->Url->build(['controller' => 'ChooseUs', 'action' => 'index']) ?>" class="nav-link <?= ($controller_current == 'ChooseUs') ? 'active' : '' ?>">
                              <i class="fas fa-user-check"></i>
                              <p>Why choose us</p>
                          </a>
                      </li>
                  <?php endif; ?>

              </ul>
          </li>
      <?php endif; ?>

        <?php /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ ?>
        <li class="nav-item has-treeview">
          <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout']) ?>" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <p>Đăng xuất</p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
