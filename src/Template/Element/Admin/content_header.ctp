<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= isset($title) ? $title : '' ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <?php if (isset($url) && count($url) > 0) : ?>
                    <ol class="breadcrumb float-sm-right">
                        <?php foreach ($url as $text => $href) : ?>
                            <li class="breadcrumb-item"><a href="<?= $href ?>"><?= $text ?></a></li>
                        <?php endforeach; ?>
                    </ol>
                <?php endif; ?>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>