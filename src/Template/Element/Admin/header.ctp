<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-danger navbar-badge"><?= isset($data['notification']) ? $data['notification'] : "" ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header"><?= isset($data['notification']) ? $data['notification'] : "0" ?> Notifications</span>
                <?php

                if (isset($allMess) && count($allMess) > 0) :
                    foreach ($allMess as $row) :
                        ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'view', $row['id']]) ?>" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> <?= $row['title'] ?>
                            <!--            <span class="float-right text-muted text-sm">3 mins</span>-->
                        </a>
                <?php
                    endforeach;
                endif;
                ?>
                <div class="dropdown-divider"></div>
                <div class="dropdown-divider"></div>
                <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'index']) ?>" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->