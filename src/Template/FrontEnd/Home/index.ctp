<?php $this->start('banner'); ?>
<?= $this->element('Front/banner') ?>
<?php $this->end(); ?>

<?php if (isset($listChoose) && !empty($listChoose)) { ?>
    <section class="section-announce s-border-bottom">
        <div class="container">
            <div class="section-heading section-heading-line">
                <h2 class="section-title"><?= __('Vì sao lựa chọn đại học Đại Nam') ?></h2>
                <div class="section-title-sub"><?= __('Đại học Đại Nam tạo điều kiện thuận lợi giúp sinh viên phát triển năng lực, khả năng tư duy sáng tạo trong thời kỳ hội nhập') ?></div>
            </div>

            <div class="row justify-content-md-center text-center px-5 mx-5 section-box-content about-top-box">
                <?php foreach ($listChoose as $key => $value) {
                $slugLang = ($value['flg_language'] == 1) ? 'en' : 'vi';
                ?>
                    <div class="col-md-4 col-6">
                        <article class="announce-item">
                            <div class="img">
                                <a href="" class="post-image"><img src="<?= $this->Root->whychooseus(isset($value['img_highlight']) ? $value['img_highlight'] : '') ?>" /></a>
                            </div>
                            <div class="text">
                                <p class="font-weight-bold"><a href=""> <?= $value['title'] ?></a></p>
                            </div>
                        </article>
                        <!-- /.announce-item -->
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- E: section-announce -->
<?php } ?>

<?php if (isset($listTB) && !empty($listTB)) { ?>
    <section class="section-announce s-border-bottom">
        <div class="container">
            <div class="section-heading section-heading-line">
                <h2 class="section-title"><?= __('Thông Báo Tuyển Sinh') ?></h2>
            </div>

            <div class="row">
                <?php foreach ($listTB as $key => $value) {
                    $slugLang = ($value['flg_language'] == 1) ? 'en' : 'vi';
                    $urlDetail = $this->Url->build([
                        'controller' => 'News',
                        'action' => 'detail',
                        'lang' => $slugLang,
                        'slug' => 'thong-bao',
                        'slug2' => $value['slug']
                    ]);
                    ?>
                    <div class="col-md-4 col-6">
                        <article class="announce-item">
                            <div class="img">
                                <a href="<?= $urlDetail ?>" class="post-image"><img src="<?= $this->Root->news(isset($value['img_highlight']) ? $value['img_highlight'] : '') ?>" alt="<?= $value['title'] ?>" /></a>
                            </div>
                            <div class="text">
                                <h3><a href="<?= $urlDetail ?>"><?= $value['title'] ?></a></h3>
                            </div>
                        </article>
                        <!-- /.announce-item -->
                    </div>
                <?php } ?>
            </div>

            <div class="text-center"><a href="/<?= $lang ?>/thong-bao" class="btn btn-orange btn-md rounded-0">Xem thêm tin</a></div>
        </div>
    </section>
    <!-- E: section-announce -->
<?php } ?>

<?php if (isset($listBTDN) && !empty($listBTDN)) { ?>
    <!-- start section-student -->
    <section class="section-student s-border-bottom">
        <div class="container">
            <div class="section-heading section-heading-line">
                <h2 class="section-title"><?= __('Tin Tức Tuyển Sinh') ?></h2>
                <div class="section-title-sub"><?= __('Tin tức tuyển sinh, hướng nghiệp mới nhất') ?></div>
            </div>

            <div class="row row-sm-xs">
                <?php foreach ($listBTDN as $key => $value) { ?>
                    <div class="col-md-3 col-6">
                        <article class="announce-item">
                            <div class="img">
                                <a href="<?= $this->Url->build([
                                    'controller' => 'Faculty',
                                    'action' => 'detail',
                                    'lang' => $value['flg_language'] == 1 ? 'en' : 'vi',
                                    'slug_faculty' => $value['department_slug'],
                                    'slug_faculty_list' => $value['cate_slug'],
                                    'slug_faculty_detail' => $value['slug']
                                ]) ?>" class="post-image">
                                    <img src="<?= $this->Root->news(isset($value['img_highlight']) ? $value['img_highlight'] : '') ?>" alt="<?= $value['title'] ?>" />
                                </a>
                            </div>
                            <div class="text">
                                <p class="font-weight-bold">
                                    <a href="<?= $this->Url->build([
                                        'controller' => 'Faculty',
                                        'action' => 'detail',
                                        'lang' => $value['flg_language'] == 1 ? 'en' : 'vi',
                                        'slug_faculty' => $value['department_slug'],
                                        'slug_faculty_list' => $value['cate_slug'],
                                        'slug_faculty_detail' => $value['slug']
                                    ]) ?>"><?= $value['title'] ?>
                                    </a>
                                </p>
                            </div>
                        </article>
                        <!-- /.announce-item -->
                    </div>
                <?php } ?>
            </div>

            <div class="text-center"><a href="/<?= $lang ?>/tin-tuc" class="btn btn-orange btn-md rounded-0"><?= __('Xem thêm về tin tức tuyển sinh') ?></a></div>
        </div>
    </section>
    <!-- E: section-student -->
<?php } ?>

<?php if (isset($config['about_title']) && !empty($config['about_title'])) { ?>
    <!-- start section-about-us -->
    <section class="section-about-us">
        <div class="container">
            <div class="section-heading section-heading-line">
                <h2 class="section-title"><?= $config['about_title'] ?></h2>
                <div class="section-title-sub"><?= $config['about_desc'] ?></div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="box-statistical">
                        <span class="text font-weight-bold"><?= __('Liên thông đại học') ?></span>
                        <div class="text-left"><?= __('Liên thông đại học ngành dược học, công nghệ thông tin...Đối tượng người tốt nghiệp cao đẳng, cao đẳng nghề đúng ngành đăng ký dự thi.') ?></div>
                        <div class="text-left mt-5"><a href="" class="text-primary">Đăng ký học liên thông</a></div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box-statistical">
                        <span class="text font-weight-bold"><?= __('Đại học chính quy') ?></span>
                        <div class="text-left"><?=  __('Tuyển sinh 13 ngành đạo tạo hệ Đại học chính quy bằng hình thức xét điểm thi THPT hoặc xét học bạ THPT, Đối tượng đã tốt nghiệp THPT') ?></div>
                        <div class="text-left mt-5"><a href="" class="text-primary">Đăng ký học đại học</a></div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box-statistical">
                        <span class="text font-weight-bold"><?= __('Văn bằng 2') ?></span>
                        <div class="text-left"><?= __('Văn bằng 2 Tiếng Anh, thời gian đào tạo ngắn, linh hoạt GDĐT cấp bằng chính quy. Đối tượng đã tốt nghiệp có bằng Đại Học') ?></div>
                        <div class="text-left mt-5"><a href="" class="text-primary">Đăng ký học văn bằng 2</a></div>
                    </div>
                </div>
            </div>

            <div class="text-center"><a href="/<?= $lang ?>/lien-he" class="btn btn-orange btn-md rounded-0"><?= __('Xem thêm thông tin') ?></a></div>
        </div>
    </section>
<?php } ?>
<!-- E: section-about-us -->
<?php $this->start('script_footer'); ?>
<script>
    // code javascrip footer
</script>
<?php $this->end(); ?>
