<!-- start section-detail -->
<section class="section-detail bg-detail-events">
    <div class="header-detail">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-lg-9">
                    <img src="<?= $this->Root->events(isset($data['img_highlight']) ? $data['img_highlight'] : '') ?>" alt="<?= $data['title']?>" class="d-lg-none">
                    <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'index', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_EVENT_VI : AppConst::SLUG_EVENT_EN]) ?>"><h4 class="mb-3">Tất cả Sự kiện</h4></a>
                    <h1 class="single-event-title"><?= $data['title']?></h1>
                    <h3><?= __("Tổ chức bởi")?>: <span><?= $data['ListDepartments']['title']?></span></h3>
                </div>
            </div>
        </div>
    </div><!-- /.header-detail -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sidebar-left">
                    <div class="sidebar-left-header d-none d-lg-block">
                        <img src="<?= $this->Root->events(isset($data['img_highlight']) ? $data['img_highlight'] : '') ?>" alt="<?= $data['title']?>">
                    </div>
                    <div class="sidebar-left-content">
                        <ul>
                            <li>
                                <i class="ico-8"></i>
                                <div class="left"><strong><?= $this->Common->getDayOfWeek($data['start_date']);?>, <?= __("ngày")?> <?= $this->Common->date($data['start_date'])?></strong></div>
                                <div class="right time"><?= $this->Common->time($data['start_date'])?> - <?= $this->Common->time($data['end_date'])?></div>
                            </li>
                            <li>
                                <i class="ico-9"></i>
                                <div class="left"><strong><?= $data['address']?></strong></div>
                                <div class="right"><a target="_blank" href="<?= $data['map']?>"><?= __("Xem bản đồ")?></a></div>
                            </li>
                            <li>
                                <i class="ico-10"></i>
                                <div class="left"><a href="mailto:<?= $data['mail']?>?subject=Sự kiện <?= $data['title']?>"><?= __("Email cho ban tổ chức")?></a></div>
                            </li>
                            <li>
                                <i class="ico-11"></i>
                                <div class="left"><strong><?= $data['telno']?></strong></div>
                            </li>
                            <li>
                                <i class="ico-12"></i>
                                <div class="left"><strong><?= __("Mở cửa cho")?>:</strong></div>
                                <div class="right"><?= $data['position']?></div>
                            </li>
                            <li>
                                <i class="ico-13"></i>
                                <div class="left"><strong><?= __("Phí tham gia")?>:</strong></div>
                                <div class="right"><?= $data['cost']?></div>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.sidebar-left -->
            </div><!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <h2 class="title-entry"><?= __("Chi tiết Sự kiện")?>:</h2>
                <article class="entry">
                    <?= html_entity_decode($data['content'])?>
                </article>

                <div class="tags">
                    <?= $this->element('Front/tags') ?>
                </div>

                <div class="bottom-entry d-flex flex-wrap justify-content-between">
                    <div class="share">
                        <span><?= __("Chia sẻ")?>:</span>
                        <?= $this->element('Front/share') ?>
                    </div>
                    <div class="entry-update"><?= __('Chỉnh sửa ngày')?> <?= $this->Common->date($data['update_date'])?></div>
                </div>
            </div><!-- /.col-lg-9 -->
        </div>
    </div>
</section>
<!-- End section-detail -->
