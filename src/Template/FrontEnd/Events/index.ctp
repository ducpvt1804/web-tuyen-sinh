<!-- start section-event -->
<section class="section-event">
    <div class="container">
        <h2><?= __("Danh Sách Sự Kiện") ?>:</h2>
        <div class="list-events">
            <?php
            if (isset($data)) :
                foreach ($data as $row) :
                    ?>
                    <article class="event-item">
                        <div class="img hover-scale">
                            <a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_EVENT_VI : AppConst::SLUG_EVENT_EN, 'detail' => $row['slug']]) ?>">
                                <img src="<?= $this->Root->events(isset($row['img_highlight']) ? $row['img_highlight'] : '') ?>" alt="<?= $row['title'] ?>">
                            </a>
                        </div>

                        <div class="text">
                            <h4><a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'findbyfaculty', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_EVENT_VI : AppConst::SLUG_EVENT_EN, 's' => 's', 'faculty' => $row['ListDepartments']['slug']]) ?>"><?= $row['ListDepartments']['title'] ?></a></h4>
                            <h3><a href="<?= $this->Url->build(['controller' => 'Events', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_EVENT_VI : AppConst::SLUG_EVENT_EN, 'detail' => $row['slug']]) ?>"><?= $row['title'] ?></a></h3>
                            <ul>
                                <li><i class="ico-16"></i> <?= $this->Common->getTimeEvents($row['start_date'], $row['end_date']); ?></li>
                                <li><i class="ico-17"></i> <?= $row['address'] ?></li>
                            </ul>
                            <?php if ($row['flg_status'] == 0) { ?>
                                <div class="status-event open-soon"><?= __("Sắp diễn ra") ?></div>
                            <?php } elseif ($row['flg_status'] == 1) { ?>
                                <div class="status-event opening"><?= __("Đang diễn ra") ?></div>
                            <?php } else { ?>
                                <div class="status-event finished"><?= __("Đã kết thúc") ?></div>
                            <?php } ?>
                        </div>
                    </article><!-- /.event-item -->
                <?php
                    endforeach;
                else :
                    ?>
                <article class="event-item">
                    <?= __("Không có dữ liệu") ?>
                </article>
            <?php endif; ?>

        </div><!-- /.list-events -->
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center justify-content-md-start">
                <?= $this->AppPaginator->prev() ?>
                <?= $this->AppPaginator->numbers(['first' => 1, 'last' => 1]) ?>
                <?= $this->AppPaginator->next() ?>
            </ul>
        </nav>
    </div>
</section>
<!-- End section-event -->
