<?php if (isset($images)) : ?>
    <?php foreach ($images as $row) :
        $img_highlight = $this->Common->getLibraryMedia($row['id']);
        ?>
        <div class="col-6 col-lg-3">
            <article class="news-item">
                <div class="img hover-scale">
                    <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                        <img src="<?= $this->Root->libraries(isset($img_highlight['LibrariesMedia']['path']) ? $img_highlight['LibrariesMedia']['path'] : '') ?>" alt="<?= $row['title']?>" alt="">
                    </a>
                </div>
                <div class="text">
                    <h3><a href="#"><?= $row['department_title']?></a></h3>
                    <div class="desc dotdotdot">
                        <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                            <?=$row['LibrariesDetail']['name_library']?>
                        </a>
                    </div>
                </div>
            </article><!-- /.news-item -->
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($videos)):?>
    <?php
    foreach ($videos as $row) :
        $img_highlight = $this->Common->getLibraryMedia($row['id']);
        ?>
        <div class="col-6 col-lg-3">
            <article class="news-item">
                <div class="img hover-scale">
                    <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slugv' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                        <img src="https://img.youtube.com/vi/<?= isset($img_highlight['LibrariesMedia']['path']) ? $img_highlight['LibrariesMedia']['path'] : '' ?>/0.jpg" alt="<?= $row['title']?>" alt="">
                        <span class="play-video"><i class="ico-14"></i></span>
                    </a>
                </div>
                <div class="text">
                    <h3><a href="#"><?= $row['department_title']?></a></h3>
                    <div class="desc dotdotdot">
                        <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slugv' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                            <?=$row['LibrariesDetail']['name_library']?>
                        </a>
                    </div>
                </div>
            </article><!-- /.news-item -->
        </div>
    <?php
    endforeach;
    ?>
<?php endif; ?>
