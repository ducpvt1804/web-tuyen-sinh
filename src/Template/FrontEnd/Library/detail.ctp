<?php if ($data['type'] == 1) { ?>
    <!-- start section-news-detail -->
    <section class="section-news-detail photo-list pt-md-60">
        <div class="container">
            <div class="section-heading">
                <h2 class="section-title"><?= $data["LibrariesDetail"]["name_library"] ?></h2>
            </div>

            <div class="post-update"><?= __("Ngày đăng") ?>: <?= $this->Common->date($data['create_date']); ?></div>

            <div class="row">
                <?php
                    if (isset($detail_library) && count($detail_library) > 0) :
                        foreach ($detail_library as $row) :
                            ?>
                        <div class="col-md-4 col-6">
                            <div class="photo-item hover-scale">
                                <a href="<?= $this->Root->libraries(isset($row['LibrariesMedia']['path']) ? $row['LibrariesMedia']['path'] : '') ?>" class="image-popup">
                                    <img src="<?= $this->Root->libraries(isset($row['LibrariesMedia']['path']) ? $row['LibrariesMedia']['path'] : '') ?>" alt="">
                                </a>
                            </div><!-- /.photo-item -->
                        </div>
                <?php
                        endforeach;
                    endif;
                    ?>
            </div>

            <div class="bottom-page d-lg-flex">
                <div class="move-gallery-photo text-center mx-auto">
                    <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'image', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN]) ?>" class="view-all-photo">
                        <i class="ico-7"></i> <?= __("Thư viện hình ảnh") ?>
                    </a>
                </div>
                <nav aria-label="Page navigation example" class="order-first">
                    <ul class="pagination justify-content-center">
                        <?= $this->AppPaginator->prev() ?>
                        <?= $this->AppPaginator->numbers(['first' => 1, 'last' => 1]) ?>
                        <?= $this->AppPaginator->next() ?>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <!-- End section-news-detail -->
<?php } else { ?>
    <!-- start section-news-detail -->
    <section class="section-news-detail video pt-md-60">
        <div class="container">
            <div class="section-heading">
                <h2 class="section-title"><?= $data["LibrariesDetail"]["name_library"] ?></h2>
            </div>

            <div class="post-update"><?= __("Ngày đăng") ?>: <?= $this->Common->date($data['create_date']); ?></div>
            <?php
                if (isset($detail_library) && count($detail_library) > 0) :
                    foreach ($detail_library as $row) :
                        ?>
                    <div class="video-youtube">
                        <iframe width="420" height="315" src="https://www.youtube.com/embed/<?= $row['LibrariesMedia']['path'] ?>"></iframe>
                    </div>
            <?php
                    endforeach;
                endif;
                ?>
            <div class="bottom-page d-lg-flex">
                <div class="move-gallery-photo text-center mx-auto">
                    <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'video', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN]) ?>" class="view-all-photo">
                        <i class="ico-7"></i> <?= __("Thư viện video") ?>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End section-news-detail -->
<?php } ?>
