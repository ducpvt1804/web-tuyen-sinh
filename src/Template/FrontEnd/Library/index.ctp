<!-- start section-library -->
<section class="section-library">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title"><?=__("Thư viện hình ảnh, video")?></h2>
        </div>

        <div class="library">
            <h3 class="title-library"><?=__("Thư viện hình ảnh")?></h3>
            <div class="row list-news">
                <?php
                if (isset($images)) :
                foreach ($images as $row) :
                    $img_highlight = $this->Common->getLibraryMedia($row['id']);
                ?>
                <div class="col-6 col-lg-3">
                    <article class="news-item">
                        <div class="img hover-scale">
                            <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                <img src="<?= $this->Root->libraries(isset($img_highlight['LibrariesMedia']['path']) ? $img_highlight['LibrariesMedia']['path'] : '') ?>" alt="<?= $row['title']?>" alt="">
                            </a>
                        </div>
                        <div class="text">
                            <h3><a href="#"><?= $row['department_title']?></a></h3>
                            <div class="desc dotdotdot">
                                <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                    <?=$row['LibrariesDetail']['name_library']?>
                                </a>
                            </div>
                        </div>
                    </article><!-- /.news-item -->
                </div>
                <?php
                endforeach;
                else:
                    ?>
                    <article class="event-item">
                        <?= __("Không có dữ liệu")?>
                    </article>
                <?php endif;?>
            </div>
            <div class="text-center">
                <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'image', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_IMAGE_GALLERY_VI : AppConst::SLUG_IMAGE_GALLERY_EN]) ?>" class="btn btn-orange btn-md rounded-0">
                    <?= __("Xem thêm")?>
                </a>
            </div>
        </div><!-- /.library -->
        <div class="library">
            <h3 class="title-library"><?= __("Thư viện video")?></h3>
            <?php if (isset($videos)):?>
            <div class="row list-news">
                <?php

                foreach ($videos as $row) :
                $img_highlight = $this->Common->getLibraryMedia($row['id']);
                ?>
                <div class="col-6 col-lg-3">
                    <article class="news-item">
                        <div class="img hover-scale">
                            <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                <img src="https://img.youtube.com/vi/<?= isset($img_highlight['LibrariesMedia']['path']) ? $img_highlight['LibrariesMedia']['path'] : '' ?>/0.jpg" alt="<?= $row['title']?>" alt="">
                                <span class="play-video"><i class="ico-14"></i></span>
                            </a>
                        </div>
                        <div class="text">
                            <h3><a href="#"><?= $row['department_title']?></a></h3>
                            <div class="desc dotdotdot">
                            <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                <?=$row['LibrariesDetail']['name_library']?>
                            </a>
                            </div>
                        </div>
                    </article><!-- /.news-item -->
                </div>
                <?php
                endforeach;
                ?>

            </div>
            <div class="text-center">
                <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'video', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN]) ?>" class="btn btn-orange btn-md rounded-0">
                    <?= __("Xem thêm")?>
                </a>
            </div>
        </div><!-- /.library -->
        <?php
        else:
            ?>
            <article class="event-item">
                <?= __("Không có dữ liệu")?>
            </article>
        <?php endif;?>
    </div>
</section>
<!-- End section-library -->
