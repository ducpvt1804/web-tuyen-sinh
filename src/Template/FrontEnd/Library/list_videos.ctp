<!-- start section-library -->
<section class="section-library">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title"><?= __("Thư viện video") ?></h2>
        </div>
        <!-- List videos -->
        <div class="library">
            <?php if (isset($videos)) : ?>
                <div class="row list-news">
                    <?php
                        foreach ($videos as $row) :
                            $img_highlight = $this->Common->getLibraryMedia($row['id']);
                            ?>
                        <div class="col-6 col-lg-3">
                            <article class="news-item">
                                <div class="img hover-scale">
                                    <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                        <img src="https://img.youtube.com/vi/<?= isset($img_highlight['LibrariesMedia']['path']) ? $img_highlight['LibrariesMedia']['path'] : '' ?>/0.jpg" alt="<?= $row['title'] ?>" alt="">
                                        <span class="play-video"><i class="ico-14"></i></span>
                                    </a>
                                </div>
                                <div class="text">
                                    <h3><a href="#"><?= $row['department_title'] ?></a></h3>
                                    <div class="desc dotdotdot">
                                        <a href="<?= $this->Url->build(['controller' => 'Library', 'action' => 'detail', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_VIDEO_LIBRARY_VI : AppConst::SLUG_VIDEO_LIBRARY_EN, 'detail' => $row['LibrariesDetail']['slug']]) ?>">
                                            <?= $row['LibrariesDetail']['name_library'] ?>
                                        </a>
                                    </div>
                                </div>
                            </article><!-- /.news-item -->
                        </div>
                    <?php
                        endforeach;
                        ?>

                </div>
                <div class="text-center">
                    <button id="more-list-video" class="btn btn-orange btn-md rounded-0 more-list-video " page="2"><i class="fa fa-spinner fa-pulse fa-lg fa-fw" style="display: none;"></i> <?= __('Xem thêm') ?></button>
                </div>
        </div><!-- /.library -->
    <?php
    else :
        ?>
        <article class="event-item">
            <?= __("Không có dữ liệu") ?>
        </article>
    <?php endif; ?>
    </div>
</section>
<!-- End section-library -->

<?php $this->start('script_footer'); ?>
<script>
    $("#more-list-video").click(function() {
        let _this = $(this);
        let page = _this.attr('page');
        if (page <= 0) {
            return;
        }
        $('.fa-pulse').show();
        let frm = new FormData();
        let media_type = 2;
        let lang = "<?= $lang ?>";
        frm.append('lang', lang);
        frm.append('page', page);
        frm.append('media_type', media_type);
        $.ajax({
            headers: {
                'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
            },
            url: '<?= $this->Url->build(['controller' => 'Library', 'action' => 'loadMoreVideo', 'lang' => $lang]) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(function(rp) {
            if (rp.trim() == '') {
                _this.attr('page', 0);
                _this.text('Đã đến cuối trang');
                return;
            }
            if (rp) {
                _this.attr('page', (Number(page) + 1));
                $('.list-news').append(rp.trim());
            } else {
                alert("Đã có lỗi xảy ra. vui lòng thử lại.");
            }
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        }).always(function() {
            $('.fa-pulse').hide();
        });
    });
</script>
<?php $this->end(); ?>
