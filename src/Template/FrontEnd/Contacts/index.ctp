<!-- start section-register -->
<section class="section-register">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title"><?= __("Liên hệ với chúng tôi") ?></h2>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <?= $this->AppForm->create(null, [
                    'type' => 'post',
                    'url' => ['controller' => 'Contacts', 'action' => 'index', 'lang' => $lang, 'slug' => $lang == 'vi' ? AppConst::SLUG_CONTACT_VI : AppConst::SLUG_CONTACT_EN],
                    'id' => 'contact-add'
                ]) ?>
                <div class="form-register">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-ft"><h3><?=$config['name']?></h3></div>
                        <p><?=__("Cơ sở chính")?>: <?=$config['address_1']?></p>
                        <p><?=__("Cơ sở 1")?>: <?=$config['address_2']?></p>
                        <p><?=__("Điện thoại")?>: <?=$config['telno']?> - Fax: <?=$config['fax']?></p>
                        <p><?=__("Email")?>: <?=$config['email']?></p>
                            <?= $config['map']?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
<!--                                <label for="input_1_1">--><?//=__("Họ và tên")?><!-- <span>(*)</span></label>-->
                                <input type="text" required name="name" class="form-control" placeholder="<?=__("Họ và tên")?> *" id="input_1_1">
                            </div>

                            <div class="form-group">
<!--                                <label for="input_1_2">--><?//=__("Email")?><!-- <span>(*)</span></label>-->
                                <input type="email" required name="email" class="form-control" placeholder="<?= __("Email")?> *" id="input_1_2">
                            </div>

                            <div class="form-group">
<!--                                <label for="input_1_3">--><?//=__("Tiêu đề")?><!-- <span>(*)</span></label>-->
                                <input type="text" required name="title" class="form-control" placeholder="<?=__("Tiêu đề")?> *" id="input_1_3">
                            </div>

                            <div class="form-group">
<!--                                <label for="input_1_5">--><?//=__("Số điện thoại")?><!--<span>(*)</span></label>-->
                                <input type="number" required name="telno" class="form-control" placeholder="<?=__("Số điện thoại")?> *" id="input_1_5">
                            </div>

                            <div class="form-group">
<!--                                <label for="input_1_6">--><?//=__("Nội dung")?><!-- <span>(*)</span></label>-->
                                <textarea rows="5" required style="width: 100%; height: 100px" name="content" class="form-control" placeholder="<?=__("Nội dung")?> *" id="input_1_6"></textarea>
                            </div>
                            <input type="hidden" name="flg_site" value="0"/>
                        </div>
                    </div>
                    <div class="text-center mt-md-4">
                        <button class="btn btn-orange btn-md rounded-0 "><?=__("Gửi")?></button>
                    </div>
                    <?= $this->Form->end() ?>
                    <div class="note">
                        <p><?=__("Xin điền đầy đủ thông tin tại các mục có dấu")?> <span class="text-red">(*)</span></p>
                        <p><?=__("Cảm ơn bạn đã gửi thông tin. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.")?></p>
                    </div>
                </div><!-- /.form-register -->
            </div><!-- /.tab-pane -->
        </div>
</section>
<!-- End section-register -->
