<!-- start section-register -->
<section class="section-register">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title"><?= __("Cảm ơn bạn đã gửi thông tin liên hệ") ?></h2>
            <p style="text-align: center; margin: 20px 0 0 0; font-size: 20px"><?=__("Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.")?></p>
        </div>
    </div>
</section>
