<?php $this->start('header'); ?>
<?= $this->element('Front/Faculty/header', [
    'menus' => $menus,
    'faculty' => isset($data['Departments']['title']) ? __($data['Departments']['title']) : ''
]) ?>
<?php $this->end(); ?>

<?php $this->start('menu'); ?>
<?= $this->element('Front/Faculty/sidebar', ['menus' => $menus]) ?>
<?php $this->end(); ?>

<section class="section-news pt-md-60">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title"><?= __('Bản Tin ' . (isset($data['Departments']['shot_title']) ? __($data['Departments']['shot_title']) : '')) ?></h2>
            <div class="section-title-sub"><?= __('Bản tin mới nhất của ' . (isset($data['Departments']['shot_title']) ? __($data['Departments']['shot_title']) : '')) ?></div>
        </div>

        <div class="row list-news">
            <?php foreach ($dataNews as $item) : ?>
                <div class="col-6 col-lg-3">
                    <article class="news-item">
                        <div class="img hover-scale"><a href="<?= $this->Url->build([
                                                                        'controller' => 'Faculty',
                                                                        'action' => 'detail',
                                                                        'lang' => $slugLang,
                                                                        'slug_faculty' => $slugFaculty,
                                                                        'slug_faculty_list' => $slugFacultyList,
                                                                        'slug_faculty_detail' => $item['slug']
                                                                    ]) ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a></div>
                        <div class="text">
                            <h3><a href="<?= $this->Url->build([
                                                    'controller' => 'Faculty',
                                                    'action' => 'detail',
                                                    'lang' => $slugLang,
                                                    'slug_faculty' => $slugFaculty,
                                                    'slug_faculty_list' => $slugFacultyList,
                                                    'slug_faculty_detail' => $item['slug']
                                                ]) ?>"><?= __(isset($data['Departments']['title']) ? __($data['Departments']['title']) : '') ?></a></h3>
                            <a href="<?= $this->Url->build([
                                                'controller' => 'Faculty',
                                                'action' => 'detail',
                                                'lang' => $slugLang,
                                                'slug_faculty' => $slugFaculty,
                                                'slug_faculty_list' => $slugFacultyList,
                                                'slug_faculty_detail' => $item['slug']
                                            ]) ?>" class="desc dotdotdot">
                                <?= __(isset($item['title']) ? __($item['title']) : '') ?>
                            </a>
                        </div>
                    </article><!-- /.news-item -->
                </div>
            <?php endforeach; ?>
        </div>

        <div class="view-more text-center">
            <button class="btn btn-orange btn-md rounded-0 more-list " page="2"><i class="fa fa-spinner fa-pulse fa-lg fa-fw" style="display: none;"></i> <?= __('Xem thêm bài viết') ?></button>
        </div>
    </div>
</section>

<?php $this->start('script_footer'); ?>
<script>
    $(".more-list").click(function() {
        let _this = $(this);
        let page = _this.attr('page');
        if (page <= 0) {
            return;
        }
        $('.fa-pulse').show();
        let frm = new FormData();
        let faculty = "<?= $data['Departments']['title'] ?>";
        let slugFacultyList = "<?= $this->getRequest()->getParam('slug_faculty_list') ?>";
        let depart_ment = "<?= $data['id'] ?>";
        frm.append('slugFacultyList', slugFacultyList);
        frm.append('depart_ment', depart_ment);
        frm.append('page', page);
        frm.append('faculty', faculty);
        $.ajax({
            headers: {
                'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
            },
            url: '<?= $this->Url->build(['controller' => 'Faculty', 'action' => 'moreList', 'lang' => $slugLang]) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(function(rp) {
            if (rp.trim() == '') {
                _this.attr('page', 0);
                _this.text('Đã đến cuối trang');
                return;
            }
            if (rp) {
                _this.attr('page', (Number(page) + 1));
                $('.list-news').append(rp.trim());
            } else {
                alert("Đã có lỗi xảy ra. vui lòng thử lại.");
            }
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        }).always(function() {
            $('.fa-pulse').hide();
        });
    });
</script>
<?php $this->end(); ?>