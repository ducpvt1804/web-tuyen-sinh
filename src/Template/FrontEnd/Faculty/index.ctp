<?php $this->start('header'); ?>
<?= $this->element('Front/Faculty/header', [
    'menus' => $menus,
    'data' => $data
]) ?>
<?php $this->end(); ?>

<?php $this->start('menu'); ?>
<?= $this->element('Front/Faculty/sidebar', ['menus' => $menus]) ?>
<?php $this->end(); ?>

<?php $this->start('banner'); ?>
<?= $this->element('Front/Faculty/banner', ['banner' => $this->Root->departments(isset($data['Departments']['banner']) ? $data['Departments']['banner'] : '')]) ?>
<?php $this->end(); ?>

<section class="section-about-faculty">
    <div class="container">
        <div class="section-heading section-heading-line">
            <h2 class="section-title text-uppercase"><?= isset($data['Departments']['shot_title']) ? __($data['Departments']['shot_title']) : '' ?></h2>
            <div class="section-title-sub"><?= __('Đào tạo Dược sĩ Đại học chuẩn mực và năng động') ?></div>
        </div>
        <div class="img">
            <img src="<?= $this->Root->departments($data['Departments']['img_highlight']) ?>" alt="">
        </div>
    </div>
</section>
<!-- End section-about-faculty -->

<!-- start section-feature -->
<section class="section-feature">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title text-uppercase"><?= __('giới thiệu Về ' . (isset($data['Departments']['shot_title']) ? __($data['Departments']['shot_title']) : '')) ?></h2>
        </div>
        <div class="row">
            <?php foreach ($dataIntro as $item) : ?>
                <div class="col-6 col-lg-3">
                    <div class="card-info">
                        <div class="card-info-img hover-scale">
                            <a href="<?= $this->Url->build([
                                'controller' => 'Faculty',
                                'action' => 'detail',
                                'lang' => $slugLang,
                                'slug_faculty' => $slugFaculty,
                                'slug_faculty_list' => $menus['intro']['slug'],
                                'slug_faculty_detail' => $item['slug']
                            ]) ?>"><img
                                    src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>"
                                    alt="<?= $item['slug'] ?>"></a>
                        </div>
                        <div class="card-info-body">
                            <h3 class="card-info-title ">
                                <a href="<?= $this->Url->build([
                                    'controller' => 'Faculty',
                                    'action' => 'detail',
                                    'lang' => $slugLang,
                                    'slug_faculty' => $slugFaculty,
                                    'slug_faculty_list' => $menus['intro']['slug'],
                                    'slug_faculty_detail' => $item['slug']
                                ]) ?>"><?= isset($item['title']) ? $this->Text->truncate(__($item['title']),90,['ellipsis' => '...','exact' => false]) : '' ?></a>
                            </h3>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="view-more text-center">
            <a href="<?= $this->Url->build([
                'controller' => 'Faculty',
                'action' => 'list',
                'lang' => $slugLang,
                'slug_faculty' => $slugFaculty,
                'slug_faculty_list' => $menus['intro']['slug']
            ]) ?>" class="btn btn-orange btn-md rounded-0"><?= __('Xem thêm bài viết mới') ?></a>
        </div>
    </div>
</section>
<!-- E: section-feature -->

<!-- start section-news-faculty -->
<section class="section-news-faculty">
    <div class="container">
        <div class="section-heading section-heading-line">
            <h2 class="section-title text-uppe section-heading-linercase"><?= __('Tin tức - hoạt động của khoa') ?></h2>
            <div
                class="section-title-sub"><?= __('Tin tức hoạt động mới nhất của khoa luôn được cập nhật liên tục') ?></div>
        </div>
        <div class="row">
            <?php foreach ($dataNews as $item) : ?>
                <div class="col-6 col-lg-4">
                    <div class="news-thumb-item">
                        <div class="img hover-scale">
                            <a href="<?= $this->Url->build([
                                'controller' => 'Faculty',
                                'action' => 'detail',
                                'lang' => $slugLang,
                                'slug_faculty' => $slugFaculty,
                                'slug_faculty_list' => $menus['new']['slug'],
                                'slug_faculty_detail' => $item['slug']
                            ]) ?>"><img
                                    src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>"
                                    alt="<?= $item['slug'] ?>"></a>
                        </div>
                        <div class="text">
                            <h3 class="dotdotdot">
                                <a href="<?= $this->Url->build([
                                    'controller' => 'Faculty',
                                    'action' => 'detail',
                                    'lang' => $slugLang,
                                    'slug_faculty' => $slugFaculty,
                                    'slug_faculty_list' => $menus['new']['slug'],
                                    'slug_faculty_detail' => $item['slug']
                                ]) ?>"><?= isset($item['title']) ? __($item['title']) : '' ?></a>
                            </h3>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="view-more text-center">
            <a href="<?= $this->Url->build([
                'controller' => 'Faculty',
                'action' => 'list',
                'lang' => $slugLang,
                'slug_faculty' => $slugFaculty,
                'slug_faculty_list' => $menus['new']['slug']
            ]) ?>" class="btn btn-orange btn-md rounded-0"><?= __('Xem thêm về Hoạt động Khoa') ?></a>
        </div>
    </div>
</section>
<!-- End: section-news-faculty -->
<?php if(isset($data) && $data['type'] != 1){?>
<!-- start section-felling-student -->
<section class="section-felling-student">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title text-uppercase"><?= __('Cảm nhận của sinh viên') ?></h2>
        </div>
        <div class="slider-student owl-carousel owl-theme owl-arrow-white">
            <?php foreach ($dataStudentFeel as $item) : ?>
                <div class="student-item">
                    <div class="img"><img
                            src="<?= $this->Root->student_feel(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>"
                            alt=""></div>
                    <div class="name-student">
                        <?= isset($item['fullname']) ? __($item['fullname']) : '' ?>
                        <br/><?= isset($item['class']) ? __($item['class']) : '' ?>
                    </div>
                    <div class="desc"><?= isset($item['content']) ? __($item['content']) : '' ?></div>
                </div><!-- /.student-item -->
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- End section-felling-student -->
<?php }?>

<!-- start section-partners -->
<section class="section-partners">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title text-uppercase"><?= __('Đối tác') ?></h2>
        </div>
        <div class="slider-partner owl-theme owl-arrow-white owl-carousel">
            <?php
            $i = 0;
            foreach ($partners as $item) : ?>
                <?php if ($i == 0) : ?>
                    <div class="partner">
                <?php endif; ?>
                <div class="img">
                    <img src="<?= $this->Root->partner(isset($item['img']) ? $item['img'] : '') ?>" alt="">
                </div>
                <?php if ($i == 7) : $i = 0; ?>
                    </div>
                <?php else : $i++;
                endif;
            endforeach; ?>
        </div>
    </div>
</section>
