<?php $this->start('header'); ?>
<?= $this->element('Front/Faculty/header', [
    'menus' => $menus,
    'faculty' => isset($data['Departments']['title']) ? __($data['Departments']['title']) : ''
]) ?>
<?php $this->end(); ?>

<?php $this->start('menu'); ?>
<?= $this->element('Front/Faculty/sidebar', ['menus' => $menus]) ?>
<?php $this->end(); ?>

<section class="section-news-detail news bg-detail-news">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center d-lg-none">
            <p class="news-post"><?= __('30/05/2019') ?></p>
            <div class="share ml-2 mb-0">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div>
        </div>
        <h2 class="title-news-detail"><?= $dataDetail['title'] ?></h2>
        <div class="short-text">
            <?= $dataDetail['short_description'] ?>
        </div>
        <div class="content-detail">
            <div class="share share-top d-none d-lg-block">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div>

            <div class="entry">
                <?= html_entity_decode($dataDetail['content']) ?>
            </div>

            <div class="bottom-entry d-flex flex-wrap justify-content-between">
                <div class="tags">
                    <?php foreach (explode(',', $seo['tags']) as $tag) : ?>
                        <a href="#"><?= trim($tag) ?></a>
                    <?php endforeach; ?>
                </div>
                <div class="share">
                    <span>Chia sẻ:</span>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.content-detail -->

        <div class="news-related">
            <h2><?= __('Bài viết liên quan') ?></h2>
            <div class="row">
                <?php foreach ($listNews as $item) : ?>
                    <div class="col-lg-4">
                        <article class="relate-item">
                            <div class="img">
                                <a href="<?= $this->Url->build([
                                                    'controller' => 'Faculty',
                                                    'action' => 'detail',
                                                    'lang' => $slugLang,
                                                    'slug_faculty' => $item['department_slug'],
                                                    'slug_faculty_list' => $item['cate_slug'],
                                                    'slug_faculty_detail' => $item['slug']
                                                ]) ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a>
                            </div>
                            <div class="text">
                                <h4 class="mb-3"><?= $item['department_title'] ?></h4>
                                <h3 class="dotdotdot" style="text-align: justify;"><a href="<?= $this->Url->build([
                                                                        'controller' => 'Faculty',
                                                                        'action' => 'detail',
                                                                        'lang' => $slugLang,
                                                                        'slug_faculty' => $item['department_slug'],
                                                                        'slug_faculty_list' => $item['cate_slug'],
                                                                        'slug_faculty_detail' => $item['slug']
                                                                    ]) ?>"><?= $item['title'] ?></a></h3>
                                <div class="desc dotdotdot" style="text-align: justify;"><?= $item['short_description'] ?></div>
                            </div>
                        </article><!-- /.relate-item -->
                    </div>
                <?php endforeach; ?>
            </div>
        </div><!-- /.news-related -->
    </div>
</section>