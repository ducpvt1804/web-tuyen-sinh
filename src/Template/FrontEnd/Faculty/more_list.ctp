<?php if (isset($data)) : ?>
    <?php foreach ($data as $item) : ?>
        <div class="col-6 col-lg-3">
            <article class="news-item">
                <div class="img hover-scale"><a href="<?= $this->Url->build([
                                                                    'controller' => 'Faculty',
                                                                    'action' => 'detail',
                                                                    'lang' => $lang,
                                                                    'slug_faculty' => $cate['slug'],
                                                                    'slug_faculty_list' => $slugFacultyList,
                                                                    'slug_faculty_detail' => $item['slug']
                                                                ]) ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a></div>
                <div class="text">
                    <h3><a href="<?= $this->Url->build([
                                                'controller' => 'Faculty',
                                                'action' => 'detail',
                                                'lang' => $lang,
                                                'slug_faculty' => $cate['slug'],
                                                'slug_faculty_list' => $slugFacultyList,
                                                'slug_faculty_detail' => $item['slug']
                                            ]) ?>"><?= $faculty ?></a></h3>
                    <a href="<?= $this->Url->build([
                                            'controller' => 'Faculty',
                                            'action' => 'detail',
                                            'lang' => $lang,
                                            'slug_faculty' => $cate['slug'],
                                            'slug_faculty_list' => $slugFacultyList,
                                            'slug_faculty_detail' => $item['slug']
                                        ]) ?>" class="desc dotdotdot"> <?= __(isset($item['title']) ? __($item['title']) : '') ?> </a>
                </div>
            </article><!-- /.news-item -->
        </div>
    <?php endforeach; ?>
<?php endif; ?>