<?php if (isset($dataNews)) : ?>
    <?php foreach ($dataNews as $item) :
            $urlCate = '';
            $urlDetail = '';
            $title = (isset($item['department_title']) && !empty(trim($item['department_title']))) ? $item['department_title'] : $item['cate_title'];
            if (!empty(trim($item['department_slug']))) {
                $urlCate = $this->Url->build([
                    'controller' => 'Faculty',
                    'action' => 'list',
                    'lang' => $slugLang,
                    'slug_faculty' => $item['department_slug'],
                    'slug_faculty_list' => $item['cate_faculty_slug'],
                ]);
                $urlDetail = $this->Url->build([
                    'controller' => 'Faculty',
                    'action' => 'detail',
                    'lang' => $slugLang,
                    'slug_faculty' => $item['department_slug'],
                    'slug_faculty_list' => $item['cate_faculty_slug'],
                    'slug_faculty_detail' => $item['slug']
                ]);
            } else {
                $urlCate = $this->Url->build([
                    'controller' => 'News',
                    'action' => 'index',
                    'lang' => $slugLang,
                    'slug' => $item['cate_slug'],
                ]);

                $urlDetail = $this->Url->build([
                    'controller' => 'News',
                    'action' => 'detail',
                    'lang' => $slugLang,
                    'slug' => $item['cate_slug'],
                    'slug2' => $item['slug'],
                ]);
            }
            ?>
        <div class="col-6 col-lg-3">
            <article class="news-item">
                <div class="img hover-scale"><a href="<?= $urlDetail ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a></div>
                <div class="text">
                    <h3><a href="<?= $urlCate ?>"><?= __($title) ?></a></h3>
                    <a href="<?= $urlDetail ?>" class="desc dotdotdot">
                        <?= isset($item['title']) ? __($item['title']) : '' ?>
                    </a>
                </div>
            </article><!-- /.news-item -->
        </div>
    <?php endforeach; ?>
<?php endif; ?>