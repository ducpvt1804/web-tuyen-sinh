<section class="section-news-detail news bg-detail-news">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center d-lg-none">
            <p class="news-post"><?= __('30/05/2019') ?></p>
            <div class="share ml-2 mb-0">
                <?= $this->element('Front/share') ?>
            </div>
        </div>
        <h2 class="title-news-detail"><?= $dataDetail['title'] ?></h2>
        <div class="short-text">
            <?= $dataDetail['short_description'] ?>
        </div>
        <div class="content-detail">
            <div class="share share-top d-none d-lg-block">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div>

            <div class="entry">
                <?= html_entity_decode($dataDetail['content']) ?>
            </div>
        </div><!-- /.content-detail -->
        <div class="bottom-entry d-flex flex-wrap justify-content-between">
            <div class="tags">
                <?= $this->element('Front/tags') ?>
            </div>
            <div class="share">
                <span><?=__('Chia sẻ')?>:</span>
                <?= $this->element('Front/share') ?>
            </div>
        </div>
        <div class="news-related">
            <h2><?= __('Bài viết liên quan') ?></h2>
            <div class="row">
                <?php foreach ($listNews as $item) :
                    $urlCate = '';
                    $urlDetail = '';
                    $title = (isset($item['department_title']) && !empty(trim($item['department_title']))) ? $item['department_title'] : $item['cate_title'];
                    if (!empty(trim($item['department_slug']))) {
                        $urlCate = $this->Url->build([
                            'controller' => 'Faculty',
                            'action' => 'list',
                            'lang' => $slugLang,
                            'slug_faculty' => $item['department_slug'],
                            'slug_faculty_list' => $item['cate_faculty_slug'],
                        ]);
                        $urlDetail = $this->Url->build([
                            'controller' => 'Faculty',
                            'action' => 'detail',
                            'lang' => $slugLang,
                            'slug_faculty' => $item['department_slug'],
                            'slug_faculty_list' => $item['cate_faculty_slug'],
                            'slug_faculty_detail' => $item['slug']
                        ]);
                    } else {
                        $urlCate = $this->Url->build([
                            'controller' => 'News',
                            'action' => 'index',
                            'lang' => $slugLang,
                            'slug' => $item['cate_slug'],
                        ]);

                        $urlDetail = $this->Url->build([
                            'controller' => 'News',
                            'action' => 'detail',
                            'lang' => $slugLang,
                            'slug' => $item['cate_slug'],
                            'slug2' => $item['slug'],
                        ]);
                    }
                    ?>
                    <div class="col-lg-4">
                        <article class="relate-item">
                            <div class="img">
                                <a href="<?= $urlDetail ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a>
                            </div>
                            <div class="text">
                                <h4><?= $item['department_title'] ?></h4>
                                <h3 class="dotdotdot"><a href="<?= $urlDetail ?>"><?= $item['title'] ?></a></h3>
                                <a href="<?= $urlDetail ?>" class="desc dotdotdot"><?= $item['short_description'] ?></a>
                            </div>
                        </article><!-- /.relate-item -->
                    </div>
                <?php endforeach; ?>
            </div>
        </div><!-- /.news-related -->
    </div>
</section>
