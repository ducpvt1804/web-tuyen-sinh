<section class="section-news pt-md-60">
    <div class="container">
        <div class="section-heading">
            <?php $url =  $_SERVER['REQUEST_URI'];?>
            <?php if ($url == '/vi/thong-bao') { ?>
                <h2 class="section-title"><?= __('Thông Báo') ?></h2>
                <div class="section-title-sub"><?= __('Thông báo mới nhất của trường Đại học Đại Nam') ?></div>
            <?php } else if ($url == '/vi/tin-tuc') { ?>
                <h2 class="section-title"><?= __('Tin Tức') ?></h2>
                <div class="section-title-sub"><?= __('Tin tức mới nhất của trường Đại học Đại Nam') ?></div>
            <?php } else if ($url == '/vi/huong-nghiep') { ?>
                <h2 class="section-title"><?= __('Hướng Nghiệp') ?></h2>
                <div class="section-title-sub"><?= __('Thông tin và việc làm cho sinh viên') ?></div>
            <?php } else if ($url == '/vi/sinh-vien') { ?>
                <h2 class="section-title"><?= __('Sinh Viên') ?></h2>
                <div class="section-title-sub"><?= __('Tin tức, hình ảnh mới nhất luôn được cập nhật liên tục') ?></div>
            <?php } else { ?>
                <h2 class="section-title"><?= __('Bản Tin Đại Nam') ?></h2>
                <div class="section-title-sub"><?= __('Bản tin mới nhất của trường Đại học Đại Nam') ?></div>
            <?php } ?>
        </div>

        <div class="row list-news">
            <?php foreach ($dataNews as $item) :
                $urlCate = '';
                $urlDetail = '';
                $title = (isset($item['department_title']) && !empty(trim($item['department_title']))) ? $item['department_title'] : $item['cate_title'];
                if (!empty(trim($item['department_slug']))) {
                    $urlCate = $this->Url->build([
                        'controller' => 'Faculty',
                        'action' => 'list',
                        'lang' => $slugLang,
                        'slug_faculty' => $item['department_slug'],
                        'slug_faculty_list' => $item['cate_faculty_slug'],
                    ]);
                    $urlDetail = $this->Url->build([
                        'controller' => 'Faculty',
                        'action' => 'detail',
                        'lang' => $slugLang,
                        'slug_faculty' => $item['department_slug'],
                        'slug_faculty_list' => $item['cate_faculty_slug'],
                        'slug_faculty_detail' => $item['slug']
                    ]);
                } else {
                    $urlCate = $this->Url->build([
                        'controller' => 'News',
                        'action' => 'index',
                        'lang' => $slugLang,
                        'slug' => $item['cate_slug'],
                    ]);

                    $urlDetail = $this->Url->build([
                        'controller' => 'News',
                        'action' => 'detail',
                        'lang' => $slugLang,
                        'slug' => $item['cate_slug'],
                        'slug2' => $item['slug'],
                    ]);
                }
                ?>
                <div class="col-6 col-lg-3">
                    <article class="news-item">
                        <div class="img hover-scale"><a href="<?= $urlDetail ?>"><img src="<?= $this->Root->news(isset($item['img_highlight']) ? $item['img_highlight'] : '') ?>" alt="<?= $item['slug'] ?>"></a></div>
                        <div class="text">
                            <h3><a href="<?= $urlCate ?>"><?= __($title) ?></a></h3>
                            <a href="<?= $urlDetail ?>" class="desc dotdotdot">
                                <?= isset($item['title']) ? __($item['title']) : '' ?>
                            </a>
                        </div>
                    </article><!-- /.news-item -->
                </div>
            <?php endforeach; ?>
        </div>

        <div class="view-more text-center">
            <button class="btn btn-orange btn-md rounded-0 more-list" page="2"><i class="fa fa-spinner fa-pulse fa-lg fa-fw" style="display: none;"></i> <?= __('Xem thêm bài viết') ?></button>
        </div>
    </div>
</section>

<?php $this->start('script_footer'); ?>
<script>
    $(".more-list").click(function() {
        let _this = $(this);
        let page = _this.attr('page');
        if (page <= 0) {
            return;
        }
        $('.fa-pulse').show();
        let frm = new FormData();
        let slug = "<?= $slugNew ?>";
        frm.append('slug', slug);
        frm.append('page', page);
        $.ajax({
            headers: {
                'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
            },
            url: '<?= $this->Url->build(['controller' => 'News', 'action' => 'moreList', 'lang' => $slugLang]) ?>',
            type: 'post',
            data: frm,
            contentType: false,
            processData: false,
            cache: false
        }).done(function(rp) {
            if (rp.trim() == '') {
                _this.attr('page', 0);
                _this.text('Đã đến cuối trang');
                return;
            }
            if (rp) {
                _this.attr('page', (Number(page) + 1));
                $('.list-news').append(rp.trim());
            } else {
                alert("Đã có lỗi xảy ra. vui lòng thử lại.");
            }
        }).fail(function() {
            alert("Đã có lỗi xảy ra. vui lòng thử lại.");
        }).always(function() {
            $('.fa-pulse').hide();
        });
    });
</script>
<?php $this->end(); ?>
