<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title></title>
</head>

<body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <h2>Hi <?= isset($user['fullname']) ? $user['fullname'] : '' ?>!</h2>
    <p>Chúng tôi đã cung cấp cho bạn một url để thay đổi mật khẩu. Vui lòng truy cập <a href="<?= isset($user['_url']) ? $user['_url'] : '' ?>">tại đây</a> để tiếp tục.</p>
</body>

</html>