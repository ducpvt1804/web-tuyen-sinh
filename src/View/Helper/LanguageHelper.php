<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class LanguageHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        // 003-Language
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '003'])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        // 003-Language
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '003']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
