<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class DepartmentHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        // 002-department
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '002'])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        // 002-department
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '002']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
