<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class StatusUsersHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
    }

    /*--------------------------------------------------------------------*/
    public function first($value)
    {
        // 006 - users.flg_status
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '006'])
            ->first();
        $title = $result['title'];
        if ($result['value'] == 0) {
            return "<span class=\"btn btn-primary btn-sm mr-2\">$title</span>";
        } elseif ($result['title'] == 1) {
            return "<span class=\"btn btn-danger btn-sm delete\">$title</span>";
        } else {
            return $title;
        }
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        // 006 - users.flg_status
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '006']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
