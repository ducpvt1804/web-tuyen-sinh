<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\I18n\Time;
use Cake\I18n\Date;

class CommonHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
        $this->DetailBanner = TableRegistry::getTableLocator()->get('DetailBanner');
        $this->EnrollmentOptions = TableRegistry::getTableLocator()->get('EnrollmentOptions');
        $this->Libraries = TableRegistry::getTableLocator()->get('Libraries');
    }

    /*--------------------------------------------------------------------*/
    public function checkImg($path)
    {
        if (file_exists($path)) {
            return 2;
        } else {
            return 1;
        }
    }

    public function getForderRoot()
    {
        return WWW_ROOT;
    }

    /*--------------------------------------------------------------------*/
    public function getTitleLanguage($lag)
    {
        if ($lag == 0) {
            $html = '<img src="/img/system/flg-vi.jpg" alt="Tiếng Việ" style="width: 30px">';
            return $html;
        } elseif ($lag == 1) {
            $html = '<img src="/img/system/flg-en.png" alt="Tiếng Anh" style="width: 30px">';
            return $html;
        } else {
            return "";
        }
    }

    /*--------------------------------------------------------------------*/
    public function getStatusTitle($status)
    {
        if ($status == 0) {
            return '<span class="badge badge-primary">Hoạt động</span>';
        } elseif ($status == 1) {
            return '<span class="badge badge-danger">Không hoạt động</span>';
        } else {
            return "";
        }
    }

    /*--------------------------------------------------------------------*/
    public function countBanner($banner_id)
    {
        $result =  $this->DetailBanner->find('all')
            ->where(['flg_delete' => 0, 'banner_id' => $banner_id])
            ->count();
        return $result;
    }

    public function convertToInt($int)
    {
        return (int) $int;
    }

    public function getDepartMent($id, $type)
    {
        $result = $this->ListDepartments
            ->find('all')
            ->where(['flg_delete' => 0, 'type' => $type, 'id' => $id])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    public function getCateDepartment($id, $type)
    {
        $result = $this->ListDepartments
            ->find('all')
            ->where(['flg_delete' => 0, 'type' => $type, 'id' => $id])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    public function getStatusRead($status)
    {
        if ($status == 0) {
            return '<span class="btn btn-primary btn-sm mr-2">Chưa đọc</span>';
        } elseif ($status == 1) {
            return '<span class="btn btn-danger btn-sm delete">Đã đọc</span>';
        } else {
            return "";
        }
    }

    public function getStatusReply($status)
    {
        if ($status == 0) {
            return '<span class="badge badge-primary">Chưa trả lời</span>';
        } elseif ($status == 1) {
            return '<span class="badge badge-danger">Trả lời</span>';
        } else {
            return "";
        }
    }

    public function getBranchName($type, $row){
        if ($type == 1) {
            return $row['branch_name'];
        } elseif ($type == 2) {
            return $row['branch_name'];
        } else {
            return $row['branch_update_name'];
        }
    }

    public function getBranchTitle($branch_parent){
        $result = $this->EnrollmentOptions
            ->find('all')
            ->where(['flg_delete' => 0, 'id' => $branch_parent, 'flg_status' => 0])
            ->first();
        if(!empty($result)) {
            if ($result['type'] == 3) {
                return $result['branch_update_name'];
            } else {
                return $result['branch_name'];
            }
        }
    }

    public function getCategoriesRegister($type){
        if ($type == 1) {
            return 'Đại học chính quy';
        } elseif ($type == 2) {
            return 'Liên thông';
        } else {
            return 'Cập nhật kiến thức chuyên môn về dược';
        }
    }

    public function getTimeEvents($start_date, $end_date){
        $s_time = new Time($start_date);
        $s_time = $s_time->format('h:i a');
        $e_time = new Time($end_date);
        $e_time = $e_time->format('h:i a');
        $date = new Date($start_date);
        $date = $date->format('d-m-Y');
        return $s_time . ' - ' . $e_time . __(' ngày ') . $date;
    }

    public function time($time){
        $time = new Time($time);
        return $time = $time->format('h:i a');
    }

    public function date($date){
        $date = new Date($date);
        return $date = $date->format('d/m/Y');
    }

    public function getDayOfWeek($date){
        $date = new Date($date);
        $date = $date->format('l');
        if($date == "Monday"){
            $date = __("Thứ 2");
        }
        if($date == "Tuesday"){
            $date = __("Thứ 3");
        }
        if($date == "Wednesday"){
            $date = __("Thứ 4");
        }
        if($date == "Thursday"){
            $date = __("Thứ 5");
        }
        if($date == "Friday"){
            $date = __("Thứ 6");
        }
        if($date == "Saturday"){
            $date = __("Thứ 7");
        }
        if($date == "Sunday"){
            $date = __("Chủ nhật");
        }
        return $date;
    }

    public function getDateTimeNow(){
        $timenow = Time::now();
        $datenow = Date::now();
        $time = $timenow->format('h');
        $mtime = $timenow->format('i a');
        $day = $this->getDayOfWeek($datenow);
        $date = $datenow->format('d/m/Y');
        return $time . 'h'.  $mtime . ', ' . $day . ', '. __("ngày") .' '. $date;
    }

    public function getLibraryMedia($id)
    {
        $data = $this->Libraries
            ->find()
            ->select([
                'Libraries.id',
                'LibrariesMedia.id',
                'LibrariesMedia.path',
            ])
            ->join([
                'LibrariesMedia' => [
                    'table' => 'libraries_media',
                    'type' => 'INNER',
                    'conditions' => [
                        0 => 'LibrariesMedia.library_id = Libraries.id',
                        1 => 'LibrariesMedia.flg_delete = 0',
                    ],
                ],
            ])
            ->where(['Libraries.id' => $id, 'Libraries.flg_delete' => 0])
            ->first();
        return $data;
    }

    /*--------------------------------------------------------------------*/
    public function convertSlug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        $str = str_replace('---', '-', $str);
        return $str;
    }
}

