<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class StatusBannerHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        //008-banner.flg_status
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '008'])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        //008-banner.flg_status
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '008']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
