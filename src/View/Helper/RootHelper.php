<?php

namespace App\View\Helper;

use Cake\View\Helper;

class RootHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /*--------------------------------------------------------------------*/
    public function banner($img = '')
    {
        $path = "/admin/upload/banner/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function bannerRam($img = '')
    {
        $path = "/admin/upload/banner/ram/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function departments($img = '')
    {
        $path = "/admin/upload/departments/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function events($img = '')
    {
        $path = "/admin/upload/events/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function image($img = '')
    {
        $path = "/admin/upload/image/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function libraries($img = '')
    {
        $path = "/admin/upload/libraries/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function logo($img = '')
    {
        $path = "/admin/upload/logo/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function news($img = '')
    {
        $path = "/admin/upload/news/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function partner($img = '')
    {
        $path = "/admin/upload/partner/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function quote($img = '')
    {
        $path = "/admin/upload/quote/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function student_feel($img = '')
    {
        $path = "/admin/upload/student_feel/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function users($img = '')
    {
        $path = "/admin/upload/users/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }

    /*--------------------------------------------------------------------*/
    public function whychooseus($img = '')
    {
        $path = "/admin/upload/whychooseus/$img";
        if (empty($img) || !file_exists(WWW_ROOT . $path)) {
            return '/img/system/no-image.png';
        }
        return $path;
    }
}
