<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class ListDepartmentHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->ListDepartments = TableRegistry::getTableLocator()->get('ListDepartments');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        $result = $this->ListDepartments
            ->find('all')
            ->where(['flg_delete' => 0, 'id' => $value])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_status' => 0]);
        $result = $this->ListDepartments
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
