<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use AppConst;

class ModulesHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Modules = TableRegistry::getTableLocator()->get('Modules');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        $result = $this->Modules
            ->find('all')
            ->where(['flg_delete' => 0, 'id' => $value, 'flg_site' => AppConst::SITE_CONST])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'flg_site' => AppConst::SITE_CONST]);
        $result = $this->Modules
            ->find('all')
            ->where($conditions)
            ->order(['id' => 'ASC'])
            ->toArray();
        return $result;
    }
}
