<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class RoleHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
        $this->RoleDetail = TableRegistry::getTableLocator()->get('RoleDetail');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        //005 role
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '005'])
            ->first();
        return !empty($result) ? $result['title'] : "";
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        //005 role
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '005']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }

    /*--------------------------------------------------------------------*/
    public function getRole($user_id)
    {
        //005 role
        $result = $this->SelectItems
            ->find('all')
            ->select($this->SelectItems)
            ->select($this->RoleDetail)
            ->join([
                'RoleDetail' => [
                    'table' => 'role_detail',
                    'type' => 'INNER',
                    'conditions' => 'SelectItems.value = RoleDetail.role_id'
                ]
            ])
            ->where([
                'SelectItems.flg_delete' => 0,
                'RoleDetail.flg_delete' => 0,
                'RoleDetail.user_id' => $user_id,
                'SelectItems.group_code' => '005'
            ])
            ->first();
        return !empty($result) ? $result : "";
    }

    /*--------------------------------------------------------------------*/
    public function getArray($user_id)
    {
        //005 role
        $result = $this->SelectItems
            ->find('all')
            ->select($this->SelectItems)
            ->select($this->RoleDetail)
            ->join([
                'RoleDetail' => [
                    'table' => 'role_detail',
                    'type' => 'INNER',
                    'conditions' => 'SelectItems.value = RoleDetail.role_id'
                ]
            ])
            ->where([
                'SelectItems.flg_delete' => 0,
                'RoleDetail.flg_delete' => 0,
                'RoleDetail.user_id' => $user_id,
                'SelectItems.group_code' => '005'
            ])
            ->toArray();
        $lst['role_detail'] = [];
        foreach ($result as $row) {
            array_push($lst['role_detail'], $row['RoleDetail']['module_id']);
        }
        return $lst;
    }
}
