<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class StatusEventHelper extends Helper
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems');
    }

    /*--------------------------------------------------------------------*/
    public function getTitle($value)
    {
        // 004 - events.flg_status
        $result = $this->SelectItems
            ->find('all')
            ->where(['flg_delete' => 0, 'value' => $value, 'group_code' => '004'])
            ->first();
        $title = $result['title'];
        if ($value === 0) {
            return "<span class=\"btn btn-primary btn-sm mr-2\">$title</span>";
        }
        if ($value === 1) {
            return "<span class=\"btn btn-danger btn-sm delete\">$title</span>";
        }
        if ($value === 2) {
            return "<span class=\"btn btn-sm mr-2 btn-success\">$title</span>";
        } else {
            return $title;
        }
    }

    /*--------------------------------------------------------------------*/
    public function all($conditions = [])
    {
        // 004 - events.flg_status
        $conditions = array_merge($conditions, ['flg_delete' => 0, 'group_code' => '004']);
        $result = $this->SelectItems
            ->find('all')
            ->where($conditions)
            ->order(['title' => 'ASC'])
            ->toArray();
        return $result;
    }
}
