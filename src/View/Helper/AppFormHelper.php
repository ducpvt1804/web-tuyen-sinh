<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;

/**
 * Class AppFormHelper
 * @package App\View\Helper
 * @property \Cake\View\Helper\FormHelper Form
 */
class AppFormHelper extends Helper
{
    public $helpers = ['Form'];

    /**
     * @var string
     */
    const ERROR_HTML_CLASS = 'is-invalid';

    /**
     * @param null $context
     * @param array $options
     * @return string
     */
    public function create($context = null, array $options = []): string
    {
        // $options['url']['_ssl'] = !Configure::read('debug');
        $options['url']['_ssl'] = !Configure::read('debug');
        return $this->Form->create($context, $options);
    }

    /**
     * @return string
     */
    public function end(): string
    {
        return $this->Form->end();
    }

    /**
     * @param string $fieldName
     * @param array $options
     * @return string
     */
    public function hidden(string $fieldName, array $options = []): string
    {
        return $this->Form->hidden($fieldName, $options);
    }

    /**
     * @param string $fieldName
     * @param array $options
     * @param array|null $errors
     * @return string
     */
    public function text(string $fieldName, array $options = [], ?array $errors): string
    {
        $options['autocomplete'] = 'off';

        $error_msg = '';
        if (isset($errors[$fieldName])) {
            $options['class'][] = self::ERROR_HTML_CLASS;
            $error_msg = $this->inputErrorMsg($fieldName, $errors);
        }

        $append_html = '';
        if (isset($options['append_html']) && $options['append_html']) {
            $append_html = $this->getAppendHtml($options['append_html']);
        }

        return $this->Form->text($fieldName, $options) . $append_html . $error_msg;
    }

    /**
     * @param string $fieldName
     * @param array $options
     * @param array|null $errors
     * @return string
     */
    public function textarea(string $fieldName, array $options = [], ?array $errors): string
    {
        $error_msg = '';
        if (isset($errors[$fieldName])) {
            $options['class'][] = self::ERROR_HTML_CLASS;
            $error_msg = $this->inputErrorMsg($fieldName, $errors);
        }

        return $this->Form->textarea($fieldName, $options) . $error_msg;
    }

    /**
     * @param $fieldName
     * @param array $options
     * @param array $attributes
     * @param array|null $errors
     * @return string
     */
    public function select($fieldName, array $options, array $attributes = [], ?array $errors): string
    {
        $error_msg = '';
        if (isset($errors[$fieldName])) {
            $attributes['class'][] = self::ERROR_HTML_CLASS;
            $error_msg = $this->inputErrorMsg($fieldName, $errors);
        }

        return $this->Form->select($fieldName, $options, $attributes) . $error_msg;
    }

    /**
     * @param $fieldName
     * @param array $options
     * @param array $attributes
     * @param array|null $errors
     * @return string
     */
    public function radio($fieldName, array $options, array $attributes = [], ?array $errors): string
    {
        $error_msg = '';
        $html = '';
        $wrap_class = '';
        $attributes['hiddenField'] = false;
        $attributes['label'] = false;

        if (isset($attributes['wrap_class'])) {
            if (is_array($attributes['wrap_class'])) {
                $wrap_class = implode(' ', $attributes['wrap_class']);
            } else {
                $wrap_class = $attributes['wrap_class'];
            }
            unset($attributes['wrap_class']);
        }

        if (isset($errors[$fieldName])) {
            $attributes['class'][] = self::ERROR_HTML_CLASS;
            $error_msg = $this->inputErrorMsg($fieldName, $errors);
            if (isset($attributes['is_table_form'])) {
                $html .= '<div class="is-invalid">';
            } else {
                $html .= '<div class="form-control is-invalid">';
            }
        }

        foreach ($options as $key => $label) {
            $html .= '<div class="' . $wrap_class . '">';
            $html .= $this->Form->radio($fieldName, [$key => $label], $attributes);
            $html .= '<label for="' . $fieldName . '-' . $key . '">' . $label . '</label>';
            $html .= '</div>';
        }

        if (isset($errors[$fieldName])) {
            $html .= '</div>';
        }

        return $html . $error_msg;
    }

    /**
     * @param $fieldName
     * @param array $options
     * @param array $attributes
     * @param array|null $errors
     * @return string
     */
    public function checkbox($fieldName, array $options, array $attributes = [], ?array $errors): string
    {
        $error_msg = '';
        $html = '';
        $wrap_class = '';
        $attributes['hiddenField'] = false;

        $request_data = $this->getRequest()->getData($fieldName);

        if (isset($attributes['wrap_class'])) {
            if (is_array($attributes['wrap_class'])) {
                $wrap_class = implode(' ', $attributes['wrap_class']);
            } else {
                $wrap_class = $attributes['wrap_class'];
            }
            unset($attributes['wrap_class']);
        }

        if (isset($errors[$fieldName])) {
            $error_msg = $this->inputErrorMsg($fieldName, $errors);
            if (isset($attributes['is_no_label'])) {
                $html .= '<div class="is-invalid clearfix">';
            } else {
                $html .= '<div class="form-control is-invalid clearfix">';
            }
        }

        $is_group = false;
        if (count($options) > 1) {
            $is_group = true;
        }

        $switch_class = 'switch-success';
        if (isset($attributes['switch_class'])) {
            $switch_class = $attributes['switch_class'];
        }

        foreach ($options as $key => $label) {
            $attributes['id'] = $is_group ? $fieldName . $key : $fieldName;
            $attributes['value'] = $key;

            if ($is_group) {
                $attributes['checked'] = (bool)($attributes['checked_list'][$key] ?? false);
            }

            if ($is_group && is_array($request_data) && array_search($key, $request_data) !== false) {
                $attributes['checked'] = true;
            } elseif (!$is_group && !is_null($request_data)) {
                $attributes['checked'] = true;
            }

            $html .= '<div class="' . $wrap_class . '">';
            $html .= '<label class="switch switch-label switch-fl ' . $switch_class . '">';
            $html .= $this->Form->checkbox($is_group ? $fieldName . '[]' : $fieldName, $attributes);
            $html .= '<span class="switch-slider" data-checked="?" data-unchecked="?"></span></label>';
            if ($label) {
                $html .= $this->Form->label($is_group ? $fieldName . $key : $fieldName, $label, ['class' => ['switch-input-label']]);
            }
            $html .= '</div>';
        }

        if (isset($errors[$fieldName])) {
            $html .= '</div>';
        }

        return $html . $error_msg;
    }

    /**
     * @param string $name
     * @param array|null $errors
     * @return string
     */
    public function inputErrorMsg(string $name, ?array $errors): string
    {
        $msg = '';
        if ($errors === null) {
            return $msg;
        }
        if (isset($errors[$name])) {
            $msg .= '<div class="invalid-feedback">';
            if (is_array($errors[$name])) {
                $msg .= current($errors[$name]);
            } else {
                $msg .= $errors[$name];
            }
            $msg .= '</div>';
        }
        return $msg;
    }

    /**
     * @param string $html
     * @return string
     */
    private function getAppendHtml(string $html): string
    {
        return '<div class="input-group-append"><span class="input-group-text">' . $html . '</span></div>';
    }

}
