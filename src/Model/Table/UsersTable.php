<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

/**
 * Users Model
 *
 * @property \App\Model\Table\CateDepartmentsTable&\Cake\ORM\Association\HasMany $CateDepartments
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\HasMany $Categories
 * @property \App\Model\Table\ChooseUsTable&\Cake\ORM\Association\HasMany $ChooseUs
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\HasMany $Departments
 * @property \App\Model\Table\EventsTable&\Cake\ORM\Association\HasMany $Events
 * @property \App\Model\Table\ListDepartmentsTable&\Cake\ORM\Association\HasMany $ListDepartments
 * @property \App\Model\Table\NewsTable&\Cake\ORM\Association\HasMany $News
 * @property \App\Model\Table\PartnersTable&\Cake\ORM\Association\HasMany $Partners
 * @property \App\Model\Table\QuotesTable&\Cake\ORM\Association\HasMany $Quotes
 * @property \App\Model\Table\RoleDetailTable&\Cake\ORM\Association\HasMany $RoleDetail
 * @property \App\Model\Table\StudentCommentsTable&\Cake\ORM\Association\HasMany $StudentComments
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('CateDepartments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Categories', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ChooseUs', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Departments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Events', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ListDepartments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('News', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Partners', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Quotes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('RoleDetail', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('StudentComments', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('user_name')
            ->maxLength('user_name', 50)
            ->allowEmptyString('user_name');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 50)
            ->allowEmptyString('password');

        $validator
            ->scalar('fullname')
            ->maxLength('fullname', 50)
            ->allowEmptyString('fullname');

        $validator
            ->scalar('sex')
            ->maxLength('sex', 3)
            ->allowEmptyString('sex');

        $validator
            ->scalar('telno')
            ->maxLength('telno', 20)
            ->allowEmptyString('telno');

        $validator
            ->scalar('address')
            ->maxLength('address', 250)
            ->allowEmptyString('address');

        $validator
            ->scalar('avarta')
            ->maxLength('avarta', 250)
            ->allowEmptyString('avarta');

        $validator
            ->date('birth_date')
            ->allowEmptyDate('birth_date');

        $validator
            ->allowEmptyString('flg_site');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->integer('flg_status')
            ->allowEmptyString('flg_status');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

    public function validationUsers(Validator $validator)
    {
        $validator
            ->maxLength('user_name', 50, 'Tên đăng nhập không được vượt quá 50 ký tự.')
            ->add('user_name', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Tên đăng nhập đã tồn tại. Vui lòng nhập tên khác.'
                ]
            ]);

        $validator
            ->maxLength('password', 50, 'Mật khẩu không được vượt quá 50 ký tự.', $validator->allowEmptyString('password', '', true));

        $validator
            ->equalToField('password_confirm', 'password', 'Mật khẩu xác phải trùng với mật khẩu.', $validator->allowEmptyString('password_confirm', '', true));

        $validator
            ->maxLength('fullname', 50, 'Họ và tên không được vượt quá 50 ký tự.')
            ->allowEmptyString('fullname', 'Họ và tên không được để trống.', false);

        $validator
            ->lessThan('birth_date', date('Y-m-d', strtotime('-17 year', strtotime(date('Y-m-d')))), "Ngày sinh không hợp lệ.")
            ->allowEmptyString('birth_date', 'Ngày sinh phải được chọn.', false);

        $validator
            ->maxLength('sex', 3)
            ->allowEmptyString('sex', 'Giới tính phải được chọn.', false);

        $validator
            ->maxLength('telno', 20, 'Số điện thoại không được vượt quá 20 ký tự.')
            ->allowEmptyString('telno', 'Số điện thoại không được để trống.', false);

        $validator
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email không hợp lệ.'
            ])
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Email đã tồn tại. Vui lòng nhập tên khác.'
                ]
            ]);

        $validator
            ->maxLength('address', 250, 'Địa chỉ không được vượt quá 250 ký tự.')
            ->allowEmptyString('address', 'Địa chỉ không được để trống.', false);

        $validator
            ->add('avarta', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $validator->allowEmpty('avarta', true));

        $validator
            ->allowEmptyString('flg_status', 'Trạng thái phải được chọn.', false);

        $validator
            ->allowEmptyString('role_id', 'Quyền hạn phải được chọn.', false);

        return $validator;
    }

    public function validationAddUsers(Validator $validator)
    {
        $this->validationUsers($validator);
        $validator
            ->allowEmptyString('user_name', 'Tên đăng nhập không được để trống.', false);

        $validator
            ->allowEmptyString('password', 'Mật khẩu không được để trống.', false);

        $validator
            ->allowEmpty('avarta', false, 'Ảnh đại diện yêu cầu phải chọn.');

        return $validator;
    }
}
