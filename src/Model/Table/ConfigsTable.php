<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Configs Model
 *
 * @property \App\Model\Table\SeosTable&\Cake\ORM\Association\BelongsTo $Seos
 *
 * @method \App\Model\Entity\Config get($primaryKey, $options = [])
 * @method \App\Model\Entity\Config newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Config[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Config|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Config saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Config patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Config[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Config findOrCreate($search, callable $callback = null, $options = [])
 */
class ConfigsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('configs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('seo', [
            'foreignKey' => 'seo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->allowEmptyString('flg_site');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('logo')
            ->maxLength('logo', 250)
            ->allowEmptyString('logo');

        $validator
            ->scalar('address_1')
            ->maxLength('address_1', 250)
            ->allowEmptyString('address_1');

        $validator
            ->scalar('address_2')
            ->maxLength('address_2', 250)
            ->allowEmptyString('address_2');

        $validator
            ->scalar('telno')
            ->maxLength('telno', 20)
            ->allowEmptyString('telno');

        $validator
            ->scalar('fax')
            ->maxLength('fax', 20)
            ->allowEmptyString('fax');

        $validator
            ->scalar('map')
            ->maxLength('map', 500)
            ->allowEmptyString('map');

        $validator
            ->scalar('facebook_address')
            ->maxLength('facebook_address', 100)
            ->allowEmptyString('facebook_address');

        $validator
            ->scalar('twitter_address')
            ->maxLength('twitter_address', 100)
            ->allowEmptyString('twitter_address');

        $validator
            ->scalar('pinterest_address')
            ->maxLength('pinterest_address', 100)
            ->allowEmptyString('pinterest_address');

        $validator
            ->allowEmptyString('flg_language');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['seo_id'], 'seo'));

        return $rules;
    }
}
