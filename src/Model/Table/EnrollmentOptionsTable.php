<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EnrollmentOptions Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\EnrollmentOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\EnrollmentOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EnrollmentOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentOption|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnrollmentOption saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnrollmentOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentOption findOrCreate($search, callable $callback = null, $options = [])
 */
class EnrollmentOptionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('enrollment_options');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('type')
            ->allowEmptyString('type');

        $validator
            ->scalar('branch_name')
            ->maxLength('branch_name', 100)
            ->allowEmptyString('branch_name');

        $validator
            ->scalar('branch_code')
            ->maxLength('branch_code', 10)
            ->allowEmptyString('branch_code');

        $validator
            ->scalar('combination_code')
            ->maxLength('combination_code', 10)
            ->allowEmptyString('combination_code');

        $validator
            ->integer('branch_parent')
            ->allowEmptyString('branch_parent');

        $validator
            ->scalar('branch_update_name')
            ->maxLength('branch_update_name', 100)
            ->allowEmptyString('branch_update_name');

        $validator
            ->allowEmptyString('flg_status');

        $validator
            ->allowEmptyString('flg_language');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
