<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CateDepartments Model
 *
 * @property \App\Model\Table\SeosTable&\Cake\ORM\Association\BelongsTo $Seos
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 *
 * @method \App\Model\Entity\CateDepartment get($primaryKey, $options = [])
 * @method \App\Model\Entity\CateDepartment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CateDepartment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CateDepartment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CateDepartment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CateDepartment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CateDepartment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CateDepartment findOrCreate($search, callable $callback = null, $options = [])
 */
class CateDepartmentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cate_departments');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('seo', [
            'foreignKey' => 'seo_id'
        ]);
        $this->belongsTo('departments', [
            'foreignKey' => 'department_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 250)
            ->allowEmptyString('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 350)
            ->allowEmptyString('slug');

        $validator
            ->allowEmptyString('flg_showheader');

        $validator
            ->allowEmptyString('flg_status');

        $validator
            ->allowEmptyString('flg_language');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }
}
