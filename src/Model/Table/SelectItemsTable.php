<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SelectItems Model
 *
 * @method \App\Model\Entity\SelectItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\SelectItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SelectItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SelectItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SelectItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SelectItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SelectItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SelectItem findOrCreate($search, callable $callback = null, $options = [])
 */
class SelectItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('select_items');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('group_code')
            ->maxLength('group_code', 4)
            ->allowEmptyString('group_code');

        $validator
            ->scalar('value')
            ->maxLength('value', 50)
            ->allowEmptyString('value');

        $validator
            ->scalar('title')
            ->maxLength('title', 50)
            ->allowEmptyString('title');

        $validator
            ->allowEmptyString('flg_site');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 100)
            ->allowEmptyString('comment');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->boolean('flg_delete')
            ->allowEmptyString('flg_delete');

        return $validator;
    }
}
