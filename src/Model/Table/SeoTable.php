<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Seo Model
 *
 * @property \App\Model\Table\CateDepartmentsTable&\Cake\ORM\Association\HasMany $CateDepartments
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\HasMany $Categories
 * @property \App\Model\Table\ConfigsTable&\Cake\ORM\Association\HasMany $Configs
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\HasMany $Departments
 * @property \App\Model\Table\EventsTable&\Cake\ORM\Association\HasMany $Events
 * @property \App\Model\Table\LibrariesTable&\Cake\ORM\Association\HasMany $Libraries
 * @property \App\Model\Table\NewsTable&\Cake\ORM\Association\HasMany $News
 *
 * @method \App\Model\Entity\Seo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Seo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Seo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Seo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Seo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Seo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Seo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Seo findOrCreate($search, callable $callback = null, $options = [])
 */
class SeoTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('seo');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('CateDepartments', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('Categories', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('Configs', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('Departments', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('Events', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('Libraries', [
            'foreignKey' => 'seo_id'
        ]);
        $this->hasMany('News', [
            'foreignKey' => 'seo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('meta_title')
            ->maxLength('meta_title', 100)
            ->allowEmptyString('meta_title');

        $validator
            ->scalar('meta_description')
            ->maxLength('meta_description', 250)
            ->allowEmptyString('meta_description');

        $validator
            ->scalar('meta_keywords')
            ->maxLength('meta_keywords', 250)
            ->allowEmptyString('meta_keywords');

        $validator
            ->scalar('tags')
            ->maxLength('tags', 250)
            ->allowEmptyString('tags');

        $validator
            ->allowEmptyString('flg_language');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

    public function validationEventVsSeo(Validator $validator)
    {
        $validator
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $validator->allowEmpty('img_highlight', 'create', 'Ảnh đại diện yêu cầu phải chọn.', true)
            ]);

        $validator
            ->requirePresence('title', 'create', 'Tên sự kiện không được để trống.')
            ->scalar('title')
            ->maxLength('title', 250, 'Tên sự kiện không được vượt quá 250 ký tự.')
            ->allowEmptyString('title', 'Tên sự kiện không được để trống.', false);

        $validator
            ->requirePresence('flg_department', 'Khoa yêu cầu phải chọn.')
            ->scalar('flg_department')
            ->maxLength('flg_department', 150)
            ->allowEmptyString('flg_department', 'Khoa yêu cầu phải chọn.', false);

        $validator
            ->requirePresence('title', 'create', 'Tên sự kiện không được để trống.')
            ->scalar('title')
            ->maxLength('title', 250, 'Tên sự kiện không được vượt quá 250 ký tự.')
            ->allowEmptyString('title', 'Tên sự kiện không được để trống.', false);

        $validator
            ->requirePresence('address', 'create', 'Địa chỉ không được để trống.')
            ->scalar('address')
            ->maxLength('address', 250, 'Tên sự kiện không được vượt quá 250 ký tự.')
            ->allowEmptyString('address', 'Địa chỉ không được để trống.', false);

        $validator
            ->requirePresence('flg_status', 'create', 'Trạng thái sự kiện yêu cầu phải chọn.')
            ->scalar('flg_status')
            ->maxLength('flg_status', 3)
            ->allowEmptyString('flg_status', 'Trạng thái sự kiện yêu cầu phải chọn.', false);

        $validator
            ->requirePresence('start_date', 'create', 'Thời gian bắt đầu sự kiện là bắt buộc.')
            ->dateTime('start_date', ['ymd', 'hi'], "Ngày bắt đầu không hợp lệ.")
            ->allowEmptyDateTime('start_date', 'Thời gian bắt đầu sự kiện là bắt buộc.', false);

        $validator
            ->requirePresence('end_date', 'create', 'Thời gian kết thúc sự kiện là bắt buộc.')
            ->dateTime('end_date', ['ymd', 'hi'], 'Ngày kết thúc không hợp lệ.')
            ->greaterThanOrEqualToField('end_date', 'start_date', 'Thời gian kết thúc phải sau thời gian bắt đầu.')
            ->allowEmptyDateTime('end_date', 'Thời gian kết thúc sự kiện là bắt buộc.', false);

        $validator
            ->requirePresence('flg_language', 'create', 'Ngôn ngữ là bắt buộc.')
            ->scalar('flg_language')
            ->maxLength('flg_language', 3)
            ->allowEmptyString('flg_language', 'Ngôn ngữ là bắt buộc.', false);

        $validator
            ->requirePresence('map', 'create', 'Link địa chỉ map không được để trống.')
            ->scalar('map')
            ->maxLength('map', 500, 'Link địa chỉ map không được vượt quá 500 ký tự.')
            ->allowEmptyString('map', 'Link địa chỉ map không được để trống.', false);

        $validator
            ->requirePresence('telno', 'create', 'Số điện thoại không được để trống.')
            ->scalar('telno')
            ->maxLength('telno', 20, 'Số điện thoại không được vượt quá 20 ký tự.')
            ->allowEmptyString('telno', 'Số điện thoại không được để trống.', false);

        $validator
            ->requirePresence('position', 'create')
            ->scalar('position')
            ->maxLength('position', 50)
            ->allowEmptyString('position', 'Trạng thái sự kiện phải chọn.', false);

        $validator
            ->requirePresence('cost', 'create', 'Phí tham gia sự kiện không được để trống.')
            ->scalar('cost')
            ->maxLength('cost', 50, 'Phí tham gia sự kiện không được vượt quá 50 ký tự.')
            ->allowEmptyString('cost', 'Phí tham gia sự kiện không được để trống.', false);

        $validator
            ->requirePresence('content', 'create', 'Chi tiết sự kiện không được để trống.')
            ->scalar('content')
            ->allowEmptyString('content', 'Chi tiết sự kiện không được để trống.', false);

        $validator
            ->scalar('meta_title')
            ->maxLength('meta_title', 100, 'Meta title không được vượt quá 100 ký tự.')
            ->allowEmptyString('meta_title', 'Meta title không được để trống.', false);

        $validator
            ->scalar('meta_description')
            ->maxLength('meta_description', 250, 'Meta description không được vượt quá 250 ký tự.')
            ->allowEmptyString('meta_description', 'Meta description không được để trống.', false);

        $validator
            ->scalar('meta_keywords')
            ->maxLength('meta_keywords', 250, 'Meta keywords không được vượt quá 250 ký tự.')
            ->allowEmptyString('meta_keywords', 'Meta keywords không được để trống.', false);

        $validator
            ->scalar('tags')
            ->maxLength('tags', 250, 'Tags không được vượt quá 250 ký tự.')
            ->allowEmptyString('tags', 'Tags không được để trống.', false);

        return $validator;
    }

    public function validationAddEventVsSeo(Validator $validator)
    {

        $this->validationEventVsSeo($validator);
        $validator
            ->allowEmpty('img_highlight', 'Ảnh đại diện yêu cầu phải chọn.', false);

        return $validator;
    }
}
