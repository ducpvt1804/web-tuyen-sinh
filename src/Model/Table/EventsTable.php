<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Events Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SeosTable&\Cake\ORM\Association\BelongsTo $Seos
 *
 * @method \App\Model\Entity\Event get($primaryKey, $options = [])
 * @method \App\Model\Entity\Event newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Event[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Event|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Event saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Event patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Event[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Event findOrCreate($search, callable $callback = null, $options = [])
 */
class EventsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('events');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Seos', [
            'foreignKey' => 'seo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('flg_department', 'Khoa yêu cầu phải chọn.')
            ->scalar('flg_department')
            ->maxLength('flg_department', 150)
            ->allowEmptyString('flg_department', 'Khoa yêu cầu phải chọn.', false);

        $validator
            ->requirePresence('title', 'create', 'Tên sự kiện không được để trống.')
            ->scalar('title')
            ->maxLength('title', 250, 'Tên sự kiện không được vượt quá 250 ký tự.')
            ->allowEmptyString('title', 'Tên sự kiện không được để trống.', false);

        $validator
            ->requirePresence('img_highlight', 'create', 'Ảnh đại diện yêu cầu phải chọn.')
            ->scalar('img_highlight')
            ->maxLength('img_highlight', 250)
            ->allowEmptyString('img_highlight', 'Ảnh đại diện yêu cầu phải chọn.', false);

        $validator
            ->requirePresence('address', 'create', 'Địa chỉ không được để trống.')
            ->scalar('address')
            ->maxLength('address', 250, 'Tên sự kiện không được vượt quá 250 ký tự.')
            ->allowEmptyString('address', 'Địa chỉ không được để trống.', false);

        $validator
            ->requirePresence('flg_status', 'create', 'Trạng thái sự kiện yêu cầu phải chọn.')
            ->scalar('flg_status')
            ->maxLength('flg_status', 3)
            ->allowEmptyString('flg_status', 'Trạng thái sự kiện yêu cầu phải chọn.', false);

        $validator
            ->requirePresence('start_date', 'create', 'Thời gian bắt đầu sự kiện là bắt buộc.')
            ->dateTime('start_date')
            ->allowEmptyDateTime('start_date', 'Thời gian bắt đầu sự kiện là bắt buộc.', false);

        $validator
            ->requirePresence('end_date', 'create', 'Thời gian kết thúc sự kiện là bắt buộc.')
            ->dateTime('end_date')
            ->allowEmptyDateTime('end_date', 'Thời gian kết thúc sự kiện là bắt buộc.', false);

        $validator
            ->requirePresence('flg_language', 'create', 'Ngôn ngữ là bắt buộc.')
            ->scalar('flg_language')
            ->maxLength('flg_language', 3)
            ->allowEmptyString('flg_language', 'Ngôn ngữ là bắt buộc.', false);

        $validator
            ->requirePresence('map', 'create', 'Link địa chỉ map không được để trống.')
            ->scalar('map')
            ->maxLength('map', 500, 'Link địa chỉ map không được vượt quá 500 ký tự.')
            ->allowEmptyString('map', 'Link địa chỉ map không được để trống.', false);

        $validator
            ->requirePresence('telno', 'create', 'Số điện thoại không được để trống.')
            ->scalar('telno')
            ->maxLength('telno', 20, 'Số điện thoại không được vượt quá 20 ký tự.')
            ->allowEmptyString('telno', 'Số điện thoại không được để trống.', false);

        $validator
            ->requirePresence('position', 'create')
            ->scalar('position')
            ->maxLength('position', 50)
            ->allowEmptyString('position', 'Trạng thái sự kiện phải chọn.', false);

        $validator
            ->requirePresence('cost', 'create', 'Phí tham gia sự kiện không được để trống.')
            ->scalar('cost')
            ->maxLength('cost', 50, 'Phí tham gia sự kiện không được vượt quá 50 ký tự.')
            ->allowEmptyString('cost', 'Phí tham gia sự kiện không được để trống.', false);

        $validator
            ->requirePresence('content', 'create', 'Chi tiết sự kiện không được để trống.')
            ->scalar('content')
            ->allowEmptyString('content', 'Chi tiết sự kiện không được để trống.', false);

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }
}
