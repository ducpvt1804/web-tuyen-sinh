<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EnrollmentRegistrations Model
 *
 * @method \App\Model\Entity\EnrollmentRegistration get($primaryKey, $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EnrollmentRegistration findOrCreate($search, callable $callback = null, $options = [])
 */
class EnrollmentRegistrationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('enrollment_registrations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('cate')
            ->allowEmptyString('cate');

        $validator
            ->scalar('fullname')
            ->maxLength('fullname', 50)
            ->allowEmptyString('fullname');

        $validator
            ->date('dob')
            ->allowEmptyDate('dob');

        $validator
            ->scalar('telno')
            ->maxLength('telno', 20)
            ->allowEmptyString('telno');

        $validator
            ->integer('sex')
            ->allowEmptyString('sex');

        $validator
            ->scalar('address')
            ->maxLength('address', 250)
            ->allowEmptyString('address');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->integer('admission')
            ->allowEmptyString('admission');

        $validator
            ->integer('sub_admission')
            ->allowEmptyString('sub_admission');

        $validator
            ->allowEmptyString('flg_status');

        $validator
            ->allowEmptyString('type');

        $validator
            ->allowEmptyString('flg_read');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
