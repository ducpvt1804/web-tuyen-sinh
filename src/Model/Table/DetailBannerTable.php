<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DetailBanner Model
 *
 * @property \App\Model\Table\BannersTable&\Cake\ORM\Association\BelongsTo $Banners
 *
 * @method \App\Model\Entity\DetailBanner get($primaryKey, $options = [])
 * @method \App\Model\Entity\DetailBanner newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DetailBanner[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DetailBanner|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailBanner saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailBanner patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DetailBanner[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DetailBanner findOrCreate($search, callable $callback = null, $options = [])
 */
class DetailBannerTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('detail_banner');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Banners', [
            'foreignKey' => 'banner_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('img')
            ->maxLength('img', 150)
            ->allowEmptyString('img');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $validator
            ->allowEmptyString('flg_delete');

        return $validator;
    }

}
