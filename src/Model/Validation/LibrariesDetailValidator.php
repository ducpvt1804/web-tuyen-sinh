<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LibrariesDetailValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('name_library', 'Dữ liệu không hợp lệ.')
            ->maxLength('name_library', 255, 'Vui lòng không nhập quá 255 ký tự.')
            ->requirePresence('name_library', true, 'Vui lòng nhập thư viện ảnh/video.')
            ->allowEmptyString('name_library', false, "Vui lòng nhập thư viện ảnh/video.");
    }
}
