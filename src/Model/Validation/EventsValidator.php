<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class EventsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $this->allowEmpty('img_highlight', true)
            ]);

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập tên sự kiện.')
            ->maxLength('title', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập tên sự kiện.', false);

        $this
            ->scalar('mail', 'Dữ liệu không hợp lệ.')
            ->requirePresence('mail', true, 'Vui lòng nhập mail ban tổ chức.')
            ->maxLength('mail', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->add('mail', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email không hợp lệ.'
            ])
            ->allowEmptyString('mail', 'Vui lòng nhập mail ban tổ chức.', false);

        $this
            ->scalar('flg_department', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_department', true, 'Vui lòng chọn khoa.')
            ->allowEmptyString('flg_department', 'Vui lòng chọn khoa.', false);

        $this
            ->scalar('address', 'Dữ liệu không hợp lệ.')
            ->requirePresence('address', true, 'Vui lòng nhập địa chỉ.')
            ->maxLength('address', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('address', 'Vui lòng nhập địa chỉ.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('start_date', 'Dữ liệu không hợp lệ.')
            ->requirePresence('start_date', true, 'Vui lòng nhập ngày.')
            ->dateTime('start_date', ['ymd', 'hi'], "Ngày không hợp lệ.")
            ->allowEmptyDateTime('start_date', 'Vui lòng nhập ngày.', false);

        $this
            ->scalar('end_date', 'Dữ liệu không hợp lệ.')
            ->requirePresence('end_date', true, 'Vui lòng nhập ngày.')
            ->dateTime('end_date', ['ymd', 'hi'], 'Ngày không hợp lệ.')
            ->greaterThanOrEqualToField('end_date', 'start_date', 'Thời gian kết thúc phải sau thời gian bắt đầu.')
            ->allowEmptyDateTime('end_date', 'Vui lòng nhập ngày.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('map', 'Dữ liệu không hợp lệ.')
            ->requirePresence('map', true, 'Vui lòng nhập link địa chỉ map.')
            ->maxLength('map', 500, 'Vui lòng không nhập quá 500 ký tự.')
            ->allowEmptyString('map', 'Vui lòng nhập link địa chỉ map.', false);

        $this
            ->scalar('telno', 'Dữ liệu không hợp lệ.')
            ->requirePresence('telno', true, 'Vui lòng nhập tên số điện thoại.')
            ->maxLength('telno', 20, 'Vui lòng không nhập quá 20 ký tự.')
            ->allowEmptyString('telno', 'Vui lòng nhập tên số điện thoại.', false);

        $this
            ->scalar('position', 'Dữ liệu không hợp lệ.')
            ->requirePresence('position', true, 'Vui lòng nhập đối tượng có thể tham gia.')
            ->maxLength('position', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('position', 'Vui lòng nhập đối tượng có thể tham gia.', false);

        $this
            ->scalar('cost', 'Dữ liệu không hợp lệ.')
            ->requirePresence('cost', true, 'Vui lòng nhập phí tham gia.')
            ->maxLength('cost', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('cost', 'Vui lòng nhập phí tham gia.', false);

        $this
            ->scalar('content', 'Dữ liệu không hợp lệ.')
            ->requirePresence('content', true, 'Vui lòng nhập chi tiết sự kiện.')
            ->allowEmptyString('content', 'Vui lòng nhập chi tiết sự kiện.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');
        return $this->errors($data);
    }
}
