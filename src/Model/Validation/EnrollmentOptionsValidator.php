<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class EnrollmentOptionsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('type', 'Dữ liệu không hợp lệ.')
            ->requirePresence('type', true, 'Vui lòng chọn danh mục.')
            ->allowEmptyString('type', 'Vui lòng chọn danh mục.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        // $this
        //     ->scalar('branch_parent', 'Dữ liệu không hợp lệ.')
        //     ->requirePresence('branch_parent', !$this->equals('type', 1), 'Vui lòng chọn ngành.')
        //     ->allowEmptyString('branch_parent', 'Vui lòng chọn ngành.', !$this->equals('type', 1));

        // $this
        //     ->scalar('branch_name', 'Dữ liệu không hợp lệ.')
        //     ->maxLength('branch_name', 100, 'Vui lòng không nhập quá 100 ký tự.')
        //     ->requirePresence('branch_name', !$this->equals('type', 1), 'Vui lòng nhập tiêu đề.')
        //     ->allowEmptyString('branch_name', 'Vui lòng nhập tiêu đề.', !$this->equals('type', 1));

        // $this
        //     ->scalar('branch_code', 'Dữ liệu không hợp lệ.')
        //     ->maxLength('branch_code', 10, 'Vui lòng không nhập quá 10 ký tự.')
        //     ->requirePresence('branch_code', !$this->equals('type', 1), 'Vui lòng nhập mã ngành.')
        //     ->allowEmptyString('branch_code', 'Vui lòng nhập mã ngành.', !$this->equals('type', 1));

        // $this
        //     ->scalar('combination_code', 'Dữ liệu không hợp lệ.')
        //     ->maxLength('combination_code', 10, 'Vui lòng không nhập quá 10 ký tự.')
        //     ->requirePresence('combination_code', !$this->equals('type', 1), 'Vui lòng nhập mã hệ liên thông.')
        //     ->allowEmptyString('combination_code', 'Vui lòng nhập mã hệ liên thông.', !$this->equals('type', 1));

        // $this
        //     ->scalar('branch_update_name', 'Dữ liệu không hợp lệ.')
        //     ->maxLength('branch_update_name', 100, 'Vui lòng không nhập quá 100 ký tự.')
        //     ->requirePresence('branch_update_name', !$this->equals('type', 3), 'Vui lòng chọn nghành nghề đăng kí cập nhật.')
        //     ->allowEmptyString('branch_update_name', 'Vui lòng chọn nghành nghề đăng kí cập nhật.', !$this->equals('type', 3));
    }
}
