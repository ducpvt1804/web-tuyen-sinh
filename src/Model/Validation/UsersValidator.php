<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class UsersValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $userTable = TableRegistry::getTableLocator()->get('Users');
        $this->provider('table', $userTable);

        $this
            ->scalar('user_name', 'Dữ liệu không hợp lệ.')
            ->maxLength('user_name', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->add('user_name', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Vui lòng nhập tên khác, tên tài khoản này đã tồn tại.'
                ]
            ]);

        $this
            ->scalar('password', 'Dữ liệu không hợp lệ.')
            ->maxLength('password', 50, 'Vui lòng không nhập quá 50 ký tự.', $this->allowEmptyString('password', '', true));

        $this
            ->scalar('password_confirm', 'Dữ liệu không hợp lệ.')
            ->equalToField('password_confirm', 'password', 'Vui lòng nhập mật khẩu xác phải trùng với mật khẩu.', $this->allowEmptyString('password_confirm', '', true));

        $this
            ->scalar('fullname', 'Dữ liệu không hợp lệ.')
            ->requirePresence('fullname', true, 'Vui lòng chọn họ tên.')
            ->maxLength('fullname', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('fullname', 'Vui lòng nhập họ tên.', false);

        $this
            ->scalar('birth_date', 'Dữ liệu không hợp lệ.')
            ->requirePresence('birth_date', true, 'Vui lòng chọn ngày sinh.')
            ->lessThan('birth_date', date('Y-m-d', strtotime('-17 year', strtotime(date('Y-m-d')))), "Ngày sinh không hợp lệ.")
            ->allowEmptyString('birth_date', 'Vui lòng chọn ngày sinh.', false);

        $this
            ->scalar('sex', 'Dữ liệu không hợp lệ.')
            ->requirePresence('sex', true, 'Vui lòng chọn giới tính.')
            ->allowEmptyString('sex', 'Vui lòng chọn giới tính.', false);

        $this
            ->scalar('telno', 'Dữ liệu không hợp lệ.')
            ->requirePresence('telno', true, 'Vui lòng nhập số điện thoại.')
            ->maxLength('telno', 20, 'Vui lòng không nhập quá 20 ký tự.')
            ->allowEmptyString('telno', 'Vui lòng nhập số điện thoại.', false);

        $this
            ->scalar('email', 'Dữ liệu không hợp lệ.')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email không hợp lệ.'
            ])
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Vui lòng nhập tên email khác, tên này đã tồn tại.'
                ]
            ]);

        $this
            ->scalar('address', 'Dữ liệu không hợp lệ.')
            ->requirePresence('address', true, 'Vui lòng nhập địa chỉ.')
            ->maxLength('address', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('address', 'Vui lòng nhập địa chỉ.', false);

        $this
            ->add('avarta', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('avarta', true));

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('role_id', 'Dữ liệu không hợp lệ.')
            ->requirePresence('role_id', true, 'Vui lòng chọn quyền hạn.')
            ->allowEmptyString('role_id', 'Vui lòng chọn quyền hạn.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('user_name', true, 'Vui lòng nhập tên tài khoản.')
            ->allowEmptyString('user_name', 'Vui lòng nhập tên tài khoản.', false);

        $this
            ->requirePresence('password', true, 'Vui lòng nhập mật khẩu.')
            ->allowEmptyString('password', 'Vui lòng nhập mật khẩu.', false);

        $this
            ->requirePresence('password_confirm', true, 'Vui lòng nhập mật khẩu xác nhận.')
            ->allowEmptyString('password_confirm', 'Vui lòng nhập mật khẩu xác nhận.', false);

        $this
            ->requirePresence('avarta', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('avarta', false, 'Vui lòng chọn ảnh.');
        return $this->errors($data);
    }
}
