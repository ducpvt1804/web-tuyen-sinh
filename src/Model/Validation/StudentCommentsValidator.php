<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class StudentCommentsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->scalar('department_id', 'Dữ liệu không hợp lệ.')
            ->requirePresence('department_id', true, 'Vui lòng chọn khoa.')
            ->allowEmptyString('department_id', 'Vui lòng chọn khoa.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái', false);

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $this->allowEmpty('img_highlight', true)
            ]);

        $this
            ->scalar('fullname', 'Dữ liệu không hợp lệ.')
            ->requirePresence('fullname', true, 'Vui lòng nhập họ tên.')
            ->maxLength('fullname', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('fullname', 'Vui lòng nhập họ tên.', false);

        $this
            ->scalar('class', 'Dữ liệu không hợp lệ.')
            ->requirePresence('class', true, 'Vui lòng nhập lớp-khóa.')
            ->maxLength('class', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('class', 'Vui lòng nhập lớp-khóa.', false);

        $this
            ->scalar('content', 'Dữ liệu không hợp lệ.')
            ->requirePresence('content', true, 'Vui lòng nhập nội dung.')
            ->maxLength('content', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('content', 'Vui lòng nhập nội dung.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');
        return $this->errors($data);
    }
}
