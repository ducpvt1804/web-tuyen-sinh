<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class NewsRelationshipValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->scalar('title')
            ->maxLength('title', 250, 'Vui lòng nhập tiêu đề tin tức không vượt quá 250 ký tự!')
            ->requirePresence('title', 'create')
            ->allowEmptyString('title', false, "Tiêu đề tin tức không được để trống!");

        $this
            ->scalar('slug')
            ->maxLength('slug', 300, 'Vui lòng nhập slug không vượt quá 300 ký tự!');

        $this
            ->scalar('flg_status')
            ->requirePresence('flg_status', 'create')
            ->allowEmptyString('flg_status', false, "Vui lòng chọn trạng thái tin tức!");

        $this
            ->scalar('short_description')
            ->maxLength('short_description', 250, 'Vui lòng nhập tóm tắt nội dung không vượt quá 250 ký tự!')
            ->requirePresence('short_description', 'create');
    }
}
