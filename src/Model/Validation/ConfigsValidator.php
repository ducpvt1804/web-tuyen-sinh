<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ConfigsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('name', 'Dữ liệu không hợp lệ.')
            ->requirePresence('name', true, '')
            ->maxLength('name', 100, 'Vui lòng không nhập quá 100 kí tự.')
            ->allowEmptyString('name', 'Trường này không được để trống.', false);

        $this
            ->scalar('page_title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('page_title', true, '')
            ->maxLength('page_title', 100, 'Vui lòng không nhập quá 100 kí tự.')
            ->allowEmptyString('page_title', 'Trường này không được để trống.', false);

        $this
            ->requirePresence('email', true, '')
            ->email('email')
            ->allowEmptyString('email', 'Trường này không được để trống.', false);

        $this
            ->scalar('address_1', 'Dữ liệu không hợp lệ.')
            ->requirePresence('address_1', true, '')
            ->maxLength('address_1', 250, 'Vui lòng không nhập quá 250 kí tự.')
            ->allowEmptyString('address_1', 'Trường này không được để trống.', false);

        $this
            ->scalar('address_2', 'Dữ liệu không hợp lệ.')
            ->requirePresence('address_2', true, '')
            ->maxLength('address_2', 250, 'Vui lòng không nhập quá 250 kí tự.')
            ->allowEmptyString('address_2', 'Trường này không được để trống.', false);

        $this
            ->scalar('telno', 'Dữ liệu không hợp lệ.')
            ->requirePresence('telno', true, '')
            ->maxLength('telno', 20, 'Vui lòng không nhập quá 20 kí tự.')
            ->allowEmptyString('telno', 'Trường này không được để trống.', false);

        $this
            ->scalar('fax', 'Dữ liệu không hợp lệ.')
            ->requirePresence('fax', true, '')
            ->maxLength('fax', 20, 'Vui lòng không nhập quá 20 kí tự.')
            ->allowEmptyString('fax', 'Trường này không được để trống.', false);

        $this
            ->scalar('map', 'Dữ liệu không hợp lệ.')
            ->requirePresence('map', true, '')
            ->maxLength('map', 500, 'Vui lòng không nhập quá 500 kí tự.')
            ->allowEmptyString('map', 'Trường này không được để trống.', false);

        $this
            ->add('logo_header', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('logo_header', true));

        $this
            ->add('logo_footer', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('logo_footer', true));

        $this
            ->scalar('facebook_address', 'Dữ liệu không hợp lệ.')
            ->requirePresence('facebook_address', true, '')
            ->maxLength('facebook_address', 100, 'Vui lòng không nhập quá 100 kí tự.')
            ->allowEmptyString('facebook_address', 'Trường này không được để trống.', false);

        $this
            ->scalar('twitter_address', 'Dữ liệu không hợp lệ.')
            ->requirePresence('twitter_address', true, '')
            ->maxLength('twitter_address', 100, 'Vui lòng không nhập quá 100 kí tự.')
            ->allowEmptyString('twitter_address', 'Trường này không được để trống.', false);

        $this
            ->scalar('pinterest_address', 'Dữ liệu không hợp lệ.')
            ->requirePresence('pinterest_address', true, '')
            ->maxLength('pinterest_address', 100, 'Vui lòng không nhập quá 100 kí tự.')
            ->allowEmptyString('pinterest_address', 'Trường này không được để trống.', false);
    }
}
