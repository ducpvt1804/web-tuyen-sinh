<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class CateDepartmentsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->scalar('department_id', 'Dữ liệu không hợp lệ.')
            ->requirePresence('department_id', true, 'Vui lòng chọn khoa.')
            ->allowEmptyString('department_id', 'Vui lòng chọn khoa.', false);

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập title.')
            ->maxLength('title', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập title.', false);

        $this
            ->scalar('flg_showheader', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_showheader', true, 'Vui lòng chọn trạng thái hiển thị.')
            ->allowEmptyString('flg_showheader', 'Vui lòng chọn trạng thái hiển thị.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);
    }
}
