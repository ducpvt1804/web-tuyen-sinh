<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;


class RoleDetailValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $this
            ->scalar('meta_tile')
            ->maxLength('meta_tile', 100)
            ->allowEmptyString('meta_tile');

        $this
            ->scalar('meta_description')
            ->maxLength('meta_description', 250)
            ->allowEmptyString('meta_description');

        $this
            ->scalar('meta_keywords')
            ->maxLength('meta_keywords', 250)
            ->allowEmptyString('meta_keywords');

        $this
            ->scalar('tags')
            ->maxLength('tags', 250)
            ->allowEmptyString('tags');

        $this
            ->allowEmptyString('flg_language');

        $this
            ->dateTime('created_date')
            ->allowEmptyDateTime('created_date');

        $this
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $this
            ->allowEmptyString('flg_delete');
    }
}
