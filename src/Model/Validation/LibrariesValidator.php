<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LibrariesValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('type', 'Dữ liệu không hợp lệ.')
            ->requirePresence('type', true, "Vui lòng chọn loại thư viện ảnh/video.")
            ->allowEmptyString('type', "Vui lòng chọn loại thư viện ảnh/video.", false);
    }
}
