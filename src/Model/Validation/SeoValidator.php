<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class SeoValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('meta_title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('meta_title', true, 'Vui lòng nhập meta title')
            ->maxLength('meta_title', 100, 'Vui lòng không nhập quá 100 ký tự.')
            ->allowEmptyString('meta_title', 'Vui lòng nhập meta title', false);

        $this
            ->scalar('meta_description', 'Dữ liệu không hợp lệ.')
            ->requirePresence('meta_description', true, 'Vui lòng nhập meta description.')
            ->maxLength('meta_description', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('meta_description', 'Vui lòng nhập meta description.', false);

        $this
            ->scalar('meta_keywords', 'Dữ liệu không hợp lệ.')
            ->requirePresence('meta_keywords', true, 'Vui lòng nhập meta keywords.')
            ->maxLength('meta_keywords', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('meta_keywords', 'Vui lòng nhập meta keywords.', false);

        $this
            ->scalar('tags', 'Dữ liệu không hợp lệ.')
            ->maxLength('tags', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('tags', 'Vui lòng nhập thẻ tags.', false);
    }
}
