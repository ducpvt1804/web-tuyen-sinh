<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class NewsDepartmentValidator  extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $this->allowEmpty('img_highlight', 'create', 'Ảnh đại diện yêu cầu phải chọn.', true)
            ]);

        $this
            ->requirePresence('flg_status', 'create', 'Mục này cần được chọn')
            ->scalar('flg_status')
            ->allowEmptyString('flg_status', 'Mục này cần được chọn', false);

        $this
            ->requirePresence('schedule', 'create', 'Thời gian là bắt buộc.')
            ->dateTime('schedule', ['ymd', 'hi'], "Thời gian không hợp lệ.")
            ->allowEmptyDateTime('schedule', 'Thời gian là bắt buộc.', false);

        $this
            ->requirePresence('date_edit', 'create', 'Thời gian là bắt buộc.')
            ->dateTime('date_edit', ['ymd', 'hi'], "Thời gian không hợp lệ.")
            ->allowEmptyDateTime('date_edit', 'Thời gian là bắt buộc.', false);

        $this
            ->requirePresence('flg_language', 'create', 'Ngôn ngữ là bắt buộc.')
            ->allowEmptyString('flg_language', 'Ngôn ngữ là bắt buộc.', false);

        $this
            ->scalar('title')
            ->maxLength('title', 250, 'Vui lòng nhập tiêu đề tin tức không vượt quá 250 ký tự!')
            ->requirePresence('title', 'create')
            ->allowEmptyString('title', "Tiêu đề tin tức không được để trống!", false);

        $this
            ->scalar('type')
            ->allowEmptyString('type', "Mục này cần được chọn", false);

        $this
            ->scalar('parent_id')
            ->allowEmptyString('parent_id', "Mục này cần được chọn", false);

        $this
            ->scalar('short_description')
            ->maxLength('short_description', 250, 'Vui lòng nhập tóm tắt nội dung không vượt quá 250 ký tự!')
            ->requirePresence('short_description', 'create');

        $this
            ->requirePresence('content', 'create')
            ->allowEmptyString('content', "Vui lòng nhập nội dung!", false);
    }

    public function editVaild($data)
    {
        return  $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->allowEmpty('img_highlight', false, 'Ảnh đại diện yêu cầu phải chọn.');
        return $this->errors($data);
    }
}
