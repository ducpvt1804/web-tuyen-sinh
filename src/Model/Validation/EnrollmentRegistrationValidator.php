<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class EnrollmentRegistrationValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
    }

    public function defaultValid($data)
    {
        $this
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $this
            ->integer('cate')
            ->allowEmptyString('cate');

        $this
            ->scalar('fullname')
            ->maxLength('fullname', 50)
            ->allowEmptyString('fullname');

        $this
            ->date('dob')
            ->allowEmptyDate('dob');

        $this
            ->scalar('telno')
            ->maxLength('telno', 20)
            ->allowEmptyString('telno');

        $this
            ->integer('sex')
            ->allowEmptyString('sex');

        $this
            ->scalar('address')
            ->maxLength('address', 250)
            ->allowEmptyString('address');

        $this
            ->email('email')
            ->allowEmptyString('email');

        $this
            ->scalar('admission')
            ->maxLength('admission', 4)
            ->allowEmptyString('admission');

        $this
            ->scalar('sub_admission')
            ->maxLength('sub_admission', 4)
            ->allowEmptyString('sub_admission');

        $this
            ->scalar('profession_connect')
            ->maxLength('profession_connect', 4)
            ->allowEmptyString('profession_connect');

        $this
            ->scalar('sys_connect')
            ->maxLength('sys_connect', 4)
            ->allowEmptyString('sys_connect');

        $this
            ->scalar('profession')
            ->maxLength('profession', 4)
            ->allowEmptyString('profession');

        $this
            ->allowEmptyString('flg_status');

        $this
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $this
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $this
            ->allowEmptyString('flg_delete');

        return $this;
    }
}
