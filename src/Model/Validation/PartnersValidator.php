<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PartnersValidator extends Validator
{

    public function __construct()
    {
        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập tiêu đề')
            ->maxLength('title', 150, 'Vui lòng không nhập quá 150 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập tiêu đề', false);

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('img_highlight', true));

        $this
            ->scalar('description', 'Dữ liệu không hợp lệ.')
            ->requirePresence('description', true, 'Vui lòng nhập mô tả.')
            ->maxLength('description', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('description', 'Vui lòng nhập mô tả.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('link', 'Dữ liệu không hợp lệ.')
            ->requirePresence('link', true, 'Vui lòng nhập liên kết.')
            ->maxLength('link', 250, 'Vui lòng không nhập quá 250 ký tự.')
            ->allowEmptyString('link', 'Vui lòng nhập liên kết.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');

        return $this->errors($data);
    }
}
