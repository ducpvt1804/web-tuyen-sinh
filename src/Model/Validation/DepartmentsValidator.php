<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class DepartmentsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('parent_key', 'Dữ liệu không hợp lệ.')
            ->requirePresence('parent_key', true, 'Vui lòng chọn khoa.')
            ->allowEmptyString('parent_key', 'Vui lòng chọn khoa.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập tên.')
            ->maxLength('title', 100, 'Vui lòng không nhập quá 100 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập tên.', false);


        $this
            ->scalar('shot_title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('shot_title', true, 'Vui lòng nhập tên ngắn.')
            ->maxLength('shot_title', 100, 'Vui lòng không nhập quá 100 ký tự.')
            ->allowEmptyString('shot_title', 'Vui lòng nhập tên ngắn.', false);


        $this
            ->scalar('description', 'Dữ liệu không hợp lệ.')
            ->requirePresence('description', true, 'Vui lòng nhập mô tả.')
            ->maxLength('description', 100, 'Vui lòng không nhập quá 100 ký tự.')
            ->allowEmptyString('description', 'Vui lòng nhập mô tả.', false);

        $this
            ->add('banner', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('banner', true));

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('img_highlight', true));
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('banner', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('banner', false, 'Vui lòng chọn ảnh.');

        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');

        return $this->errors($data);
    }
}
