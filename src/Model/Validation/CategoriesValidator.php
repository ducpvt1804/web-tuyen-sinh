<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class CategoriesValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->maxLength('title', 100, 'Vui lòng không nhập quá 100 ký tự.')
            ->requirePresence('title', true, 'Vui lòng nhập tên danh mục.')
            ->allowEmptyString('title', "Vui lòng nhập tên danh mục.", false);

        $this
            ->scalar('slug', 'Dữ liệu không hợp lệ.')
            ->maxLength('slug', 350, 'Vui lòng không nhập quá 350 ký tự.');

        $this
            ->scalar('type', 'Dữ liệu không hợp lệ.')
            ->requirePresence('type', true, "Vui lòng chọn loại danh mục.")
            ->allowEmptyString('type', "Vui lòng chọn loại danh mục.", false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('flg_show', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_show', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_show', 'Vui lòng chọn trạng thái.', false);
    }
}
