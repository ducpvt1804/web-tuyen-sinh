<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class NewsDepartmentValidator  extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập tiêu đề.')
            ->allowEmptyString('title', 'Vui lòng nhập tiêu đề.', false)
            ->maxLength('title', 250, 'Vui lòng không nhập quá 250 ký tự.');


        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $this->allowEmpty('img_highlight', true)
            ]);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('schedule', 'Dữ liệu không hợp lệ.')
            ->requirePresence('schedule', true, 'Vui lòng chọn ngày.')
            ->dateTime('schedule', ['ymd', 'hi'], 'Thời gian không hợp lệ.')
            ->greaterThanOrEqualToField('schedule', 'date_edit', 'Thời gian publish phải sau thời gian viết bài.')
            ->allowEmptyDateTime('schedule', 'Vui lòng chọn ngày.', false);

        $this
            ->scalar('date_edit', 'Dữ liệu không hợp lệ.')
            ->requirePresence('date_edit', true, 'Vui lòng chọn ngày.')
            ->dateTime('date_edit', ['ymd', 'hi'], 'Thời gian không hợp lệ.')
            ->allowEmptyDateTime('date_edit', 'Vui lòng chọn ngày.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);

        $this
            ->scalar('slug', 'Dữ liệu không hợp lệ.')
            ->maxLength('slug', 300, 'Vui lòng không nhập quá 300 ký tự.');

        $this
            ->scalar('short_description', 'Dữ liệu không hợp lệ.')
            ->requirePresence('short_description', true, 'Vui lòng nhập nội dung tóm tắt.')
            ->allowEmptyString('short_description', 'Vui lòng nhập nội dung tóm tắt.', false)
            ->maxLength('short_description', 250, 'Vui lòng không nhập quá 250 ký tự.');

        $this
            ->scalar('content', 'Dữ liệu không hợp lệ.')
            ->requirePresence('content', true, 'Vui lòng nhập nội dung.')
            ->allowEmptyString('content', 'Vui lòng nhập nội dung.', false);

        $this
            ->scalar('type', 'Dữ liệu không hợp lệ.')
            ->requirePresence('type', true, 'Vui lòng chọn khoa.')
            ->allowEmptyString('type', "Vui lòng chọn khoa.", false);

        $this
            ->scalar('parent_id', 'Dữ liệu không hợp lệ.')
            ->requirePresence('parent_id', true, 'Vui lòng chọn danh mục tin.')
            ->allowEmptyString('parent_id', "Vui lòng chọn danh mục tin.", false);
    }

    public function editVaild($data)
    {
        return  $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh');
        return $this->errors($data);
    }
}
