<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class QuotesValidator extends Validator
{

    public function __construct()
    {
        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, '')
            ->maxLength('title', 150, 'Vui lòng không nhập quá 150 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập tiêu đề.', false);

        $this
            ->scalar('position', 'Dữ liệu không hợp lệ.')
            ->requirePresence('position', true, '')
            ->maxLength('position', 150, 'Vui lòng không nhập quá 150 ký tự.')
            ->allowEmptyString('position', 'Vui lòng nhập chức danh.', false);

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('img_highlight', true));

        $this
            ->scalar('content', 'Dữ liệu không hợp lệ.')
            ->requirePresence('content', true, '')
            ->maxLength('content', 500,  'Vui lòng không nhập quá 500 ký tự.')
            ->allowEmptyString('content', 'Vui lòng nhập nội dung.', false);

        $this
            ->scalar('cate', 'Dữ liệu không hợp lệ.')
            ->requirePresence('cate', true, 'Vui lòng chọn loại.')
            ->allowEmptyString('cate', 'Vui lòng chọn loại.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');

        return $this->errors($data);
    }
}
