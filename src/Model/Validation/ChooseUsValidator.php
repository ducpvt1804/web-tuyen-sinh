<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ChooseUsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập tiêu đề.')
            ->maxLength('title', 150, 'Vui lòng không nhập quá 150 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập tiêu đề.', false);

        $this
            ->scalar('description', 'Dữ liệu không hợp lệ.')
            ->requirePresence('description', true, 'Vui lòng nhập nội dung.')
            ->maxLength('description', 150, 'Vui lòng không nhập quá 150 ký tự.')
            ->allowEmptyString('description', 'Vui lòng nhập nội dung.', false);

        $this
            ->add('img_highlight', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ'
            ], $this->allowEmpty('img_highlight', true));

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);
    }

    public function editVaild($data)
    {
        return $this->errors($data);
    }

    public function addVaild($data)
    {
        $this
            ->requirePresence('img_highlight', true, 'Vui lòng chọn ảnh.')
            ->allowEmpty('img_highlight', false, 'Vui lòng chọn ảnh.');

        return $this->errors($data);
    }
}
