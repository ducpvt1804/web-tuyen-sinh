<?php namespace App\Model\Validation;

use Cake\Validation\Validator;

class DetailBannerValidator extends Validator {
  
    public function __construct()
    {
        parent::__construct();
    }

    public function defaultValid($data)
    {
        $this
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $this
            ->scalar('img')
            ->maxLength('img', 150)
            ->allowEmptyString('img');

        $this
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $this
            ->dateTime('update_date')
            ->allowEmptyDateTime('update_date');

        $this
            ->allowEmptyString('flg_delete');

        return $this;
    }
}