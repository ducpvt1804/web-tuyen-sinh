<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class BannerValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();

        $this
            ->add('addimg', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Định dạng ảnh không hợp lệ',
                $this->allowEmpty('addimg', 'create', '', true)
            ]);

        $this
            ->scalar('title', 'Dữ liệu không hợp lệ.')
            ->requirePresence('title', true, 'Vui lòng nhập title banner.')
            ->maxLength('title', 50, 'Vui lòng không nhập quá 50 ký tự.')
            ->allowEmptyString('title', 'Vui lòng nhập title banner..', false);

        $this
            ->scalar('mode', 'Dữ liệu không hợp lệ.')
            ->requirePresence('mode', true, 'Vui lòng chọn giao diện hiển thị.')
            ->allowEmptyString('mode', 'Vui lòng chọn giao diện hiển thị.', false);

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);

        $this
            ->scalar('flg_language', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_language', true, 'Vui lòng chọn ngôn ngữ.')
            ->allowEmptyString('flg_language', 'Vui lòng chọn ngôn ngữ.', false);
    }
}
