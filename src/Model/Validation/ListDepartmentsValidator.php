<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ListDepartmentsValidator extends Validator
{

    public function __construct()
    {
        parent::__construct();
        $this
            ->scalar('address', 'Dữ liệu không hợp lệ.')
            ->maxLength('title', 150, 'Vui lòng không nhập quá 50 ký tự.')
            ->requirePresence('title', 'create', 'Vui lòng nhập title.')
            ->notEmptyString('title', 'Vui lòng nhập title.');

        $this
            ->scalar('flg_status', 'Dữ liệu không hợp lệ.')
            ->requirePresence('flg_status', true, 'Vui lòng chọn trạng thái.')
            ->allowEmptyString('flg_status', 'Vui lòng chọn trạng thái.', false);
    }
}
