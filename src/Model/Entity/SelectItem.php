<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SelectItem Entity
 *
 * @property int $id
 * @property string|null $group_code
 * @property string|null $value
 * @property string|null $title
 * @property int|null $flg_site
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property bool|null $flg_delete
 */
class SelectItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'group_code' => true,
        'value' => true,
        'title' => true,
        'flg_site' => true,
        'comment' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true
    ];
}
