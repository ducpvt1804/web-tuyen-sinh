<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Quote Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $title
 * @property string|null $position
 * @property string|null $img_highlight
 * @property string|null $content
 * @property int|null $cate
 * @property int|null $flg_status
 * @property int|null $flg_language
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\User $user
 */
class Quote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'title' => true,
        'position' => true,
        'img_highlight' => true,
        'content' => true,
        'cate' => true,
        'flg_status' => true,
        'flg_language' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'user' => true
    ];
}
