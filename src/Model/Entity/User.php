<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string|null $user_name
 * @property string|null $email
 * @property string|null $password
 * @property string|null $fullname
 * @property string|null $sex
 * @property string|null $telno
 * @property string|null $address
 * @property string|null $avarta
 * @property \Cake\I18n\FrozenDate|null $birth_date
 * @property int|null $flg_site
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_status
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\CateDepartment[] $cate_departments
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\ChooseU[] $choose_us
 * @property \App\Model\Entity\Department[] $departments
 * @property \App\Model\Entity\Event[] $events
 * @property \App\Model\Entity\ListDepartment[] $list_departments
 * @property \App\Model\Entity\News[] $news
 * @property \App\Model\Entity\Partner[] $partners
 * @property \App\Model\Entity\Quote[] $quotes
 * @property \App\Model\Entity\RoleDetail[] $role_detail
 * @property \App\Model\Entity\StudentComment[] $student_comments
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_name' => true,
        'email' => true,
        'password' => true,
        'fullname' => true,
        'sex' => true,
        'telno' => true,
        'address' => true,
        'avarta' => true,
        'birth_date' => true,
        'flg_site' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_status' => true,
        'flg_delete' => true,
        'cate_departments' => true,
        'categories' => true,
        'choose_us' => true,
        'departments' => true,
        'events' => true,
        'list_departments' => true,
        'news' => true,
        'partners' => true,
        'quotes' => true,
        'role_detail' => true,
        'student_comments' => true,
        'remindpw_key' => true,
        'time_out' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
