<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EnrollmentRegistration Entity
 *
 * @property int $id
 * @property int|null $cate
 * @property string|null $fullname
 * @property \Cake\I18n\FrozenDate|null $dob
 * @property string|null $telno
 * @property int|null $sex
 * @property string|null $address
 * @property string|null $email
 * @property int|null $admission
 * @property int|null $sub_admission
 * @property int|null $flg_status
 * @property int|null $type
 * @property int|null $flg_read
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 */
class EnrollmentRegistration extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cate' => true,
        'fullname' => true,
        'dob' => true,
        'telno' => true,
        'sex' => true,
        'address' => true,
        'email' => true,
        'admission' => true,
        'sub_admission' => true,
        'flg_status' => true,
        'type' => true,
        'flg_read' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true
    ];
}
