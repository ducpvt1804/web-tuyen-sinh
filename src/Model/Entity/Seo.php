<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Seo Entity
 *
 * @property int $id
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property string|null $tags
 * @property int|null $flg_language
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\CateDepartment[] $cate_departments
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Config[] $configs
 * @property \App\Model\Entity\Department[] $departments
 * @property \App\Model\Entity\Event[] $events
 * @property \App\Model\Entity\Library[] $libraries
 * @property \App\Model\Entity\News[] $news
 */
class Seo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'tags' => true,
        'flg_language' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'cate_departments' => true,
        'categories' => true,
        'configs' => true,
        'departments' => true,
        'events' => true,
        'libraries' => true,
        'news' => true
    ];
}
