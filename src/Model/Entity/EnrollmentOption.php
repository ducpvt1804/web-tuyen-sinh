<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EnrollmentOption Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $type
 * @property string|null $branch_name
 * @property string|null $branch_code
 * @property string|null $combination_code
 * @property int|null $branch_parent
 * @property string|null $branch_update_name
 * @property int|null $flg_status
 * @property int|null $flg_language
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 */
class EnrollmentOption extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'type' => true,
        'branch_name' => true,
        'branch_code' => true,
        'combination_code' => true,
        'branch_parent' => true,
        'branch_update_name' => true,
        'flg_status' => true,
        'flg_language' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true
    ];
}
