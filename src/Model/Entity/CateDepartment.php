<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CateDepartment Entity
 *
 * @property int $id
 * @property int|null $seo_id
 * @property int|null $department_id
 * @property string|null $title
 * @property string|null $slug
 * @property int|null $flg_showheader
 * @property int|null $flg_status
 * @property int|null $flg_language
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\Seo $seo
 * @property \App\Model\Entity\Department $department
 */
class CateDepartment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'seo_id' => true,
        'department_id' => true,
        'title' => true,
        'slug' => true,
        'flg_showheader' => true,
        'flg_status' => true,
        'flg_language' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'seo' => true,
        'department' => true
    ];
}
