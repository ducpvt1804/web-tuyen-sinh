<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ListDepartment Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $title
 * @property string|null $slug
 * @property int|null $flg_status
 * @property string|null $url
 * @property int|null $type
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 */
class ListDepartment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'title' => true,
        'slug' => true,
        'flg_status' => true,
        'url' => true,
        'type' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true
    ];
}
