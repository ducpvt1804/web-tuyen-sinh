<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StudentComment Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $department_id
 * @property string|null $img_highlight
 * @property string|null $fullname
 * @property string|null $class
 * @property string|null $content
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Department $department
 */
class StudentComment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'department_id' => true,
        'img_highlight' => true,
        'fullname' => true,
        'class' => true,
        'content' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'user' => true,
        'department' => true
    ];
}
