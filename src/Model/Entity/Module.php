<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Module Entity
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $controller_name
 * @property string|null $action
 * @property string|null $icon
 * @property int|null $flg_site
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\RoleDetail[] $role_detail
 */
class Module extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'controller_name' => true,
        'action' => true,
        'icon' => true,
        'flg_site' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'role_detail' => true
    ];
}
