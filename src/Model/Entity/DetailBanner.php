<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DetailBanner Entity
 *
 * @property int $id
 * @property int|null $banner_id
 * @property string|null $img
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\Banner $banner
 */
class DetailBanner extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'banner_id' => true,
        'img' => true,
        'title' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'banner' => true
    ];
}
