<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $seo_id
 * @property string|null $flg_department
 * @property string|null $title
 * @property string|null $slug
 * @property string|null $img_highlight
 * @property string|null $address
 * @property string|null $flg_status
 * @property \Cake\I18n\FrozenTime|null $start_date
 * @property \Cake\I18n\FrozenTime|null $end_date
 * @property string|null $flg_language
 * @property string|null $map
 * @property string|null $telno
 * @property string|null $position
 * @property string|null $cost
 * @property string|null $content
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Seo $seo
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'seo_id' => true,
        'flg_department' => true,
        'title' => true,
        'slug' => true,
        'img_highlight' => true,
        'address' => true,
        'flg_status' => true,
        'start_date' => true,
        'end_date' => true,
        'flg_language' => true,
        'map' => true,
        'telno' => true,
        'position' => true,
        'cost' => true,
        'content' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'user' => true,
        'seo' => true,
        'mail' => true
    ];
}
