<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Config Entity
 *
 * @property int $id
 * @property int|null $seo_id
 * @property int|null $flg_site
 * @property string $name
 * @property string|null $email
 * @property string|null $logo
 * @property string|null $address_1
 * @property string|null $address_2
 * @property string|null $telno
 * @property string|null $fax
 * @property string|null $map
 * @property string|null $facebook_address
 * @property string|null $twitter_address
 * @property string|null $pinterest_address
 * @property int|null $flg_language
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 *
 * @property \App\Model\Entity\Seo $seo
 */
class Config extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'seo_id' => true,
        'flg_site' => true,
        'name' => true,
        'email' => true,
        'logo' => true,
        'address_1' => true,
        'address_2' => true,
        'telno' => true,
        'fax' => true,
        'map' => true,
        'facebook_address' => true,
        'twitter_address' => true,
        'pinterest_address' => true,
        'flg_language' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'seo' => true
    ];
}
