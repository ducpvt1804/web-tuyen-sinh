<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Department Entity
 *
 * @property int $id
 * @property int|null $seo_id
 * @property int|null $cate_id
 * @property string|null $banner
 * @property string|null $img_highlight
 * @property int|null $flg_language
 * @property string|null $title
 * @property string|null $shot_title
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property \Cake\I18n\FrozenTime|null $update_date
 * @property int|null $flg_delete
 * @property int|null $type
 *
 * @property \App\Model\Entity\Seo $seo
 * @property \App\Model\Entity\Cate $cate
 * @property \App\Model\Entity\CateDepartment[] $cate_departments
 */
class Department extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'seo_id' => true,
        'cate_id' => true,
        'banner' => true,
        'img_highlight' => true,
        'flg_language' => true,
        'title' => true,
        'shot_title' => true,
        'description' => true,
        'create_date' => true,
        'update_date' => true,
        'flg_delete' => true,
        'type' => true,
        'seo' => true,
        'cate' => true,
        'cate_departments' => true
    ];
}
