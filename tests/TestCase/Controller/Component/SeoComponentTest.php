<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\SeoComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\SeoComponent Test Case
 */
class SeoComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\SeoComponent
     */
    public $Seo;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Seo = new SeoComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Seo);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
