<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoleDetailTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RoleDetailTable Test Case
 */
class RoleDetailTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RoleDetailTable
     */
    public $RoleDetail;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RoleDetail',
        'app.Roles',
        'app.Users',
        'app.Modules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RoleDetail') ? [] : ['className' => RoleDetailTable::class];
        $this->RoleDetail = TableRegistry::getTableLocator()->get('RoleDetail', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RoleDetail);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
