<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DetailBannerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DetailBannerTable Test Case
 */
class DetailBannerTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DetailBannerTable
     */
    public $DetailBanner;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DetailBanner',
        'app.Banners'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DetailBanner') ? [] : ['className' => DetailBannerTable::class];
        $this->DetailBanner = TableRegistry::getTableLocator()->get('DetailBanner', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DetailBanner);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
