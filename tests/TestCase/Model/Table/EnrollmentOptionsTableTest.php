<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EnrollmentOptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EnrollmentOptionsTable Test Case
 */
class EnrollmentOptionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EnrollmentOptionsTable
     */
    public $EnrollmentOptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.EnrollmentOptions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EnrollmentOptions') ? [] : ['className' => EnrollmentOptionsTable::class];
        $this->EnrollmentOptions = TableRegistry::getTableLocator()->get('EnrollmentOptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EnrollmentOptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
