<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SelectItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SelectItemsTable Test Case
 */
class SelectItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SelectItemsTable
     */
    public $SelectItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SelectItems'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SelectItems') ? [] : ['className' => SelectItemsTable::class];
        $this->SelectItems = TableRegistry::getTableLocator()->get('SelectItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SelectItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
