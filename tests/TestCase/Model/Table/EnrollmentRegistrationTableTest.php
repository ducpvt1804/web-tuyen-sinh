<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EnrollmentRegistrationTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EnrollmentRegistrationTable Test Case
 */
class EnrollmentRegistrationTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EnrollmentRegistrationTable
     */
    public $EnrollmentRegistration;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.EnrollmentRegistration',
        'app.People'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EnrollmentRegistration') ? [] : ['className' => EnrollmentRegistrationTable::class];
        $this->EnrollmentRegistration = TableRegistry::getTableLocator()->get('EnrollmentRegistration', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EnrollmentRegistration);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
